const express = require('express');
const router = express.Router();
const messageController = require('../controllers/messageController');

router.get('/', messageController.findAll);
router.post('/room/:id', messageController.findRoomMessage);
router.post('/send', messageController.sendMessage);

module.exports = router;
