const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const pusherController = require('../controllers/pusherController');


let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        console.log("ext ",ext)
        if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png" && ext !== ".PNG" && ext !== ".pdf") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        cb(null, true)
    },
}).single("image");

router.post('/connect/:id', pusherController.connect);
router.get('/rooms/:id', pusherController.getRooms)
router.post('/connect-room', pusherController.connectRoom);
router.post('/send-message', pusherController.sendMessage);
router.post('/send-file', upload, pusherController.sendFileInMessage);
router.post('/disconnect/:id', pusherController.disconnect);

module.exports = router;
