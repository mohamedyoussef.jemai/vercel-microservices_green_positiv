const Message = require('../models/message.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    findAll: async (req, res) => {
        loggerFile.info("in findAll messages");
        try {
            const messages = await Message.find();
            if (messages) {
                loggerFile.debug('get all messages done')
                return res.status(200).json(messages);
            } else {
                loggerFile.debug('aucun messages éxistants')
                return res.status(200).json({message: "messages inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    findRoomMessage: async (req, res) => {
        loggerFile.info("findRoomMessage");
        const {id} = req.params;
        let usersId = []
        let usersUsername = []
        let finalMessages = []
        try {
            let messages = await Message.find({
                $or: [{idSender: id}, {idReceiver: id}]
            }).sort({createdAt: 1});
            //get id Others users

            await messages.map(el => {
                if (el.idSender == id && !usersId.includes(el.idReceiver)) {
                    usersId.push(el.idReceiver)
                    usersUsername.push(el.usernameReceiver)
                } else if (!usersId.includes(el.idSender)) {
                    usersId.push(el.idSender)
                    usersUsername.push(el.usernameSender)
                }
            });
            await usersId.forEach(function (item, index) {
                let array = messages.filter(el => el.idSender == item || el.idReceiver == item);
                if (id !== item) finalMessages.push({
                    idReceiver: item,
                    usernameReceiver: usersUsername[index],
                    messages: array
                })
            });
            loggerFile.debug("findRoomMessage done");
            return res.status(200).send(finalMessages);
        } catch
            (error) {
            loggerFile.error(error);
        }
    },
    sendMessage: async (req, res) => {
        loggerFile.info("in sendMessage");
        const {idSender, idReceiver, usernameSender, usernameReceiver, content} = req.body;
        try {
            let message = new Message({idSender, idReceiver, usernameSender, usernameReceiver, content});
            await Message.create(message);
            loggerFile.debug("message created")
            return res.status(200).json({message: "message envoyé"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: "probléme lors de l'envoi du message"});
        }
    }
}
