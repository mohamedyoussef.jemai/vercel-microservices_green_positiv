const loggerFile = require("../config/logger");
const path = require('path');
const utils = require('../config/utils');
const roomService = require("../services/roomService");
const pusher = require("../config/pusher");
const Message = require("../models/message.model");
const cloudinary = require('../config/cloudinary');

var connectedUsers = []
var rooms = [];

module.exports = {
    connect: async (req, res) => {
        loggerFile.info("in connect");
        try {
            const {id} = req.params;
            await pusher.trigger(`users`, 'add', {
                idUser: id
            });
            loggerFile.debug("add user done")
            return res.json({message: "user connected"})
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }, getRooms: async (req, res) => {
        loggerFile.info("in getRooms");
        try {
            const {id} = req.params;
            const rooms = await roomService.getRooms(id)
            return res.status(200).json({rooms: rooms})
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    connectRoom: async (req, res) => {
        loggerFile.info("in findAll messages");
        try {
            let {idSender, idReceiver, usernameSender, usernameReceiver} = req.body
            let room = await roomService.verifRoom([idSender, idReceiver])
            if (room == null) {
                room = await roomService.createRoom([idSender, idReceiver]);
                if (!connectedUsers.includes(idSender)) {
                    connectedUsers.push(idSender);
                }
                await pusher.trigger(`users`, 'add', {
                    users: connectedUsers
                });
                rooms.push({idRoom: room._id})
                await pusher.trigger(`rooms`, 'add', {
                    room: room
                });
            } else {
                if (!connectedUsers.includes(idReceiver)) {
                    connectedUsers.push(idReceiver);
                }
                await pusher.trigger(`users`, 'add', {
                    users: connectedUsers
                });
                rooms.push({idRoom: room._id})
                await pusher.trigger(`rooms`, 'add', {
                    room: room
                });
            }
            loggerFile.debug("room created");
            return res.json({message: "room created"})
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    sendMessage: async (req, res) => {
        loggerFile.info("in sendMessage");
        var showedReceiver, showedSender = false;
        try {
            let {idSender, idReceiver, usernameSender, usernameReceiver, content} = req.body
            let room = await roomService.verifRoom([idSender, idReceiver])
            if (room == null) {
                loggerFile.debug("Room inéxistante");
                return res.status(401).json({message: "Room inéxistante"})
            } else {
                if (connectedUsers.includes(idSender) && connectedUsers.includes(idReceiver)) {
                    showedSender = true;
                    showedReceiver = true;
                    await Message.updateMany({idReceiver: idReceiver}, {showedReceiver: true}, {upsert: false});
                } else if (connectedUsers.includes(idSender) && !connectedUsers.includes(idReceiver)) {
                    showedSender = true;
                    showedReceiver = false;
                } else {
                    showedSender = false;
                    showedReceiver = false;
                }
                let message = new Message({
                    idSender,
                    idReceiver,
                    usernameSender,
                    usernameReceiver,
                    content,
                    showedSender,
                    showedReceiver
                });
                await Message.create(message).then(async data => {
                    await pusher.trigger(`messages`, 'add', {
                        room: room,
                        message: data
                    });
                });
                loggerFile.debug("message created")
                return res.status(200).json({message: "message envoyé"})
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    disconnect: async (req, res) => {
        loggerFile.info("in disconnect");
        try {
            const {id} = req.params;
            await pusher.trigger(`users`, 'remove', {
                idUser: id
            });
            loggerFile.debug("user disconnected");
            return res.json({message: "user disconnected"})
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    sendFileInMessage: async (req, res) => {
        loggerFile.info("in sendFileInMessage");
        var showedReceiver, showedSender = false;
        try {
            let type = ""
            if (req.validityFormat) {
                let ext = path.extname(req.file.originalname);
                if (ext === '.pdf')
                    type = "pdf"
                else type = "image"

                const result = await cloudinary.uploader.upload(req.file.path);
                req.body.content = result.secure_url;
                const {idSender, idReceiver, usernameSender, usernameReceiver, content} = req.body;
                let room = await roomService.verifRoom([idSender, idReceiver])
                if (room == null) {
                    loggerFile.debug("Room inéxistante");
                    return res.status(401).json({message: "Room inéxistante"})
                } else {
                    if (connectedUsers.includes(idSender) && connectedUsers.includes(idReceiver)) {
                        showedSender = true;
                        showedReceiver = true;
                        await Message.updateMany({idReceiver: idReceiver}, {showedReceiver: true}, {upsert: false});
                    } else if (connectedUsers.includes(idSender) && !connectedUsers.includes(idReceiver)) {
                        showedSender = true;
                        showedReceiver = false;
                    } else {
                        showedSender = false;
                        showedReceiver = false;
                    }
                    let message = new Message({idSender, idReceiver, usernameSender, usernameReceiver, content, type, showedSender,
                        showedReceiver});
                    await Message.create(message).then(async data => {
                        await pusher.trigger(`messages`, 'add', {
                            room: room,
                            message: data
                        });
                    });
                    loggerFile.debug("file created")
                    return res.status(200).json({message: "fichier téléchargé", data: message});
                }
            } else {
                res.status(400).json({message: "format indésirable"})
            }

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: "probléme lors de l'envoi du message"});
        }
    }
}
