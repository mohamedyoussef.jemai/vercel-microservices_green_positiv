const Room = require('../models/room.model')
const loggerFile = require("../config/logger");

module.exports = {
    // create user
    verifRoom: async (users) => {
        loggerFile.info('in verifRoom');
        try {
            let room = await Room.find({
                $and: [
                    {user1: {$in: users}},
                    {user2: {$in: users}},
                ]
            });
            if (room.length != 0) {
                return room[0]
            } else {
                return null
            }
        } catch (error) {
            loggerFile.error(error);

        }
    },
    createRoom: async (users) => {
        loggerFile.info('in createRoom');
        try {
            let user1 = users[0]
            let user2 = users[1]
            let room = new Room({user1, user2});
            return Room.create(room).then(data => {
                if (data) {
                    return data
                } else {
                    return;
                }
            }).catch(err => {
                loggerFile.error(err);
            })
        } catch (error) {
            loggerFile.error(error);

        }
    },
    getPlayerIndex: (connectedUsers, username) => {
        loggerFile.info('in getPlayerIndex');
        let position = -1
        try {
            connectedUsers.forEach(function callback(user, index) {
                if (user.username == username)
                    position = index;
            });
            return position
        } catch (error) {
            loggerFile.error(error);
        }
    },
    checkIfUserExist: (connectedUsers, username) => {
        loggerFile.info('in checkIfUserExist');
        let test = false
        try {
            connectedUsers.forEach(function callback(user, index) {
                if (user.username === username)
                    test = true;
            });
            return test
        } catch (error) {
            loggerFile.error(error);
        }
    },
    getRooms: (id) => {
        loggerFile.info('in getRooms');
        return Room.find({
            $or: [{user1: id}, {user2: id}]
        }).then(data => {
            if (data) {
                return data
            } else {
                return [];
            }
        }).catch(err => {
            loggerFile.error(err);
        })
    }
}
