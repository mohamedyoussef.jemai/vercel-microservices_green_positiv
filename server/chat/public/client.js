//get data from currentUser
const id = window.prompt("Enter the id");
const username = window.prompt("Enter the username");

//initialize
var rooms = []
var users = []
var connectedUsers = []
var allMessages = []

var pusher = new Pusher('70479cebefa3d9baa33f', {
    cluster: "mt1",
    forceTLS: true
});
pusher.logToConsole = true;

const connectForm = document.getElementById("connect-form");
const connectRoomForm = document.getElementById("connect-room");
const sendForm = document.getElementById("send-form");
const disconnectForm = document.getElementById("disconnect-form");

//pusher channels
const channelRoom = pusher.subscribe('rooms');
const messageRoom = pusher.subscribe('messages');
const usersRoom = pusher.subscribe('users');

const initialize = () => {
    fetch("http://localhost:8010/chat/room/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(async res => res.json())
        .then(async json => {
            console.log("messages ", json)
            messages = json
            await messages.forEach(async function callback(item, index) {
                //init users
                if (!users.includes(item.idReceiver)) {
                    users.push(item.idReceiver);
                    allMessages.push(item.messages)
                }
            });
            await users.forEach(async function callback(item, index) {
                await allMessages[index].forEach(async function callback(message, index) {
                    if (id === message.idSender) {
                        $('#books-list').append($('<li>').html('<div style="color: green;"><b>' + message.usernameSender + ': ' + message.content + '</b></div>'));
                    }
                    if ((id === message.idReceiver) && (message.showedReceiver == false)) {
                        $('#books-list').append($('<li>').html('<div style="color: blue;"><b>' + message.usernameSender + ': ' + message.content + '</b></div>'));
                    } else if (id === message.idReceiver) {
                        $('#books-list').append($('<li>').html('<div style="color: red;"><b>' + message.usernameSender + ': ' + message.content + '</b></div>'));
                    }

                });

            });
        })
}
const addConnect = (e) => {
    e.preventDefault();
//call route '/post' to trigger event with new book
    fetch("http://localhost:8010/pusher/connect/" + id, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(res => res.json())
        .then(json => {
            initialize()
            console.log(json.message)
            //bind the event of the presence channel to get the data attched with it
        })
}
const connectRoom = (e) => {
    e.preventDefault();

    //store name of new book in a variable
    var idReceiverConnect = document.getElementById('idReceiverConnect').value;
    var usernameReceiverConnect = document.getElementById('usernameReceiverConnect').value;

//call route '/post' to trigger event with new book
    if (connectedUsers.includes(id)) {
        fetch("http://localhost:8010/pusher/connect-room", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idSender: id,
                idReceiver: idReceiverConnect,
                usernameSender: username,
                usernameReceiver: usernameReceiverConnect
            })
        })
            .then(res => res.json())
            .then(json => {
                console.log("connected :", json.message)
                //bind the event of the presence channel to get the data attched with it
            })
    } else {
        console.log("user disconnected")
    }

}
const sendMessage = (e) => {
    e.preventDefault();

    if (connectedUsers.includes(id)) {
        //store name of new book in a variable
        var idReceiver = document.getElementById('idReceiver').value;
        var usernameReceiver = document.getElementById('usernameReceiver').value;
        var content = document.getElementById('content').value;

//call route '/post' to trigger event with new book
        fetch("http://localhost:8010/pusher/send-message", {
            method: "POST",
            headers: {
                'Content-Type': 'application/json',
            },
            body: JSON.stringify({
                idSender: id,
                idReceiver: idReceiver,
                usernameSender: username,
                usernameReceiver: usernameReceiver,
                content: content
            })
        })
            .then(res => res.json())
            .then(json => {
                console.log(json.message)
                //bind the event of the presence channel to get the data attched with it
            })
    } else {
        console.log("user disconnected")
    }

}
const removeConnect = (e) => {
    e.preventDefault();

//call route '/post' to trigger event with new book
    fetch(`http://localhost:8010/pusher/disconnect/${id}`, {
        method: "POST",
        headers: {
            'Content-Type': 'application/json',
        },
    })
        .then(res => res.json())
        .then(json => {
            console.log(json.message)
            //bind the event of the presence channel to get the data attched with it
        })
}

//add a event Listener on form submit
connectForm.addEventListener("submit", addConnect);
connectRoomForm.addEventListener("submit", connectRoom);
sendForm.addEventListener("submit", sendMessage);
disconnectForm.addEventListener("submit", removeConnect);


channelRoom.bind('add', async function (data) {
    if (!rooms.includes(data.room._id)) {
        this.rooms.push(data.room._id)
    }
    console.log("rooms", rooms)
    if (id === data.room.user1) {
        console.log(id === data.room.user1)
        if (!users.includes(data.room.user2)) {
            users.push(data.room.user2);
        }
    } else if (id === data.room.user2) {
        console.log(id === data.room.user2)
        if (!users.includes(data.room.user1)) {
            users.push(data.room.uroomsser1);
        }
    }
});
messageRoom.bind('add', async function (data) {
    if (rooms.includes(data.room._id)) {
        if (id === data.message.idSender) {
//render the data of a event in division using jQuery
            $('#books-list').append($('<li>').html('<div style="color: green;"><b>' + data.message.usernameSender + ': ' + data.message.content + '</b></div>'));
        }
        if (id === data.message.idReceiver) {
//render the data of a event in division using jQuery
            $('#books-list').append($('<li>').html('<div style="color: red;"><b>' + data.message.usernameSender + ': ' + data.message.content + '</b></div>'));
        }
    } else {
        console.log("not our business")
    }
    if (id === data.room.user1) {
        console.log(id === data.room.user1)
        if (!users.includes(data.room.user2)) {
            users.push(data.room.user2);
        }
    } else if (id === data.room.user2) {
        console.log(id === data.room.user2)
        if (!users.includes(data.room.user1)) {
            users.push(data.room.user1);
        }
    }
});
usersRoom.bind('add', async function (data) {
    if (!connectedUsers.includes(data.idUser))
        connectedUsers.push(data.idUser);
})
usersRoom.bind('remove', async function (data) {
    var index = connectedUsers.indexOf(data.idUser);
    connectedUsers.splice(index, 1);
})
