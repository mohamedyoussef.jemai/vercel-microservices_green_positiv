const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let messageSchema = new Schema({
    idSender: {
        type: String,
        required: true
    },
    idReceiver: {
        type: String,
        required: true
    },
    usernameSender: {
        type: String,
        required: true,
    },
    usernameReceiver: {
        type: String,
        required: true,
    },
    showedSender: {
        type: Boolean,
    },
    showedReceiver: {
        type: Boolean,
    },
    content: {
        type: String,
        default: ""
    },
    type: {
        type: String,
        default: "text"
    },
    date: {
        type: Date,
        default: Date.now()
    }
}, {
    collection: 'messages',
    timestamps: true
})

const Message = mongoose.model("Message", messageSchema);
module.exports = Message;

