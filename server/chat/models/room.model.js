const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let roomSchema = new Schema({
    user1: {
        type: String,
        required: true
    },
    user2: {
        type: String,
        required: true
    },
}, {
    collection: 'rooms',
    timestamps: true
})

const Room = mongoose.model("Room", roomSchema);
module.exports = Room;

