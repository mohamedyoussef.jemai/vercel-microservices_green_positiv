//imports
require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

//deployment
//const serveStatic = require('serve-static');
//const path = require('path');

const app = express();
const port = process.env.PORT //8010;


//dev
app.use(logger('dev'));
//models

require('./models/message.model');
require('./models/room.model')

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static('public'));

//get index.html page
app.get('/page/', (req, res) => {
    res.sendFile(__dirname + '/public/pusher.html');
});

mongoose.connect(process.env.DB_URI_USER_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    loggerFile.info('database Connected');
    console.log('database Connected prod');
}).catch((err) => {
    loggerFile.error(err);
    console.log("error ", err.message);
});

//routes define
let messageRouter = require('./routes/message.route');
let pusherRouter = require('./routes/pusher.route');

//routes prefix
app.use('/chat', messageRouter);
app.use('/pusher', pusherRouter);


//start server
app.listen(port, () => {
    console.log(`server running on port ${port}`)
    loggerFile.info(`server running on port ${port}`);
})
module.exports = app;
