require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    findCompany: async (id) => {
        try {
            let data = {}
            await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`).then(function (response) {
                if (response.data) {
                    loggerFile.debug('findCompany');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get company by id ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return {status: 400};
        }
    },
    findCompanyAndMission: async (idCompany, idMission) => {
        try {
            let data = {
                idCompany: idCompany,
                idMission: idMission
            };
            await axios.post(`${process.env.URL_COMPANY}/missions/verif-devis`, data).then(function (response) {
                console.log("response.data ",response)
                if (response.data) {
                    loggerFile.debug('findCompanyAndMission');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get verification devis');
                    data = {status: 400};
                }
            }).catch(function (err) {
                return {status: 400};
            });
            return data;
        } catch (error) {
            return {status: 400};
        }
    },
}
