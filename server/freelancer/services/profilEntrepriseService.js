const notificationService = require("../services/notificationService");
const axios = require('axios').default;
const profilServerURL = process.env.URL_PROFIL;

const loggerFile = require("../config/logger");

module.exports = {
    createContactDetails: async (data) => {
        loggerFile.info("in createContactDetails")
        try {
            let test = false;
            await axios.post(`${profilServerURL}/profil-entreprise/`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("createContactDetails data done")
                    test = true
                } else {
                    loggerFile.debug('problem in createContactDetails');
                    test = false
                }
            }).catch(function (err) {

                test = false
            });
            return test;
        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateContactDetails: async (data) => {
        loggerFile.info("in updateContactDetails")
        try {
            let test = false;
            await notificationService.createDetailsContactNotif(data.id_freelancer, data);
            await axios.patch(`${profilServerURL}/profil-entreprise/`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('updateContactDetails data done')
                    test = true;
                } else {
                    loggerFile.debug('problem in updateContactDetails');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateLegalRepresentative: async (data) => {
        loggerFile.info("in UpdateLegalRepresentative")
        try {
            await notificationService.createLegalRepresentativeNotif(data.id_freelancer, data);
            await axios.patch(`${profilServerURL}/profil-entreprise/legal-representative`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("UpdateLegalRepresentative data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in UpdateLegalRepresentative');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateIbanAccount: async (data) => {
        loggerFile.info("in updateIbanAccount")
        try {
            await axios.patch(`${profilServerURL}/profil-entreprise/payment/iban`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("updateIbanAccount data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in updateIbanAccount');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }

    },
    updateTaxe: async (data) => {
        loggerFile.info("in UpdateTaxe")
        try {
            const {token} = req.headers;
            if (!data.taxe) {
                loggerFile.debug("il faut une valeur de taxe")
                return res.status(400).json({message: "il faut une valeur de taxe"});
            }
            if (data.taxe < 0 || data.taxe > 100) {
                loggerFile.debug("le taxe doit être compris entre 0 et 100")
            }
            await notificationService.createTaxeNotif(req.auth._idConnected, req.body);
            await axios.patch(`${profilServerURL}/profil-entreprise/taxes`, req.body, {
                headers: {
                    'token': token
                }
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("UpdateTaxe data done")
                } else {
                    loggerFile.debug('problem in UpdateTaxe');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateLegalMention: async (req, res) => {
        loggerFile.info('in UpdateLegalMention')
        try {
            const {token} = req.headers;
            await notificationService.createLegalMentionNotif(req.auth._idConnected, req.body);
            await axios.patch(`${profilServerURL}/profil-entreprise/legal-mention`, req.body, {
                headers: {
                    'token': token
                }
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('mention légale ajoutée')
                } else {
                    loggerFile.debug('problem in UpdateLegalMention');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    update: async (id, data) => {
        loggerFile.info('in update')
        try {
            await axios.patch(`${profilServerURL}/profil-entreprise/update/${id}`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('profile modifié')
                    return res.status(200).json({message: "profile entreprise modifié"});
                } else {
                    loggerFile.debug('problem in update');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    getProfileEntrepriseById: async (id) => {
        try {
            await axios.get(`${profilServerURL}/profil-entreprise/${id}`).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('profile récupéré')
                } else {
                    loggerFile.debug('problem in update');
                }
            }).catch(function (err) {

            });
        } catch (error) {
            loggerFile.error(error)
        }
    },
    delete: async (id) => {
        loggerFile.info('in delete profile');
        try {
            let test = false;
            await axios.delete(`${process.env.URL_PROFIL}/profil-entreprise/delete/${id}`).then(function (response) {
                if (response.status == 200 && response.data.message == "done") {
                    loggerFile.debug('delete profile done');
                    test = true;
                } else {
                    loggerFile.debug('problem in profile user ');
                    test = false;
                }
            }).catch(function (err) {

                test = false;
            });
            return test;
        } catch (error) {
            return;
        }
    },
}
