require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    getLanguages: async (req, res) => {
        loggerFile.info('in getLanguages');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/languages`);
            if (response) {
                loggerFile.debug("getLanguages done")
                return response.data;
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getLegalForm: async (req, res) => {
        loggerFile.info('in getLegalForm');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/legal-forms`);
            if (response) {
                loggerFile.debug("getLegalForm done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }

    },
    getCountry: async (req, res) => {
        loggerFile.info('in getCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/countries`);
            if (response) {
                loggerFile.debug("getCountry done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getIbanCountry: async (req, res) => {
        loggerFile.info('in getIbanCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/iban-countries`);
            if (response) {
                loggerFile.debug("getIbanCountry done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getIbanUsCaOtherCountry: async (req, res) => {
        loggerFile.info('in getIbanUsCaOtherCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/iban-us-ca-others-countries`);
            if (response) {
                loggerFile.debug("getIbanUsCaOtherCountry done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getSectorActivity: async (req, res) => {
        loggerFile.info(" in getSectorActivity");
        try {
            const response = await axios.get(`${process.env.URL_API}/api/sector-activity`);
            if (response) {
                loggerFile.debug("getSectorActivity done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
