require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    testSkillsNeeded: async (freelance, skillsNeeded) => {
        loggerFile.info('testSkillsNeeded');
        try {
            let test = false;
            freelance.skills.forEach((item, index) => {
                if (skillsNeeded.includes(item))
                    test = true;
            });
            return test
        } catch (error) {
            return false;
        }
    },
    testSkillsAppreciated: async (freelance, skillsAppreciated) => {
        loggerFile.info('testSkillsAppreciated');
        try {
            let test = false;
            freelance.skills.forEach((item, index) => {
                if (skillsAppreciated.includes(item))
                    test = true;
            });
            return test
        } catch (error) {
            return false;
        }
    }, testLanguage: async (freelance, languages) => {
        loggerFile.info('testLanguage');
        try {
            let test = false;
            freelance.languages.forEach((item, index) => {
                if (languages.includes(item.name))
                    test = true;
            });
            return test
        } catch (error) {
            return false;
        }
    },
}
