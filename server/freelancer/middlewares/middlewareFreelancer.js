require('dotenv/config')
const jwt = require('jsonwebtoken');
const Freelancer = require("../models/freelancer.model");
const legalForms = [
    "Association loi 1901 - avec TVA", "Association loi 1901 - sans TVA", "Auto-entreprise / micro entreprise", "Auto-entreprise / micro entreprise avec option TVA", "Couveuses d'entreprises (société)", "EI (entreprise individuelle)", "EIRL", "MDA/AGESSA sans précompte ni TVA", "MDA/AGESSA sans précompte, avec option TVA", "SARL à capital variable", "SARL/EURL", "SAS/SASU", "SCIC", "SCOP", "SEP (société en participation)", "Société Anonyme (SA)", "Société civile", "Société de portage salarial", "Société en cours d'immatriculation"
];
const ibanType = ['iban', 'iban-us', 'iban-ca', 'others', 'empty'];
const languagesLevels = ['BASIC', 'CONVERSATIONAL', 'FLUENT', 'NATIVE'];
const adminService = require("../services/adminService");
const apiService = require("../services/apiService");
const loggerFile = require("../config/logger");
const levels = ['Junior', 'Intermédiaire', 'Senior'];
const utils = require('../config/utils');

module.exports = {
    middlewareValidityCreationFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationFreelancer")
        const {
            username,
            email,
            lastName,
            firstName,
            phone,
            password,
            repeatPassword,
            title_profile,
            localisation,
            level,
            price_per_day,
            show_price,
            confidentiality
        } = req.body;
        try {
            if (title_profile.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in title_profile")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (username.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in username")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (email.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in email")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (lastName.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (!/^\d+$/.test(phone) || phone.length !== 10) {
                loggerFile.debug("creation Freelancer data : problem in phone")
                return res.status(400).send({message: "le champ téléphone doit contenir que 10 chiffres"});
            }
            if (password.trim().length === 0 || password.length < 8) {
                loggerFile.debug("creation Freelancer data : problem in password")
                return res.status(400).send({message: "le mot de passe est erroné"});
            }
            if (repeatPassword.trim().length === 0 || repeatPassword.length < 8) {
                loggerFile.debug("creation Freelancer data : problem in repeatPassword")
                return res.status(400).send({message: "les mots de passe ne sont pas égaux"});
            }
            if (password != repeatPassword) {
                loggerFile.debug("creation Freelancer data : problem in equals passwords")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (localisation.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in localisation")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (!levels.includes(level) || level.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in level")
                return res.status(400).send({message: "le niveau est inapproprié"});
            }
            if (price_per_day < 0 || price_per_day.empty) {
                loggerFile.debug("creation Freelancer data : problem in price_per_day")
                return res.status(400).send({message: "le montant doit être positif"});
            }
            if (typeof show_price !== "boolean") {
                loggerFile.debug("creation Freelancer data : problem in show_price")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (typeof confidentiality !== "boolean") {
                loggerFile.debug("creation Freelancer data : problem in confidentiality")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            } else {
                loggerFile.debug('validate creationFreelancer done')
                return next();
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: "réessayez l'action"});
        }
    },
    middlewareJobExist: async (req, res, next) => {
        loggerFile.info("in middlewareJobExist")
        try {
            let {jobCat} = req.body;
            await adminService.getJobs().then(response => {
                if (response.status == 200) {
                    let job = response.data.find(subject => subject._id === jobCat);
                    if (job) {
                        loggerFile.debug("middlewareJobExist done step 2")
                        return next();
                    } else {
                        loggerFile.debug("le métier n'éxiste pas")
                        return res.status(400).json({message: `le métier n'éxiste pas`});
                    }
                } else {
                    loggerFile.debug("un probléme est survenu")
                    return res.status(400).json({message: `un probléme est survenu`});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareSkillExist: async (req, res, next) => {
        loggerFile.info("in middlewareSkillExist")
        let skills = req.body.skills;
        try {
            if (skills) {
                await adminService.getSkills().then(response => {
                    if (response.status == 200) {
                        loggerFile.debug("middlewareSkillExist done")
                        req.body.allskills = response.data;
                        return next();
                    } else {
                        return res.status(400).send({message: `erreur lors du chargement des skills`});
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).send({message: err.message});
                })
            } else {
                loggerFile.debug("no skills to verify")
                return next();
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistUsernameFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameFreelancer")
        let {username} = req.body;
        try {
            await Freelancer.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsernameFreelancer done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailFreelancer")
        let {email} = req.body;
        try {
            await Freelancer.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else return next();
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareTokenFreelancer")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Freelancer.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err, utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Freelancer") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareExistUsernameUpdateFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameUpdateFreelancer")

        let {username} = req.body;
        if (username === req.auth.usernameConnected) {
            loggerFile.debug('verify username equal usernameConnected')
            return next()
        } else {
            try {
                await Freelancer.countDocuments({username: username}).then(count => {
                    if (count >= 1) {
                        loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                        return res.status(401).json({
                            message: "l'utilisateur existe, essayez un autre identifiant"
                        });
                    } else {
                        loggerFile.debug("verify username exist update Freelance done")
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(401).json({
                        message: err.message
                    });
                });
            } catch (error) {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareExistEmailUpdateFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailUpdateFreelancer")

        let {email} = req.body;
        if (email === req.auth.emailConnected) {
            loggerFile.debug('verify email equal emailConnected')
            return next()
        } else {
            try {
                await Freelancer.countDocuments({email: email}).then(count => {
                    if (count >= 1) {
                        loggerFile.debug("cette adresse mail existe, essayez une autre")
                        return res.status(401).json({
                            message: "cette adresse mail existe, essayez une autre"
                        });
                    } else {
                        loggerFile.debug("verify username exist update Freelance done")
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(401).json({
                        message: err.message
                    });
                });
            } catch (error) {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareValidityUpdateFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareValidityUpdateFreelancer")

        const {
            lastName,
            firstName,
            phone,
            description,
            url_fb,
            url_github,
            url_twitter,
            url_linkedin,
            disponibility,
            disponibility_freq,
        } = req.body;
        try {
            if (lastName && typeof lastName !== "string") {
                loggerFile.debug("creation Freelancer data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName && typeof firstName !== "string") {
                loggerFile.debug("creation Freelancer data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (phone && (!/^\d+$/.test(phone) || typeof phone !== "string")) {
                loggerFile.debug("creation Freelancer data : problem in phone")
                return res.status(400).send({message: "le champ téléphone doit contenir que 10 chiffres"});
            }
            if (description && typeof description !== "string") {
                loggerFile.debug("creation Freelancer data : problem in description")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (url_fb && typeof url_fb !== "string") {
                loggerFile.debug("creation Freelancer data : problem in url_fb")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (url_github && typeof url_github !== "string") {
                loggerFile.debug("creation Freelancer data : problem in url_github")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (url_linkedin && typeof url_linkedin !== "string") {
                loggerFile.debug("creation Freelancer data : problem in url_linkedin")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (url_twitter && typeof url_twitter !== "string") {
                loggerFile.debug("creation Freelancer data : problem in url_twitter")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (disponibility && typeof disponibility !== "boolean") {
                loggerFile.debug("creation Freelancer data : problem in disponibility")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (disponibility_freq < 0 || disponibility_freq > 5) {
                loggerFile.debug("creation Freelancer data : problem in disponibility_freq")
                return res.status(400).send({message: "la fréquence de la disponibilité est erroné"});
            }
            if (disponibility_freq && typeof disponibility_freq !== "number") {
                loggerFile.debug("creation Freelancer data : problem in disponibility_freq")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            } else {
                loggerFile.debug("verify data before update freelance done")
                return next()
            }

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareFreelancerExist: async (req, res, next) => {
        loggerFile.info("in middlewareFreelancerExist")

        try {
            let freelancer = await Freelancer.findById(req.auth._idConnected);
            if (freelancer) {
                loggerFile.debug("middlewareFreelancerExist done")
                return next();
            } else {
                loggerFile.debug("freelancer not found")
                return res.status(400).json({message: "freelancer introuvable"})

            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateLegalForms: async (req, res, next) => {
        loggerFile.info("in validateLegalForms")
        try {
            if (legalForms.includes(req.body.legal_form)) {
                loggerFile.debug("validateLegalForms done")
                return next();
            } else {
                loggerFile.debug("la forme légale est inéxistante")
                return res.status(400).json({message: "la forme légale est inéxistante"})
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateIbanTypeAndData: async (req, res, next) => {
        loggerFile.info("in validateIbanTypeAndData")
        let {
            type_iban,
            cb_iban_postal,
            cb_iban_iban,
            cb_iban_account_number,
            cb_iban_aba_transit_number,
            cb_iban_branch_code,
            cb_iban_number_institution,
            cb_iban_bic_swift
        } = req.body;
        try {
            if (ibanType.includes(req.body.type_iban)) {
                switch (type_iban) {
                    case 'iban': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_iban) || cb_iban_iban.length != 14) {
                            return res.status(400).json({message: "iban erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'iban-us': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_aba_transit_number) || cb_iban_aba_transit_number.length != 9) {
                            return res.status(400).json({message: "ABA Transit number erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'iban-ca': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_branch_code) || cb_iban_branch_code.length != 5) {
                            return res.status(400).json({message: "Code guichet erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_number_institution) || cb_iban_number_institution.length != 3) {
                            return res.status(400).json({message: "Numéro de l'institution erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'others': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!cb_iban_bic_swift.match("^[A-Z]+$") || cb_iban_bic_swift.length != 8) {
                            return res.status(400).json({message: "BIC/SWIFT erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next()
                        }
                    }
                        break;
                }
            } else {
                loggerFile.debug("le type d'iban n'est pas conforme")
                return res.status(400).json({message: "le type d'iban n'est pas conforme"})
            }
        } catch
            (err) {
            loggerFile.error(err, utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateCountryLegalRepresentative: async (req, res, next) => {
        loggerFile.info("in validateCountryLegalRepresentative")
        try {
            let {country_of_birth, nationality} = req.body;
            const response = await apiService.getCountry();
            if (response.data.includes(country_of_birth)) {
                if (response.data.includes(nationality)) {
                    loggerFile.debug("validateCountryLegalRepresentative done")
                    return next();
                } else {
                    loggerFile.debug("la nationalité est introuvable")
                    return res.status(400).json({message: "la nationalité est introuvable"});
                }
            } else {
                loggerFile.debug("le pays de naissance est introuvable")
                return res.status(400).json({message: "le pays de naissance est introuvable"});
            }
        } catch
            (err) {
            loggerFile.error(err, utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateIbanCountry: async (req, res, next) => {
        loggerFile.info("in validateIbanCountry")
        try {
            let {type_iban, cb_iban_country, cb_iban_account_country} = req.body;
            let namesIbanCountry = [];
            let namesIbanUsCaOthersCountry = [];

            if (type_iban == "iban") {
                let response = await apiService.getIbanCountry();
                response.data.map(async (el) => {
                    await namesIbanCountry.push(el.name);
                });
            } else {
                let response = await apiService.getIbanUsCaOtherCountry()
                response.data.map(async (el) => {
                    await namesIbanUsCaOthersCountry.push(el.name);
                });
            }
            if (type_iban == 'iban' && namesIbanCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'iban-us' && namesIbanUsCaOthersCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'iban-ca' && namesIbanUsCaOthersCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'others' && namesIbanUsCaOthersCountry.includes(cb_iban_country) && namesIbanUsCaOthersCountry.includes(cb_iban_account_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else {
                loggerFile.debug("le pays ne corresponds pas")
                return res.status(400).json({message: "le pays ne corresponds pas"});
            }
        } catch
            (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    verifyMentionLegalCreation: async (req, res, next) => {
        loggerFile.info("in verifyMentionLegalCreation")

        let {sas, siret, rcs, naf, tva_intracom, days} = req.body;
        try {
            if (!sas || typeof sas !== "number" || sas < 0) {
                loggerFile.debug("sas non conforme")
                return res.status(400).json({message: "sas non conforme"});
            }
            if (!siret || typeof siret !== "string" || siret.length !== 14 || !/^\d+$/.test(siret)) {
                loggerFile.debug("siret non conforme")
                return res.status(400).json({message: "siret non conforme"});
            }
            if (!rcs || typeof rcs !== "string" || !/^[a-zA-Z]+$/.test(rcs)) {
                loggerFile.debug("rcs non conforme")
                return res.status(400).json({message: "rcs non conforme"});
            }
            let nafNumbers = naf.slice(0, naf.length - 1);
            if (!naf || typeof naf !== "string" || naf.length !== 5 || !naf.match("^[A-Z0-9]+$") || !/^\d+$/.test(nafNumbers) || !naf[naf.length - 1].match("^[A-Z]+$")) {
                loggerFile.debug("naf non conforme")
                return res.status(400).json({message: "naf non conforme"});
            }
            let tva_numbers = tva_intracom.slice(2, tva_intracom.length);
            if (!tva_intracom || typeof tva_intracom !== "string" || !tva_intracom[0].match("F") || !tva_intracom[1].match("R") || !/^\d+$/.test(tva_numbers)) {
                loggerFile.debug("tva_intracom non conforme")
                return res.status(400).json({message: "tva_intracom non conforme"});
            }
            if (days < 0 || days > 60) {
                loggerFile.debug("days non conforme")
                return res.status(400).json({message: "Nombre de jours erroné"});
            } else {
                loggerFile.debug("verifyMentionLegalCreation done")
                return next();
            }

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getSkills: async (req, res, next) => {
        loggerFile.info("in getSkills")
        try {
            let response = await adminService.getSkills();
            if (response) {
                loggerFile.debug("getSkills done")
                req.body.allSkills = response.data;
                return next();
            } else {
                loggerFile.debug("erreur lors du chargement des skills")
                return res.status(400).send({message: `erreur lors du chargement des skills`});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: err.message});
        }
    },
    getJobs: async (req, res, next) => {
        loggerFile.info("in getJobs")
        try {
            let response = await adminService.getJobs();
            if (response) {
                req.body.jobs = response.data;
                loggerFile.debug("getJobs done")
                return next();
            } else {
                loggerFile.debug("erreur lors du chargement des jobs")
                return res.status(400).send({message: `erreur lors du chargement des jobs`});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenFreelancerOrAdminOrRoot: async (req, res, next) => {
        loggerFile.info("in middlewareTokenFreelancerOrAdminOrRoot")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            let decoded = jwt.decode(token);
            if (decoded) {
                const _id = decoded._idConnected;
                const user = await Freelancer.findById(_id);
                jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                    if (err) return res.status(401).json({user: false, error: "token invalide"});
                    if (user && (user.role === "Freelancer" || user.role === "Admin" || user.role === "Root")) {
                        req.auth = decoded2
                        loggerFile.debug("middlewareTokenFreelancerOrAdminOrRoot done")
                        return next();
                    } else {
                        loggerFile.debug("accés non autorisé")
                        return res.status(401).json({user: false, error: "accés non autorisé"});
                    }
                });
            } else {
                loggerFile.debug("token invalide")
                return res.status(401).json({user: false, error: "token invalide"});
            }
        }
    },
    middlewareAddInterest: async (req, res, next) => {
        loggerFile.info("in middlewareAddInterest")
        let id = req.auth._idConnected;
        let {interest} = req.body;
        try {
            let freelancer = await Freelancer.findById(id);
            if (freelancer.interest.includes(interest)) {
                loggerFile.debug("centre d'intérét déja ajouté")
                return res.status(400).json({message: "centre d'intérét déja ajouté"})
            } else {
                loggerFile.debug("middlewareAddInterest done")
                next();
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    ExistParamFreelancer: async (req, res, next) => {
        loggerFile.info("in ExistParamFreelancer")
        try {
            let freelancer = await Freelancer.findById(req.params.id);
            if (freelancer) {
                loggerFile.debug("freelancer trouvé")
                return next();
            } else {
                loggerFile.debug("freelancer introuvable")
                return res.status(400).json({message: "freelancer introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err, utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
}

