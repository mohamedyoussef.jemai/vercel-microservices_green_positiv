const Experience = require("../models/experience.model");
const Freelancer = require('../models/freelancer.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

let sector_activity =
    [
        "AVIATION_AEROSPACE",
        "IT",
        "FOOD",
        "ARCHITECTURE",
        "CRAFTS",
        "CIVIC_SOCIAL",
        "AUTOMOBILE",
        "BANKING_INSURANCE",
        "BIOTECH",
        "CIVIL_ENGINEERING",
        "RESEARCH",
        "CHEMICAL",
        "FILM",
        "SMALL_RETAIL",
        "CONSULTING",
        "CULTURE",
        "MILITARY",
        "LEISURE",
        "E_COMMERCE",
        "PUBLISHING",
        "EDUCATION",
        "SOFTWARE",
        "ENERGY",
        "ENVIRONMENT",
        "RETAIL",
        "HIGH_TECH",
        "HOSPITALITY",
        "REAL_ESTATE",
        "IMPORT_EXPORT",
        "MECHANICAL",
        "PRIMARY",
        "PHARMA",
        "GAMES",
        "LUXURY",
        "COSMETICS",
        "NANOTECH",
        "IOT",
        "PRESS",
        "SOCIAL",
        "RH",
        "RESTAURANTS",
        "HEALTH",
        "MEDICAL",
        "PUBLIC",
        "SAFETY",
        "SPORTS",
        "TELECOM",
        "TRANSPORT",
        "LOGISTIC",
        "TRAVEL",
        "WINE"];

module.exports = {
    //verify if domain is correct
    verifySector: async (req, res, next) => {
        loggerFile.info("in verifySector")
        if (!sector_activity.includes(req.body.domain)) {
            loggerFile.debug("le secteur est inéxistant")
            return await res.status(400).json({message: "le secteur est inéxistant"});
        }
        return await next();
    },
    addExperience: async (req, res, next) => {
        loggerFile.info("in addExperience")
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {
                society,
                domain,
                title,
                isFreelancer,
                actuallyPost,
                place,
                dateBegin,
                dateEnd,
                description,
                skills
            } = req.body;
            let experience = new Experience({
                society,
                domain,
                title,
                isFreelancer,
                actuallyPost,
                place,
                dateBegin,
                dateEnd,
                description,
                skills
            });

            if (Date.parse(dateBegin) > Date.parse(dateEnd)) {
                loggerFile.debug("Changer la période, l'intervalle est erroné")
                return res.status(400).json({message: "Changer la période, l'intervalle est erroné"});
            }
            await Experience.create(experience).then(data => {
                if (data) {
                    loggerFile.debug("addExperience data done")
                    req.experience = data;
                    return next();
                }
            }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de l'ajout de l'expérience"});
                }
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyExperienceFreelancer: async (req, res, next) => {
        loggerFile.info("in verifiyExperienceFreelancer")

        const idFreelancer = req.auth._idConnected;
        const idExperience = req.params.id;
        try {
            const freelancer = await Freelancer.findById(idFreelancer);
            if (freelancer.experiences.includes(idExperience)) {
                loggerFile.debug("verifiyExperienceFreelancer done")
                return next();
            } else {
                loggerFile.debug("Expérience inéxistante pour ce freelancer")
                return res.status(400).json({message: "Expérience inéxistante pour ce freelancer"});

            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getExperiences: async (req, res, next) => {
        loggerFile.info("in getExperiences")
        var experiences = [];
        const idFreelancer = req.params.id;
        try {
            const freelancer = await Freelancer.findById(idFreelancer);
            if (freelancer != null && freelancer.experiences.length > 0) {
                await freelancer.experiences.forEach(async function callback(experienceId, index) {
                    let exp = await Experience.findById(experienceId);
                    await experiences.push(exp);
                });
                loggerFile.debug("getExperiences data done")
                req.body.experiences = experiences;
                return next();
            } else {
                loggerFile.debug("getExperiences data done")
                req.body.experiences = [];
                return next();
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}

