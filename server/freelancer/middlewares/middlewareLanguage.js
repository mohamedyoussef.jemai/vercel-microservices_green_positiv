const Freelancer = require('../models/freelancer.model');
const apiService = require("../services/apiService");
const loggerFile = require("../config/logger");

const languagesLevels = ["NATIVE", "BASIC", "CONVERSATIONAL", "FLUENT"];

module.exports = {
    verifyLanguage: async (req, res, next) => {
        loggerFile.info("in verifyLanguage")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {language} = req.body;
            let names = [];
            let arrayLanguages = await apiService.getLanguages();
            await arrayLanguages.map((el) => {
                names.push(el.name);
            });
            if (!names.includes(language.name)) {
                loggerFile.debug("la langue est introuvable")
                return res.status(400).json({message: "la langue est introuvable"});
            }
            if (!languagesLevels.includes(req.body.language.level)) {
                loggerFile.debug("la niveau est introuvable")
                return res.status(400).json({message: "le niveau est introuvable"});
            } else {
                loggerFile.debug("verifyLanguage done")
                return next();
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    LanguageExist: async (req, res, next) => {
        loggerFile.info("in LanguageExist")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {language} = req.body;
            let test = true;
            let freelancer = await Freelancer.findById(req.auth._idConnected);
            await freelancer.languages.forEach(function callback(langue, index) {
                if (langue.name == language.name) {
                    test = false;
                    loggerFile.debug("Vous avez déja saisis cette langue")
                    return res.status(400).json({message: "Vous avez déja saisis cette langue"});
                }
            });
            if (test) {
                req.test = true;
                loggerFile.debug("LanguageExist done")
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

