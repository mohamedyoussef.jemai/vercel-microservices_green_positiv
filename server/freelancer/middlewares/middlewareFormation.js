const Formation = require("../models/formation.model");
const Freelancer = require('../models/freelancer.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    addFormation: async (req, res, next) => {
        loggerFile.info('in addFormation')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {name, institute, type, year, description} = req.body;
            let formation = new Formation({name, institute, type, year, description});
            await Formation.create(formation).then(data => {
                if (data) {
                    loggerFile.debug("addFormation data done")
                    req.formation = data;
                    return next();
                }
            }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de l'ajout de la formation"});
                }
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyFormationFreelancer: async (req, res, next) => {
        loggerFile.info('in verifiyFormationFreelancer')
        const idFreelancer = req.auth._idConnected;
        const idFormation = req.params.id;
        try {
            const freelancer = await Freelancer.findById(idFreelancer);
            if (freelancer.formations.includes(idFormation)) {
                loggerFile.debug("verifyFormationFreelancer done")
                return next();
            } else {
                loggerFile.debug("formation non éxistante pour ce freelancer")
                return res.status(400).json({message: "formation inéxistante pour ce freelancer"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFormation: async (req, res, next) => {
        loggerFile.info('in getFormation')
        var formations = [];
        const idFreelancer = req.params.id;
        const freelancer = await Freelancer.findById(idFreelancer);
        try {
            if (freelancer != null && freelancer.formations.length > 0) {
                await freelancer.formations.forEach(async function callback(formationId, index) {
                    let formation = await Formation.findById(formationId);
                    await formations.push(formation);
                });
                loggerFile.debug('getFormation data done')
                req.body.formations = formations;
                return next();
            } else {
                loggerFile.debug('getFormation data done')
                req.body.formations = [];
                return next();
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}

