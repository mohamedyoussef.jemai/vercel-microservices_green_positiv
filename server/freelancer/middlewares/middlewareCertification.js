const Certification = require("../models/certification.model");
const Freelancer = require('../models/freelancer.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    addCertification: async (req, res, next) => {
        loggerFile.info("in addCertification")
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {name, organism, type, year, description, place} = req.body;
            let certification = new Certification({name, organism, type, year, description, place});
            await Certification.create(certification).then(data => {
                if (data) {
                    loggerFile.debug("certification created")
                    req.certification = data;
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de l'ajout de la certification"});
                }
            );
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyCertificationFreelancer: async (req, res, next) => {
        loggerFile.info("in verifiyCertificationFreelancer")
        const idFreelancer = req.auth._idConnected;
        const idCertification = req.params.id;
        try {
            const freelancer = await Freelancer.findById(idFreelancer);
            if (freelancer.certifications.includes(idCertification)) {
                loggerFile.debug("verifiyCertificationFreelancer done")
                return next();
            } else {
                loggerFile.debug("Certification inéxistante pour ce freelancer")
                return res.status(400).json({message: "Certification inéxistante pour ce freelancer"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getCertification: async (req, res, next) => {
        loggerFile.info("in getCertification")
        var certifications = [];
        const idFreelancer = req.params.id;
        try {
            const freelancer = await Freelancer.findById(idFreelancer);
            if (freelancer != null && freelancer.certifications.length > 0) {
                await freelancer.certifications.forEach(async function callback(certificationId, index) {
                    let certif = await Certification.findById(certificationId);
                    await certifications.push(certif);
                });
                loggerFile.debug('getCertification done')
                req.body.certifications = certifications;
                return next();
            } else {
                loggerFile.debug('getCertification done')
                req.body.certifications = [];
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else return res.status(400).json({message: error.message});
        }
    }
}

