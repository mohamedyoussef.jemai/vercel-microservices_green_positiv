require('dotenv').config();
const Formation = require('../models/formation.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    findById: async (req, res) => {
        loggerFile.info("in findById formation");
        try {
            let {id} = req.params;
            const formation = await Formation.findById(id);
            if (formation) {
                loggerFile.debug('in findById formation done')
                return res.status(200).json(formation);
            } else {
                loggerFile.debug('aucune formation éxistante')
                return res.status(200).json({message: "formation inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update formation');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFormation = req.body;
        try {
            await Formation.findByIdAndUpdate(id, newFormation);
            loggerFile.debug('formation modifiée')
            return res.status(200).json({message: "formation modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

