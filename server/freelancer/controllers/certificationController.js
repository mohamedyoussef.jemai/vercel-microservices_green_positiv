require('dotenv').config();
const Certification = require('../models/certification.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    findById: async (req, res) => {
        loggerFile.info("in findById certification");
        try {
            let {id} = req.params;
            const certification = await Certification.findById(id);
            if (certification) {
                loggerFile.debug('in findById certification done')
                return res.status(200).json(certification);
            } else {
                loggerFile.debug('aucune certification éxistante')
                return res.status(200).json({message: "certification inéxistante"});
            }
        } catch (error) {
           loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update certification');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newCertification = req.body;
        try {
            await Certification.findByIdAndUpdate(id, newCertification);
            loggerFile.debug('certification modifiée')
            return res.status(200).json({message: "certification modifiée"});
        } catch (error) {
           loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

