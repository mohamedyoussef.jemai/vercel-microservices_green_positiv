require('dotenv').config();
const Experience = require('../models/experience.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    findById: async (req, res) => {
        loggerFile.info("in findById experience");
        try {
            let {id} = req.params;
            const experience = await Experience.findById(id);
            if (experience) {
                loggerFile.debug('in findById experience done')
                return res.status(200).json(experience);
            } else {
                loggerFile.debug('aucune experience éxistante')
                return res.status(200).json({message: "experience inéxistante"});
            }
        } catch (error) {
           loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update experience');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newExperience = req.body;
        try {
            await Experience.findByIdAndUpdate(id, newExperience);
            loggerFile.debug('experience modifiée')
            return res.status(200).json({message: "experience modifiée"});
        } catch (error) {
           loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

