require('dotenv').config();
const crypto = require('crypto');
const fs = require('fs');
const Freelancer = require('../../models/freelancer.model');
const Formation = require('../../models/formation.model');
const Certification = require('../../models/certification.model');
const Experience = require('../../models/experience.model');
const notificationService = require("../../services/notificationService");
const profilEntrepriseService = require("../../services/profilEntrepriseService");
const authService = require("../../services/authService");
const mailService = require("../../services/mailingService");
const adminService = require("../../services/adminService");
const ObjectId = require('mongoose').Types.ObjectId;
const loggerFile = require("../../config/logger");

module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create freelancer');
        const {
            username,
            email,
            lastName,
            firstName,
            phone,
            password,
            jobCat,
            title_profile,
            localisation,
            level,
            skills,
            price_per_day,
            show_price,
            confidentiality,
            description
        } = req.body;
        try {
            const freelancer = new Freelancer({
                username,
                email,
                lastName,
                firstName,
                phone,
                password,
                jobCat,
                title_profile,
                localisation,
                level,
                skills,
                price_per_day,
                show_price,
                confidentiality,
                description
            });
            freelancer.role = "Freelancer";
            freelancer.code_verification = crypto.randomBytes(20).toString('hex');
            freelancer.validated = false;
            await Freelancer.create(freelancer);
            return res.status(200).json({message: "Freelancer Créé"});
        } catch (error) {
            loggerFile.error(error);
            res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll freelancers");
        try {
            const freelancers = await Freelancer.find();
            if (freelancers) {
                loggerFile.debug('get all freelancers done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('aucun freelancers éxistants')
                return res.status(200).json({message: "freelancers inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find freelancer by ID");
        const {id} = req.params;
        try {
            const freelancer = await Freelancer.findById(id);
            if (freelancer) {
                let jobCat = "";
                await req.body.jobs.forEach(async function callback(job, index) {
                    if (freelancer.jobCat == job._id) {
                        jobCat = await job.name;
                    }
                });
                loggerFile.debug('get freelancer by id');
                return await res.status(200).json({
                    freelancer: freelancer,
                    certifications: req.body.certifications,
                    formations: req.body.formations,
                    experiences: req.body.experiences,
                    jobCat: jobCat,
                    skills: req.body.allSkills
                });
            } else {
                loggerFile.debug('freelancer inéxistant');
                return res.status(200).json({message: "freelancer inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else {
                loggerFile.error(error);
                return res.status(400).json({message: error});
            }
        }
    },
    findOneById: async (req, res) => {
        loggerFile.info("in findOneById freelancer by ID");
        const {id} = req.params;
        try {
            const freelancer = await Freelancer.findById(id);
            if (freelancer) {
                loggerFile.debug('get freelancer by id done');
                return await res.status(200).json({username: freelancer.username, role: freelancer.role});
            } else {
                loggerFile.debug('freelancer inéxistant');
                return res.status(200).json({message: "freelancer inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else {
                loggerFile.error(error);
                return res.status(400).json({message: error});
            }
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update freelancer');
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFreelancer = req.body;
        try {
            await Freelancer.findByIdAndUpdate(id, newFreelancer);
            loggerFile.debug('freelancer modifié')
            return res.status(200).json({message: "Freelancer modifié"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    //verif account via a code sended in creation account
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let freelancer = await Freelancer.findById(id);
            if (freelancer && freelancer.code_verification === token) {
                await Freelancer.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err.message);
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(400).json({message: "l'utilisateur est inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    blockFreelancer: async (req, res) => {
        loggerFile.info("in blockFreelancer")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {validated: false});
            let freelancer = await Freelancer.findById(id);
            loggerFile.info("in send email after bloqued")
            await mailService.block({
                email: freelancer.email
            });
            loggerFile.debug('freelancer bloqué')
            return res.status(200).json({message: "Freelancer bloqué"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    //validate freelance
    unblockFreelancer: async (req, res) => {
        loggerFile.info("in unblockFreelancer")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {validated: true});
            let freelancer = await Freelancer.findById(id);
            loggerFile.info("in send email after unblocked")
            await mailService.unblock({email: freelancer.email});
            loggerFile.debug('freelancer débloqué')
            return res.status(200).json({message: "Freelancer débloqué"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addFormation: async (req, res) => {
        loggerFile.info("in addFormation")
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let formation = req.formation;
            await Freelancer.findByIdAndUpdate(id, {$push: {formations: formation._id}}, {new: true, upsert: true});
            loggerFile.debug('addFormation data done')
            return res.status(200).json({message: "Formation ajoutée"});

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateFormation: async (req, res) => {
        loggerFile.info("in updateFormation")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {id} = req.params;
            await Formation.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateFormation data done')
                    return res.status(200).json({message: "Formation modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: "erreur lors de la mise à jour de la formation"});
                }
            );
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteFormation: async (req, res) => {
        loggerFile.info("in deleteFormation")
        try {
            let {id} = req.body;
            let idFormation = req.params.id;
            await Formation.findByIdAndDelete(idFormation);
            await Freelancer.findByIdAndUpdate(id, {$pull: {formations: new ObjectId(idFormation)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteFormation done');
                    return res.status(200).json({message: "Formation supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addCertification: async (req, res) => {
        loggerFile.info("in addCertification")

        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let certification = req.certification;
            await Freelancer.findByIdAndUpdate(id, {$push: {certifications: certification._id}}, {
                new: true,
                upsert: true
            });
            loggerFile.debug('addCertification data done')
            return res.status(200).json({message: "Certificat ajouté"});

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateCertification: async (req, res) => {
        loggerFile.info("in updateCertification")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {id} = req.params;
            await Certification.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateCertification data done')
                    return res.status(200).json({message: "Certification modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: "erreur lors de la mise à jour de la certification"});
                }
            );
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteCertification: async (req, res) => {
        loggerFile.info("in deleteCertification")
        try {
            let {id} = req.body;
            let idCertification = req.params.id;
            await Certification.findByIdAndDelete(idCertification);
            await Freelancer.findByIdAndUpdate(id, {$pull: {certifications: new ObjectId(idCertification)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug("deleteCertification done")
                    return res.status(200).json({message: "Certification supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err)
                return res.status(400).json({message: err.message})
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addExperience: async (req, res) => {
        loggerFile.info("in addExperience")

        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let experience = req.experience;
            await Freelancer.findByIdAndUpdate(id, {$push: {experiences: experience._id}}, {
                new: true,
                upsert: true
            });
            loggerFile.debug('addExperience data done')
            return res.status(200).json({message: "Expérience ajoutée"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateExperience: async (req, res) => {
        loggerFile.info("in updateExperience")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.params.id;
            await Experience.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateExperience data done')
                    return res.status(200).json({message: "Expérience modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: "erreur lors de la mise à jour de l'éxpérience "});
                }
            );
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteExperience: async (req, res) => {
        loggerFile.info("in deleteExperience")
        try {
            let {id} = req.body;
            let idExperience = req.params.id;
            await Experience.findByIdAndDelete(idExperience);
            await Freelancer.findByIdAndUpdate(id, {$pull: {experiences: new ObjectId(idExperience)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteExperience done')
                    return res.status(200).json({message: "Expérience supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message})
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addLanguage: async (req, res) => {
        loggerFile.info("in addLanguage")
        try {
            let {id} = req.body;

            if (req.test) {
                await Freelancer.findByIdAndUpdate(id, {$push: {languages: req.body.language}}, {
                    new: true,
                    upsert: true,
                    multi: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addLanguage data done');
                        return res.status(200).json({message: "Langue ajoutée"});
                    }
                }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                })
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateLanguage: async (req, res) => {
        loggerFile.info("in updateLanguage")
        try {
            let {id} = req.body;

            let newFreelancer = await Freelancer.findById(id);
            let {name, level} = req.body.language;
            const index = newFreelancer.languages.findIndex((el) => el.name === name);
            newFreelancer.languages[index].level = level;

            await Freelancer.findByIdAndUpdate(id, newFreelancer, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('updateLanguage data done')
                    return res.status(200).json({message: "Langue modifiée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteLanguage: async (req, res) => {
        loggerFile.info("in deleteLanguage")
        try {
            let {id} = req.body;

            let newFreelancer = await Freelancer.findById(id);
            let {name} = req.body.language;
            const index = newFreelancer.languages.findIndex((el) => el.name === name);
            newFreelancer.languages.splice(index, 1);

            await Freelancer.findByIdAndUpdate(id, newFreelancer, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug("deleteLanguage done")
                    return res.status(200).json({message: "Langue supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    getSkills: async (req, res) => {
        loggerFile.info("in getSkills")
        try {
            let response = await adminService.getSkills();
            loggerFile.debug("getSkills data done")
            return res.status(200).json(response);
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addInterest: async (req, res) => {
        loggerFile.info("in addInterest")
        let {id, interest} = req.body;
        try {
            if (interest) {
                await Freelancer.findByIdAndUpdate(id, {$push: {interest: interest}}, {
                    new: true,
                    upsert: true,
                    multi: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addInterest data done');
                        return res.status(200).json({message: "Centre d'intérét ajouté"});
                    }
                }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                })
            } else {
                loggerFile.error('les  informations sont vide');
                return res.status(400).json({message: "les  informations sont vide"});
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteInterest: async (req, res) => {
        loggerFile.info("in deleteInterest")
        try {
            let {id} = req.body;
            let newFreelancer = await Freelancer.findById(id);
            const index = newFreelancer.interest.findIndex((el) => el === req.body.interest);
            if (index != -1) {
                newFreelancer.interest.splice(index, 1);
                await Freelancer.findByIdAndUpdate(id, newFreelancer, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('deleteInterest done')
                        return res.status(200).json({message: "Centre d'intérét Effacé"});
                    }
                }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                })
            } else {
                loggerFile.debug("centre d'intérét inéxistant");
                return res.status(400).json({message: "Centre d'intérét inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    countUsername: async (req, res) => {
        loggerFile.info("in countUsername")
        let {username} = req.body;
        try {
            await Freelancer.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("countUsername done")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    countEmail: async (req, res) => {
        loggerFile.info("in countEmail")
        let {email} = req.body;
        try {
            await Freelancer.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'email existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'email existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("countEmail done")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    getCertificaiton: async (req, res) => {
        loggerFile.info("in getCertificaiton freelancers");
        try {
            const certifications = await Certification.find();
            if (certifications) {
                loggerFile.debug('get all certifications done')
                return res.status(200).json(certifications);
            } else {
                loggerFile.debug('aucun certifications éxistants')
                return res.status(200).json({message: "certifications inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    getExperiences: async (req, res) => {
        loggerFile.info("in getExperiences freelancers");
        try {
            const experiences = await Experience.find();
            if (experiences) {
                loggerFile.debug('get all experiences done')
                return res.status(200).json(experiences);
            } else {
                loggerFile.debug('aucun experiences éxistants')
                return res.status(200).json({message: "experiences inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    getFormations: async (req, res) => {
        loggerFile.info("in getFormations freelancers");
        try {
            const formations = await Formation.find();
            if (formations) {
                loggerFile.debug('get all formations done')
                return res.status(200).json(formations);
            } else {
                loggerFile.debug('aucun formations éxistants')
                return res.status(200).json({message: "formations inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete freelance')
        const {id} = req.params;
        try {
            await Freelancer.findByIdAndDelete(id).then(data => {
                if (data) {
                    return res.status(200).json({message: "Freelance effacé"})
                }
            }).catch(err => {
                loggerFile.error(error)
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    }
}

