require('dotenv').config();
const crypto = require('crypto');
const Freelancer = require('../models/freelancer.model');
const Formation = require('../models/formation.model');
const Certification = require('../models/certification.model');
const Experience = require('../models/experience.model');
const notificationService = require("../services/notificationService");
const profilEntrepriseService = require("../services/profilEntrepriseService");
const authService = require("../services/authService");
const mailService = require("../services/mailingService");
const adminService = require("../services/adminService");
const missionService = require("../services/missionService");
const ObjectId = require('mongoose').Types.ObjectId;
const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create freelancer');
        const {
            username,
            email,
            lastName,
            firstName,
            phone,
            password,
            jobCat,
            title_profile,
            localisation,
            level,
            skills,
            price_per_day,
            show_price,
            confidentiality,
            description
        } = req.body;
        try {
            let testAuthCreation = await authService.validateCreation({
                username: username,
                email: email
            });
            if (!testAuthCreation.state && testAuthCreation.message != "done") {
                return res.status(400).json({message: testAuthCreation.message});
            } else {
                const freelancer = new Freelancer({
                    username,
                    email,
                    lastName,
                    firstName,
                    phone,
                    password,
                    jobCat,
                    title_profile,
                    localisation,
                    level,
                    skills,
                    price_per_day,
                    show_price,
                    confidentiality,
                    description
                });
                freelancer.role = "Freelancer";
                freelancer.code_verification = crypto.randomBytes(20).toString('hex');
                freelancer.validated = false;
                let freelancerCreated = await Freelancer.create(freelancer);
                if (freelancerCreated) {
                    let data = {
                        name: "",
                        address: "",
                        address_plus: "",
                        legal_form: "Association loi 1901 - avec TVA",
                        id_freelancer: freelancerCreated._id
                    }
                    let testProfil = await profilEntrepriseService.createContactDetails(data);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de l'ajout du profil"});

                    let testAuth = await authService.create({
                        username: username,
                        email: email,
                        password: password,
                        idUser: freelancerCreated._id,
                        role: "Freelancer"
                    });
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la création de compte"});

                    mailService.create({
                        email: freelancerCreated.email,
                        code_verification: freelancerCreated.code_verification,
                        id: freelancerCreated._id,
                        role: "Freelancer"
                    });
                    if (confidentiality) adminService.addSubscription(freelancerCreated.email);
                    if (!testAuth) return res.status(400).json({message: "probléme lors de la création de compte"});
                    else return res.status(200).json({message: "Freelancer créé"});
                }
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll freelancers");
        try {
            const freelancers = await Freelancer.find();
            if (freelancers) {
                loggerFile.debug('get all freelancers done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('aucun freelancers éxistants')
                return res.status(200).json({message: "freelancers inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find freelancer by ID");
        const {id} = req.params;
        try {
            const freelancer = await Freelancer.findById(id);
            if (freelancer) {
                let jobCat = "";
                await req.body.jobs.forEach(async function callback(job, index) {
                    if (freelancer.jobCat == job._id) {
                        jobCat = await job.name;
                    }
                });
                loggerFile.debug('get freelancer by id');
                return await res.status(200).json({
                    freelancer: freelancer,
                    certifications: req.body.certifications,
                    formations: req.body.formations,
                    experiences: req.body.experiences,
                    jobCat: jobCat,
                    skills: req.body.allSkills
                });
            } else {
                loggerFile.debug('freelancer inéxistant');
                return res.status(200).json({message: "freelancer inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    findOneById: async (req, res) => {
        loggerFile.info("in findOneById freelancer by ID");
        const {id} = req.params;
        try {
            const freelancer = await Freelancer.findById(id);
            if (freelancer) {
                loggerFile.debug('get freelancer by id done');
                return await res.status(200).json({username: freelancer.username, role: freelancer.role});
            } else {
                loggerFile.debug('freelancer inéxistant');
                return res.status(200).json({message: "freelancer inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update freelancer');
        let id = req.auth._idConnected;
        const {token} = req.headers;
        const {email, confidentiality} = req.body
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFreelancer = req.body;
        try {
            adminService.updateSubscription(token, email, confidentiality);
            await Freelancer.findByIdAndUpdate(id, newFreelancer);
            loggerFile.debug('freelancer modifié')
            return res.status(200).json({message: "Freelancer modifié"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateByAdmin: async (req, res) => {
        loggerFile.info('in updateByAdmin freelancer');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFreelancer = req.body;
        try {
            await Freelancer.findByIdAndUpdate(id, newFreelancer);
            loggerFile.debug('freelancer modifié')
            return res.status(200).json({message: "Freelancer modifié"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete freelancer')
        const {id} = req.params;
        try {
            await Freelancer.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                    }
                    if (data.documents.length !== 0) {
                        await data.documents.forEach(async function callback(document, index) {
                            await cloudinary.uploader.destroy(data.cloudinary_doc_id);
                        });
                    }
                    await Freelancer.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseService.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    loggerFile.debug('Freelancer effacé')
                    return res.status(200).json({message: "Freelancer effacé"})
                } else {
                    loggerFile.debug('freelancer inéxistant')
                    return res.status(400).json({message: "Freelancer inéxistant"})
                }
            }).catch(error => {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let freelancer = await Freelancer.findById(id);
            if (freelancer && freelancer.code_verification === token) {
                await Freelancer.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(400).json({message: "l'utilisateur est inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentByAdmin: async (req, res) => {
        loggerFile.info("in validateDocumentByAdmin")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, true);
                    loggerFile.info("in send email after validate document")
                    await mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents du Freelancer validés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la validiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unValidateDocumentByAdmin: async (req, res) => {
        loggerFile.debug('in unValidateDocumentByAdmin');
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, false);
                    loggerFile.debug("unValidateDocumentByAdmin update done");
                    loggerFile.info("in send email after unvalidate document")
                    await mailService.unvalidateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents du Freelancer non validés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de l'invalidiation des documents"});
                }
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllVisibileAndValidated: async (req, res) => {
        loggerFile.info('in findAllVisibileAndValidated')
        try {
            const freelancers = await Freelancer.find({validated: true, visibility: 1});
            if (freelancers) {
                loggerFile.debug('get visible and validated freelancers data done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('freelancers inéxistants')
                return res.status(200).json({message: "freelancers inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateImageProfile: async (req, res) => {
        loggerFile.info('in updateImageProfile')
        let id = req.auth._idConnected;
        let new_image = "";
        let cloudinary_id = "";
        try {
            let user = await Freelancer.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_image != undefined)
                        await cloudinary.uploader.destroy(user.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement de l'image"})
                }
            } else {
                new_image = req.body.old_image;
            }
            user.image = await new_image;
            user.cloudinary_id = cloudinary_id;
            await Freelancer.findByIdAndUpdate(id, user);
            loggerFile.debug('image téléchargée avec succés')
            return res.status(200).json({message: "image téléchargée avec succés"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addFormation: async (req, res) => {
        loggerFile.info("in addFormation")
        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let formation = req.formation;
            await Freelancer.findByIdAndUpdate(id, {$push: {formations: formation._id}}, {new: true, upsert: true});
            loggerFile.debug('addFormation data done')
            return res.status(200).json({message: "Formation ajoutée"});

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFormation: async (req, res) => {
        loggerFile.info("in updateFormation")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {id} = req.params;
            await Formation.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateFormation data done')
                    return res.status(200).json({message: "Formation modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de la mise à jour de la formation"});
                }
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteFormation: async (req, res) => {
        loggerFile.info("in deleteFormation")
        try {
            let idFormation = req.params.id;
            await Formation.findByIdAndDelete(idFormation);
            await Freelancer.findByIdAndUpdate(req.auth._idConnected, {$pull: {formations: new ObjectId(idFormation)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteFormation done');
                    return res.status(200).json({message: "Formation supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addCertification: async (req, res) => {
        loggerFile.info("in addCertification")

        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let certification = req.certification;
            await Freelancer.findByIdAndUpdate(id, {$push: {certifications: certification._id}}, {
                new: true,
                upsert: true
            });
            loggerFile.debug('addCertification data done')
            return res.status(200).json({message: "Certificat ajouté"});

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateCertification: async (req, res) => {
        loggerFile.info("in updateCertification")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let {id} = req.params;
            await Certification.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateCertification data done')
                    return res.status(200).json({message: "Certification modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de la mise à jour de la certification"});
                }
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteCertification: async (req, res) => {
        loggerFile.info("in deleteCertification")
        try {
            let idCertification = req.params.id;
            await Certification.findByIdAndDelete(idCertification);
            await Freelancer.findByIdAndUpdate(req.auth._idConnected, {$pull: {certifications: new ObjectId(idCertification)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug("deleteCertification done")
                    return res.status(200).json({message: "Certification supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addExperience: async (req, res) => {
        loggerFile.info("in addExperience")

        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let experience = req.experience;
            await Freelancer.findByIdAndUpdate(id, {$push: {experiences: experience._id}}, {
                new: true,
                upsert: true
            });
            loggerFile.debug('addExperience data done')
            return res.status(200).json({message: "Expérience ajoutée"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateExperience: async (req, res) => {
        loggerFile.info("in updateExperience")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.params.id;
            await Experience.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateExperience data done')
                    return res.status(200).json({message: "Expérience modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de la mise à jour de l'éxpérience "});
                }
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteExperience: async (req, res) => {
        loggerFile.info("in deleteExperience")
        try {
            let idExperience = req.params.id;
            await Experience.findByIdAndDelete(idExperience);
            await Freelancer.findByIdAndUpdate(req.auth._idConnected, {$pull: {experiences: new ObjectId(idExperience)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteExperience done')
                    return res.status(200).json({message: "Expérience supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addLanguage: async (req, res) => {
        loggerFile.info("in addLanguage")
        try {
            if (req.test) {
                await Freelancer.findByIdAndUpdate(req.auth._idConnected, {$push: {languages: req.body.language}}, {
                    new: true,
                    upsert: true,
                    multi: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addLanguage data done');
                        return res.status(200).json({message: "Langue ajoutée"});
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                })
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateLanguage: async (req, res) => {
        loggerFile.info("in updateLanguage")

        try {
            let newFreelancer = await Freelancer.findById(req.auth._idConnected);
            let {name, level} = req.body.language;
            const index = newFreelancer.languages.findIndex((el) => el.name === name);
            newFreelancer.languages[index].level = level;

            await Freelancer.findByIdAndUpdate(req.auth._idConnected, newFreelancer, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('updateLanguage data done')
                    return res.status(200).json({message: "Langue modifiée"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            })
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteLanguage: async (req, res) => {
        loggerFile.info("in deleteLanguage")

        try {
            let newFreelancer = await Freelancer.findById(req.auth._idConnected);
            let {name} = req.body.language;
            const index = newFreelancer.languages.findIndex((el) => el.name === name);
            newFreelancer.languages.splice(index, 1);

            await Freelancer.findByIdAndUpdate(req.auth._idConnected, newFreelancer, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug("deleteLanguage done")
                    return res.status(200).json({message: "Langue supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            })
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addInterest: async (req, res) => {
        loggerFile.info("in addInterest")
        let {interest} = req.body;
        try {
            if (interest) {
                await Freelancer.findByIdAndUpdate(req.auth._idConnected, {$push: {interest: interest}}, {
                    new: true,
                    upsert: true,
                    multi: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addInterest data done');
                        return res.status(200).json({message: "Centre d'intérét ajouté"});
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                })
            } else {
                loggerFile.error('les  informations sont vide');
                return res.status(400).json({message: "les  informations sont vide"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteInterest: async (req, res) => {
        loggerFile.info("in deleteInterest")
        try {
            let newFreelancer = await Freelancer.findById(req.auth._idConnected);
            const index = newFreelancer.interest.findIndex((el) => el === req.body.interest);
            if (index != -1) {
                newFreelancer.interest.splice(index, 1);
                await Freelancer.findByIdAndUpdate(req.auth._idConnected, newFreelancer, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('deleteInterest done')
                        return res.status(200).json({message: "Centre d'intérét Effacé"});
                    }
                }).catch(err => {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                })
            } else {
                loggerFile.debug("centre d'intérét inéxistant");
                return res.status(400).json({message: "Centre d'intérét inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFreelancersWorkedIn: async (req, res) => {
        loggerFile.info("in getFreelancersWorkedIn")
        let array = req.body.worked_in;
        let size = array.length;
        let worked_in = [];
        try {
            if (size != 0) {
                await array.forEach(async function callback(idFreelance, index) {
                    let freelance = await Freelancer.findById(idFreelance);
                    let model = {
                        firstName: freelance.firstName,
                        lastName: freelance.lastName,
                        email: freelance.email,
                        phone: freelance.phone,
                        title_profile: freelance.title_profile
                    }
                    await worked_in.push(model);
                    if (size - 1 == index) {
                        return res.status(200).json(worked_in);
                    }
                });
            } else return res.status(200).json([]);
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFreelancersFavorite: async (req, res) => {
        loggerFile.info("in getFreelancersFavorite")

        let array = req.body.favorites;
        let size = array.length;
        let favorites = [];
        try {
            var itemsProcessed = 0;
            if (size != 0) {
                array.forEach(async function callback(idFreelance, index) {
                    await Freelancer.findById(idFreelance).then(async data => {
                        let model = {
                            _id: data._id,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            email: data.email,
                            phone: data.phone,
                            image: data.image,
                            title_profile: data.title_profile
                        }
                        await favorites.push(model);
                        itemsProcessed++;
                        if (itemsProcessed === size) {
                            return res.status(200).json(favorites);
                        }
                    }).catch(err => {
                        loggerFile.error(err);
                    });
                });

            } else return res.status(200).json([]);
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    countUsername: async (req, res) => {
        loggerFile.info("in countUsername")
        let {username} = req.body;
        try {
            await Freelancer.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("countUsername done")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    countEmail: async (req, res) => {
        loggerFile.info("in countEmail")
        let {email} = req.body;
        try {
            await Freelancer.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'email existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'email existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("countEmail done")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadKabisDocuments: async (req, res) => {
        loggerFile.info('in uploadKabisDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_kabis_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new kabis');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_kabis_id = result.public_id;
                        let newFreelancer = req.body;
                        newFreelancer.documents = freelancer.documents;
                        newFreelancer.documents[1] = docs[0]
                        newFreelancer.cloudinary_kabis_id = cloudinary_kabis_id;
                        newFreelancer.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFreelancer);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFreelance = req.body;
                newFreelance.documents = freelancer.documents;
                newFreelance.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFreelance);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadVigilanceDocuments: async (req, res) => {
        loggerFile.info('in uploadVigilanceDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_vigilance_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new vigilance');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_vigilance_id = result.public_id;
                        let newFreelance = req.body;
                        newFreelance.documents = freelancer.documents;
                        newFreelance.documents[2] = docs[0]
                        newFreelance.cloudinary_vigilance_id = cloudinary_vigilance_id;
                        newFreelance.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFreelance);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFreelance = req.body;
                newFreelance.documents = frelancer.documents;
                newFreelance.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFreelance);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadSasuDocuments: async (req, res) => {
        loggerFile.info('in uploadSasuDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_sasu_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_sasu_id = result.public_id;
                        let newFrelancer = req.body;
                        newFrelancer.documents = freelancer.documents;
                        newFrelancer.documents[3] = docs[0]
                        newFrelancer.cloudinary_sasu_id = cloudinary_sasu_id;
                        newFrelancer.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFrelancer);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFrelancer = req.body;
                newFrelancer.documents = freelancer.documents;
                newFrelancer.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFrelancer);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadDocuments: async (req, res) => {
        loggerFile.info('in uploadDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_doc_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_doc_id = result.public_id;
                        let newFrelancer = req.body;
                        newFrelancer.documents = freelancer.documents;
                        newFrelancer.documents[0] = docs[0]
                        newFrelancer.cloudinary_doc_id = cloudinary_doc_id;
                        newFrelancer.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFrelancer);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFrelancer = req.body;
                newFrelancer.documents = freelancer.documents;
                newFrelancer.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFrelancer);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getRequiredProfiles: async (req, res) => {
        let finalFreelancers = []
        let {title_profile, jobCat, level, skillsNeeded, skillsAppreciated, languages} = req.body;
        try {
            let freelancers = await Freelancer.find({
                title_profile: title_profile,
                jobCat: jobCat,
                level: level,
                visibility: 1,
                validated: true
            });
            let freelancersNeeded = []
            let freelancersAppreciated = []
            await freelancers.forEach(async function (freelance, index, array2) {
                let testLanguage = await missionService.testLanguage(freelance, languages);
                if (testLanguage) finalFreelancers.push(freelance)
            })

            await freelancers.forEach(async function (freelance, index, array) {
                let testSkillsNeeded = await missionService.testSkillsNeeded(freelance, skillsNeeded);
                if (testSkillsNeeded) freelancersNeeded.push(freelance)
            })
            await freelancers.forEach(async function (freelance, index, array2) {
                let testSkillsAppreciated = await missionService.testSkillsNeeded(freelance, skillsAppreciated);
                if (testSkillsAppreciated) freelancersAppreciated.push(freelance)
            })

            return res.status(200).send({
                freelancers: finalFreelancers.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        languages: el.languages
                    }
                }),
                freelancersNeeded: freelancersNeeded.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                }),
                freelancersAppreciated: freelancersAppreciated.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                })
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error})
        }
    }
}
