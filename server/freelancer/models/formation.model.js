const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let FormationSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    institute: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['en cours', 'terminé'],
        default: 'en cours',
        required: true
    },
    year: {
        type: String
    },
    description: {
        type: String
    }
}, {
    collection: 'formations',
    timestamps: true
})

const Formation = mongoose.model("Formation", FormationSchema);
module.exports = Formation;

