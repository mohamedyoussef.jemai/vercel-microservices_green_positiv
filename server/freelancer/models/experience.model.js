const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ExperienceSchema = new Schema({
    society: {
        type: String,
        required: true
    },
    domain: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    isFreelancer: {
        type: Boolean,
        required: true

    },
    place: {
        type: String,
        required: true

    },
    actuallyPost: {
        type: Boolean,
        required: true

    },
    dateBegin: {
        type: Date,
    },
    dateEnd: {
        type: Date,
    },
    description: {
        type: String,
    },
    skills: {
        type: [{
            type: String
        }],
    },
}, {
    collection: 'experiences',
    timestamps: true
})

const Experience = mongoose.model("Experience", ExperienceSchema);
module.exports = Experience;

