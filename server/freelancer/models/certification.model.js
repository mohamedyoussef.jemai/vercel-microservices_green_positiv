const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let CertificationSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    organism: {
        type: String,
        required: true
    },
    type: {
        type: String,
        enum: ['en cours', 'terminé'],
        default: 'en cours',
        required: true
    },
    year: {
        type: String
    },
    place: {
        type: String
    },
    description: {
        type: String
    }
}, {
    collection: 'certifications',
    timestamps: true
})

const Certification = mongoose.model("Certification", CertificationSchema);
module.exports = Certification;

