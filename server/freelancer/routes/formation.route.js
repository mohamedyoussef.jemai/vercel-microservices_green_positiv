const express = require('express');
const router = express.Router();
const formationController = require('../controllers/formationController');

router.get('/get/:id', formationController.findById);
router.patch('/:id', formationController.update);

module.exports = router;
