const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const freelancerController = require('../controllers/freelancerController');

const {
    middlewareValidityCreationFreelancer,
    middlewareJobExist,
    middlewareExistUsernameFreelancer,
    middlewareExistEmailFreelancer,
    middlewareSkillExist,
    middlewareTokenFreelancer,
    middlewareExistUsernameUpdateFreelancer,
    middlewareExistEmailUpdateFreelancer,
    middlewareValidityUpdateFreelancer,
    getSkills,
    getJobs,
    middlewareAddInterest
} = require('../middlewares/middlewareFreelancer');
const {
    addFormation, verifiyFormationFreelancer, getFormation
} = require('../middlewares/middlewareFormation');
const {
    addCertification, verifiyCertificationFreelancer, getCertification
} = require('../middlewares/middlewareCertification');
const {
    addExperience, verifiyExperienceFreelancer, verifySector, getExperiences
} = require('../middlewares/middlewareExperience');
const {
    verifyLanguage, LanguageExist
} = require('../middlewares/middlewareLanguage');

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        console.log(file.originalname)
        let ext = path.extname(file.originalname);
        console.log(ext)
        if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        console.log(req.validityFormat)
        cb(null, true)
    },
}).single("image");

let uploadDocuments = multer({
    storage: multer.diskStorage({}),
}).array("documents", 5);

router.get('/all', freelancerController.findAll);
router.get('/get/:id', getCertification, getFormation, getExperiences, getJobs, getSkills, freelancerController.findOne);
router.get('/find/:id', freelancerController.findOneById);
router.get('/check/:id/:token', freelancerController.checkEmail);
router.get('/', freelancerController.findAllVisibileAndValidated);
router.patch('/documents-validated/:id', freelancerController.validateDocumentByAdmin);
router.patch('/documents-unvalidated/:id', freelancerController.unValidateDocumentByAdmin);
router.post('/', middlewareExistUsernameFreelancer, middlewareExistEmailFreelancer, middlewareValidityCreationFreelancer, middlewareJobExist, freelancerController.create);
router.patch('/', middlewareTokenFreelancer, middlewareExistUsernameUpdateFreelancer, middlewareExistEmailUpdateFreelancer, middlewareValidityUpdateFreelancer, middlewareSkillExist, freelancerController.update);
router.patch('/upload-profile', middlewareTokenFreelancer, upload, freelancerController.updateImageProfile);
router.patch('/documents', middlewareTokenFreelancer, uploadDocuments, freelancerController.uploadDocuments);
router.patch('/kabis-documents', middlewareTokenFreelancer, uploadDocuments, freelancerController.uploadKabisDocuments);
router.patch('/vigilance-documents', middlewareTokenFreelancer, uploadDocuments, freelancerController.uploadVigilanceDocuments);
router.patch('/sasu-documents', middlewareTokenFreelancer, uploadDocuments, freelancerController.uploadSasuDocuments);
router.delete('/:id', freelancerController.delete);
router.post('/count-username', freelancerController.countUsername);
router.post('/count-email', freelancerController.countEmail);
//admin
router.patch('/update-admin/:id', middlewareValidityUpdateFreelancer, freelancerController.updateByAdmin);

//formations
router.post("/add-formation", middlewareTokenFreelancer, addFormation, freelancerController.addFormation);
router.patch("/update-formation/:id", middlewareTokenFreelancer, verifiyFormationFreelancer, freelancerController.updateFormation);
router.patch("/delete-formation/:id", middlewareTokenFreelancer, verifiyFormationFreelancer, freelancerController.deleteFormation);
//certifications
router.post("/add-certification", middlewareTokenFreelancer, addCertification, freelancerController.addCertification);
router.patch("/update-certification/:id", middlewareTokenFreelancer, verifiyCertificationFreelancer, freelancerController.updateCertification);
router.patch("/delete-certification/:id", middlewareTokenFreelancer, verifiyCertificationFreelancer, freelancerController.deleteCertification);
//experiences
router.post("/add-experience", middlewareTokenFreelancer, verifySector, addExperience, freelancerController.addExperience);
router.patch("/update-experience/:id", middlewareTokenFreelancer, verifiyExperienceFreelancer, verifySector, freelancerController.updateExperience);
router.patch("/delete-experience/:id", middlewareTokenFreelancer, verifiyExperienceFreelancer, freelancerController.deleteExperience);
//languages
router.post("/add-language", middlewareTokenFreelancer, LanguageExist, verifyLanguage, freelancerController.addLanguage);
router.patch("/update-language", middlewareTokenFreelancer, verifyLanguage, freelancerController.updateLanguage);
router.patch("/delete-language", middlewareTokenFreelancer, freelancerController.deleteLanguage);
//interest
router.patch("/add-interest", middlewareTokenFreelancer, middlewareAddInterest, freelancerController.addInterest);
router.patch("/delete-interest", middlewareTokenFreelancer, freelancerController.deleteInterest);
//company
router.post("/get-worked-in", freelancerController.getFreelancersWorkedIn);
router.post("/get-favorites", freelancerController.getFreelancersFavorite);


router.post("/required-profile", freelancerController.getRequiredProfiles);

module.exports = router;
