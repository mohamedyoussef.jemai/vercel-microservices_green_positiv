const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationController');


router.patch('/check/:id', notificationController.checkNotification);
router.get('/', notificationController.getAllNotification);
router.get('/checked', notificationController.getAllCheckedNotification);
router.get('/unchecked', notificationController.getAllUnCheckedNotification);
router.post('/', notificationController.getAllNotificationByPeriod);
router.post('/checked', notificationController.getAllCheckedNotificationByPeriod);
router.post('/unchecked', notificationController.getAllUnCheckedNotificationByPeriod);

module.exports = router;
