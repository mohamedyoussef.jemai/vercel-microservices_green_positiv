const express = require('express');
const router = express.Router();
const freelancerController = require('../../controllers/test/freelancerControllerTest');

const {
    addFormation
} = require('../../middlewares/middlewareFormation');
const {
    addCertification,
} = require('../../middlewares/middlewareCertification');
const {
    addExperience
} = require('../../middlewares/middlewareExperience');

router.get('/all', freelancerController.findAll);
router.get('/get/:id', freelancerController.findOne);
router.get('/find/:id', freelancerController.findOneById);
router.delete('/:id', freelancerController.delete);
router.get('/check/:id/:token', freelancerController.checkEmail);

router.patch('/block/:id', freelancerController.blockFreelancer);
router.patch('/unblock/:id', freelancerController.unblockFreelancer);

router.post('/', freelancerController.create);
router.patch('/', freelancerController.update);
router.delete('/:id', freelancerController.delete);
router.post('/count-username', freelancerController.countUsername);
router.post('/count-email', freelancerController.countEmail);

//formations
router.post("/add-formation", addFormation, freelancerController.addFormation);
router.patch("/update-formation/:id", freelancerController.updateFormation);
router.patch("/delete-formation/:id", freelancerController.deleteFormation);
//certifications
router.post("/add-certification", addCertification, freelancerController.addCertification);
router.patch("/update-certification/:id", freelancerController.updateCertification);
router.patch("/delete-certification/:id", freelancerController.deleteCertification);
//experiences
router.post("/add-experience", addExperience, freelancerController.addExperience);
router.patch("/update-experience/:id", freelancerController.updateExperience);
router.patch("/delete-experience/:id", freelancerController.deleteExperience);
//languages
router.post("/add-language", freelancerController.addLanguage);
router.patch("/update-language", freelancerController.updateLanguage);
router.patch("/delete-language", freelancerController.deleteLanguage);
//get skills
router.post("/skills", freelancerController.getSkills);
//interest
router.patch("/add-interest", freelancerController.addInterest);
router.patch("/delete-interest", freelancerController.deleteInterest);

//get all
router.get('/formations', freelancerController.getFormations);
router.get('/certifications', freelancerController.getCertificaiton);
router.get('/experiences', freelancerController.getExperiences);

module.exports = router;
