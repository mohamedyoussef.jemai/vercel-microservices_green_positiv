const express = require('express');
const router = express.Router();
const certificationController = require('../controllers/certificationController');

router.get('/get/:id', certificationController.findById);
router.patch('/:id', certificationController.update);

module.exports = router;
