const express = require('express');
const router = express.Router();
const experienceController = require('../controllers/experienceController');

router.get('/get/:id', experienceController.findById);
router.patch('/:id', experienceController.update);

module.exports = router;
