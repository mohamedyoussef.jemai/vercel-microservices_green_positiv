require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/skills.model');
require('./models/freelancer.model');
require('./models/certification.model');
require('./models/formation.model');
require('./models/experience.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("uploads"));

app.use(logger('combined'));

mongoose.connect(process.env.DB_URI_FREELANCER_LOCAL_PROD, {
    useUnifiedTopology: true,
}).then(() => {
    app.listen(port, () => {
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.info(err);
});

let notificationRouter = require('./routes/notification.route');
let freelanderRouter = require('./routes/freelancer.route');
let certificationRouter = require('./routes/certification.route');
let formationRouter = require('./routes/formation.route');
let experienceRouter = require('./routes/experience.route');

app.use('/notification', notificationRouter);
app.use('/freelancer', freelanderRouter);
app.use('/certification', certificationRouter);
app.use('/formation', formationRouter);
app.use('/experience', experienceRouter);

module.exports = app;
