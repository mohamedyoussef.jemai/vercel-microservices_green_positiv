exports.getClientAddress = (req, err) => {
    let ip = (req.headers['x-forwarded-for'] || '').split(',')[0]
        || req.connection.remoteAddress;
    let userAgent = req.get('user-agent');
    let url = req.hostname + req.url
    let body = req.body
    let headers = req.headers
    return {ip: ip, userAgent: userAgent, url: url, headers: headers, body: body, err: err}
};
exports.getClientIp = (req) => {
    let ipAddress = req.connection.remoteAddress;
    if (!ipAddress) {
        return '';
    }
    if (ipAddress.substr(0, 7) == "::ffff:") {
        ipAddress = ipAddress.substr(7)
    }
    return ipAddress;
};
