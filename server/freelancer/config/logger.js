const {createLogger, transports, format} = require('winston');
require('dotenv/config')
require('winston-mongodb')

const logger = createLogger({
    transports: [
        new transports.File({
            filename: 'logger_dev',
            level: 'info',
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.File({
            filename: 'logger_dev',
            level: 'debug',
            format: format.combine(format.timestamp(), format.json())
        }),
        new transports.MongoDB({
            db: process.env.DB_URI_FREELANCER_LOCAL_PROD,
            level: 'error',
            collection: 'logs',
            format: format.combine(format.timestamp(), format.metadata()),
        }),
    ]
})

module.exports = logger;
