//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Freelance = require('../models/freelancer.model');
let Formation = require('../models/formation.model');
let Certification = require('../models/certification.model');
let Experience = require('../models/experience.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

let idFreelancer = "";
let code_verification = "";
let idFormation = "";
let idCertification = "";
let idExperience = "";

chai.use(chaiHttp);
//Our parent block
describe('Freelances', () => {
    before((done) => { //Before each test we empty the database
        try {
            Formation.deleteMany();
            Certification.deleteMany();
            Experience.deleteMany();
            Freelance.remove({}, async (err) => {
                done();
            });
        } catch (err) {
            console.log("error before ", err.message)
        }

    });
    /*
     * Test Step 1 : Creation account
    */
    describe('Step 1 : Creation & Login', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all freelancers', () => {
            it('it should GET all the freelancers', (done) => {
                chai.request(server)
                    .get('/freelancer/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Freelance
        */
        describe('/POST create freelance', () => {
            it('it should POST a freelance', (done) => {
                let data = {
                    username: "freelancer",
                    email: "youssefjoejemai16@gmail.com",
                    lastName: "jemai",
                    firstName: "youssef",
                    phone: "2016247033",
                    password: "azerty123",
                    repeatPassword: "azerty123",
                    jobCat: "621ff61d3ee6075281fccd83",
                    title_profile: "Développeur Vuejs",
                    localisation: "Tunisie, Tunis",
                    level: "Senior",
                    price_per_day: 250,
                    show_price: false,
                    confidentiality: false
                }
                chai.request(server)
                    .post('/freelancer')
                    .set('content-type', 'application/json')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Freelancer Créé');
                    });
                done();
            });
        });
    });
    /*
         * Test Step 2 : update profil
        */
    describe('Step 2 : update profile freelance', () => {
        /*
         * Test the /GET id freelancer
        */
        describe('/GET id freelancer', () => {
            it('it should GET all the freelancers', (done) => {
                chai.request(server)
                    .get('/freelancer/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idFreelancer = res.body[0]._id;
                        console.log("idFreelancer !! ", idFreelancer)
                        code_verification = res.body[0].code_verification;
                        /*
                         * Test the /PATCH route UPDATE Freelance
                        */
                        describe('/PATCH UPDATE freelance', () => {
                            it('it should update a freelance', (done2) => {
                                let data = {
                                    lastName: "test2",
                                    firstName: "test2",
                                    phone: "1234567890",
                                    title_profile: "Développeur Vuejs 2",
                                    localisation: "Tunisie, Tunis 2",
                                    signed_client: true,
                                    greenQuestion: "test",
                                    description: "test",
                                    id: idFreelancer
                                }
                                chai.request(server)
                                    .patch('/freelancer')
                                    .send(data)
                                    .end((err, res) => {
                                        res.should.have.status(200);
                                        res.body.should.have.property('message');
                                        res.body.message.should.be.eql('Freelancer modifié');
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });
        /*
         * Test the /check email
        */
        describe('/GET CHECK EMAIL freelance', () => {
            it('it should check email of a freelance', (done) => {
                chai.request(server)
                    .get(`/freelancer/check/${idFreelancer}/${code_verification}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Email vérifié');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : crud formation
    */
    describe('Step 3 : crud formation freelance', () => {
        /*
         * Test the /post FORMATION freelancer
        */
        describe('/POST FORMATION id freelancer', () => {
            it('it should POST FORMATION the freelancers', (done) => {
                let data = {
                    name: "licence STIC",
                    institute: "Isetcom",
                    type: "en cours",
                    year: "",
                    description: "test",
                    id: idFreelancer
                }
                chai.request(server)
                    .post('/freelancer/add-formation')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Formation ajoutée");
                        done();
                    });
            });
        });
        /*
         * Test the /get all formations
        */
        describe('/GET all formations by id', () => {
            it('it should GET formations by id', (done) => {
                chai.request(server)
                    .get('/freelancer/formations')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idFormation = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /patch FORMATION freelancer
        */
        describe('/PATCH FORMATION id freelancer', () => {
            it('it should PATCH FORMATION the freelancers', (done) => {
                let data = {
                    name: "licence STIC",
                    institute: "Isetcom",
                    type: "terminé",
                    year: "2019",
                    description: "test 2",
                    id: idFreelancer
                }
                chai.request(server)
                    .patch('/freelancer/update-formation/' + idFormation)
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Formation modifiée");
                        done();
                    });
            });
        });
        /*
         * Test the /delete FORMATION freelancer
        */
        describe('/DELETE FORMATION id freelancer', () => {
            it('it should DELETE FORMATION the freelancers', (done) => {

                chai.request(server)
                    .patch('/freelancer/delete-formation/' + idFormation)
                    .send({id: idFreelancer})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Formation supprimée");
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : crud certification
    */
    describe('Step 4 : crud certification freelance', () => {
        /*
         * Test the /post certification freelancer
        */
        describe('/POST certification id freelancer', () => {
            it('it should POST certification the freelancers', (done) => {
                let data = {
                    name: "java developer",
                    organism: " centre 1",
                    type: "en cours",
                    year: "",
                    description: "java basics",
                    place: "Tunis",
                    id: idFreelancer
                }
                chai.request(server)
                    .post('/freelancer/add-certification')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Certificat ajouté");
                        done();
                    });
            });
        });
        /*
         * Test the /get all certificaitons
        */
        describe('/GET all certificaiton by id', () => {
            it('it should GET certifications by id', (done) => {
                chai.request(server)
                    .get('/freelancer/certifications')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idCertification = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /patch certification freelancer
        */
        describe('/PATCH certification id freelancer', () => {
            it('it should PATCH certification the freelancers', (done) => {
                let data = {
                    name: "java developer",
                    organism: " centre 2",
                    type: "terminé",
                    year: "2020",
                    description: "java basics",
                    place: "Tunis",
                    id: idFreelancer
                }
                chai.request(server)
                    .patch('/freelancer/update-certification/' + idCertification)
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Certification modifiée");
                        done();
                    });
            });
        });
        /*
         * Test the /delete certification freelancer
        */
        describe('/DELETE certification id freelancer', () => {
            it('it should DELETE certification the freelancers', (done) => {
                chai.request(server)
                    .patch('/freelancer/delete-certification/' + idCertification)
                    .send({id: idFreelancer})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Certification supprimée");
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 5 : crud experience
    */
    describe('Step 5 : crud experience freelance', () => {
        /*
         * Test the /post experience freelancer
        */
        describe('/POST experience id freelancer', () => {
            it('it should POST experience the freelancers', (done) => {
                let data = {
                    society: "eb elite",
                    domain: "IOT",
                    title: "developor backend",
                    isFreelancer: true,
                    place: "Tunis",
                    actuallyPost: false,
                    dateBegin: "2020-01-15",
                    dateEnd: "2020-01-16",
                    description: "hello world",
                    skills: ["skill 1 ", "skill 3", "skill 5"],
                    id: idFreelancer
                }
                chai.request(server)
                    .post('/freelancer/add-experience')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Expérience ajoutée");
                        done();
                    });
            });
        });
        /*
         * Test the /get all experiences
        */
        describe('/GET all experiences by id', () => {
            it('it should GET Experiences by id', (done) => {
                chai.request(server)
                    .get('/freelancer/experiences')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idExperience = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /patch experience freelancer
        */
        describe('/PATCH experience id freelancer', () => {
            it('it should PATCH experience the freelancers', (done) => {
                let data = {
                    society: "eb elite 2",
                    domain: "IOT",
                    title: "developor backend 2",
                    isFreelancer: true,
                    place: "Tunis",
                    actuallyPost: false,
                    dateBegin: "2020-01-15",
                    dateEnd: "2020-01-16",
                    description: "hello world 2",
                    skills: ["skill 1 ", "skill 3"],
                    id: idFreelancer
                }
                chai.request(server)
                    .patch('/freelancer/update-experience/' + idExperience)
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Expérience modifiée");
                        done();
                    });
            });
        });
        /*
         * Test the /delete experience freelancer
        */
        describe('/DELETE experience id freelancer', () => {
            it('it should DELETE experience the freelancers', (done) => {

                chai.request(server)
                    .patch('/freelancer/delete-experience/' + idExperience)
                    .send({id: idFreelancer})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Expérience supprimée");
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 6 : crud languages
    */
    describe('Step 6 : add languages & crud interest freelance', () => {
        /*
         * Test the /post language
        */
        describe('/POST language id freelancer', () => {
            it('it should POST language the freelancers', (done) => {
                let data = {language: {name: "Manx", level: "BASIC"}, id: idFreelancer}
                chai.request(server)
                    .post('/freelancer/add-language')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Langue ajoutée");
                    });
                done();
            });
        });
        /*
         * Test the /patch interest add
        */
        describe('/PATCH add interest id freelancer', () => {
            it('it should PATCH interest the freelancers', (done) => {
                let data = {interest: "interest", id: idFreelancer};

                chai.request(server)
                    .patch('/freelancer/add-interest')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Centre d'intérét ajouté");
                        done();
                    });
            });
        });
        /*
        * Test the /patch interest remove
       */
        describe('/PATCH remove interest id freelancer', () => {
            it('it should PATCH interest the freelancers', (done) => {
                let data = {interest: "interest", id: idFreelancer};

                chai.request(server)
                    .patch('/freelancer/delete-interest')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Centre d'intérét Effacé");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 7 : find one Freelance and delete
   */
    describe('Step 7 : find one Freelance and delete', () => {
        /*
         * Test the /get one freelance with id
        */
        describe('/GET one freelance with id', () => {
            it('it should get one freelance by id', (done) => {
                chai.request(server)
                    .get(`/freelancer/find/${idFreelancer}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        /*
        * Test the /DELETE freelance by id
       */
        describe('/DELETE freelance by id', () => {
            it('it should delete freelance by id', (done) => {
                chai.request(server)
                    .delete(`/freelancer/${idFreelancer}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message')
                        res.body.message.should.be.eql('Freelance effacé')
                        done();
                    });
            });
        });
    });
});
