//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Company = require('../models/company.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();

let idFreelancer = "6221560dbfc2abfd35f9d2a9";
let idCompany = "";


chai.use(chaiHttp);
//Our parent block
describe('Companies', () => {
    before((done) => { //Before each test we empty the database
        Company.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation account
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all companies', () => {
            it('it should GET all the companies', (done) => {
                chai.request(server)
                    .get('/company/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Company
        */
        describe('/POST create Company', () => {
            it('it should POST a Company', (done) => {
                let data = {
                    username: "testcompany",
                    email: "testcompany@gmail.com",
                    lastName: "company",
                    firstName: "company",
                    phone: "2016247033",
                    password: "azerty123",
                    repeatPassword: "azerty123",
                    name: "albero",
                    size: 0,
                    departement: "RH",
                    searchable: "alone"
                }
                chai.request(server)
                    .post('/company')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Entreprise Créée');
                        done();
                    });
            });
        });

    });
    /*
     * Test Step 2 : update profil
    */
    describe('Step 2 : update profile company', () => {
        /*
        * Test the /GET id company
       */
        describe('/GET id company', () => {
            it('it should GET all the companies', (done) => {
                chai.request(server)
                    .get('/company/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idCompany = res.body[0]._id;
                        code_verification = res.body[0].code_verification;
                        /*
                         * Test the /PATCH route UPDATE company
                        */
                        describe('/PATCH UPDATE company', () => {
                            it('it should update a company', (done2) => {
                                let data = {
                                    lastName: "test2",
                                    firstName: "test2",
                                    phone: "1234567890",
                                    localisation: "Tunisie, Tunis 2",
                                    id: idCompany
                                }
                                chai.request(server)
                                    .patch('/company')
                                    .send(data)
                                    .end((err, res) => {
                                        res.should.have.status(200);
                                        res.body.should.have.property('message');
                                        res.body.message.should.be.eql('Entreprise modifiée');
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : add and remove favoris
    */
    describe('Step 3 : add and remove favoris', () => {
        /*
         * Test the /patch add favoris freelancer
        */
        describe('/PATCH add favoris ', () => {
            it('it should  add freelancer to favorites list', (done) => {
                chai.request(server)
                    .patch('/company/add-favoris-freelance/' + idFreelancer)
                    .send({id: idCompany})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("ajout du freelance à la liste des favoris effectué");
                        done();
                    });
            });
        });
        /*
         * Test the /patch remove favoris freelancer
        */
        describe('/PATCH add favoris ', () => {
            it('it should remove freelancer to favorites list', (done) => {
                chai.request(server)
                    .patch('/company/remove-favoris-freelance/' + idFreelancer)
                    .send({id: idCompany})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("suppression du freelance à la liste des favoris effectuée");
                        done();
                    });
            });
        });
        /*
         * Test Step 4 : collaborator module
        */
        describe('Step 4 : collaborator module', () => {
            /*
             * Test the /post collab
            */
            describe('/POST create collab', () => {
                it('it should POST create collab', (done) => {
                    let data = {
                        username: "collab2",
                        email: "collab2@gmail.com",
                        password: "azerty123",
                        departement: "RH",
                        id: idCompany
                    }
                    chai.request(server)
                        .post('/company/create-collab')
                        .send(data)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.have.property('message');
                            done();
                        });
                });
            });
        });
    });
    /*
     * Test Step 5 : find one Company and delete
    */
    describe('Step 5 : find one Company and delete', () => {
        /*
         * Test the /get one Company with id
        */
        describe('/GET one Company with id', () => {
            it('it should get one Company by id', (done) => {
                chai.request(server)
                    .get(`/company/find/${idCompany}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        /*
        * Test the /DELETE Company by id
       */
        describe('/DELETE Company by id', () => {
            it('it should delete Company by id', (done) => {
                chai.request(server)
                    .delete(`/company/${idCompany}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message')
                        res.body.message.should.be.eql('Entreprise effacée')
                        done();
                    });
            });
        });
    });
});
