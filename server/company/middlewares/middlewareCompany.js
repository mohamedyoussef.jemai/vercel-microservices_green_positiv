require('dotenv/config')
const jwt = require('jsonwebtoken');
const axios = require('axios').default;
const utils = require('../config/utils');

const loggerFile = require("../config/logger");

const Company = require("../models/company.model");
const departements = ['IT', 'MARKETING', 'ADMINISTRATION', 'PURCHASING', 'ACCOUNTING', 'RH', 'OPERATION', 'SALES', 'MANAGEMENT', 'PRODUCT', 'COMMUNICATION'];

module.exports = {
    middlewareValidityCreationCompany: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationCompany")
        const {
            lastName,
            firstName,
            phone,
            departement,
        } = req.body;
        try {
            if (lastName && typeof lastName !== "string") {
                loggerFile.debug("creation Company data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName && typeof lastName !== "string") {
                loggerFile.debug("creation Company data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (phone && (!/^\d+$/.test(phone) || phone.length !== 10)) {
                loggerFile.debug("creation Company data : problem in phone")
                return res.status(400).send({message: "le champ téléphone doit contenir que 10 chiffres"});
            }
            if (departement && (!departements.includes(departement) || typeof departement !== "string")) {
                loggerFile.debug("creation Company data : problem in departement")
                return res.status(400).send({message: "le département n'existe pas"});
            } else {
                loggerFile.debug('validate creationCompany done')
                return next();
            }
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: "réessayez l'action"});
        }
    },
    middlewareExistUsernameCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameCompany")
        let {username} = req.body;
        try {
            await Company.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsernameCompany done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailCompany")
        let {email} = req.body;
        try {
            await Company.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else return next();
            }).catch(err => {
                loggerFile.error(utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenCompany: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompany")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Company.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Company") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenCollab: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompany")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Company.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Collab") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenCompanyAndCollab: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompanyAndCollab")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Company.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && (user.role === "Company" || user.role === "Collab")) {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            loggerFile.error("accés non autorisé")
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareExistUsernameUpdateCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameUpdateCompany")

        let {username} = req.body;
        if (username === req.auth.usernameConnected) {
            loggerFile.debug('verify username equal usernameConnected')
            return next()
        } else {
            try {
                await Company.countDocuments({username: username}).then(count => {
                    if (count >= 1) {
                        loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                        return res.status(401).json({
                            message: "l'utilisateur existe, essayez un autre identifiant"
                        });
                    } else {
                        loggerFile.debug("verify username exist update Company done")
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(401).json({
                        message: err.message
                    });
                });
            } catch (error) {
                loggerFile.error(utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareExistEmailUpdateCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailUpdateCompany")

        let {email} = req.body;
        if (email === req.auth.emailConnected) {
            loggerFile.debug('verify email equal emailConnected')
            return next()
        } else {
            try {
                await Company.countDocuments({email: email}).then(count => {
                    if (count >= 1) {
                        loggerFile.debug("cette adresse mail existe, essayez une autre")
                        return res.status(401).json({
                            message: "cette adresse mail existe, essayez une autre"
                        });
                    } else {
                        loggerFile.debug("verify username exist update Company done")
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(401).json({
                        message: err.message
                    });
                });
            } catch (error) {
                loggerFile.error(utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareCompanyExist: async (req, res, next) => {
        loggerFile.info("in middlewareCompanyExist")

        try {
            let company = await Company.findById(req.auth._idConnected);
            if (company) {
                loggerFile.debug("middlewareCompanyExist done")
                return next();
            } else {
                loggerFile.debug("company not found")
                return res.status(400).json({message: "company introuvable"})

            }
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    ExistParamCompany: async (req, res, next) => {
        loggerFile.info("in ExistParamCompany")
        try {
            let company = await Company.findById(req.params.id);
            if (company) {
                loggerFile.debug("company trouvé")
                return next();
            } else {
                loggerFile.debug("company introuvable")
                return res.status(400).json({message: "entreprise introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateFacturationDetails: async (req, res, next) => {
        loggerFile.info("in validateFacturationDetails")
        try {
            let {social_reason, siret, tva_intracom, address, address_plus} = req.body;
            if (!social_reason || typeof social_reason !== "string") {
                loggerFile.debug("social_reason non conforme")
                return res.status(400).json({message: "social_reason non conforme"});
            }
            if (!address || typeof address !== "string") {
                loggerFile.debug("address non conforme")
                return res.status(400).json({message: "address non conforme"});
            }
            if (!address_plus || typeof address_plus !== "string") {
                loggerFile.debug("address_plus non conforme")
                return res.status(400).json({message: "address_plus non conforme"});
            }
            if (!siret || typeof siret !== "string" || siret.length !== 14 || !/^\d+$/.test(siret)) {
                loggerFile.debug("siret non conforme")
                return res.status(400).json({message: "siret non conforme"});
            }
            let tva_numbers = tva_intracom.slice(2, tva_intracom.length);
            if (!tva_intracom || typeof tva_intracom !== "string" || !tva_intracom[0].match("F") || !tva_intracom[1].match("R") || !/^\d+$/.test(tva_numbers)) {
                loggerFile.debug("tva_intracom non conforme")
                return res.status(400).json({message: "tva_intracom non conforme"});
            } else {
                loggerFile.debug("validateFacturationDetails done")
                next();
            }
        } catch
            (err) {
            loggerFile.error(utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    verifFreelanceExist: async (req, res, next) => {
        loggerFile.info("in verifFreelanceExist")
        try {
            let {id} = req.params;
            await axios.get(`${process.env.URL_FREELANCER}/freelancer/get/${id}`).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('verifFreelanceExist');
                    next();
                } else {
                    loggerFile.debug('problem in get admin jobs ');
                    return res.status(400).send({message: "Freelance inéxistant"});
                }
            }).catch(function (err) {
                loggerFile.error(utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifyFavoriteFreelanceExist: async (req, res, next) => {
        loggerFile.info("in verifyFavoriteFreelanceExist")
        try {
            let id = req.auth._idConnected;
            let idFreelance = req.params.id
            let company = await Company.findById(id);
            if (company.favorites.includes(idFreelance)) {
                return res.status(400).json({message: "Freelancer déja existant dans la liste des favoris"})
            } else {
                next()
            }
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifyAccessCollab: async (req, res, next) => {
        loggerFile.info("in verifyAccessCollab")
        let {id} = req.params;
        try {
            let company = await Company.findById(id);
            if (company.active_collab) {
                loggerFile.debug("in verifyAccessCollab done")
                next();
            } else {
                loggerFile.debug("in verifyAccessCollab refuse done")
                return res.status(400).json({message: "lien de partage inactif"})
            }
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareValidityCollaborator: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCollaborator")
        const {
            username,
            email,
            password,
            departement,
            lastName,
            firstName
        } = req.body;
        try {
            if (username.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in username")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (email.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in email")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (lastName.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (password.trim().length === 0 || password.length < 8) {
                loggerFile.debug("creation Company data : problem in password")
                return res.status(400).send({message: "le mot de passe est erroné"});
            }
            if (!departements.includes(departement) || departement.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in departement")
                return res.status(400).send({message: "le département n'éxiste pas"});
            } else {
                loggerFile.debug('validate creationCollaborator done')
                return next();
            }
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: "réessayez l'action"});
        }
    },
    getWorkedInFreelancers: async (req, res, next) => {
        loggerFile.info("in getWorkedInFreelancers")
        try {
            let {id} = req.params;
            let company = await Company.findById(id);
            await axios.post(`${process.env.URL_FREELANCER}/freelancer/get-worked-in`, {
                worked_in: company.worked_in
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('getWorkedInFreelancers done');
                    req.worked_in = response.data;
                    next();
                } else {

                    return res.status(400).json({message: response.message});
                }
            }).catch(function (err) {
                loggerFile.error(utils.getClientAddress(req, err))
            });
        } catch
            (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFavoriteFreelancers: async (req, res, next) => {
        loggerFile.info("in getFavoriteFreelancers")
        try {
            let {id} = req.params;
            let company = await Company.findById(id);
            await axios.post(`${process.env.URL_FREELANCER}/freelancer/get-favorites`, {
                favorites: company.favorites
            }).then(async function (response) {
                if (response.status == 200) {
                    loggerFile.debug('getFavoriteFreelancers done');
                    req.favorites = await response.data;
                    next();
                } else {
                    return res.status(400).json({message: response.message});
                }
            }).catch(function (err) {
                loggerFile.error(utils.getClientAddress(req, err))
            });
        } catch
            (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getCollabFavoriteList: async (req, res, next) => {
        loggerFile.info("in getFavoriteFreelancers")
        let id = req.auth._idConnected;
        try {
            const collab = await Company.findById(id);
            if (collab.company_collaborator[0]) {
                const company = await Company.findById(collab.company_collaborator[0])
                await axios.post(`${process.env.URL_FREELANCER}/freelancer/get-favorites`, {
                    favorites: company.favorites,
                }).then(function (response) {
                    if (response.status == 200) {
                        loggerFile.debug('getFavoriteFreelancers done');
                        req.favorites = response.data;
                        next();
                    } else {
                        return res.status(400).json({message: response.data.message});
                    }
                }).catch(function (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                });
            } else {
                return res.status(200).json({message: "le collaborateur doit être validé avant"});
            }

        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}

