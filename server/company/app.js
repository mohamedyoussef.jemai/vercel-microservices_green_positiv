require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/company.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("uploads"));

app.use(logger('combined'));

let companyRouter = require('./routes/company.route');

app.use('/company', companyRouter);

mongoose.connect(process.env.DB_URI_COMPANY_LOCAL_PROD, {
    useUnifiedTopology: true,
}).then(() => {
    app.listen(port, () => {
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.info(err);
});

module.exports = app;
