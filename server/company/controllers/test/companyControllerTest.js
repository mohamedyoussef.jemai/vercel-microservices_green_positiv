require('dotenv').config();
const crypto = require('crypto');
const fs = require('fs');
const loggerFile = require("../../config/logger");
const Company = require('../../models/company.model');
const notificationService = require("../../services/notificationService");
const profilEntrepriseService = require("../../services/profilEntrepriseService");
const authService = require("../../services/authService");
const mailService = require("../../services/mailingService");
module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create company');
        const {
            username,
            email,
            lastName,
            firstName,
            phone,
            password,
            name,
            size,
            departement,
            searchable
        } = req.body;
        try {
            const company = new Company({
                username,
                email,
                lastName,
                firstName,
                phone,
                departement,
                searchable
            });
            company.role = "Company";
            company.code_verification = crypto.randomBytes(20).toString('hex');

            await Company.create(company);
            return res.status(200).json({message: "Entreprise Créée"});

        } catch (error) {
            loggerFile.error(error);
            res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll companies");
        try {
            const companies = await Company.find({role: "Company"});
            if (companies) {
                loggerFile.debug('get all companies done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('aucun companies éxistants')
                return res.status(200).json({message: "Entreprises inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update company');
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newCompany = req.body;
        try {
            await Company.findByIdAndUpdate(id, newCompany);
            loggerFile.debug('company modifié')
            return res.status(200).json({message: "Entreprise modifiée"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    //verif account via a code sended in creation account
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let company = await Company.findById(id);
            if (company && company.code_verification === token) {
                await Company.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err.message);
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(200).json({message: "l'utilisateur est inéxistant"});
            }

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    //upload image profile or replace old image
    createCollaborator: async (req, res) => {
        loggerFile.debug('in createCollaborator');
        let {id} = req.body;
        let {username, departement, email, password} = req.body;
        try {
            let collab = new Company({
                username,
                email,
                departement,
            });
            collab.role = "Collab";
            collab.validated = false;
            await Company.create(collab).then(async data => {
                if (data) {
                    await Company.findByIdAndUpdate(id, {$push: {worked_with: data._id}}, {
                        new: true,
                        upsert: true
                    });
                    return res.status(200).json({
                        message: "collaborateur créé",
                        url: `${process.env.URL_COMPANY}/company/add-collab/${id}/${data._id}`
                    });
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "un problème est survenu lors de la création d'un collaborateur"});
                }
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    //add a freelance to favorite
    addFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in addFavoriteFreelance');
        let {id} = req.body;
        let idFreelance = req.params.id;
        try {
            await Company.findByIdAndUpdate(id, {$push: {favorites: idFreelance}}, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('addFavoriteFreelance done');
                    return res.status(200).json({message: "ajout du freelance à la liste des favoris effectué"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "un problème est survenu lors de l'ajout du freelance de la liste des favoris"});
                }
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    //remove a freelance to favorite
    removeFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in removeFavoriteFreelance');
        let {id} = req.body;
        let idFreelance = req.params.id;
        try {
            await Company.findByIdAndUpdate(id, {$pull: {favorites: idFreelance}}, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('removeFavoriteFreelance done');
                    return res.status(200).json({message: "suppression du freelance à la liste des favoris effectuée"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "un problème est survenu lors de la suppression du freelance de la liste des favoris"});
                }
            })
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    findOneById: async (req, res) => {
        loggerFile.info("in findOneById company by ID");
        const {id} = req.params;
        try {
            const company = await Company.findById(id);
            if (company) {
                loggerFile.debug('get company by id done');
                return await res.status(200).json({username: company.username, role: company.role});
            } else {
                loggerFile.debug('company inéxistant');
                return res.status(200).json({message: "entreprise inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "entreprise inéxistante"});
            } else {
                loggerFile.error(error);
                return res.status(400).json({message: error});
            }
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete company')
        const {id} = req.params;
        try {
            await Company.findByIdAndDelete(id).then(data => {
                if (data) {
                    return res.status(200).json({message: "Entreprise effacée"})
                }
            }).catch(err => {
                loggerFile.error(error)
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    }
}

