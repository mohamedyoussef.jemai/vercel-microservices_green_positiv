require('dotenv').config();
const crypto = require('crypto');
const loggerFile = require("../config/logger");
const Company = require('../models/company.model');
const Mission = require('../models/mission.model');
const requiredProfil = require('../models/requiredProfil.model');

const notificationService = require("../services/notificationService");
const profilEntrepriseService = require("../services/profilEntrepriseService");

const authService = require("../services/authService");
const mailService = require("../services/mailingService");
const adminService = require("../services/adminService");
const cloudinary = require('../config/cloudinary');
const ObjectId = require('mongoose').Types.ObjectId;
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create company');
        const {
            username,
            email,
            lastName,
            firstName,
            phone,
            password,
            name,
            size,
            departement,
            searchable
        } = req.body;
        try {
            let testAuthCreation = await authService.validateCreation({
                username: username,
                email: email
            });
            if (!testAuthCreation.state && testAuthCreation.message != "done") {
                return res.status(400).json({message: testAuthCreation.message});
            } else {
                const company = new Company({
                    username,
                    email,
                    lastName,
                    firstName,
                    phone,
                    departement,
                    searchable
                });
                company.role = "Company";
                company.code_verification = crypto.randomBytes(20).toString('hex');
                company.validated = false;
                let companyCreated = await Company.create(company);
                if (companyCreated) {
                    try {
                        let data = {
                            name: name,
                            size: size,
                            id_company: companyCreated._id
                        }
                        let testProfil = await profilEntrepriseService.create(data);
                        if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de l'ajout du profil"});

                        //create user
                        let testAuth = await authService.create({
                            username: username,
                            email: email,
                            password: password,
                            idUser: companyCreated._id,
                            role: "Company"
                        });
                        mailService.create({
                            email: companyCreated.email,
                            code_verification: companyCreated.code_verification,
                            id: companyCreated._id,
                            role: "Company"
                        });
                        adminService.addSubscription(companyCreated.email);
                        if (!testAuth) return res.status(400).json({message: "probléme lors de la création de compte"});
                        else return res.status(200).json({message: "Entreprise créée"});
                    } catch (err) {
                        loggerFile.error(err,utils.getClientAddress(req, err))
                        return res.status(400).json({message: err.message});
                    }
                }
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },

    findAll: async (req, res) => {
        loggerFile.info("in findAll companies");
        try {
            const companies = await Company.find({role: "Company"});
            if (companies) {
                loggerFile.debug('get all companies done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('aucun companies éxistants')
                return res.status(200).json({message: "Entreprises inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update company');
        let id = req.auth._idConnected;
        const {token} = req.headers;
        const {email, confidentiality} = req.body

        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newCompany = req.body;
        try {
            adminService.updateSubscription(token, email, confidentiality);
            await Company.findByIdAndUpdate(id, newCompany);
            loggerFile.debug('company modifié')
            return res.status(200).json({message: "Entreprise modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete company')
        const {id} = req.params;
        try {
            await Company.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                        await cloudinary.uploader.destroy(data.cloudinary_doc_id);
                    }
                    await Company.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseService.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});

                    loggerFile.debug('Entreprise effacée')
                    return res.status(200).json({message: "Entreprise effacée"})
                } else {
                    loggerFile.debug('Entreprise inéxistante')
                    return res.status(400).json({message: "Entreprise inéxistante"})
                }
            }).catch(error => {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let company = await Company.findById(id);
            if (company && company.code_verification === token) {
                await Company.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(200).json({message: "l'utilisateur est inéxistant"});
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadDocuments: async (req, res) => {
        loggerFile.info('in uploadDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_doc_id = "";
        try {
            let company = await Company.findById(id);
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    await notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_doc_id = result.public_id;
                        let newCompany = req.body;
                        newCompany.documents = docs;
                        newCompany.cloudinary_doc_id = cloudinary_doc_id;
                        newCompany.documents_val = false;
                        await Company.findByIdAndUpdate(id, newCompany);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
                if (req.body.old_documents != "") {
                    loggerFile.debug('deleting old documents');
                    let array = req.body.old_documents.split(",");
                    await array.forEach(function callback(oldDoc, index) {
                        cloudinary.uploader.destroy(company.cloudinary_doc_id);
                    });
                }
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newCompany = req.body;
                newCompany.documents = docs;
                newCompany.documents_val = false;
                await Company.findByIdAndUpdate(id, newCompany);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: "Erreur lors du traitement des documents"});

        }
    },
    findAllDocumentValidated: async (req, res) => {
        loggerFile.info('in findAllDocumentValidated');
        try {
            const companies = await Company.find({documents_val: true});
            if (companies) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('companies valides inéxistants')
                return res.status(200).json({message: "Entreprises valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllDocumentUnvalidated: async (req, res) => {
        loggerFile.info('in findAllDocumentUnvalidated');
        try {
            const companies = await Company.find({documents_val: false});
            if (companies) {
                loggerFile.debug('companies data done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug("companies invalides inexistants")
                return res.status(200).json({message: "entreprises invalides inexistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error});
        }
    },
    findAllValidated: async (req, res) => {
        loggerFile.info("in findAllValidated")
        try {
            const companies = await Company.find({validated: true});
            if (companies) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(companies);
            } else {
                loggerFile.debug("companies valides inéxistants")
                return res.status(200).json({message: "entreprises valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllUnvalidated: async (req, res) => {
        loggerFile.info("in findAllUnvalidated")
        try {
            const companies = await Company.find({validated: false});
            if (companies) {
                loggerFile.debug("findAllUnvalidated data done")
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('companies invalides inéxistants');
                return res.status(200).json({message: "entreprises invalides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    blockCompany: async (req, res) => {
        loggerFile.info("in blockCompany")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {validated: false});
            let company = await Company.findById(id);
            loggerFile.info("in send email after bloqued")
            mailService.block({
                email: company.email
            });
            loggerFile.debug('entreprise bloqué')
            return res.status(200).json({message: "Entreprise bloquée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unblockCompany: async (req, res) => {
        loggerFile.info("in unblockCompany")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {validated: true});
            let company = await Company.findById(id);
            loggerFile.info("in send email after unbloqued")
            mailService.unblock({
                email: company.email
            });
            loggerFile.debug('entreprise débloquée')
            return res.status(200).json({message: "entreprise débloquée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentByAdmin: async (req, res) => {
        loggerFile.info("in validateDocumentByAdmin")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, true);
                    loggerFile.info("in send email after validate document")
                    await mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents de l'entreprise validées"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la valdiation des documents"});
                }
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    unValidateDocumentByAdmin: async (req, res) => {
        loggerFile.debug('in unValidateDocumentByAdmin');
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, false);
                    loggerFile.info("in send email after unvalidate document")
                    await mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents de l'entreprise non validées"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(200).json({message: "un problème est survenu au niveau de l'invaldiation des documents"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllValidated: async (req, res) => {
        loggerFile.info('in findAllValidated')
        try {
            const companies = await Company.find({validated: true});
            if (companies) {
                loggerFile.debug('get validated companies data done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('companies inéxistants')
                return res.status(200).json({message: "entreprises inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateImageProfile: async (req, res) => {
        loggerFile.info('in updateImageProfile')
        let id = req.auth._idConnected;
        let new_image = "";
        let cloudinary_id = "";
        try {
            let user = await Company.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_image != undefined)
                        await cloudinary.uploader.destroy(user.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement de l'image"})
                }
            } else {
                new_image = req.body.old_image;
            }
            user.image = await new_image;
            user.cloudinary_id = cloudinary_id;
            await Company.findByIdAndUpdate(id, user);
            loggerFile.debug('image téléchargée avec succés')
            return res.status(200).json({message: "image téléchargée avec succés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find company by ID");
        const {id} = req.params;
        let worked_with = [];
        let worked_in = req.worked_in;
        let favorites = req.favorites;
        try {
            const company = await Company.findById(id);
            if (company) {
                loggerFile.debug('get company by id');
                let size = company.worked_with.length;
                if (size != 0) {
                    await company.worked_with.forEach(async function callback(collabId, index) {
                        let collab = await Company.findById(collabId);
                        let model = {
                            _id: collab._id,
                            username: collab.username,
                            email: collab.email,
                            validated: collab.validated,
                            departement: collab.departement,
                            firstName: collab.firstName,
                            lastName: collab.lastName
                        };
                        await worked_with.push(model);
                        if (size - 1 == index) {
                            return await res.status(200).json({
                                company: company,
                                worked_with: worked_with,
                                worked_in: worked_in,
                                favorites: favorites
                            });
                        }
                    });
                } else {
                    return await res.status(200).json({
                        company: company,
                        worked_with: worked_with,
                        worked_in: worked_in,
                        favorites: favorites
                    });
                }
            } else {
                loggerFile.debug('company inéxistante');
                return res.status(200).json({message: "Entreprise inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "Entreprise inéxistante"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    createCollaborator: async (req, res) => {
        loggerFile.debug('in createCollaborator');
        let id = req.auth._idConnected;
        let {username, departement, email, password, firstName, lastName} = req.body;
        try {
            let collab = new Company({
                username,
                email,
                departement,
                firstName,
                lastName
            });
            collab.role = "Collab";
            collab.validated = false;
            await Company.create(collab).then(async data => {
                if (data) {
                    await Company.findByIdAndUpdate(id, {$push: {worked_with: data._id}}, {
                        new: true,
                        upsert: true
                    }).then(async response => {
                        if (response) {
                            let testAuth = await authService.create({
                                username: username,
                                email: email,
                                password: password,
                                idUser: data._id,
                                role: "Collab"
                            });
                            if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la création de compte"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de l'ajout d'un collaborateur à la liste"});
                        }
                    });
                    return res.status(200).json({
                        message: "collaborateur créé",
                        url: `${process.env.URL_COMPANY}/company/add-collab/${id}/${data._id}`
                    });
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de la création d'un collaborateur"});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addCollaborator: async (req, res) => {
        loggerFile.debug('in addCollaborator');
        let {id, idCollab} = req.params;
        try {
            await Company.findById(id).then(async data => {
                if (data && data.worked_with.includes(idCollab)) {

                    await Company.findByIdAndUpdate(idCollab, {$push: {company_collaborator: id}, validated: true}, {
                        new: true,
                        upsert: true
                    }).then(async response => {
                        if (response) {
                            loggerFile.debug("confirmation compte collab step 2 update done")
                            return res.status(200).json({message: "confirmation du collaborateur effectué"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de la confirmation du collaborateur"});
                        }
                    })

                } else return res.status(400).json({message: "le collaborateur ne fait pas partie de l'entreprise"});
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de l'ajout d'un collaborateur"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeCollaborator: async (req, res) => {
        loggerFile.debug('in removeCollaborator');
        let {id, idCollab} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {$pull: {worked_with: new ObjectId(idCollab)}}, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    await Company.findByIdAndDelete(idCollab).then(async data2 => {
                        if (data2) {
                            await authService.delete(idCollab);
                            return res.status(200).json({message: "Collaborateur supprimé"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de la suppression d'un collaborateur"});
                        }
                    })
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de la suppression d'un collaborateur"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in addFavoriteFreelance');
        let id = req.auth._idConnected;
        let idFreelance = req.params.id;
        try {
            if (req.auth.role === 'Collab') {
                const collab = await Company.findById(id);
                if (collab.company_collaborator[0]) {
                    const company = await Company.findById(collab.company_collaborator[0])
                    if (company.favorites.includes(idFreelance)) {
                        return res.status(401).json({message: "freelance éxistant"});
                    } else {
                        await Company.findByIdAndUpdate(company._id, {$push: {favorites: idFreelance}}, {
                            new: true,
                            upsert: true
                        }).then(async data => {
                            if (data) {
                                loggerFile.debug('addFavoriteFreelance done');
                                return res.status(200).json({message: "ajout du freelance à la liste des favoris effectué"});
                            }
                        }).catch(err => {
                            if (err) {
                                loggerFile.error(err,utils.getClientAddress(req, err))
                                return res.status(400).json({message: "un problème est survenu lors de l'ajout du freelance de la liste des favoris"});
                            }
                        })
                    }
                } else {
                    return res.status(200).json({message: "le collaborateur doit être validé avant"});
                }

            } else {
                await Company.findByIdAndUpdate(id, {$push: {favorites: idFreelance}}, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addFavoriteFreelance done');
                        return res.status(200).json({message: "ajout du freelance à la liste des favoris effectué"});
                    }
                }).catch(err => {
                    if (err) {
                        loggerFile.error(err,utils.getClientAddress(req, err))
                        return res.status(400).json({message: "un problème est survenu lors de l'ajout du freelance de la liste des favoris"});
                    }
                })
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in removeFavoriteFreelance');
        let id = req.auth._idConnected;
        let idFreelance = req.params.id;
        try {
            if (req.auth.role === 'Collab') {
                const collab = await Company.findById(id);
                if (collab.company_collaborator[0]) {
                    const company = await Company.findById(collab.company_collaborator[0])
                    if (!company.favorites.includes(idFreelance)) {
                        return res.status(401).json({message: "freelance inéxistant"});
                    } else {
                        await Company.findByIdAndUpdate(company._id, {$pull: {favorites: idFreelance}}, {
                            new: true,
                            upsert: true
                        }).then(async data => {
                            if (data) {
                                loggerFile.debug('removeFavoriteFreelance done');
                                return res.status(200).json({message: "suppression du freelance à la liste des favoris effectué"});
                            }
                        }).catch(err => {
                            if (err) {
                                loggerFile.error(err,utils.getClientAddress(req, err))
                                return res.status(400).json({message: "un problème est survenu lors de la suppression du freelance de la liste des favoris"});
                            }
                        })
                    }
                } else {
                    return res.status(200).json({message: "le collaborateur doit être validé avant"});
                }

            } else {
                await Company.findByIdAndUpdate(id, {$pull: {favorites: idFreelance}}, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('removeFavoriteFreelance done');
                        return res.status(200).json({message: "suppression du freelance de la liste des favoris effectué"});
                    }
                }).catch(err => {
                    if (err) {
                        loggerFile.error(err,utils.getClientAddress(req, err))
                        return res.status(400).json({message: "un problème est survenu lors de la suppression du freelance de la liste des favoris"});
                    }
                })
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCollaboratorFavorite: async (req, res) => {
        loggerFile.debug('in getAllCollaboratorFavorite');
        try {
            if (req.favorites) {
                return res.status(200).json({favorites: req.favorites});
            } else {
                return res.status(200).json({favorites: req.favorites});
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error));
            return res.status(400).json({message: error.message});
        }
    },
    findOneCollab: async (req, res) => {
        loggerFile.info("in find collab by ID");
        const {_idConnected} = req.auth;
        const {id} = req.params;

        try {
            if (id === _idConnected) {
                const company = await Company.findById(id);
                if (company) {
                    loggerFile.debug('get company by id');
                    return await res.status(200).json({
                        company: company
                    });
                } else {
                    loggerFile.debug('collaborateur inéxistant');
                    return res.status(200).json({message: "Collaborateur inéxistante"});
                }
            } else {
                loggerFile.debug('id not conform');
                return res.status(200).json({message: "une erreur est survenu."});
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "Collaborateur inéxistante"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    findCompanyMissions: async (req, res) => {
        loggerFile.info("findCompanyMissions");
        const {_idConnected} = req.auth;
        let missions = []
        try {
            let company = await Company.findById(_idConnected);
            if (company.missions.length > 0) {
                company.missions.forEach(async function (id, index, array) {
                    await Mission.findById(id).then(async data => {
                        let profile = await requiredProfil.findById(data.profilSearched)
                        await missions.push({mission: data, profile: profile})
                        if (missions.length == array.length) {
                            return res.status(200).send(missions)
                        }
                    }).catch(err => {
                        loggerFile.error(err,utils.getClientAddress(req, err))
                    })
                });
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error});
        }
    }
}
