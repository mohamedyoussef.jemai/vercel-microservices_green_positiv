const express = require('express');
const router = express.Router();
const companyController = require('../../controllers/test/companyControllerTest');


router.post('/',  companyController.create);
router.get('/all', companyController.findAll);
router.get('/find/:id', companyController.findOneById);
router.get('/check/:id/:token', companyController.checkEmail);
router.patch('/', companyController.update);
router.delete('/:id', companyController.delete);

router.patch('/add-favoris-freelance/:id', companyController.addFavoriteFreelance);
router.patch('/remove-favoris-freelance/:id', companyController.removeFavoriteFreelance);

router.post('/create-collab', companyController.createCollaborator)

module.exports = router;
