const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const companyController = require('../controllers/companyController');

const {
    middlewareExistUsernameCompany,
    middlewareExistEmailCompany,
    middlewareValidityCreationCompany,
    middlewareTokenCompany,
    verifFreelanceExist,
    verifyFavoriteFreelanceExist,
    verifyAccessCollab,
    middlewareValidityCollaborator,
    middlewareTokenCollab,
    getWorkedInFreelancers,
    getFavoriteFreelancers,
    middlewareTokenCompanyAndCollab,
    getCollabFavoriteList,
} = require('../middlewares/middlewareCompany');

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        cb(null, true)
    },
}).single("image");

let uploadDocuments = multer({
    storage: multer.diskStorage({}),
}).array("documents", 5);

router.post('/', middlewareExistUsernameCompany, middlewareExistEmailCompany, middlewareValidityCreationCompany, companyController.create);
router.get('/all', companyController.findAll);
router.get('/get-collab/:id', middlewareTokenCollab, companyController.findOneCollab);
router.get('/get/:id', getWorkedInFreelancers, getFavoriteFreelancers, companyController.findOne);
router.get('/check/:id/:token', companyController.checkEmail);
router.patch('/', middlewareTokenCompanyAndCollab, middlewareValidityCreationCompany, companyController.update);
router.delete('/:id', companyController.delete);
router.patch('/documents', middlewareTokenCompany, uploadDocuments, companyController.uploadDocuments);
router.get('/documents-validated', companyController.findAllDocumentValidated);
router.get('/documents-unvalidated', companyController.findAllDocumentUnvalidated);
router.get('/validated', companyController.findAllValidated);
router.get('/unvalidated', companyController.findAllUnvalidated);
router.patch('/block/:id', companyController.blockCompany);
router.patch('/unblock/:id', companyController.unblockCompany);
router.patch('/documents-validated/:id', companyController.validateDocumentByAdmin);
router.patch('/documents-unvalidated/:id', companyController.unValidateDocumentByAdmin);
router.get('/', companyController.findAllValidated);
router.patch('/upload-profile', middlewareTokenCompanyAndCollab, upload, companyController.updateImageProfile);

router.patch('/add-favoris-freelance/:id', middlewareTokenCompanyAndCollab, verifFreelanceExist, verifyFavoriteFreelanceExist, companyController.addFavoriteFreelance);
router.patch('/remove-favoris-freelance/:id', middlewareTokenCompanyAndCollab, verifFreelanceExist, companyController.removeFavoriteFreelance);

router.post('/create-collab', middlewareTokenCompany, middlewareExistUsernameCompany, middlewareExistEmailCompany, middlewareValidityCollaborator, companyController.createCollaborator)
router.get('/add-collab/:id/:idCollab', verifyAccessCollab, companyController.addCollaborator);
router.patch('/remove-collab/:id/:idCollab', companyController.removeCollaborator);

router.get('/favorite-collab', middlewareTokenCollab, getCollabFavoriteList, companyController.getAllCollaboratorFavorite);

router.get('/missions', middlewareTokenCompany, companyController.findCompanyMissions);

module.exports = router;
