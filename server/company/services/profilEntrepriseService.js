const axios = require('axios').default;
const profilServerURL = process.env.URL_PROFIL;

const loggerFile = require("../config/logger");

module.exports = {
    create: async (data) => {
        loggerFile.info("in createContactDetails")
        try {
            let test = false;
            await axios.post(`${profilServerURL}/profil-entreprise-company/`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("create profile data done")
                    test = true
                } else {
                    loggerFile.debug('problem in create profile');
                    test = false
                }
            }).catch(function (err) {
                test = false
            });
            return test;
        } catch (error) {
            return false
        }
    },
    validate: async (id, bool) => {
        loggerFile.info('in validate')
        try {
            await axios.patch(`${profilServerURL}/profil-entreprise-company/validate`, {
                id_freelancer: id,
                validated: bool
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('validation profil modifié')
                } else {
                    loggerFile.debug('problem in update');
                }
            }).catch(function (err) {
                return
            });
        } catch (error) {
            return
        }
    },
    getProfileEntrepriseById: async (id) => {
        try {
            await axios.get(`${profilServerURL}/profil-entreprise-company/${id}`).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('profile récupéré')
                    return
                } else {
                    loggerFile.debug('problem in update');
                    return
                }
            }).catch(function (err) {
                return
            });
        } catch (error) {
            l
            return
        }
    },
    delete: async (id) => {
        loggerFile.info('in delete profile');
        try {
            let test = false;
            await axios.delete(`${process.env.URL_PROFIL}/profil-entreprise-company/delete/${id}`).then(function (response) {
                if (response.status == 200 && response.data.message == "done") {
                    loggerFile.debug('delete profile done');
                    test = true;
                } else {
                    loggerFile.debug('problem in profile user ');
                    test = false;
                }
            }).catch(function (err) {
                test = false;
            });
            return test;
        } catch (error) {
            return false
        }
    },
}
