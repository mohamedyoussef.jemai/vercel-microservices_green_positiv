require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    findAgence: async (id) => {
        try {
            let data = {}
            await axios.get(`${process.env.URL_AGENCE}/agence/get/${id}`).then(function (response) {
                if (response.data) {
                    loggerFile.debug('findAgence');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get agence by id ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return data = {status: 400};
        }
    }
}
