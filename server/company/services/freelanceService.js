require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    getRequiredProfiles: async (profile) => {
        loggerFile.info('in getProfiles');
        let data = {
            title_profile: profile.title_profile,
            jobCat: profile.jobCat,
            level: profile.level,
            skillsNeeded: profile.skillsNeeded,
            skillsAppreciated: profile.skillsAppreciated,
            languages: profile.languages,
        }
        try {
            await axios.post(`${process.env.URL_FREELANCER}/freelancer/required-profile`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('getProfiles');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get required profiles');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return data;
        }
    },
    findFreelance: async (id) => {
        try {
            let data = {}
            await axios.get(`${process.env.URL_FREELANCER}/freelancer/get/${id}`).then(function (response) {
                if (response.data) {
                    loggerFile.debug('findFreelance');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get freelancer by id ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return  {status: 400};
        }
    }
}
