require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    getSectorActivity: async (req, res) => {
        loggerFile.info(" in getSectorActivity");
        try {
            const response = await axios.get(`${process.env.URL_API}/api/sector-activity`);
            if (response) {
                loggerFile.debug("getSectorActivity done")
                return res.status(200).json(response);
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
