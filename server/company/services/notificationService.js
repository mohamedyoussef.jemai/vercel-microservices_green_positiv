const axios = require('axios').default;
const Company = require('../models/company.model');
const notificationServerURL = process.env.URL_NOTIFICATION;
const loggerFile = require("../config/logger");

module.exports = {
    createUploadDocumentNotif: async (id, filename) => {
        loggerFile.info("in createUploadDocumentNotif");
        let changes = {};
        try {
            let company = await Company.findById(id);
            changes.filename = filename;
            let username = company.username;
            let role = company.role;
            changes.state = "upload";
            changes.idCompany = company._id;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/company/upload-document`, {
                    username: username,
                    role: role,
                    changes: changes,
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create upload document done');
                    } else {
                        loggerFile.debug('problem notification upload document create');
                    }
                }).catch(function (err) {
                    return false
                });
                loggerFile.debug('createUploadDocumentNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            return;
        }
    },
}
