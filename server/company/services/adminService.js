require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    getSkills: async () => {
        loggerFile.info('in getSkills');
        try {
            let data = null;
            await axios.get(`${process.env.URL_ADMIN}/skills/`).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('get admin jobs done');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get admin jobs ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return data;
        }
    },
    getJobs: async () => {
        loggerFile.info('in getJobs');
        try {
            let data = null;
            await axios.get(`${process.env.URL_ADMIN}/jobs/`).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('get admin jobs done');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get admin jobs ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                data = {status: 400};
            });
            return data;
        } catch (error) {
            return data;
        }
    },
    addSubscription: async (email) => {
        loggerFile.info('in addSubscription');
        try {
            let test = false;
            await axios.post(`${process.env.URL_ADMIN}/subscriptions/`, {
                email: email
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('add Subscription done');
                    test = true
                } else {
                    loggerFile.debug('problem in get admin jobs ');
                    test = false
                }
            }).catch(function (err) {
                test = false
            });
            return test;
        } catch (error) {
            return false;
        }
    },
    updateSubscription: async (token, email, active) => {
        loggerFile.info('in updateSubscription');
        try {
            let test = false;
            await axios.patch(`${process.env.URL_ADMIN}/subscriptions/`, {
                email: email,
                active: active
            }, {
                headers: {
                    'token': token
                }
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('patch Subscription done');
                    test = true
                } else {
                    loggerFile.debug('problem in patch subcriptions ');
                    test = false
                }
            }).catch(function (err) {
                test = false
            });
            return test;
        } catch (error) {
            return false;
        }
    },
}
