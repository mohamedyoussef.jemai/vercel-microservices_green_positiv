const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let devisSchema = new Schema({
    id_mission: {
        type: Schema.Types.ObjectId,
        ref: 'Mission',
        required: true
    },
    id_freelance: {
        type: Schema.Types.ObjectId,
        ref: 'Freelance',
    },
    id_agence: {
        type: Schema.Types.ObjectId,
        ref: 'Agence',
    },
    dateBegin: {
        type: Date
    },
    dateEnd: {
        type: Date
    },
    price_per_day: {
        type: Number,
        required: true,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} doit être un entier'
        }
    },
    budget: {
        type: Number,
    },
    confirmed: {
        type: Boolean
    },
    state: {
        type: String,
        enum: ["en cours", "terminé","supprimé"],
        default: "en cours"
    },
}, {
    collection: 'devis',
    timestamps: true
})

const devis = mongoose.model("Devis", devisSchema);
module.exports = devis;
