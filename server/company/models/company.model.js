const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let companySchema = new Schema({
    username: {
        type: String,
        required: true
    },
    role: {
        type: String,
        default: "Company"
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    lastName: {
        type: String,
    },
    firstName: {
        type: String,
    },
    phone: {
        type: String
    },
    lastConnection: {
        type: Date,
        default: Date.now
    },
    departement: {
        type: String,
        enum: ['IT', 'MARKETING', 'ADMINISTRATION', 'PURCHASING', 'ACCOUNTING', 'RH', 'OPERATION', 'SALES', 'MANAGEMENT', 'PRODUCT', 'COMMUNICATION'],
        default: 'IT',
        required: true
    },
    searchable: {
        type: String,
        enum: ['alone', 'green'],
        default: 'alone'
    },
    validated: {
        type: Boolean,
    },
    email_verification: {
        type: Number,
        default: 0
    },
    code_verification: {
        type: String,
    },
    confidentiality: {
        type: Boolean,
        default: 0
    },
    documents: {
        type: [{
            type: String
        }],
    },
    documents_val: {
        type: Boolean,
        default: 0
    },
    image: {
        type: String,
    },
    signed_company: {
        type: Boolean,
        default: 0
    },
    worked_with: {
        type: []
    },
    favorites: {
        type: []
    },
    worked_in: {
        type: []
    },
    company_collaborator: {
        type: [],
    },
    access_collab: {
        type: String,
        enum: ['all', 'admin'],
        default: 'admin'
    },
    invit_collab: {
        type: String,
        enum: ['all', 'admin'],
        default: 'admin'
    },
    active_collab: {
        type: Boolean,
        default: false,
    },
    greenQuestion: {
        type: String
    },
    cloudinary_id: {
        type: String,
    },
    cloudinary_doc_id: {
        type: String,
    },
    missions: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Mission'
        }],
    },
}, {
    collection: 'companies',
    timestamps: true
})

const Company = mongoose.model("Company", companySchema);
module.exports = Company;

