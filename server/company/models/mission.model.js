const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let missionSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    id_company: {
        type: Schema.Types.ObjectId,
        ref: 'Company'
    },
    objectif: {
        type: String,
    },
    description: {
        type: String,
    },
    supp_month: {
        type: Boolean
    },
    dateBegin: {
        type: Date
    },
    budget: {
        type: Number,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} doit être un entier'
        }
    },
    price_per_day: {
        type: Number,
        required: true,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} doit être un entier'
        }
    },
    show_price: {
        type: Boolean
    },
    work_frequence: {
        type: Number
    },
    telework: {
        type: Boolean
    },
    nb_days_telework: {
        type: Number,
    },
    period_per_month: {
        type: Number,
    },
    local_city: {
        type: String,
    },
    profilSearched: {
        type: Schema.Types.ObjectId,
        ref: 'requiredProfil'
    },
    comments: {
        type: String,
    },
    document: {
        type: String,
        default: ""
    },
    cloudinary_doc_id: {
        type: String,
    },
    freelancers: {
        type: [{type: String}],
        default: []
    },
    id_agence: {
        type: Schema.Types.ObjectId,
        ref: 'Agence'
    }
}, {
    collection: 'missions',
    timestamps: true
})

const Mission = mongoose.model("Mission", missionSchema);
module.exports = Mission;

