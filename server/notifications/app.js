require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/notificationAdmin.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

let adminRouter = require('./routes/notificationAdmin.route');
let postRouter = require('./routes/notificationPost.route');
let freelanceRouter = require('./routes/notificationFreelance.route');
let agenceRouter = require('./routes/notificationAgence.route');
let companyRouter = require('./routes/notificationCompany.route');

app.use('/admin', adminRouter);
app.use('/posts', postRouter);
app.use('/freelance', freelanceRouter);
app.use('/agence', agenceRouter);
app.use('/company', companyRouter);

mongoose.connect(process.env.DB_URI_NOTIF_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    loggerFile.info(`server running on port ${port}`);
}).catch((err) => {
    loggerFile.info(err);
});

module.exports = app;
