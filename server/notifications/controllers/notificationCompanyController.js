const notificationCompany = require("../models/notificationCompany.model")
const loggerFile = require("../config/logger");
const utils = require("../config/utils");
module.exports = {
    createUploadDocumentNotif: async (req, res) => {
        loggerFile.info("in createUploadDocumentNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationCompany({username, role, changes});
            await notificationCompany.create(notif);
            loggerFile.debug('createUploadDocumentNotif data done');
            return res.status(200).json({message: "notification créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createProfilEntrepriseNotif: async (req, res) => {
        loggerFile.info("in createProfilEntrepriseNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationCompany({username, role, changes});
            await notificationCompany.create(notif);
            loggerFile.debug('createProfilEntrepriseNotif data done');
            return res.status(200).json({message: "notification créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createFacturationDetailsNotif: async (req, res) => {
        loggerFile.info("in createFacturationDetailsNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationCompany({username, role, changes});
            await notificationCompany.create(notif);
            loggerFile.debug('createFacturationDetailsNotif data done');
            return res.status(200).json({message: "notification créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createContactComptaNotif: async (req, res) => {
        loggerFile.info("in createContactComptaNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationCompany({username, role, changes});
            await notificationCompany.create(notif);
            loggerFile.debug('createContactComptaNotif data done');
            return res.status(200).json({message: "notification créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    checkNotification: async (req, res) => {
        loggerFile.info('in notification');
        let {id} = req.params;
        try {
            let notif = await notificationCompany.findById(id);
            if (notif) {
                await notificationCompany.findByIdAndUpdate(id, {checked: true});
                loggerFile.debug('checkNotification data done');
                return res.status(200).json({message: "notification checked"});
            } else return res.status(400).json({message: "notification inéxistante"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotification: async (req, res) => {
        loggerFile.info('in getAllNotification');
        try {
            let notifications = await notificationCompany.find();
            loggerFile.debug('getAllNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllCheckedNotification');
        try {
            let notifications = await notificationCompany.find({checked: true});
            loggerFile.debug('getAllCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllUnCheckedNotification');
        try {
            let notifications = await notificationCompany.find({checked: false});
            loggerFile.debug('getAllUnCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllNotificationByPeriod');
        try {
            let notifications = await notificationCompany.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }
            });
            loggerFile.debug('getAllNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllCheckedNotificationByPeriod');
        try {
            let notifications = await notificationCompany.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: true
            });
            loggerFile.debug('getAllCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllUnCheckedNotificationByPeriod');
        try {
            let notifications = await notificationCompany.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: false
            });
            loggerFile.debug('getAllUnCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll notification Freelance");
        try {
            let notifications = await notificationCompany.find();
            loggerFile.debug('findAll data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
}
