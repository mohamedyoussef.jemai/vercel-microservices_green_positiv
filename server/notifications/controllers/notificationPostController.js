const loggerFile = require("../config/logger");
let NotificationAdmin = require('../models/notificationAdmin.model');
const utils = require("../config/utils");

module.exports = {
    checkNotification: async (req, res) => {
        loggerFile.info('in notification');
        let {id} = req.params;
        try {
            let notif = await NotificationAdmin.findById(id);
            if (notif) {
                await NotificationAdmin.findByIdAndUpdate(id, {checked: true});
                loggerFile.debug('checkNotification data done');
                return res.status(200).json({message: "notification checked"});
            } else return res.status(400).json({message: "notification inéxistante"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotification: async (req, res) => {
        loggerFile.info('in getAllNotification');
        try {
            let notifications = await NotificationAdmin.find();
            loggerFile.debug('getAllNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllCheckedNotification');
        try {
            let notifications = await NotificationAdmin.find({checked: true});
            loggerFile.debug('getAllCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllUnCheckedNotification');
        try {
            let notifications = await NotificationAdmin.find({checked: false});
            loggerFile.debug('getAllUnCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllNotificationByPeriod');
        try {
            let notifications = await NotificationAdmin.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }
            });
            loggerFile.debug('getAllNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllCheckedNotificationByPeriod');
        try {
            let notifications = await NotificationAdmin.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: true
            });
            loggerFile.debug('getAllCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllUnCheckedNotificationByPeriod');
        try {
            let notifications = await NotificationAdmin.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: false
            });
            loggerFile.debug('getAllUnCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getPostEditor: async (req, res) => {
        loggerFile.info('in getPostEditor');
        let finalNtofications = [];
        try {
            let notifications = await NotificationAdmin.find({
                username: req.auth.usernameConnected,
            });
            notifications.map(async (el) => {
                if (el.changes.state == 'published' || el.changes.state == 'bloqued') {
                    finalNtofications.push(el);
                }
            });
            loggerFile.debug('getPostEditor data done');
            return res.status(200).json(finalNtofications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getPostEditorTest: async (req, res) => {
        loggerFile.info('in getPostEditorTest');
        let finalNtofications = [];
        try {
            let notifications = await NotificationAdmin.find({
                username: req.body.usernameConnected,
            });
            notifications.map(async (el) => {
                if (el.changes.state == 'published' || el.changes.state == 'bloqued') {
                    finalNtofications.push(el);
                }
            });
            loggerFile.debug('getPostEditor data done');
            return res.status(200).json(finalNtofications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}

