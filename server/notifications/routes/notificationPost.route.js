const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationPostController');

router.patch('/check/:id', notificationController.checkNotification);
router.get('/', notificationController.getAllNotification);
router.get('/checked', notificationController.getAllCheckedNotification);
router.get('/unchecked', notificationController.getAllUnCheckedNotification);
router.post('/period', notificationController.getAllNotificationByPeriod);
router.post('/period/checked', notificationController.getAllCheckedNotificationByPeriod);
router.post('/period/unchecked', notificationController.getAllUnCheckedNotificationByPeriod);
router.get('/editor', notificationController.getPostEditor);
router.post('/editorTest', notificationController.getPostEditorTest);

module.exports = router;
