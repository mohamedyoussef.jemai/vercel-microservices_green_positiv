//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let notificationFreelance = require('../models/notificationFreelance.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idNotif = "";

chai.use(chaiHttp);
//Our parent block
describe('Notification Freelance', () => {
    before((done) => { //Before each test we empty the database
        notificationFreelance.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationFreelance', () => {
            it('it should GET all the freelance notifications', (done) => {
                chai.request(server)
                    .get('/freelance')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route createDetailsContactNotif notif Freelance
        */
        describe('/PATCH create freelance createDetailsContactNotif', () => {
            it('it should create freelance createDetailsContactNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        name: "test entreprise",
                        address: "Paris",
                        address_plus: "13éme",
                        legal_form: "SARL à capital variable",
                        state: "details"
                    }
                }
                chai.request(server)
                    .patch('/freelance/create-details-contact')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
          * Test the /PATCH route createLegalRepresentativeNotif notif Freelance
         */
        describe('/PATCH create freelance createLegalRepresentativeNotif', () => {
            it('it should create freelance createLegalRepresentativeNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        lastname: "jemai",
                        firstname: "youssef",
                        birthday: "1998-01-16",
                        postal: "2036",
                        city_of_birth: "Ariana",
                        country_of_birth: "Tunisie",
                        nationality: "Tunisie",
                        state: "representation"
                    }
                }
                chai.request(server)
                    .patch('/freelance/create-legal-representative')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route create taxe notif Freelance
        */
        describe('/PATCH create freelance createTaxeNotif', () => {
            it('it should create freelance createTaxeNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        taxe: 13,
                        state: "taxe"

                    }

                }
                chai.request(server)
                    .patch('/freelance/taxe')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createLegalMentionNotif notif Freelance
       */
        describe('/PATCH create freelance createLegalMentionNotif', () => {
            it('it should create freelance createLegalMentionNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        sas: 20,
                        siret: "84492526300015",
                        rcs: "test",
                        naf: "1234A",
                        tva_intracom: "FR28844925263",
                        days: 0,
                        state: "mention"
                    }
                }
                chai.request(server)
                    .patch('/freelance/legal-mention')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Freelance
       */
        describe('/PATCH create freelance createIbanNotif iban', () => {
            it('it should create freelance createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        type_iban: "iban",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_country: "Norvège",
                        cb_iban_iban: "58965896587458",
                        state: "iban"
                    }
                }
                chai.request(server)
                    .patch('/freelance/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Freelance
       */
        describe('/PATCH create freelance createIbanNotif iban-us', () => {
            it('it should create freelance createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        type_iban: "iban-us",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_account_number:"12345678",
                        cb_iban_aba_transit_number:"123456789",
                        cb_iban_account_type: "epargne",
                        state: "iban-us"
                    }
                }
                chai.request(server)
                    .patch('/freelance/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Freelance
       */
        describe('/PATCH create freelance createIbanNotif iban-ca', () => {
            it('it should create freelance createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        type_iban: "iban-ca",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_account_number:"12345678",
                        cb_iban_branch_code: "12345",
                        cb_iban_number_institution: "614",
                        cb_iban_bank_name:"bank test",
                        state:"iban-ca"
                    }
                }
                chai.request(server)
                    .patch('/freelance/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Freelance
       */
        describe('/PATCH create freelance createIbanNotif iban-ca', () => {
            it('it should create freelance createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        type_iban: "others",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_bic_swift:"TNATBTTN",
                        cb_iban_account_country: "Canada",
                        state:"iban-others"
                    }
                }
                chai.request(server)
                    .patch('/freelance/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createUploadDocumentNotif notif Freelance
         */
        describe('/PATCH create freelance createUploadDocumentNotif', () => {
            it('it should create freelance createUploadDocumentNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Freelancer",
                    changes: {
                        "documents": "documents_1645008952887_examen_tp_securité_5_2_2021_2022.pdf",
                        "state": "documents",
                    }
                }
                chai.request(server)
                    .patch('/freelance/upload-document')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 2 : Get all notification and Check freelance
    */
    describe('Step 2 : Get all notification and check one', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAdmin', () => {
            it('it should GET all the admins notifications', (done) => {
                chai.request(server)
                    .get('/freelance')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(9);
                        idNotif = res.body[0]._id;
                        done();
                    });

            });
        });
        describe('/PATCH check notification', () => {
            it('it should check freelance notification', (done) => {
                console.log("idNotif ", idNotif)
                chai.request(server)
                    .patch('/freelance/check/' + idNotif)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification checked');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : Get All notifications types
    */
    describe('Step 3 : Get all notification types', () => {
        /*
         * Test the /GET checked notification route
        */
        describe('/GET CHECKED notification', () => {
            it('it should get checked notification freelance', (done) => {
                chai.request(server)
                    .get(`/freelance/checked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
        /*
         * Test the /GET unchecked notification route
        */
        describe('/GET UNCHECKED notification', () => {
            it('it should get unchecked notification freelance', (done) => {
                chai.request(server)
                    .get(`/freelance/unchecked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(8);
                        done();
                    });
            });
        });
    });
});
