//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let notificationCompany = require('../models/notificationCompany.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idNotif = "";

chai.use(chaiHttp);
//Our parent block
describe('Notification Company', () => {
    before((done) => { //Before each test we empty the database
        notificationCompany.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationComapny', () => {
            it('it should GET all the company notifications', (done) => {
                chai.request(server)
                    .get('/company')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createUploadDocumentNotif notif Company
         */
        describe('/PATCH create Company createUploadDocumentNotif', () => {
            it('it should create Company createUploadDocumentNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Company",
                    changes: {
                        "filename": "documents_1645008952887_examen_tp_securité_5_2_2021_2022.pdf",
                        "state": "upload",
                    }
                }
                chai.request(server)
                    .patch('/company/upload-document')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
          * Test the /PATCH route createProfilEntrepriseNotif notif company
         */
        describe('/PATCH create company createProfilEntrepriseNotif', () => {
            it('it should create company createProfilEntrepriseNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Company",
                    changes: {
                        logo: "documents_1645008952887_examen_tp_securité_5_2_2021_2022.png",
                        old_logo: "",
                        name: "test",
                        size: 3,
                        sector_activity: "TRAVEL",
                        state: "profil"
                    }
                }
                chai.request(server)
                    .patch('/company/profil')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route createFacturationDetailsNotif notif company
        */
        describe('/PATCH create company createFacturationDetailsNotif', () => {
            it('it should create company createFacturationDetailsNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Company",
                    changes: {
                        social_reason: "albero",
                        siret: "84492526300015",
                        tva_intracom: "FR28844925263",
                        address: "tunisie",
                        address_plus: "soukraa",
                        state: "facturation"
                    }
                }
                chai.request(server)
                    .patch('/company/facture')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route createContactComptaNotif notif company
        */
        describe('/PATCH create company createContactComptaNotif', () => {
            it('it should create company createContactComptaNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Company",
                    changes: {
                        lastName: "test",
                        firstName: "test 2 ",
                        email: "email",
                        send_compta: true
                    }
                }
                chai.request(server)
                    .patch('/company/contact')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 2 : Get all notification and Check company
    */
    describe('Step 2 : Get all notification and check one', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAdmin', () => {
            it('it should GET all the company notifications', (done) => {
                chai.request(server)
                    .get('/company')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(4);
                        idNotif = res.body[0]._id;
                        done();
                    });

            });
        });
        describe('/PATCH check notification', () => {
            it('it should check company notification', (done) => {
                console.log("idNotif ", idNotif)
                chai.request(server)
                    .patch('/company/check/' + idNotif)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification checked');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : Get All notifications types
    */
    describe('Step 3 : Get all notification types', () => {
        /*
         * Test the /GET checked notification route
        */
        describe('/GET CHECKED notification', () => {
            it('it should get checked notification company', (done) => {
                chai.request(server)
                    .get(`/company/checked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
        /*
         * Test the /GET unchecked notification route
        */
        describe('/GET UNCHECKED notification', () => {
            it('it should get unchecked notification company', (done) => {
                chai.request(server)
                    .get(`/company/unchecked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(3);
                        done();
                    });
            });
        });
    });
});
