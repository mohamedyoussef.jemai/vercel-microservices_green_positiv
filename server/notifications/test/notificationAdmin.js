//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let notificationAdmin = require('../models/notificationAdmin.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idNotif = "";

chai.use(chaiHttp);
//Our parent block
describe('Notification Admin', () => {
    before((done) => { //Before each test we empty the database
        notificationAdmin.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAdmin', () => {
            it('it should GET all the admins notifications', (done) => {
                chai.request(server)
                    .get('/admin')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Freelance
        */
        describe('/POST create notification admin', () => {
            it('it should create notification admin', (done) => {
                let data = {
                    username: "test",
                    role: "Editor",
                    changes: {
                        title: "test",
                        content: "this is content post editor micro service",
                        url_fb: "https://trello2.com/",
                        url_instagram: "https://trello2.com/",
                        url_twitter: "https://trello2.com/",
                        url_linkedin: "",
                        resume: "test",
                        author: "editor",
                        state: "created",
                        idPost: "621cd0245131f0fc064e887b",
                        image: "image_1646055460305_sustainableIT_green_energy_800px",
                    }
                }
                chai.request(server)
                    .post('/admin/create')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('article créé');
                        done();
                    });
            });
        });
        /*
          * Test the /PATCH notification
         */
        describe('/PATCH update notification admin', () => {
            it('it should update notification admin', (done) => {
                let data = {
                    username: "test",
                    role: "Editor",
                    changes: {
                        title: "test 2",
                        content: "this is content post editor micro service 2",
                        url_linkedin: "https://trello2.com/",
                        state: "updated",
                        idPost: "621cd0245131f0fc064e887b",
                        image: "image2_1646055460305_sustainableIT_green_energy_800px",
                    }
                }
                chai.request(server)
                    .patch('/admin/update')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('article modifié');
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 2 : validate and unvalidate post
   */
    describe('Step 2 : validate and unvalidate post', () => {
        /*
         * Test the /POST validate post
        */
        describe('/PATCH validate notification', () => {
            it('it should validate post notification', (done) => {
                let changes = {};
                changes.state = "published"
                changes.message = "l'article test validated a été publié"
                changes.idPost = "621cd09a1585db4d4888c620";
                let data = {
                    username: "test validation",
                    changes: changes
                }

                chai.request(server)
                    .patch('/admin/validate')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('article publié');
                        done();
                    });
            });
        });
        /*
         * Test the /POST unvalidate post
        */
        describe('/PATCH unvalidate notification', () => {
            it('it should unvalidate post notification', (done) => {
                let changes = {};
                changes.state = "bloqued"
                changes.message = "l'article test validated a été publié"
                changes.idPost = "621cd09a1585db4d4888c620";
                let data = {
                    username: "test validation",
                    changes: changes
                }

                chai.request(server)
                    .patch('/admin/unvalidate')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('article dépublié');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : Get all notification and Check post
    */
    describe('Step 3 : Get all notification and check one', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAdmin', () => {
            it('it should GET all the admins notifications', (done) => {
                chai.request(server)
                    .get('/posts')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(4);
                        idNotif = res.body[0]._id;
                        done();
                    });

            });
        });
        describe('/PATCH check notification', () => {
            it('it should check post notification', (done) => {
                console.log("idNotif ",idNotif)
                chai.request(server)
                    .patch('/posts/check/' + idNotif)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification checked');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : Get All notifications types
    */
    describe('Step 4 : Get all notification types', () => {
        /*
         * Test the /GET checked notification route
        */
        describe('/GET CHECKED notification', () => {
            it('it should get checked notification post', (done) => {
                chai.request(server)
                    .get(`/posts/checked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
        /*
         * Test the /GET unchecked notification route
        */
        describe('/GET UNCHECKED notification', () => {
            it('it should get unchecked notification post', (done) => {
                chai.request(server)
                    .get(`/posts/unchecked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(3);
                        done();
                    });
            });
        });
    });
    describe('Step 5 : Get editor posts', () => {
        /*
         * Test the /GET editor notification route
        */
        describe('/GET editor notification posts', () => {
            it('it should get editor notification posts', (done) => {
                let body = {
                    usernameConnected: "test validation"
                }
                chai.request(server)
                    .post(`/posts/editorTest`)
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(2);
                        done();
                    });
            });
        });
    });
});
