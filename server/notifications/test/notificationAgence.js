//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let notificationAgence = require('../models/notificationAgence.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idNotif = "";

chai.use(chaiHttp);
//Our parent block
describe('Notification Agence', () => {
    before((done) => { //Before each test we empty the database
        notificationAgence.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAgence', () => {
            it('it should GET all the Agence notifications', (done) => {
                chai.request(server)
                    .get('/agence')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route createDetailsContactNotif notif Freelance
        */
        describe('/PATCH create Agence createDetailsContactNotif', () => {
            it('it should create Agence createDetailsContactNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        name: "test entreprise",
                        address: "Paris",
                        address_plus: "13éme",
                        legal_form: "SARL à capital variable",
                        state: "details"
                    }
                }
                chai.request(server)
                    .patch('/agence/create-details-contact')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
          * Test the /PATCH route createLegalRepresentativeNotif notif Freelance
         */
        describe('/PATCH create Agence createLegalRepresentativeNotif', () => {
            it('it should create Agence createLegalRepresentativeNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        lastname: "jemai",
                        firstname: "youssef",
                        birthday: "1998-01-16",
                        postal: "2036",
                        city_of_birth: "Ariana",
                        country_of_birth: "Tunisie",
                        nationality: "Tunisie",
                        state: "representation"
                    }
                }
                chai.request(server)
                    .patch('/agence/create-legal-representative')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route create taxe notif Agence
        */
        describe('/PATCH create Agence createTaxeNotif', () => {
            it('it should create Agence createTaxeNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        taxe: 13,
                        state: "taxe"

                    }

                }
                chai.request(server)
                    .patch('/agence/taxe')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createLegalMentionNotif notif Freelance
       */
        describe('/PATCH create Agence createLegalMentionNotif', () => {
            it('it should create Agence createLegalMentionNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        sas: 20,
                        siret: "84492526300015",
                        rcs: "test",
                        naf: "1234A",
                        tva_intracom: "FR28844925263",
                        days: 0,
                        state: "mention"
                    }
                }
                chai.request(server)
                    .patch('/agence/legal-mention')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Agence
       */
        describe('/PATCH create Agence createIbanNotif iban', () => {
            it('it should create Agence createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        type_iban: "iban",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_country: "Norvège",
                        cb_iban_iban: "58965896587458",
                        state: "iban"
                    }
                }
                chai.request(server)
                    .patch('/agence/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Agence
       */
        describe('/PATCH create Agence createIbanNotif iban-us', () => {
            it('it should create Agence createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        type_iban: "iban-us",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_account_number: "12345678",
                        cb_iban_aba_transit_number: "123456789",
                        cb_iban_account_type: "epargne",
                        state: "iban-us"
                    }
                }
                chai.request(server)
                    .patch('/agence/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif Agence
       */
        describe('/PATCH create Agence createIbanNotif iban-ca', () => {
            it('it should create Agence createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        type_iban: "iban-ca",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_account_number: "12345678",
                        cb_iban_branch_code: "12345",
                        cb_iban_number_institution: "614",
                        cb_iban_bank_name: "bank test",
                        state: "iban-ca"
                    }
                }
                chai.request(server)
                    .patch('/agence/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createIbanNotif notif agence
       */
        describe('/PATCH create agence createIbanNotif iban-ca', () => {
            it('it should create agence createIbanNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        type_iban: "others",
                        cb_iban_name_lastname: "youssef jemai",
                        cb_iban_address_holder: "youssef",
                        cb_iban_postal: "20365",
                        cb_iban_city: "Tunisie",
                        cb_iban_region: "sud",
                        cb_iban_country: "Norvège",
                        cb_iban_bic_swift: "TNATBTTN",
                        cb_iban_account_country: "Canada",
                        state: "iban-others"
                    }
                }
                chai.request(server)
                    .patch('/agence/iban')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH route create createUploadDocumentNotif notif agence
         */
        describe('/PATCH create agence createUploadDocumentNotif', () => {
            it('it should create agence createUploadDocumentNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    changes: {
                        "documents": "documents_1645008952887_examen_tp_securité_5_2_2021_2022.pdf",
                        "state": "documents",
                    }
                }
                chai.request(server)
                    .patch('/agence/upload-document')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 2 : Get all notification and Check agence
    */
    describe('Step 2 : Get all notification and check one', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all notificationAdmin', () => {
            it('it should GET all the admins notifications', (done) => {
                chai.request(server)
                    .get('/agence')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(9);
                        idNotif = res.body[0]._id;
                        done();
                    });

            });
        });
        describe('/PATCH check notification', () => {
            it('it should check post notification', (done) => {
                console.log("idNotif ", idNotif)
                chai.request(server)
                    .patch('/agence/check/' + idNotif)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification checked');
                        done();
                    });
            });
        });
    });
    /*
      * Test Step 3 : Creation Offer notification
     */
    describe('Step 3 : Creation Offer notification', () => {
        /*
          * Test the /POST route add Offer
         */
        describe('/POST create agence createAddOfferNotif', () => {
            it('it should create agence createAddOfferNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    offer_name: "offre 13",
                    changes: {
                        name: "offre 13",
                        domain: "RH",
                        price: 28,
                        show_price: true,
                        description: "test"
                    }
                }
                chai.request(server)
                    .patch('/agence/add-offer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route update Offer
        */
        describe('/PATCH create agence createUpdateOfferNotif', () => {
            it('it should create agence createUpdateOfferNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    offer_name: "offre 13",
                    changes: {
                        name: "offre 14",
                        price: 57,
                        show_price: false,
                        description: "test updated",
                        state: "offer_updated"
                    }
                }
                chai.request(server)
                    .patch('/agence/update-offer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH route upload document Offer
        */
        describe('/PATCH create agence createUploadOfferDocumentNotif', () => {
            it('it should create agence createUploadOfferDocumentNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    offer_name: "offre 13",
                    changes: {
                        filename: "documents_1646133231970_Résultat_analyse_Besma_JEMAI.pdf",
                        state: "offer_upload",
                        idAgence: "621dff33de6c3646c947b466"
                    }
                }
                chai.request(server)
                    .patch('/agence/upload-document-offer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE route add Offer
        */
        describe('/DELETE create agence createDeleteOfferNotif', () => {
            it('it should create agence createDeleteOfferNotif ', (done) => {
                let data = {
                    username: "test",
                    role: "Agence",
                    offer_name: "offre 13",
                    changes: {
                        state: "offer_deleted"
                    }
                }
                chai.request(server)
                    .patch('/agence/delete-offer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('notification créé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : Get All notifications types
    */
    describe('Step 4 : Get all notification types', () => {
        /*
         * Test the /GET checked notification route
        */
        describe('/GET CHECKED notification', () => {
            it('it should get checked notification agence', (done) => {
                chai.request(server)
                    .get(`/agence/checked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
        /*
         * Test the /GET unchecked notification route
        */
        describe('/GET UNCHECKED notification', () => {
            it('it should get unchecked notification agence', (done) => {
                chai.request(server)
                    .get(`/agence/unchecked`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(12);
                        done();
                    });
            });
        });
    });
});
