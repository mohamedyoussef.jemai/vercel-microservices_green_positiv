const passport = require('passport');
const loggerFile = require("../config/logger");
const User = require("../models/user.model");
const crypto = require('crypto');
let mailingService = require('../services/mailingService')
const utils = require('../config/utils');
const jwt = require("jsonwebtoken");

module.exports = {
    login: async (req, res) => {
        loggerFile.info("in login");
        await passport.authenticate('local', async function (err, user, info) {
            if (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(404).json({message: err.message});
            }
            if (user) {
                try {
                    if (user.validated) {
                        const token = user.generateJwt();
                        const refreshToken = user.generateJwtRefresh();
                        res.status(200);
                        res.json({
                            "token": token,
                            "refreshToken": refreshToken
                        });
                    } else {
                        return res.status(401).json({
                            message: "votre compte est bloqué",
                        });
                    }

                } catch (err) {
                    return res.status(400).send({
                        message: "erreur de connexion",
                        err: err.message
                    });
                }
            } else {
                loggerFile.debug("not found");
                return res.status(401).json(info);
            }
        })(req, res);
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll");
        try {
            const users = await User.find();
            loggerFile.debug("findAll users done")
            return res.status(200).json(users);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    create: async (req, res) => {
        loggerFile.info('in create user');
        const {
            username,
            email,
            idUser,
            role,
            password,
            nameAgence
        } = req.body;
        try {
            let user;
            if (role === 'Agence') {
                user = new User({
                    username,
                    email,
                    idUser,
                    role,
                    nameAgence
                });
            } else {
                user = new User({
                    username,
                    email,
                    idUser,
                    role
                });
            }
            user.setPassword(password);
            user.validated = true;
            let userCreated = await User.create(user);
            if (userCreated) {
                loggerFile.info('user created');
                return res.status(200).json({message: "done"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    updatePassword: async (req, res) => {
        loggerFile.info('in updatePassword');
        const {
            password,
            repeatPassword
        } = req.body;
        try {
            if (password.length < 8) {
                res.status(401).json({message: "le mot de passe doit être de longueur minimum 8"});
            }
            if (password != repeatPassword) {
                res.status(401).json({message: "les mots de passes ne sont pas égaux"});
            }
            const token = req.auth;
            await User.findOne({idUser: token._idConnected}).then(async response => {
                await response.setPassword(password);
                await User.findByIdAndUpdate(response._id, response).then(response2 => {
                    if (response2) {
                        loggerFile.info('user password updated');
                        return res.status(200).json({message: "mot de passe modifié"});
                    }
                }).catch(err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                });
            }).catch(err2 => {
                loggerFile.debug("user inéxistant ", err2)
                return res.status(401).json({message: "utilisateur inéxistant"});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete user')
        const {id} = req.params;
        try {
            await User.findOneAndDelete({idUser: id});
            loggerFile.debug('User effacé')
            return res.status(200).json({message: "done"})

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    block: async (req, res) => {
        loggerFile.info('in block');
        try {
            let {id} = req.params;
            await User.findOneAndUpdate({idUser: id}, {validated: false}).then(response => {
                if (response) {
                    loggerFile.debug("user blocked ")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.debug("user inéxistant ", err)
                return res.status(401).json({message: "utilisateur inéxistant"});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    unblock: async (req, res) => {
        loggerFile.info('in unblock');
        try {
            let {id} = req.params;
            await User.findOneAndUpdate({idUser: id}, {validated: true}).then(response => {
                if (response) {
                    loggerFile.debug("user unblocked ")
                    return res.status(200).json({message: "done"});
                }
            }).catch(err => {
                loggerFile.debug("user inéxistant ", err)
                return res.status(401).json({message: "utilisateur inéxistant"});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    checkedToken: async (req, res) => {
        const {token} = req.headers;
        jwt.verify(token, process.env.MY_SECRET, (err) => {
            if (err) {
                loggerFile.info("token expiré");
                return res.status(401).json({token: false, error: "token expiré"});
            } else return res.status(200).json({token: true, message: "continue"});
        });
    },
    getCurrentUser: async (req, res) => {
        loggerFile.info("in getCurrentUser by ID");
        const id = req.auth._idConnected;
        try {
            const user = await User.findOne({idUser: id});
            if (user) {
                loggerFile.debug('get user by id');
                if (user.role === 'Agence') {
                    return res.status(200).json({
                        username: user.username,
                        email: user.email,
                        role: user.role,
                        idUser: user.idUser,
                        nameAgence: user.nameAgence
                    });
                } else {
                    return res.status(200).json({
                        username: user.username,
                        email: user.email,
                        role: user.role,
                        idUser: user.idUser
                    });
                }
            } else {
                loggerFile.debug('user inéxistant');
                return res.status(200).json({message: "user inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "user inéxistant"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    getUser: async (req, res) => {
        loggerFile.info("in getUser by ID");
        const {id} = req.params;
        try {
            const user = await User.findOne({idUser: id});
            if (user) {
                loggerFile.debug('get user by id');
                if (user.role === 'Agence') {
                    return res.status(200).json({
                        username: user.username,
                        email: user.email,
                        role: user.role,
                        idUser: user.idUser,
                        nameAgence: user.nameAgence
                    });
                } else {
                    return res.status(200).json({
                        username: user.username,
                        email: user.email,
                        role: user.role,
                        idUser: user.idUser
                    });
                }
            } else {
                loggerFile.debug('user inéxistant');
                return res.status(200).json({message: "user inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "user inéxistant"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    forgotPassword: async (req, res) => {
        loggerFile.info("in forgotPassword");
        try {
            let {email} = req.body;
            let user = await User.findOne({email: email});
            if (!user) return res.status(400).json({message: "utilisateur inéxistant"});
            try {
                let randomPwd = await crypto.randomBytes(16).toString('hex');
                user.salt = await crypto.randomBytes(16).toString('hex');
                user.hash = await crypto.pbkdf2Sync(randomPwd, user.salt, 1000, 64, 'sha512').toString('hex');
                await User.findByIdAndUpdate(user._id, user);
                let testMailing = await mailingService.forgotPassword({email: email, password: randomPwd});
                if (!testMailing) return res.status(400).json({message: "probléme lors de l'envoi de l'email"});
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
            }
            return res.status(200).json({message: "lien de récupération envoyé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: "erreur lors du récupération du mot de passe"});
        }
    },
    validateCreation: async (req, res) => {
        loggerFile.info('in validateCreation');
        const {
            username,
            email
        } = req.body;
        try {
            await User.countDocuments({username: username}).then(async count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(200).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    await User.countDocuments({email: email}).then(count2 => {
                        if (count2 >= 1) {
                            loggerFile.debug("l'adresse mail existe, essayez une autre")
                            return res.status(200).json({
                                message: "l'adresse mail existe, essayez une autre"
                            });
                        } else {
                            loggerFile.debug("countUsername done")
                            return res.status(200).json({message: "done"});
                        }
                    }).catch(err2 => {
                        loggerFile.error(utils.getClientAddress(req, err2))
                        return res.status(401).json({
                            message: err2.message
                        });
                    });
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    },
    updateNameAgence: async (req, res) => {
        loggerFile.info('in updateNameAgence');
        const {
            nameAgence,
        } = req.body;
        try {
            const token = req.auth;
            await User.findOne({idUser: token._idConnected}).then(async response => {
                let id = response._id;
                await User.findByIdAndUpdate(id, {nameAgence: nameAgence}).then(data => {
                    loggerFile.info('nom agence modifié');
                    return res.status(200).json({message: "nom agence modifié"});
                }).catch(err => {
                    return res.status(401).json({message: "erreur lors de la mise à jour du nom de l'agence"});
                });
            }).catch(err2 => {
                return res.status(401).json({message: "erreur lors de la récupération du user"});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    },
    refresh: async (req, res) => {
        const {email, username} = req.body;
        try {
            const user = await User.findOne({username: username, email: email});
            if (user) {
                const token = user.generateJwt();
                const response = {
                    token: token,
                }
                loggerFile.debug("refresh done for the user ", username)
                return res.status(200).json(response);
            } else {
                loggerFile.debug("user inéxistant ")
                return res.status(401).json({message: "erreur lors de la récupération du token"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    }
}
