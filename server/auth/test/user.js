//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let User = require('../models/user.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Users', () => {
    before((done) => { //Before each test we empty the database
        User.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all users', () => {
            it('it should GET all the users', (done) => {
                chai.request(server)
                    .get('/auth/users')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Freelance
        */
        describe('/POST create freelance', () => {
            it('it should not POST a user without username and password', (done) => {
                let user = {
                    username: "freelancer",
                    password: "azerty123",
                    email: "freelancer@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Freelancer"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('done');
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Freelance username Exist
        */
        describe('/POST create another freelance', () => {
            it('it should not POST a user and username exist', (done) => {
                let user = {
                    username: "freelancer",
                    password: "azerty123",
                    email: "freelancer@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Freelancer"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("l'utilisateur existe, essayez un autre identifiant");
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Freelance Email Exist
        */
        describe('/POST create another freelance', () => {
            it('it should not POST a user and email exist', (done) => {
                let user = {
                    username: "freelancer2",
                    password: "azerty123",
                    email: "freelancer@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Freelancer"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("cette adresse mail existe, essayez une autre");
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Agence
        */
        describe('/POST create agence', () => {
            it('it should not POST a user without username and password', (done) => {
                let user = {
                    username: "agence",
                    password: "azerty123",
                    email: "agence@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Agence"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('done');
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Company
        */
        describe('/POST create company', () => {
            it('it should not POST a user without username and password', (done) => {
                let user = {
                    username: "company",
                    password: "azerty123",
                    email: "company@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Company"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('done');
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create Collab
        */
        describe('/POST create Collab', () => {
            it('it should not POST a user without username and password', (done) => {
                let user = {
                    username: "collab",
                    password: "azerty123",
                    email: "collab@gmail.com",
                    idUser: "6218a8c34cc6e4de1a52830c",
                    role: "Collab"

                }
                chai.request(server)
                    .post('/auth/create')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('done');
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 2 : Login && forgot password
   */
    describe('Step 2 : Authentication module', () => {
        /*
         * Test the /POST LOGIN
        */
        describe('/POST LOGIN', () => {
            it('it should login user with good credentials', (done) => {
                let body = {username: "freelancer", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        done();
                    });
            });
        });
        /*
         * Test the /POST LOGIN bad username
        */
        describe('/POST LOGIN with bad username', () => {
            it('it should login user with good credentials', (done) => {
                let body = {username: "freelancer3", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(401);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("utilisateur inéxistant");
                        done();
                    });
            });
        });
        /*
         * Test the /POST LOGIN bad password
        */
        describe('/POST LOGIN with bad password', () => {
            it('it should login user with good credentials', (done) => {
                let body = {username: "freelancer3", password: "azerty1234"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(401);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("utilisateur inéxistant");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH update password after login
        */
        describe('/patch UPDATE PASSWORD', () => {
            it('it should update password after login correctly', (done) => {
                let body = {username: "freelancer", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        describe('/PATCH UPDATE PASSWORD', () => {
                            it('it should update user password correctly', (done2) => {
                                let body = {password: "azerty1234", repeatPassword: "azerty1234"};
                                let token = res.body.token;
                                chai.request(server)
                                    .patch('/auth/update-password')
                                    .set({'token': token})
                                    .send(body)
                                    .end((err2, res2) => {
                                        res2.should.have.status(200);
                                        res2.body.should.have.property('message');
                                        res2.body.message.should.be.eql("mot de passe modifié");
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH fail update password length after login
        */
        describe('/patch UPDATE PASSWORD', () => {
            it('it should not update because password length < 8', (done) => {
                let body = {username: "freelancer", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        describe('/PATCH UPDATE PASSWORD', () => {
                            it('it should not update password because password length < 8', (done2) => {
                                let body = {password: "test", repeatPassword: "test"};
                                let token = res.body.token;
                                chai.request(server)
                                    .patch('/auth/update-password')
                                    .set({'token': token})
                                    .send(body)
                                    .end((err2, res2) => {
                                        res2.should.have.status(401);
                                        res2.body.should.have.property('message');
                                        res2.body.message.should.be.eql("le mot de passe doit être de longueur minimum 8");
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH fail update passwords not equals after login
        */
        describe('/patch UPDATE PASSWORD', () => {
            it('it should not update because password length < 8', (done) => {
                let body = {username: "freelancer", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        describe('/PATCH UPDATE PASSWORD', () => {
                            it('it should not update password because passwords are not equal', (done2) => {
                                let body = {password: "test1234", repeatPassword: "test1111"};
                                let token = res.body.token;
                                chai.request(server)
                                    .patch('/auth/update-password')
                                    .set({'token': token})
                                    .send(body)
                                    .end((err2, res2) => {
                                        res2.should.have.status(401);
                                        res2.body.should.have.property('message');
                                        res2.body.message.should.be.eql("les mots de passes ne sont pas égaux");
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : Block/Unblock user
    */
    describe('Step 3 : Get All And Block / unblock users', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all users', () => {
            it('it should GET the 4 users created ', (done) => {
                chai.request(server)
                    .get('/auth/users')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array').lengthOf(4);
                        // res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test The Block user
        */
        describe('Block a user', () => {
            it('it should block user freelance2', (done) => {
                let data = {
                    username: "freelancer2",
                    email: "freelancer2@gmail.com",
                    idUser: "621b62af714b60b962a1fddb",
                    role: "Freelancer"
                }
                let password = "azerty123";
                let user = new User(data);
                user.setPassword(password);
                user.validated = true;
                user.save(user).then(data => {
                    chai.request(server)
                        .patch(`/auth/block/${user.idUser}`)
                        .end((err, res) => {
                            res.should.have.status(200);
                            res.body.should.be.a('object');
                            res.body.should.have.property('message').eql('done');

                        });
                    done();
                }).catch(err => {
                    console.log("error", err.message)
                });
            });
        });
        /*
         * Test The UnBlock user
        */
        describe('Unblock a user', () => {
            it('it should unblock user freelance2', (done) => {
                let id = "621b62af714b60b962a1fddb";
                chai.request(server)
                    .patch(`/auth/unblock/${id}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('done');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : Delete user
    */
    describe('Step 4 : Delete users', () => {
        /*
         * Test the /GET route
        */
        describe('/DELETE user ', () => {
            it('it should delete user freelance2 ', (done) => {
                let id = "621b62af714b60b962a1fddb";
                chai.request(server)
                    .delete(`/auth/delete/${id}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('object');
                        res.body.should.have.property('message').eql('done');
                        done();
                    });
            });
        });
    });
});
