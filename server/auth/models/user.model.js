const mongoose = require('mongoose');
const Schema = mongoose.Schema;
const crypto = require('crypto');
const jwt = require('jsonwebtoken');

let userSchema = new Schema({
    username: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true
    },
    role: {
        type: String,
        enum: ['Freelancer', 'Agence', 'Company', 'Collab'],
    },
    idUser: {
        type: String
    },
    nameAgence: {
        type: String
    },
    hash: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    validated: {
        type: Boolean
    }
}, {
    collection: 'users',
    timestamps: true
})

//set Password crypted
userSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};
//validate password crypted
userSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};
//generate jwt token //600
userSchema.methods.generateJwt = function () {
    if (this.role === 'Agence') {
        return jwt.sign({
            _idConnected: this.idUser,
            emailConnected: this.email,
            usernameConnected: this.username,
            nameAgenceConnected: this.nameAgence,
            role: this.role,
        }, process.env.MY_SECRET, {expiresIn: '600s'});
    } else {
        return jwt.sign({
            _idConnected: this.idUser,
            emailConnected: this.email,
            usernameConnected: this.username,
            role: this.role,
        }, process.env.MY_SECRET, {expiresIn: '600s'}); // DO NOT KEEP YOUR SECRET IN THE CODE!
    }
};

//generate jwt REFRESH token // 86400
userSchema.methods.generateJwtRefresh = function () {
    if (this.role === 'Agence') {
        return jwt.sign({
            _idConnected: this.idUser,
            emailConnected: this.email,
            usernameConnected: this.username,
            nameAgenceConnected: this.nameAgence,
            role: this.role,
        }, process.env.MY_SECRET_REFRESH, {expiresIn: '86400s'});
    } else {
        return jwt.sign({
            _idConnected: this.idUser,
            emailConnected: this.email,
            usernameConnected: this.username,
            role: this.role,
        }, process.env.MY_SECRET_REFRESH, {expiresIn: '86400s'});
    }
};

const User = mongoose.model("User", userSchema);
module.exports = User;

