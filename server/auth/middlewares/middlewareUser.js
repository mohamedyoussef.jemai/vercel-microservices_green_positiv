require('dotenv/config')
const jwt = require('jsonwebtoken');
const {isJwtExpired} = require('jwt-check-expiration');
const User = require("../models/user.model");
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareTokenRefresh: async (req, res, next) => {
        loggerFile.info("in middlewareTokenRefresh")
        const {refreshToken, username, email} = req.body;
        try {
            if (!refreshToken) {
                loggerFile.debug("need token")
                return res.status(401).json({user: false, error: "need token"});
            }
            var decoded = jwt.decode(refreshToken);
            let expiredBool = await isJwtExpired(refreshToken);
            if (expiredBool) {
                loggerFile.debug("token expiré");
                return res.status(401).json({message: "token expiré"});
            } else {
                if (decoded) {
                    const _id = decoded._idConnected;
                    await User.findOne({username: username, email: email, idUser: _id}).then(async user => {
                        if (user) {
                            jwt.verify(refreshToken, process.env.MY_SECRET_REFRESH, (err, decoded) => {
                                if (err) {
                                    loggerFile.debug("invalid token");
                                    return res.status(401).json({user: false, error: "invalid token"});
                                }
                                if (user && (user.role === "Freelancer" || user.role === "Agence" || user.role === "Company" || user.role === "Collaborator")) {
                                    req.auth = decoded
                                } else {
                                    loggerFile.debug("accés non autorisé");
                                    return res.status(401).json({user: false, error: "accés non autorisé"});
                                }
                            })
                        } else {
                            loggerFile.debug("utilisateur inéxistant");
                            return res.status(401).json({user: false, error: "utilisateur inéxistant"});
                        }

                    }).catch(err => {
                        loggerFile.error(err, utils.getClientAddress(req, err))
                        return res.status(400).json({message: err.message});
                    });
                } else {
                    loggerFile.debug("invalid token");
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            }
            return next();
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(401).json({user: false, error: error.message});

        }

    },
    middlewareExistUsername: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsername")
        let {username} = req.body;
        try {
            await User.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur éxiste, essayez un autre identifiant")
                    return res.status(200).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsername done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailUser: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailUser")
        let {email} = req.body;
        try {
            await User.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("cette adresse mail existe, essayez une autre")
                    return res.status(200).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else return next();
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getRole: async (req, res, next) => {
        loggerFile.info("in getRole")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            let decoded = jwt.decode(token);
            if (decoded) {
                const _id = decoded._idConnected;
                await User.findOne({idUser: _id}).then(response => {
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) return res.status(401).json({user: false, error: "token invalide"});
                        if (response.role === "Freelancer" || response.role === "Agence" || response.role === "Company" || response.role === "Collab") {
                            req.auth = decoded2
                            loggerFile.debug("getRole done")
                            return next();
                        } else {
                            loggerFile.debug("accés non autorisé")
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                }).catch(err => {
                    loggerFile.debug("user inéxistant ", err)
                    return res.status(401).json({message: "utilisateur inéxistant"});
                })

            } else {
                loggerFile.debug("token invalide")
                return res.status(401).json({user: false, error: "token invalide"});
            }
        }
    }
}

