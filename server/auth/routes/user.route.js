const express = require('express');
const router = express.Router();
const userController = require('../controllers/userController');
const {
    middlewareExistEmailUser,
    middlewareExistUsername,
    getRole,
    middlewareTokenRefresh
} = require('../middlewares/middlewareUser')

router.get('/users', userController.findAll);
router.get('/', userController.findAll);
router.post('/login', userController.login);
router.post('/current-user', getRole, userController.getCurrentUser);
router.get('/get/:id', userController.getUser);
router.post('/forgot-password', userController.forgotPassword);
router.post('/create', middlewareExistUsername, middlewareExistEmailUser, userController.create);
router.patch('/update-password', getRole, userController.updatePassword);
router.patch('/update-agence', getRole, userController.updateNameAgence);
router.patch('/unblock/:id', userController.unblock);
router.patch('/block/:id', userController.block);
router.patch('/unblock/:id', userController.unblock);
router.delete('/delete/:id', userController.delete);
router.post('/validate-creation', userController.validateCreation);
router.post('/refresh', middlewareTokenRefresh, userController.refresh);

module.exports = router;
