require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT //8005;

app.use(logger('dev'));

require('./models/user.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

require('./config/passport');

let userRouter = require('./routes/user.route');

app.use('/auth', userRouter);

mongoose.connect(process.env.DB_URI_USER_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    app.listen(port, () => {
        console.log(`server running on port ${port}`)
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.info(err);
});

module.exports = app;
