const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const User = mongoose.model('User');
const loggerFile = require("../config/logger");

passport.use(new LocalStrategy({
        usernameField: 'username'
    },
    function (username, password, done) {
        loggerFile.info("in middleware passport user")
        User.findOne({username: username}, function (err, user) {
            if (err) {
                loggerFile.error(err)
                return done(err);
            }
            // Return if user not found in database
            if (!user) {
                loggerFile.debug("utilisateur inéxistant")
                return done(null, false, {
                    message: 'utilisateur inéxistant'
                });
            }
            // Return if password is wrong
            if (!user.validPassword(password)) {
                loggerFile.debug("mot de passe erroné")
                return done(null, false, {
                    message: 'mot de passe erroné'
                });
            }
            // If credentials are correct, return the user object
            return done(null, user);
        });
    }
));
