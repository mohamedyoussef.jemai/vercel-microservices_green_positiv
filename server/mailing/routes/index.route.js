const express = require('express');
const router = express.Router();
const mailingController = require("../controllers/mailingController");

router.post('/create', mailingController.createAccount);
router.get('/', mailingController.page);
router.post('/forgot-password', mailingController.forgotPassword);
router.post('/block', mailingController.blockUser);
router.post('/unblock', mailingController.unblockUser);
router.post('/validateDocument', mailingController.validateDocument);
router.post('/unvalidateDocument', mailingController.unvalidateDocument);
router.post('/validateOffer', mailingController.validateDocumentOffer);
router.post('/unvalidateOffer', mailingController.unvalidateDocumentOffer);

module.exports = router;
