//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('Mailing', () => {
    /*
     * Test Step 1 : Creation Mail
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /POST mail creation
        */
        describe('/POST mail creation', () => {
            it('it should send email creation', (done) => {
                let data = {
                    id: "621c9a9cf9f456c99eefa3a3",
                    email: "brabra.ella@hotmail.fr",
                    code_verification: "e7b93bc1b38223ad28325866f64c80a0f6bf2217",
                    role: "Freelancer"

                }
                chai.request(server)
                    .post('/email/create')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('mail envoyé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 2 : Block & Unblock account Mail
    */
    describe('Step 2 : Block & Unblock account', () => {
        /*
         * Test the /POST mail block
        */
        describe('/POST mail block', () => {
            it('it should send email block account', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr"
                }
                chai.request(server)
                    .post('/email/block')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('email de blocage envoyé');
                        done();
                    });
            });
        });
        /*
         * Test the /POST mail unblock
        */
        describe('/POST mail unblock', () => {
            it('it should send email unblock account', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr"
                }
                chai.request(server)
                    .post('/email/unblock')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('email de déblocage envoyé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : Validate & Unvalidate account Mail
    */
    describe('Step 3 : Validate & Unvalidate account', () => {
        /*
         * Test the /POST mail validate Document
        */
        describe('/POST mail validate', () => {
            it('it should send email validate account', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr"
                }
                chai.request(server)
                    .post('/email/validateDocument')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('email de validation de document envoyé');
                        done();
                    });
            });
        });
        /*
          * Test the /POST mail unvalidate Document
         */
        describe('/POST mail unvalidate', () => {
            it('it should send email unvalidate account', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr"
                }
                chai.request(server)
                    .post('/email/unvalidateDocument')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('email de dévalidation de document envoyé');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : Validate & Unvalidate offer Mail
    */
    describe('Step 4 : Validate & Unvalidate offer', () => {
        /*
         * Test the /POST mail validate Document offer
        */
        describe('/POST mail validate offer', () => {
            it('it should send email validate offer', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr",
                    name: "offre test"
                }
                chai.request(server)
                    .post('/email/validateOffer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("email de validation d'offre envoyé");
                        done();
                    });
            });
        });
        /*
         * Test the /POST mail ybvalidate Document offer
        */
        describe('/POST mail unvalidate offer', () => {
            it('it should send email unvalidate offer', (done) => {
                let data = {
                    email: "brabra.ella@hotmail.fr",
                    name: "offre test"
                }
                chai.request(server)
                    .post('/email/unvalidateOffer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("email de dévalidation d'offre envoyé");
                        done();
                    });
            });
        });
    });
});
