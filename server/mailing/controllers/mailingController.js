require('dotenv').config();
const loggerFile = require("../config/logger");
const nodemailer = require("nodemailer");
const utils = require("../config/utils")
let smtpTransport = nodemailer.createTransport({
    port: 2525, //587, //465
    host: "smtp.elasticemail.com",
    auth: {
        user: process.env.EMAIL_SEND,
        pass: process.env.EMAIL_PASSWORD,
    },
});

module.exports = {
    createAccount: async (req, res) => {
        loggerFile.info('in create account');
        const {id, email, code_verification, role} = req.body;
        try {
            switch (role) {
                case "Freelancer": {
                    await smtpTransport.sendMail({
                        from: process.env.EMAIL_SEND,
                        to: email,
                        subject: "Confirmation de votre compte",
                        html: "<h1><b style='color:#5CACF2';>Bienvenue chez GreenPositiv !</b></h1><br><b><h3><p style='color:#F2B8A2'>Veuillez confirmer votre compte pour continue la préparation de votre profil. cliquez sur ce <a href='" + process.env.URL_FREELANCER + "/freelancer/check/" + id + "/" + code_verification + "'> lien </a>. </p></h3></b>" +
                            "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                        attachments: [{
                            filename: 'Green_positive.png',
                            path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                            cid: 'lg243567'
                        }]
                    });
                }
                    break;
                case "Agence": {
                    await smtpTransport.sendMail({
                        from: process.env.EMAIL_SEND,
                        to: email,
                        subject: "Confirmation de votre compte",
                        html: "<h1><b style='color:#5CACF2';>Bienvenue chez GreenPositiv !</b></h1><br><b><h3><p style='color:#F2B8A2'>Veuillez confirmer votre compte pour continue la préparation de votre profil. cliquez sur ce <a href='" + process.env.URL_AGENCE + "/agence/check/" + id + "/" + code_verification + "'> lien </a>. </p></h3></b>" +
                            "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                        attachments: [{
                            filename: 'Green_positive.png',
                            path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                            cid: 'lg243567'
                        }]
                    });
                }
                    break;
                case "Company": {
                    await smtpTransport.sendMail({
                        from: process.env.EMAIL_SEND,
                        to: email,
                        subject: "Confirmation de votre compte",
                        html: "<h1><b style='color:#5CACF2';>Bienvenue chez GreenPositiv !</b></h1><br><b><h3><p style='color:#F2B8A2'>Veuillez confirmer votre compte pour continue la préparation de votre profil. cliquez sur ce <a href='" + process.env.URL_COMPANY + "/company/check/" + id + "/" + code_verification + "'> lien </a>. </p></h3></b>" +
                            "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                        attachments: [{
                            filename: 'Green_positive.png',
                            path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                            cid: 'lg243567'
                        }]
                    });
                }
                    break;
                default :
                    return res.status(401).json({message: "probléme lors de l'envoi d'email de création"});
            }
            loggerFile.info("in send email after created")
            return res.status(200).json({message: "mail envoyé"});
        } catch (error) {
            console.log("error",error)
           // loggerFile.error(error, utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    blockUser: async (req, res) => {
        loggerFile.info('in block account');
        let {email} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "Blocage compte",
                html: "<h1><b style='color:#5CACF2';>Votre compte a été bloqué.</b></h1><br><b><h3><p>Veuillez contacter un administrateur.</p></h3></b>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
            return res.status(200).json({message: "email de blocage envoyé"});
        } catch (err) {
            loggerFile.error(err, utils.getClientAddress(req, err))
        }
    },
    unblockUser: async (req, res) => {
        loggerFile.info('in unblockUser account');
        let {email} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "Blocage compte",
                html: "<h1><b style='color:#5CACF2';>Votre compte a été débloqué.</b></h1><br><b><h3><p>Veuillez contacter un administrateur en cas de besoin.</p></h3></b>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
        return res.status(200).json({message: "email de déblocage envoyé"});
    },
    validateDocument: async (req, res) => {
        loggerFile.info('in validateDocument account');
        let {email} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "valdiation document",
                html: "<h1><b style='color:#5CACF2';>Votre document a été validé.</b></h1><br>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
        return res.status(200).json({message: "email de validation de document envoyé"});
    },
    unvalidateDocument: async (req, res) => {
        loggerFile.info('in validateDocument account');
        let {email} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "dévaldiation document",
                html: "<h1><b style='color:#5CACF2';>Votre document a été dévalidé.</b></h1><br>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
            return res.status(200).json({message: "email de dévalidation de document envoyé"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
    },
    validateDocumentOffer: async (req, res) => {
        loggerFile.info('in validateDocumentOffer account');
        let {email, name} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "Validation offre",
                html: "<h1><b style='color:#5CACF2';>Votre offre " + name + " a été validé</b></h1><br>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
            return res.status(200).json({message: "email de validation d'offre envoyé"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
    },
    unvalidateDocumentOffer: async (req, res) => {
        loggerFile.info('in unvalidateDocumentOffer account');
        let {email, name} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "Validation offre",
                html: "<h1><b style='color:#5CACF2';>Votre offre " + name + " a été dévalidé</b></h1><br>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
            return res.status(200).json({message: "email de dévalidation d'offre envoyé"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
    },
    forgotPassword: async (req, res) => {
        loggerFile.info('in forgotPassword account');
        let {email, password} = req.body;
        try {
            await smtpTransport.sendMail({
                from: process.env.EMAIL_SEND,
                to: email,
                subject: "Récupération mot de passe",
                html: "<h1><b style='color:#5CACF2';>Voici votre nouveau mot de passe " + password + "</b></h1><br>" +
                    "<img src='cid:lg243567' alt='GreenPositiv' width='350' height='350'></center>",
                attachments: [{
                    filename: 'Green_positive.png',
                    path: 'https://res.cloudinary.com/green-positiv/image/upload/v1649082664/Green_positive_om4oww.png',
                    cid: 'lg243567'
                }]
            });
            return res.status(200).json({message: "email de récupération de mot de passe envoyé"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
        }
    },
    page: async (req, res) => {
        return res.status(200).json({message: "server mailing on"});
    }
}

