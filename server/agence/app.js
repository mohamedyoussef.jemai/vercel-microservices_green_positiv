require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/offer.model');
require('./models/reference.model');
require('./models/agence.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: true}));
app.use(express.static("uploads"));

app.use(logger('combined'));

let notificationRouter = require('./routes/notification.route');
let agenceRouter = require('./routes/agence.route');
let offerRouter = require('./routes/offer.route');
let referenceRouter = require('./routes/reference.route');

app.use('/notification', notificationRouter);
app.use('/agence', agenceRouter);
app.use('/offer', offerRouter);
app.use('/reference', referenceRouter);

mongoose.connect(process.env.DB_URI_AGENCE_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    loggerFile.info('database Connected');
    app.listen(port, () => {
        console.log(`server running on port ${port}`)
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.error(err);
})
module.exports = app;
