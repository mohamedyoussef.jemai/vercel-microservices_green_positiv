//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let Agence = require('../models/agence.model');
let Offer = require('../models/offer.model');
let Reference = require('../models/reference.model');
//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
const axios = require('axios').default;


let idAgence = "";
let code_verification = "";
let idOffre = "";
let idReference = "";

chai.use(chaiHttp);
//Our parent block
describe('Agence', () => {
    before((done) => { //Before each test we empty the database
        try {
            Offer.deleteMany();
            Reference.deleteMany();
            Agence.remove({}, async (err) => {
                done();
            });
        } catch (err) {
            console.log("error before ", err.message)
        }

    });
    /*
     * Test Step 1 : Creation account
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all agences', () => {
            it('it should GET all the agences', (done) => {
                chai.request(server)
                    .get('/agence/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(0);
                        done();
                    });
            });
        });
        /*
         * Test the /POST route create agence
        */
        describe('/POST create agence', () => {
            it('it should POST a agence', (done) => {
                let data = {
                    username: "test",
                    nameAgence: "test",
                    email: "test@gmail.com",
                    lastName: "agence",
                    firstName: "agence",
                    phone: "2016247034",
                    password: "azerty123",
                    repeatPassword: "azerty123",
                    jobCat: "621ff61d3ee6075281fccd83",
                    localisation: "Tunisie, Tunis",
                    confidentiality: false
                }
                chai.request(server)
                    .post('/agence')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Agence créée');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 2 : update agence
    */
    describe('Step 2 : update agence', () => {
        /*
         * Test the /GET id agence
        */
        describe('/GET id agence', () => {
            it('it should GET all the agences', (done) => {
                chai.request(server)
                    .get('/agence/all')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idAgence = res.body[0]._id;
                        code_verification = res.body[0].code_verification;
                        /*
                         * Test the /PATCH route UPDATE agence
                         */
                        describe('/PATCH UPDATE agence', () => {
                            it('it should update a agence', (done2) => {
                                let data = {
                                    lastName: "test2",
                                    firstName: "test2",
                                    phone: "1234567890",
                                    localisation: "Tunisie, Tunis 2",
                                    signed_client: true,
                                    greenQuestion: "test",
                                    description: "test",
                                    id: idAgence
                                }
                                chai.request(server)
                                    .patch('/agence')
                                    .send(data)
                                    .end((err, res) => {
                                        res.should.have.status(200);
                                        res.body.should.have.property('message');
                                        res.body.message.should.be.eql('Agence modifiée');
                                        done2();
                                    });
                            });
                        });
                        done();
                    });
            });
        });

        /*
         * Test the /check email
        */
        describe('/GET CHECK EMAIL agence', () => {
            it('it should check email of a agence', (done) => {
                chai.request(server)
                    .get(`/agence/check/${idAgence}/${code_verification}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Email vérifié');
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 3 : crud offre
    */
    describe('Step 3 : crud offre agence', () => {
        /*
         * Test the /post Offre agence
        */
        describe('/POST Offre id agence', () => {
            it('it should POST Offre the agence', (done) => {
                let data = {
                    name: "offre 15",
                    domain: "RH",
                    price: 28,
                    show_price: true,
                    description: "test",
                    id: idAgence
                }
                chai.request(server)
                    .post('/agence/add-offer')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Offre ajoutée");
                        done();
                    });
            });
        });
        /*
        * Test the /GET all offers
       */
        describe('/GET offers agence', () => {
            it('it should get offers', (done) => {
                chai.request(server)
                    .get('/agence/offers')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idOffre = res.body[0]._id
                        done();
                    });
            });
        });
        /*
         * Test the /patch offer agence
        */
        describe('/PATCH Offre id AGENCE', () => {
            it('it should PATCH Offre the agence', (done) => {
                let data = {
                    name: "offre 16",
                    domain: "RH",
                    price: 30,
                    show_price: true,
                    description: "test",
                    id: idAgence
                }
                chai.request(server)
                    .patch('/agence/update-offer/' + idOffre)
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Offre modifiée");
                        done();
                    });
            });
        });
        /*
         * Test the /delete offer agence
        */
        describe('/DELETE Offer id agence', () => {
            it('it should DELETE Offer the agence', (done) => {

                chai.request(server)
                    .patch('/agence/delete-offer/' + idOffre)
                    .send({id: idAgence})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Offre supprimée");
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 4 : crud references
    */
    describe('Step 4 : crud references agence', () => {
        /*
         * Test the /post references agence
        */
        describe('/POST references', () => {
            it('it should POST reference', (done) => {
                let data = {
                    client: "reference 1",
                    domain: "RH",
                    title: " test title",
                    place: "france",
                    dateBegin: "2020-01-15",
                    dateEnd: "2020-01-16",
                    confidential: false,
                    id: idAgence
                }
                chai.request(server)
                    .post('/agence/add-reference')
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("référence client ajoutée");
                        done();
                    });
            });
        });
        /*
         * Test the /get all references
        */
        describe('/GET all references', () => {
            it('it should GET all references', (done) => {
                chai.request(server)
                    .get('/agence/references')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idReference = res.body[0]._id
                        done();
                    });
            });
        });
        /*
         * Test the /patch reference agence
        */
        describe('/PATCH reference id agence', () => {
            it('it should PATCH reference the agence', (done) => {
                let data = {
                    client: "reference 2",
                    domain: "RH",
                    title: " test title",
                    place: "france",
                    dateBegin: "2020-01-15",
                    dateEnd: "2020-01-16",
                    confidential: false,
                    id: idAgence
                }
                chai.request(server)
                    .patch('/agence/update-reference/' + idReference)
                    .send(data)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("référence client modifiée");
                        done();
                    });
            });
        });
        /*
         * Test the /delete reference agence
        */
        describe('/DELETE reference', () => {
            it('it should DELETE reference ', (done) => {

                chai.request(server)
                    .patch('/agence/delete-reference/' + idReference)
                    .send({id: idAgence})
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("référence client supprimée");
                        done();
                    });
            });
        });
    });
    /*
     * Test Step 5 : find one Agence and delete
    */
    describe('Step 5 : find one Agence and delete', () => {
        /*
         * Test the /get one agence with id
        */
        describe('/GET one agence with id', () => {
            it('it should get one agence by id', (done) => {
                chai.request(server)
                    .get(`/agence/find/${idAgence}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        /*
        * Test the /DELETE agence by id
       */
        describe('/DELETE agence by id', () => {
            it('it should delete agence by id', (done) => {
                chai.request(server)
                    .delete(`/agence/${idAgence}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message')
                        res.body.message.should.be.eql('Agence effacé')
                        done();
                    });
            });
        });
    });
});
