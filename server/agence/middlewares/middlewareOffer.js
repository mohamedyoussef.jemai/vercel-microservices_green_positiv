const Offer = require("../models/offer.model");
const Agence = require('../models/agence.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

let sector_activity =
    [
        "AVIATION_AEROSPACE",
        "IT",
        "FOOD",
        "ARCHITECTURE",
        "CRAFTS",
        "CIVIC_SOCIAL",
        "AUTOMOBILE",
        "BANKING_INSURANCE",
        "BIOTECH",
        "CIVIL_ENGINEERING",
        "RESEARCH",
        "CHEMICAL",
        "FILM",
        "SMALL_RETAIL",
        "CONSULTING",
        "CULTURE",
        "MILITARY",
        "LEISURE",
        "E_COMMERCE",
        "PUBLISHING",
        "EDUCATION",
        "SOFTWARE",
        "ENERGY",
        "ENVIRONMENT",
        "RETAIL",
        "HIGH_TECH",
        "HOSPITALITY",
        "REAL_ESTATE",
        "IMPORT_EXPORT",
        "MECHANICAL",
        "PRIMARY",
        "PHARMA",
        "GAMES",
        "LUXURY",
        "COSMETICS",
        "NANOTECH",
        "IOT",
        "PRESS",
        "SOCIAL",
        "RH",
        "RESTAURANTS",
        "HEALTH",
        "MEDICAL",
        "PUBLIC",
        "SAFETY",
        "SPORTS",
        "TELECOM",
        "TRANSPORT",
        "LOGISTIC",
        "TRAVEL",
        "WINE"];

module.exports = {
    getOffers: async (req, res, next) => {
        loggerFile.info("in getOffers")
        var offers = [];
        const idAgence = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence != null && agence.offers.length > 0) {
                await agence.offers.forEach(async function callback(offerId, index) {
                    let offer = await Offer.findById(offerId);
                    await offers.push(offer);
                });
                loggerFile.debug('getOffers done')
                req.body.offers = offers;
                return next();
            } else {
                loggerFile.debug('getOffers done')
                req.body.offers = offers;
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "offre inéxistante"});
            } else return res.status(400).json({message: error.message});
        }
    },
    addOffer: async (req, res, next) => {
        loggerFile.info('in addOffer')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        try {
            let {name, domain, price, show_price, description} = req.body;
            let offer = new Offer({name, domain, price, show_price, description});
            offer.id_agence = id;
            await Offer.create(offer).then(data => {
                if (data) {
                    loggerFile.debug("addOffer data done")
                    req.offer = data;
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de l'ajout de l'offre"});
                }
            );
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyOfferAgence: async (req, res, next) => {
        loggerFile.info('in verifiyOfferAgence')
        const idAgence = req.auth._idConnected;
        const idOffer = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence.offers.includes(idOffer)) {
                loggerFile.debug("verifiyOfferAgence done")
                return next();
            } else {
                loggerFile.debug("offre non éxistante pour cette agence")
                return res.status(400).json({message: "offre inéxistante pour cette agence"});
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyOfferAgenceData: async (req, res, next) => {
        loggerFile.info('in verifiyOfferAgenceData')
        let {domain, price} = req.body;
        try {
            if (!sector_activity.includes(domain)) {
                loggerFile.debug("error domain in verifiyOfferAgenceData")
                return res.status(400).send({message: "le domaine est incorrect"});
            }
            if (price <= 0 || typeof price !== "number") {
                loggerFile.debug("error price in verifiyOfferAgenceData")
                return res.status(400).send({message: "le prix doit être supérieur à 0"});
            } else {
                loggerFile.debug("verifiyOfferAgenceData done")
                return next()

            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

