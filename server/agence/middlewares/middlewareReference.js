const Reference = require("../models/reference.model");
const Agence = require('../models/agence.model');
const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');

let sector_activity =
    [
        "AVIATION_AEROSPACE",
        "IT",
        "FOOD",
        "ARCHITECTURE",
        "CRAFTS",
        "CIVIC_SOCIAL",
        "AUTOMOBILE",
        "BANKING_INSURANCE",
        "BIOTECH",
        "CIVIL_ENGINEERING",
        "RESEARCH",
        "CHEMICAL",
        "FILM",
        "SMALL_RETAIL",
        "CONSULTING",
        "CULTURE",
        "MILITARY",
        "LEISURE",
        "E_COMMERCE",
        "PUBLISHING",
        "EDUCATION",
        "SOFTWARE",
        "ENERGY",
        "ENVIRONMENT",
        "RETAIL",
        "HIGH_TECH",
        "HOSPITALITY",
        "REAL_ESTATE",
        "IMPORT_EXPORT",
        "MECHANICAL",
        "PRIMARY",
        "PHARMA",
        "GAMES",
        "LUXURY",
        "COSMETICS",
        "NANOTECH",
        "IOT",
        "PRESS",
        "SOCIAL",
        "RH",
        "RESTAURANTS",
        "HEALTH",
        "MEDICAL",
        "PUBLIC",
        "SAFETY",
        "SPORTS",
        "TELECOM",
        "TRANSPORT",
        "LOGISTIC",
        "TRAVEL",
        "WINE"];

module.exports = {
    getReferences: async (req, res, next) => {
        loggerFile.info("in getReferences")
        var references = [];
        const idAgence = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence != null && agence.references.length > 0) {
                await agence.references.forEach(async function callback(referenceId, index) {
                    let reference = await Reference.findById(referenceId);
                    await references.push(reference);
                });
                loggerFile.debug('getReferences done')
                req.body.references = references;
                return next();
            } else {
                loggerFile.debug('getReferences done')
                req.body.references = references;
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "référence client inéxistante"});
            } else return res.status(400).json({message: err.message});
        }
    },
    addReference: async (req, res, next) => {
        loggerFile.info('in addReference')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        try {
            let {client, domain, title, place, dateBegin, dateEnd, description, confidential} = req.body;
            let reference = new Reference({
                client,
                domain,
                title,
                place,
                dateBegin,
                dateEnd,
                description,
                confidential
            });
            reference.id_agence = id;
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                reference.image = result.secure_url;
                reference.cloudinary_id = result.public_id;

                await Reference.create(reference).then(data => {
                    if (data) {
                        loggerFile.debug("addReference data done")
                        req.reference = data;
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                        return res.status(400).json({message: "erreur lors de l'ajout de la référence client"});
                    }
                );
            } else {
                return res.status(400).json({message: "image inexistante"});
            }


        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyReferenceAgence: async (req, res, next) => {
        loggerFile.info('in verifiyReferenceAgence')
        const idAgence = req.auth._idConnected;
        const idReference = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence.references.includes(idReference)) {
                loggerFile.debug("verifiyReferenceAgence done")
                return next();
            } else {
                loggerFile.debug("référence client non éxistante pour cette agence")
                return res.status(400).json({message: "référence client inéxistante pour cette agence"});
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyReferenceAgenceData: async (req, res, next) => {
        loggerFile.info('in verifiyReferenceAgenceData')
        let {domain, dateBegin, dateEnd} = req.body;
        try {
            if (!sector_activity.includes(domain)) {
                loggerFile.debug("error domain in verifiyReferenceAgenceData")
                return res.status(400).send({message: "le domaine est incorrect"});
            }
            if (Date.parse(dateBegin) > Date.parse(dateEnd)) {
                loggerFile.debug("Changer la période, l'intervalle est erroné")
                return res.status(400).json({message: "Changer la période, l'intervalle est erroné"});
            } else {
                loggerFile.debug("verifiyReferenceAgenceData done")
                return next()

            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

