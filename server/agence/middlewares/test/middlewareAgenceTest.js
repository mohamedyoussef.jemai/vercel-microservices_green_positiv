const Offer = require("../../models/offer.model");
const Agence = require("../../models/agence.model");
const Reference = require("../../models/reference.model");
const loggerFile = require("../../config/logger");


module.exports = {
    addOffer: async (req, res, next) => {
        loggerFile.info('in addOffer')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.body;
        try {
            let {name, domain, price, show_price, description} = req.body;
            let offer = new Offer({name, domain, price, show_price, description});
            offer.id_agence = id;
            await Offer.create(offer).then(data => {
                if (data) {
                    loggerFile.debug("addOffer data done")
                    req.offer = data;
                    return next();
                }
            }).catch(err => {
                    loggerFile.debug(err)
                    return res.status(400).json({message: "erreur lors de l'ajout de l'offre"});
                }
            );
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addReference: async (req, res, next) => {
        loggerFile.info('in addReference')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.body;
        try {
            let {client, domain, title, place, dateBegin, dateEnd, description, confidential} = req.body;
            let reference = new Reference({
                client,
                domain,
                title,
                place,
                dateBegin,
                dateEnd,
                description,
                confidential
            });
            reference.id_agence = id;
            await Reference.create(reference).then(data => {
                if (data) {
                    loggerFile.debug("addReference data done")
                    req.reference = data;
                    return next();
                }
            }).catch(err => {
                    loggerFile.debug(err)
                    return res.status(400).json({message: "erreur lors de l'ajout de la référence client"});
                }
            );
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
}

