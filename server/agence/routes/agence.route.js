const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const agenceController = require('../controllers/agenceController');

const {
    middlewareExistUsernameAgence,
    middlewareJobExist,
    middlewareExistNameAgence,
    middlewareExistEmailAgence,
    middlewareValidityCreationAgence,
    getJobs,
    middlewareTokenAgence,
    middlewareExistUsernameUpdateAgence,
    middlewareExistEmailUpdateAgence,
    middlewareExistNameAgencelUpdateAgence,
    middlewareValidityUpdateAgence
} = require('../middlewares/middlewareAgence');
const {
    addOffer,
    getOffers,
    verifiyOfferAgence,
    verifiyOfferAgenceData
} = require('../middlewares/middlewareOffer');

const {
    addReference,
    getReferences,
    verifiyReferenceAgence,
    verifiyReferenceAgenceData
} = require('../middlewares/middlewareReference');

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        console.log("extension ",ext)
        cb(null, true)
    },
}).single("image");

let uploadDocumentsOffer = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        if (ext !== ".pdf") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        cb(null, true)
    },
}).single("documents");

let uploadDocuments = multer({
    storage: multer.diskStorage({}),
}).array("documents", 4);

router.post('/', middlewareExistUsernameAgence, middlewareExistNameAgence, middlewareExistEmailAgence, middlewareValidityCreationAgence, middlewareJobExist, agenceController.create);
router.get('/all', agenceController.findAll);
router.get('/get/:id', getOffers, getReferences, getJobs, agenceController.findOne);
router.get('/find/:id', agenceController.findOneById);
router.get('/check/:id/:token', agenceController.checkEmail);
router.get('/', agenceController.findAllVisibileAndValidated);
router.get('/validated', agenceController.findAllValidated);
router.get('/unvalidated', agenceController.findAllUnvalidated);
router.patch('/block/:id', agenceController.blockAgence);
router.patch('/unblock/:id', agenceController.unblockAgence);
router.patch('/', middlewareTokenAgence, middlewareExistUsernameUpdateAgence, middlewareExistEmailUpdateAgence, middlewareExistNameAgencelUpdateAgence, middlewareValidityUpdateAgence, agenceController.update);
router.patch('/upload-profile', middlewareTokenAgence, upload, agenceController.updateImageProfile);
router.delete('/:id', agenceController.delete);
router.patch('/documents', middlewareTokenAgence, uploadDocuments, agenceController.uploadDocuments);
router.patch('/kabis-documents', middlewareTokenAgence, uploadDocuments, agenceController.uploadKabisDocuments);
router.patch('/vigilance-documents', middlewareTokenAgence, uploadDocuments, agenceController.uploadVigilanceDocuments);
router.patch('/sasu-documents', middlewareTokenAgence, uploadDocuments, agenceController.uploadSasuDocuments);
router.patch('/validate-document/:id', agenceController.validateDocumentByAdmin);
router.patch('/unvalidate-document/:id', agenceController.unvalidateDocumentByAdmin);
router.get('/documents-validated', agenceController.findAllDocumentValidated);
router.get('/documents-unvalidated', agenceController.findAllDocumentUnvalidated);

router.post("/add-offer", middlewareTokenAgence, verifiyOfferAgenceData, addOffer, agenceController.addOffer);
router.patch("/update-offer/:id", middlewareTokenAgence, verifiyOfferAgenceData, verifiyOfferAgence, agenceController.updateOffer);
router.patch("/delete-offer/:id", middlewareTokenAgence, verifiyOfferAgence, agenceController.deleteOffer);
router.patch('/documents-offer/:id', middlewareTokenAgence, verifiyOfferAgence, uploadDocumentsOffer, agenceController.uploadOfferDocuments);
router.patch('/validate-offer/:id', agenceController.validateOfferDocumentByAdmin);
router.patch('/unvalidate-offer/:id', agenceController.unvalidateOfferDocumentByAdmin);

router.post("/add-reference", upload, middlewareTokenAgence, verifiyReferenceAgenceData, addReference, agenceController.addReference);
router.patch("/update-reference/:id", upload, middlewareTokenAgence, verifiyReferenceAgenceData, verifiyReferenceAgence, agenceController.updateReference);
router.patch("/delete-reference/:id", middlewareTokenAgence, verifiyReferenceAgence, agenceController.deleteReference);

router.patch('/update-admin/:id', middlewareValidityUpdateAgence, agenceController.updateByAdmin);

router.post("/required-profile", agenceController.getRequiredProfiles);

module.exports = router;
