const express = require('express');
const router = express.Router();
const agenceController = require('../../controllers/test/agenceControllerTest');

const {
    addOffer, addReference
} = require('../../middlewares/test/middlewareAgenceTest');


router.post('/', agenceController.create);
router.get('/all', agenceController.findAll);
router.get('/offers', agenceController.getOffers);
router.get('/references', agenceController.getReferences);
router.get('/find/:id', agenceController.findOneById);
router.get('/check/:id/:token', agenceController.checkEmail);
router.patch('/', agenceController.update);
router.delete('/:id', agenceController.delete);


//offres
router.post("/add-offer", addOffer, agenceController.addOffer);
router.patch("/update-offer/:id", agenceController.updateOffer);
router.patch("/delete-offer/:id", agenceController.deleteOffer);

//references
router.post("/add-reference", addReference, agenceController.addReference);
router.patch("/update-reference/:id", agenceController.updateReference);
router.patch("/delete-reference/:id", agenceController.deleteReference);

module.exports = router;
