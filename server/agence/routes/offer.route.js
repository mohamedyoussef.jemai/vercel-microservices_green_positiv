const express = require('express');
const router = express.Router();
const offerController = require('../controllers/offerController');

router.get('/get/:id', offerController.findById);
router.patch('/:id', offerController.update);

module.exports = router;
