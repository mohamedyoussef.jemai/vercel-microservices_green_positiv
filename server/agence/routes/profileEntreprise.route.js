const express = require('express');
const router = express.Router();
const profileEntrepriseController = require('../controllers/profileEntrepriseController');
const {
    middlewareTokenAgence,
    middlewareAgenceExist,
    validateLegalForms,
    validateIbanTypeAndData,
    validateCountryLegalRepresentative,
    validateIbanCountry,
    verifyMentionLegalCreation,
    ExistParamAgence
} = require('../middlewares/middlewareAgence');

router.post('/', middlewareTokenAgence, middlewareAgenceExist, validateLegalForms, profileEntrepriseController.createContactDetails);
router.patch('/', middlewareTokenAgence, middlewareAgenceExist, validateLegalForms, profileEntrepriseController.updateContactDetails);
router.patch('/legal-representative', middlewareTokenAgence, middlewareAgenceExist, validateCountryLegalRepresentative, profileEntrepriseController.UpdateLegalRepresentative);
router.patch('/payment/iban', middlewareTokenAgence, middlewareAgenceExist, validateIbanTypeAndData, validateIbanCountry, profileEntrepriseController.updateIbanAccount);
router.patch('/taxes', middlewareTokenAgence, middlewareAgenceExist, profileEntrepriseController.UpdateTaxe);
router.patch('/legal-mention', middlewareTokenAgence, middlewareAgenceExist, verifyMentionLegalCreation, profileEntrepriseController.UpdateLegalMention);

router.get('/', profileEntrepriseController.findAll);
router.get('/:id',ExistParamAgence , profileEntrepriseController.findOne);

module.exports = router;
