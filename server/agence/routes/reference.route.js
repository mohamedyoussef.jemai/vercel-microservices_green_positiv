const express = require('express');
const router = express.Router();
const referenceController = require('../controllers/referenceController');

router.get('/get/:id', referenceController.findById);
router.patch('/:id', referenceController.update);

module.exports = router;
