require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    create: async (data) => {
        loggerFile.info('in create user');
        try {
            let test = false;
            await axios.post(`${process.env.URL_AUTH}/auth/create`, data).then(function (response) {
                if (response.status == 200 && response.data.message == "done") {
                    loggerFile.debug('add agence done');
                    test = true;
                } else {
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            return test;
        } catch (error) {
            loggerFile.error(error);
        }
    },
    delete: async (id) => {
        loggerFile.info('in delete user');
        try {
            let test = false;
            await axios.delete(`${process.env.URL_AUTH}/auth/delete/${id}`).then(function (response) {
                if (response.status == 200 && response.data.message == "done") {
                    loggerFile.debug('delete freelancer done');
                    test = true;
                } else {
                    loggerFile.debug('problem in delete user ');
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            return test;
        } catch (error) {
            loggerFile.error(error);
        }
    },
    validateCreation: async (data) => {
        loggerFile.info('in valdiateCreation user');
        try {
            let test = false;
            await axios.post(`${process.env.URL_AUTH}/auth/validate-creation`, data).then(function (response) {
                if (response.status == 200 && response.data.message == "done") {
                    loggerFile.debug('add agence done');
                    test = {state: true, message: response.data.message};
                } else {
                    test = {state: false, message: response.data.message};
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = {state: false, message: err.message};
            });
            return test;
        } catch (error) {
            loggerFile.error(error);
        }
    },
    updateNameAgence: async (data, token) => {
        loggerFile.info('in updateNameAgence user');
        try {
            let test = false;
            await axios.patch(`${process.env.URL_AUTH}/auth/update-agence`, data, {
                headers: {
                    token: token
                }
            }).then(function (response) {
                if (response.status == 200 && response.data.message == "nom agence modifié") {
                    loggerFile.debug('update agence done');
                    test = true;
                } else {
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            return test;
        } catch (error) {
            loggerFile.error(error);
        }
    },
}
