require('dotenv').config();
const Reference = require('../models/reference.model');
const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');

module.exports = {

    findById: async (req, res) => {
        loggerFile.info("in findById reference");
        try {
            let {id} = req.params;
            const reference = await Reference.findById(id);
            if (reference) {
                loggerFile.debug('in findById Reference done')
                return res.status(200).json(reference);
            } else {
                loggerFile.debug('aucune référence éxistante')
                return res.status(200).json({message: "référence inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update Reference');
        let {id} = req.params;
        let new_image = "";
        let cloudinary_id = "";
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let reference = await Reference.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    await cloudinary.uploader.destroy(reference.cloudinary_id);

                } catch (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                }
            } else {
                return res.status(400).json({message: "image inexistante"});
            }
            const newReference = req.body;
            newReference.image = new_image;
            newReference.cloudinary_id = cloudinary_id;
            await Reference.findByIdAndUpdate(id, newReference);
            loggerFile.debug('référence modifiée')
            return res.status(200).json({message: "référence modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

