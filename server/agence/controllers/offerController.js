require('dotenv').config();
const Offer = require('../models/offer.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {

    findById: async (req, res) => {
        loggerFile.info("in findById offer");
        try {
            let {id} = req.params;
            const offer = await Offer.findById(id);
            if (offer) {
                loggerFile.debug('in findById offer done')
                return res.status(200).json(offer);
            } else {
                loggerFile.debug('aucune offre éxistante')
                return res.status(200).json({message: "offre inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update offer');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newOffer = req.body;
        try {
            await Offer.findByIdAndUpdate(id, newOffer);
            loggerFile.debug('offre modifiée')
            return res.status(200).json({message: "offre modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

