require('dotenv').config();
const crypto = require('crypto');
const fs = require('fs');
const Agence = require('../../models/agence.model');
const Offer = require('../../models/offer.model');
const Reference = require('../../models/reference.model');
const notificationService = require("../../services/notificationService");
const profilEntrepriseService = require("../../services/profilEntrepriseService");
const authService = require("../../services/authService");
const mailService = require("../../services/mailingService");
const adminService = require("../../services/adminService");
const ObjectId = require('mongoose').Types.ObjectId;
const loggerFile = require("../../config/logger");


module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create agence');
        const {
            username,
            email,
            nameAgence,
            lastName,
            firstName,
            phone,
            password,
            jobCat,
            localisation,
            confidentiality
        } = req.body;
        try {
            const agence = new Agence({
                username,
                email,
                nameAgence,
                lastName,
                firstName,
                phone,
                password,
                jobCat,
                localisation,
                confidentiality
            });
            agence.role = "Agence";
            agence.code_verification = crypto.randomBytes(20).toString('hex');
            agence.validated = false;
            await Agence.create(agence);
            return res.status(200).json({message: "Agence créée"});
        } catch (error) {
            loggerFile.error(error);
            res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll agences");
        try {
            const agences = await Agence.find();
            if (agences) {
                loggerFile.debug('get all agences done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('aucune agences éxistantes')
                return res.status(200).json({message: "agences inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    findOneById: async (req, res) => {
        loggerFile.info("in findOneById agence by ID");
        const {id} = req.params;
        try {
            const agence = await Agence.findById(id);
            if (agence) {
                loggerFile.debug('get agence by id done');
                return await res.status(200).json({username: agence.username, role: agence.role});
            } else {
                loggerFile.debug('agence inéxistant');
                return res.status(200).json({message: "agence inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "agence inéxistant"});
            } else {
                loggerFile.error(error);
                return res.status(400).json({message: error});
            }
        }
    },
    //verif account via a code sended in creation account
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let agence = await Agence.findById(id);
            if (agence && agence.code_verification === token) {
                await Agence.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err.message);
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(400).json({message: "l'utilisateur est inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update agence');
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newAgence = req.body;
        try {
            await Agence.findByIdAndUpdate(id, newAgence);
            loggerFile.debug('Agence modifié')
            return res.status(200).json({message: "Agence modifiée"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    addOffer: async (req, res) => {
        loggerFile.info("in addOffer")
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let offer = req.offer;
            await Agence.findByIdAndUpdate(id, {$push: {offers: offer._id}}, {new: true, upsert: true});
            await notificationService.createAddOfferNotif(id, req.body);
            loggerFile.debug('addOffer data done')
            return res.status(200).json({message: "Offre ajoutée"});

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateOffer: async (req, res) => {
        loggerFile.info("in updateOffer")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.params.id;
            await notificationService.createUpdateOfferNotif(id, req.body)
            await Offer.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateOffer data done')
                    return res.status(200).json({message: "Offre modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err)
                    return res.status(400).json({message: "erreur lors de la mise à jour de l'offre"});
                }
            );

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteOffer: async (req, res) => {
        loggerFile.info("in deleteOffer")
        try {
            let {id} = req.body;
            let idOffer = req.params.id;
            await notificationService.createDeleteOfferNotif(id, idOffer);
            await Offer.findByIdAndDelete(idOffer);
            await Agence.findByIdAndUpdate(id, {$pull: {offers: new ObjectId(idOffer)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteOffer done');
                    return res.status(200).json({message: "Offre supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    addReference: async (req, res) => {
        loggerFile.info("in addReference")
        let {id} = req.body;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let reference = req.reference;
            await Agence.findByIdAndUpdate(id, {$push: {references: reference._id}}, {new: true, upsert: true});
            loggerFile.debug('addReference data done')
            return res.status(200).json({message: "référence client ajoutée"});

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    updateReference: async (req, res) => {
        loggerFile.info("in updateReference")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.params.id;

            await Reference.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateReference data done')
                    return res.status(200).json({message: "référence client modifiée"});
                }
            }).catch(err => {
                    loggerFile.error(err)
                    return res.status(400).json({message: "erreur lors de la mise à jour de la référence client"});
                }
            );

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    deleteReference: async (req, res) => {
        loggerFile.info("in deleteReference")
        try {
            let {id} = req.body;
            let idReference = req.params.id;
            await Reference.findByIdAndDelete(idReference);
            await Agence.findByIdAndUpdate(id, {$pull: {references: new ObjectId(idReference)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteReference done');
                    return res.status(200).json({message: "référence client supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    getOffers: async (req, res) => {
        loggerFile.info("in getOffers agences");
        try {
            const offers = await Offer.find();
            if (offers) {
                loggerFile.debug('get all offers done')
                return res.status(200).json(offers);
            } else {
                loggerFile.debug('aucune offers éxistantes')
                return res.status(200).json({message: "offers inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    getReferences: async (req, res) => {
        loggerFile.info("in getReferences agences");
        try {
            const references = await Reference.find();
            if (references) {
                loggerFile.debug('get all references done')
                return res.status(200).json(references);
            } else {
                loggerFile.debug('aucune references éxistantes')
                return res.status(200).json({message: "references inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete agence')
        const {id} = req.params;
        try {
            await Agence.findByIdAndDelete(id).then(data => {
                if (data) {
                    return res.status(200).json({message: "Agence effacé"})
                }
            }).catch(err => {
                loggerFile.error(error)
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    }
}

