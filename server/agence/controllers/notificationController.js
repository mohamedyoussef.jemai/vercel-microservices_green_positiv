const loggerFile = require("../config/logger");
const axios = require('axios').default;
const notificationServerURL = process.env.URL_NOTIFICATION;
const utils = require('../config/utils');

module.exports = {
    checkNotification: async (req, res) => {
        loggerFile.info('in checkNotification');
        let {id} = req.params;
        try {
            await axios.patch(`${notificationServerURL}/agence/check/${id}`).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('checkNotification data done');
                    return res.status(200).json({message: "notification checked"});
                } else {
                    loggerFile.debug('problem in notification checked');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotification: async (req, res) => {
        loggerFile.info('in getAllNotification');
        try {
            await axios.get(`${notificationServerURL}/agence/`).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllNotification data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in get notification');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllCheckedNotification');
        try {
            await axios.get(`${notificationServerURL}/agence/checked`).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllCheckedNotification data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in get notification checked');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllUnCheckedNotification');
        try {
            await axios.get(`${notificationServerURL}/agence/unchecked`).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllUnCheckedNotification data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in get notification unchecked');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllNotificationByPeriod');
        try {
            await axios.post(`${notificationServerURL}/agence/period`, {
                dateBegin: dateBegin,
                dateEnd: dateEnd
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllNotificationByPeriod data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in getAllNotificationByPeriod');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
            loggerFile.debug('getAllNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllCheckedNotificationByPeriod');
        try {
            await axios.post(`${notificationServerURL}/agence/period/checked`, {
                dateBegin: dateBegin,
                dateEnd: dateEnd
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllCheckedNotificationByPeriod data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in getAllCheckedNotificationByPeriod');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
            loggerFile.debug('getAllCheckedNotificationByPeriod data done');
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllUnCheckedNotificationByPeriod');
        try {
            await axios.post(`${notificationServerURL}/agence/period/unchecked`, {
                dateBegin: dateBegin,
                dateEnd: dateEnd
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('getAllUnCheckedNotificationByPeriod data done');
                    return res.status(200).json(response2.data);
                } else {
                    loggerFile.debug('problem in getAllUnCheckedNotificationByPeriod');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

