require('dotenv').config();
const crypto = require('crypto');
const Agence = require('../models/agence.model');
const Offer = require('../models/offer.model');
const Reference = require('../models/reference.model');
const notificationService = require("../services/notificationService");
const profilEntrepriseService = require("../services/profilEntrepriseService");
const authService = require("../services/authService");
const mailService = require("../services/mailingService");
const adminService = require("../services/adminService");
const ObjectId = require('mongoose').Types.ObjectId;
const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');


module.exports = {
    create: async (req, res) => {
        loggerFile.info('in create agence');
        const {
            username,
            email,
            nameAgence,
            lastName,
            firstName,
            phone,
            password,
            jobCat,
            localisation,
            confidentiality
        } = req.body;
        try {
            let testAuthCreation = await authService.validateCreation({
                username: username,
                email: email
            });
            if (!testAuthCreation.state && testAuthCreation.message != "done") {
                return res.status(400).json({message: testAuthCreation.message});
            } else {
                const agence = new Agence({
                    username,
                    email,
                    nameAgence,
                    lastName,
                    firstName,
                    phone,
                    password,
                    jobCat,
                    localisation,
                    confidentiality
                });
                agence.role = "Agence";
                agence.code_verification = crypto.randomBytes(20).toString('hex');
                agence.validated = false;
                let agenceCreated = await Agence.create(agence);
                if (agenceCreated) {
                    let data = {
                        name: "",
                        address: "",
                        address_plus: "",
                        legal_form: "Association loi 1901 - avec TVA",
                        id_agence: agenceCreated._id
                    }
                    let testProfil = await profilEntrepriseService.createContactDetails(data);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de l'ajout du profil"});

                    let testAuth = await authService.create({
                        username: username,
                        email: email,
                        password: password,
                        idUser: agenceCreated._id,
                        nameAgence: nameAgence,
                        role: "Agence"
                    });
                    mailService.create({
                        email: agenceCreated.email,
                        code_verification: agenceCreated.code_verification,
                        id: agenceCreated._id,
                        role: "Agence"
                    });
                    if (confidentiality) adminService.addSubscription(agenceCreated.email);
                    if (!testAuth) return res.status(400).json({message: "probléme lors de la création de compte"});
                    else return res.status(200).json({message: "Agence créée"});
                }
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll agences");
        try {
            const agences = await Agence.find();
            if (agences) {
                loggerFile.debug('get all agences done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('aucune agences éxistantes')
                return res.status(200).json({message: "agences inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find agence by ID");
        const {id} = req.params;
        try {
            const agence = await Agence.findById(id);
            if (agence) {
                let jobCat = "";
                await req.body.jobs.forEach(async function callback(job, index) {
                    if (agence.jobCat == job._id) {
                        jobCat = await job.name;
                    }
                });
                loggerFile.debug('get agence by id');
                return await res.status(200).json({
                    agence: agence,
                    offers: req.body.offers,
                    references: req.body.references,
                    jobCat: jobCat
                });
            } else {
                loggerFile.debug('agence inéxistante');
                return res.status(200).json({message: "agence inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "agence inéxistante"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    findOneById: async (req, res) => {
        loggerFile.info("in findOneById agence by ID");
        const {id} = req.params;
        try {
            const agence = await Agence.findById(id);
            if (agence) {
                loggerFile.debug('get agence by id done');
                return await res.status(200).json({username: agence.username, role: agence.role});
            } else {
                loggerFile.debug('agence inéxistant');
                return res.status(200).json({message: "agence inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "agence inéxistant"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    checkEmail: async (req, res) => {
        loggerFile.info('in check email')
        let {id, token} = req.params;
        if (!token || !id) {
            loggerFile.debug("informations erronés");
            return res.status(400).json({message: "informations erronés"});
        }
        try {
            let agence = await Agence.findById(id);
            if (agence && agence.code_verification === token) {
                await Agence.findByIdAndUpdate(id, {
                    code_verification: '',
                    email_verification: 1
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('Email vérifié')
                        return res.status(200).json({message: "Email vérifié"});
                    }
                }).catch(async err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "le code est erroné"});
                });
            } else {
                loggerFile.debug("l'utilisateur est inéxistant");
                return res.status(400).json({message: "l'utilisateur est inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllVisibileAndValidated: async (req, res) => {
        loggerFile.info('in findAllVisibileAndValidated')
        try {
            const agences = await Agence.find({validated: true, visibility: 1});
            if (agences) {
                loggerFile.debug('get visible and validated agences data done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('agences inéxistantes')
                return res.status(200).json({message: "agences inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllValidated: async (req, res) => {
        loggerFile.info("in findAllValidated")
        try {
            const agences = await Agence.find({validated: true});
            if (agences) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(agences);
            } else {
                loggerFile.debug("agences valides inéxistantes")
                return res.status(200).json({message: "agences valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllUnvalidated: async (req, res) => {
        loggerFile.info("in findAllUnvalidated")
        try {
            const agences = await Agence.find({validated: false});
            if (agences) {
                loggerFile.debug("findAllUnvalidated data done")
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('agences invalides inéxistantes');
                return res.status(200).json({message: "agences invalides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    blockAgence: async (req, res) => {
        loggerFile.info("in blockAgence")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {validated: false}).then(async response => {
                if (response) {
                    let agence = await Agence.findById(id);
                    loggerFile.info("in send email after bloqued")
                    mailService.block({
                        email: agence.email
                    });
                    loggerFile.debug('Agence bloquée')
                    return res.status(200).json({message: "Agence bloquée"});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unblockAgence: async (req, res) => {
        loggerFile.info("in unblockAgence")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {validated: false}).then(async response => {
                if (response) {
                    let agence = await Agence.findById(id);
                    loggerFile.info("in send email after unbloqued")
                    mailService.block({
                        email: agence.email
                    });
                    loggerFile.debug('Agence débloquée')
                    return res.status(200).json({message: "Agence débloquée"});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update agence');
        const {token} = req.headers;
        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newAgence = req.body;
        const {email, confidentiality, nameAgence} = req.body
        try {
            adminService.updateSubscription(token, email, confidentiality);
            await Agence.findByIdAndUpdate(id, newAgence).then(async data => {
                if (nameAgence) {
                    let updateNameAgence = {nameAgence: nameAgence}
                    let test = await authService.updateNameAgence(updateNameAgence, token);
                    if (test) {
                        loggerFile.debug('Agence modifié')
                        return res.status(200).json({message: "Agence modifiée"});
                    } else {
                        loggerFile.debug('Error in update User')
                        return res.status(200).json({message: "Erreur lors de la modification de l'utilisateur"});
                    }
                } else {
                    loggerFile.debug('Agence modifié')
                    return res.status(200).json({message: "Agence modifiée"});
                }

            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(200).json({message: "Erreur lors de la modification de l'agence"});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateByAdmin: async (req, res) => {
        loggerFile.info('in updateByAdmin agence');
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newagence = req.body;
        try {
            await Agence.findByIdAndUpdate(id, newagence);
            loggerFile.debug('agence modifiée')
            return res.status(200).json({message: "Agence modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateImageProfile: async (req, res) => {
        loggerFile.info('in updateImageProfile')
        let id = req.auth._idConnected;
        let new_image = "";
        let cloudinary_id = "";
        try {
            let user = await Agence.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_image)
                        await cloudinary.uploader.destroy(user.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement de l'image"})
                }
            } else {
                new_image = req.body.old_image;
            }
            user.image = await new_image;
            user.cloudinary_id = cloudinary_id;
            await Agence.findByIdAndUpdate(id, user);
            loggerFile.debug('image téléchargée avec succés')
            return res.status(200).json({message: "image téléchargée avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete agence')
        const {id} = req.params;
        try {
            await Agence.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                        await cloudinary.uploader.destroy(data.cloudinary_doc_id);

                    }
                    await Agence.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseService.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});

                    loggerFile.debug('Agence effacé')
                    return res.status(200).json({message: "Agence effacé"})
                } else {
                    loggerFile.debug('Agence inéxistant')
                    return res.status(400).json({message: "Agence inéxistant"})
                }
            }).catch(error => {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    addOffer: async (req, res) => {
        loggerFile.info("in addOffer")
        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let offer = req.offer;
            await Agence.findByIdAndUpdate(id, {$push: {offers: offer._id}}, {new: true, upsert: true});
            await notificationService.createAddOfferNotif(id, req.body);
            loggerFile.debug('addOffer data done')
            return res.status(200).json({message: "Offre ajoutée"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateOffer: async (req, res) => {
        loggerFile.info("in updateOffer")

        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.params.id;
            await notificationService.createUpdateOfferNotif(id, req.body)
            await Offer.findByIdAndUpdate(id, req.body).then(data => {
                if (data) {
                    loggerFile.debug('updateOffer data done')
                    return res.status(200).json({message: "Offre modifiée"});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de la mise à jour de l'offre"});
                }
            );

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteOffer: async (req, res) => {
        loggerFile.info("in deleteOffer")
        try {
            let id = req.auth._idConnected;
            let idOffer = req.params.id;
            await notificationService.createDeleteOfferNotif(id, idOffer);
            let offer = await Offer.findById(idOffer);
            if (offer.cloudinary_id) {
                await cloudinary.uploader.destroy(offer.cloudinary_id);
            }
            await Offer.findByIdAndDelete(idOffer);
            await Agence.findByIdAndUpdate(req.auth._idConnected, {$pull: {offers: new ObjectId(idOffer)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteOffer done');
                    return res.status(200).json({message: "Offre supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadOfferDocuments: async (req, res) => {
        loggerFile.info('in uploadDocuments')
        let {id} = req.params;
        let new_doc = "";
        let cloudinary_id = "";
        try {
            let offer = await Offer.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_doc = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_documents)
                        await cloudinary.uploader.destroy(offer.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement du fichier"})
                }
            } else {
                new_doc = req.body.old_documents;
            }
            offer.documents = await new_doc;
            offer.cloudinary_id = cloudinary_id;
            await Offer.findByIdAndUpdate(id, offer);
            await notificationService.createUploadOfferDocumentNotif(req.auth._idConnected, offer.name, req.file.filename);
            loggerFile.debug('document téléchargé avec succés')
            return res.status(200).json({message: "document téléchargé avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateOfferDocumentByAdmin: async (req, res) => {
        loggerFile.info("in validateOfferDocumentByAdmin")
        let {id} = req.params;
        try {
            await Offer.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    loggerFile.debug("validate offer")
                    loggerFile.info("in send email after validate offer")
                    let agence = await Agence.findById(data.id_agence);
                    await mailService.validateDocumentOffer({
                        email: agence.email,
                        name: data.name
                    });
                    return res.status(200).json({message: "Offre validé"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la validation de l'offre"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateOfferDocumentByAdmin: async (req, res) => {
        loggerFile.info("in unvalidateOfferDocumentByAdmin")
        let {id} = req.params;
        try {
            await Offer.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    loggerFile.debug("devalidate offer")
                    loggerFile.info("in send email after unvalidate offer")
                    let agence = await Agence.findById(data.id_agence);
                    await mailService.unvalidateDocumentOffer({
                        email: agence.email,
                        name: data.name
                    });
                    return res.status(200).json({message: "Offre dévalidé"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la dévalidation de l'offre"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentByAdmin: async (req, res) => {
        loggerFile.info("in validateDocumentByAdmin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, true);
                    loggerFile.debug("validateDocumentByAdmin update done")
                    loggerFile.info("in send email after validated document agence")
                    await mailService.validateDocument({email: data.email});
                    return res.status(200).json({message: "Documents de l'agence validés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la valdiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateDocumentByAdmin: async (req, res) => {
        loggerFile.info("in unvalidateDocumentByAdmin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, false);
                    loggerFile.debug("unvalidateDocumentByAdmin update done")
                    loggerFile.info("in send email after validated document agence")
                    await mailService.unvalidateDocument({email: data.email});
                    return res.status(200).json({message: "Documents de l'agence dévalidés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la dévaldiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllDocumentValidated: async (req, res) => {
        loggerFile.info('in findAllDocumentValidated');
        try {
            const agences = await Agence.find({documents_val: true});
            if (agences) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('agences valides inéxistants')
                return res.status(200).json({message: "agences valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllDocumentUnvalidated: async (req, res) => {
        loggerFile.info('in findAllDocumentUnvalidated');
        try {
            const agences = await Agence.find({documents_val: false});
            if (agences) {
                loggerFile.debug('agences data done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug("agences invalides inexistants")
                return res.status(200).json({message: "agences invalides inexistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error});
        }
    },
    addReference: async (req, res) => {
        loggerFile.info("in addReference")
        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let reference = req.reference;
            await Agence.findByIdAndUpdate(id, {$push: {references: reference._id}}, {new: true, upsert: true});
            loggerFile.debug('addReference data done')
            return res.status(200).json({message: "référence client ajoutée"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateReference: async (req, res) => {
        loggerFile.info('in update Reference');
        let {id} = req.params;
        let new_image = "";
        let cloudinary_id = "";
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let reference = await Reference.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    await cloudinary.uploader.destroy(reference.cloudinary_id);

                } catch (err) {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                }
            } else {
                return res.status(400).json({message: "image inexistante"});
            }
            const newReference = req.body;
            newReference.image = new_image;
            newReference.cloudinary_id = cloudinary_id;
            await Reference.findByIdAndUpdate(id, newReference);
            loggerFile.debug('référence modifiée')
            return res.status(200).json({message: "référence modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteReference: async (req, res) => {
        loggerFile.info("in deleteReference")
        try {
            let idReference = req.params.id;
            let reference = await Reference.findById(idReference);
            if (reference.cloudinary_id) {
                await cloudinary.uploader.destroy(reference.cloudinary_id);
            }
            await Reference.findByIdAndDelete(idReference);
            await Agence.findByIdAndUpdate(req.auth._idConnected, {$pull: {references: new ObjectId(idReference)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteReference done');
                    return res.status(200).json({message: "référence client supprimée"});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadKabisDocuments: async (req, res) => {
        loggerFile.info('in uploadKabisDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_kabis_id = "";
        let agence = await Agence.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new kabis');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_kabis_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[1] = docs[0]
                        newAgence.cloudinary_kabis_id = cloudinary_kabis_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadVigilanceDocuments: async (req, res) => {
        loggerFile.info('in uploadVigilanceDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_vigilance_id = "";
        let agence = await Agence.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new vigilance');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_vigilance_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[2] = docs[0]
                        newAgence.cloudinary_vigilance_id = cloudinary_vigilance_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadSasuDocuments: async (req, res) => {
        loggerFile.info('in uploadSasuDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_sasu_id = "";
        let agence = await Agence.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_sasu_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[3] = docs[0]
                        newAgence.cloudinary_sasu_id = cloudinary_sasu_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },

    uploadDocuments: async (req, res) => {
        loggerFile.info('in uploadDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let id = req.auth._idConnected;
        let docs = [];
        let cloudinary_doc_id = "";
        let agence = await Agence.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    notificationService.createUploadDocumentNotif(id, file.filename)
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_doc_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[0] = docs[0]
                        newAgence.cloudinary_doc_id = cloudinary_doc_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getRequiredProfiles: async (req, res) => {
        let finalOffers = []
        let {jobCat, minPrice, maxPrice} = req.body;
        try {
            let agences = await Agence.find({
                jobCat: jobCat,
            });
            let agencesId = agences.map(el => {
                return el._id
            })

            finalOffers = await Offer.find({
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }, documents_val: true,
                id_agence: {$in: agencesId}
            });
            return res.status(200).send({
                agences: agences.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        nameAgence: el.nameAgence,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image
                    }
                }),
                offers: finalOffers
            });
        }catch(error){
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message:error.message})
        }
    }
}

