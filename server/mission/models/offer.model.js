const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let OfferSchema = new Schema({
    name: {
        type: String,
        required: true
    },
    domain: {
        type: String,
        required: true
    },
    price: {
        type: Number
    },
    show_price: {
        type: Number,
        default: 0
    },
    description: {
        type: String,
    },
    id_agence: {
        type: String,
        default: ""
    },
    documents: {
        type: String
    },
    documents_val: {
        type: Boolean,
        default: false
    },
    cloudinary_id: {
        type: String,
    }
}, {
    collection: 'offers',
    timestamps: true
})

const Offer = mongoose.model("Offer", OfferSchema);
module.exports = Offer;

