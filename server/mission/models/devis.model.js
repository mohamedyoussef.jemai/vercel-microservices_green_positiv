const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let devisSchema = new Schema({
    id_mission: {
        type: Schema.Types.ObjectId,
        ref: 'Mission',
        required: true
    },
    id_freelance: {
        type: Schema.Types.ObjectId,
        ref: 'Freelance',
    },
    id_agence: {
        type: Schema.Types.ObjectId,
        ref: 'Agence',
    },
    id_offer: {
        type: Schema.Types.ObjectId,
        ref: 'Offer',
    },
    offer: {
        type: JSON
    },
    id_company: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
    },
    dateBegin: {
        type: Date
    },
    dateEnd: {
        type: Date
    },
    total: {
        type: Number,
    },
    totalTva: {
        type: Number,
    },
    tva: {
        type: Number,
    },
    totalGreen: {
        type: Number,
    },
    totalGreenTva: {
        type: Number,
    },
    totalUser: {
        type: Number,
    },
    confirmed: {
        type: Boolean
    },
    state: {
        type: String,
        enum: ["en cours", "terminé", "supprimé"],
        default: "en cours"
    },
    tasks: {
        type: JSON
    },
}, {
    collection: 'devis',
    timestamps: true
})

const devis = mongoose.model("Devis", devisSchema);
module.exports = devis;
