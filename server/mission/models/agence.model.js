const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let agenceSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    nameAgence: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        default: "Agence"
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    lastName: {
        type: String,
    },
    firstName: {
        type: String,
    },
    phone: {
        type: String
    },
    lastConnection: {
        type: Date,
        default: Date.now
    },
    visibility: {
        type: Number,
        default: 1
    },
    jobCat: {
        type: String,
        required: true
    },
    localisation: {
        type: String,
    },
    email_verification: {
        type: Number,
        default: 0
    },
    code_verification: {
        type: String,
    },
    confidentiality: {
        type: Boolean,
        default: 0
    },
    description: {
        type: String,
        default: ""
    },
    image: {
        type: String,
    },
    signed_client: {
        type: Boolean,
        default: 0
    },
    documents: {
        type: [{
            type: String
        }],
        default: ["", "", "", ""]
    },
    documents_val: {
        type: Boolean,
        default: 0
    },
    url_fb: {
        type: String,
        default: ''
    },
    url_twitter: {
        type: String,
        default: ''
    },
    url_linkedin: {
        type: String,
        default: ''
    },
    validated: {
        type: Boolean,
        required: true,
        Default: 0
    },
    offers: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Offer'
        }],
    },
    references: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Reference'
        }],
    },
    greenQuestion: {
        type: String,
        default: ""
    },
    cloudinary_id: {
        type: String,
    },
    cloudinary_doc_id: {
        type: String,
    },
    cloudinary_kabis_id: {
        type: String,
    },
    cloudinary_vigilance_id: {
        type: String,
    },
    cloudinary_sasu_id: {
        type: String,
    },
}, {
    collection: 'agences',
    timestamps: true
})

const Agence = mongoose.model("Agence", agenceSchema);
module.exports = Agence;

