const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let requiredProfilSchema = new Schema({
    title_profile: {
        type: String,
    },
    jobCat: {
        type: String,
        required: true
    },
    level: {
        type: String,
        enum: ['Junior', 'Intermédiaire', 'Senior'],
        default: 'Junior',
        required: true
    },
    skillsNeeded: {
        type: [{
            type: String,
        }],
    },
    skillsAppreciated: {
        type: [{
            type: String,
        }],
    },
    languages: {
        type: []
    },
}, {
    collection: 'profile-wanted',
    timestamps: true
})

const requiredProfil = mongoose.model("requiredProfil", requiredProfilSchema);
module.exports = requiredProfil;

