require('dotenv/config')
const jwt = require('jsonwebtoken');
const loggerFile = require("../config/logger");
const Company = require("../models/company.model");
const Freelancer = require("../models/freelancer.model");
const Agence = require("../models/agence.model");
const utils = require("../config/utils");

module.exports = {
    middlewareTokenCompany: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompany")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Company.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Company") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareVerifyToken: async (req, res, next) => {
        loggerFile.info("in middlewareVerifyDevis")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (decoded2.role == 'Company' || decoded2.role == 'Freelancer' || decoded2.role == 'Agence' || decoded2.role == 'Collab') {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            loggerFile.error("accés non autorisé")
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    loggerFile.error("token invalide")
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareTokenFreelancer")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Freelancer.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Freelancer") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenAgence: async (req, res, next) => {
        loggerFile.info("in middlewareTokenAgence")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Agence.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err);
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Agence") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenRoot: async (req, res, next) => {
        loggerFile.info("in middlewareTokenRoot")
        const {token} = req.headers;
        if (!token) {
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await Agence.findById(_id);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.role === "Root") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
}

