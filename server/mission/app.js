require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');
const fs = require('fs');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/mission.model');
require('./models/devis.model');
require('./models/company.model');
require('./models/freelancer.model');
require('./models/offer.model');
require('./models/agence.model');
require('./models/requiredProfil.model');
require('./models/demand.model');

app.use(logger('combined'));

let missionRouter = require('./routes/mission.route');
let freelancerRouter = require('./routes/freelancer.route');
let agenceRouter = require('./routes/agence.route');
let demandRouter = require('./routes/demand.route');
let paymentRouter = require('./routes/payment.route');

app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("uploads"));

app.use(cors());
app.use(express.json());
app.use('/payment', paymentRouter);
app.use('/missions', missionRouter);
app.use('/freelancer', freelancerRouter);
app.use('/agence', agenceRouter);
app.use('/demands', demandRouter);

mongoose.connect(process.env.DB_URI_MISSION_LOCAL_PROD, {
    useUnifiedTopology: true,
}).then(() => {
    app.listen(port, () => {
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.error(err);
    console.info("error ", err.message);
});

module.exports = app;
