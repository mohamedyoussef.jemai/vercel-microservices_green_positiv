const express = require('express');
const router = express.Router();
const freelancerController = require('../controllers/freelancerController');

const {
    middlewareTokenFreelancer
} = require('../middlewares/middlewareUser');
const {
    verifInvite,
    verifDevisExistByIdUser,
    verifDisponibility,
    verifWorkTimeDisponibility
} = require('../middlewares/middlewareMission');

router.post('/send-devis', middlewareTokenFreelancer, verifDisponibility, verifInvite, verifDevisExistByIdUser,verifWorkTimeDisponibility, freelancerController.sendDevis);
router.get("/devis", middlewareTokenFreelancer, freelancerController.getDevisFreelance);

module.exports = router;
