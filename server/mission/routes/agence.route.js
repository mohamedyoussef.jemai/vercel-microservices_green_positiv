const express = require('express');
const router = express.Router();
const agenceController = require('../controllers/agenceController');


const {
    middlewareTokenAgence
} = require('../middlewares/middlewareUser');
const {
    verifInvite,
    verifDevisExistByIdUser,
    verifDisponibility
} = require('../middlewares/middlewareMission');


router.post('/send-devis', middlewareTokenAgence, verifDisponibility, verifInvite, verifDevisExistByIdUser, agenceController.sendDevis);
router.get("/devis",middlewareTokenAgence, agenceController.getDevisAgence);
router.get("/offers",middlewareTokenAgence, agenceController.getOfferAgence);

module.exports = router;
