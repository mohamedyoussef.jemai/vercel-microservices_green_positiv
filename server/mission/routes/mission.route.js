const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const missionController = require('../controllers/missionController');

const {
    middlewareTokenCompany,
    middlewareVerifyToken
} = require('../middlewares/middlewareUser');
const {
    addMission,
    verifiyMissionCompany,
    addRequiredProfil,
    middlewareJobExist,
    updateRequiredProfil,
    verifFreelanceExist,
    verifAgenceExist,
    verifDevisExistByIdUser
} = require('../middlewares/middlewareMission');

let uploadOneDocument = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        cb(null, true)
    },
}).single("document");

//missions
router.post("/", uploadOneDocument, middlewareTokenCompany, middlewareJobExist, addRequiredProfil, addMission, missionController.create);
router.patch("/:id", uploadOneDocument, middlewareTokenCompany, verifiyMissionCompany, updateRequiredProfil, missionController.update);
router.delete("/:id", middlewareTokenCompany, verifiyMissionCompany, missionController.delete);
router.get("/get/:id",middlewareVerifyToken,verifDevisExistByIdUser, missionController.findOne);
router.get("/profiles/:id", middlewareTokenCompany, verifiyMissionCompany, missionController.findFreelancedSearched);
router.post("/profiles-agences/:id", middlewareTokenCompany, verifiyMissionCompany, missionController.findAgenceSearched);

router.get("/devis", middlewareTokenCompany, missionController.getDevisFromMission);
router.get("/devis/:id", middlewareTokenCompany, missionController.getDevisFromMissionById);
router.get("/get-devis/:id", middlewareTokenCompany, missionController.getDevisById);
router.patch("/freelance/accept/:id", middlewareTokenCompany, verifFreelanceExist, missionController.acceptFreelance);
router.patch("/freelance/refuse/:id", middlewareTokenCompany, verifFreelanceExist, missionController.refuseFreelance);
router.post("/freelance/remove/:id",middlewareTokenCompany, verifFreelanceExist, missionController.removeFreelance);

router.patch("/agence/accept/:id", middlewareTokenCompany, verifAgenceExist, missionController.acceptAgence);
router.patch("/agence/refuse/:id", middlewareTokenCompany, verifAgenceExist, missionController.refuseAgence);
router.post("/agence/remove/:id", middlewareTokenCompany, verifAgenceExist, missionController.removeAgence);

router.get('/all', middlewareTokenCompany, missionController.findCompanyMissions);

module.exports = router;
