const express = require('express');
const router = express.Router();
const stripeController = require('../controllers/stripeController');

const {
    middlewareTokenCompany,
    middlewareTokenFreelancer,
    middlewareTokenAgence
} = require('../middlewares/middlewareUser');

router.post("/get/:id", middlewareTokenCompany, express.raw({type: "application/json"}), stripeController.create);
router.get("/freelance", middlewareTokenFreelancer, stripeController.findAllFreelancePayment);
router.get("/agence", middlewareTokenAgence, stripeController.findAllAgencePayment);
router.post("/", middlewareTokenCompany, stripeController.getPayment);

module.exports = router;
