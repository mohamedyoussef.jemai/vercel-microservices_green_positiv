const express = require('express');
const router = express.Router();
const demandController = require('../controllers/demandController');

const {
    middlewareTokenCompany,
    middlewareTokenRoot,
} = require('../middlewares/middlewareUser');
const {
    verifiyMissionCompany,
} = require('../middlewares/middlewareMission');

router.post("/:id", middlewareTokenCompany,verifiyMissionCompany, demandController.create);
router.get("/", middlewareTokenCompany, demandController.findAllByIdUser);
router.get("/:id", middlewareTokenCompany, demandController.findDemandByIdAndByIdUser);
router.patch("/validate/:id", middlewareTokenCompany, demandController.validateDemand);
router.patch("/unvalidate/:id", middlewareTokenCompany, demandController.unValidateDemand);

router.patch("/:id", demandController.updateSearch);
router.get("/all", middlewareTokenRoot, demandController.findAllInProcess);

module.exports = router;
