require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    findAgence: async (id) => {
        try {
            let data = {}
            await axios.get(`${process.env.URL_AGENCE}/agence/get/${id}`).then(function (response) {
                console.log("response", response)
                if (response.data) {
                    loggerFile.debug('findAgence');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get agence by id ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                return {status: 400};
            });
            return data;
        } catch (error) {
            l
            return {status: 400};
        }
    },
    getRequiredAgenceProfiles: async (profile, minPrice, maxPrice) => {
        loggerFile.info('in getProfiles');
        let data = {
            jobCat: profile.jobCat,
            minPrice: minPrice,
            maxPrice: maxPrice
        }
        try {
            await axios.post(`${process.env.URL_AGENCE}/agence/required-profile`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('getProfiles');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get required profiles');
                    data = {status: 400};
                }
            }).catch(function (err) {
                return {status: 400};
            });
            return data;
        } catch (error) {
            return {status: 400};
        }
    },
}
