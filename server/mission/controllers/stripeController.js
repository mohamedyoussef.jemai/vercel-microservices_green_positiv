const Devis = require('../models/devis.model');
const Payment = require('../models/payment.model');
const Mission = require('../models/mission.model');
const Company = require('../models/company.model');
const loggerFile = require("../config/logger");
const stripe = require("../config/stripe")
const utils = require("../config/utils")
module.exports = {
    create: async (req, res) => {
        const {id} = req.params;
        const {_idConnected} = req.auth;
        const {amount} = req.body
        try {
            const devis = await Devis.findById(id)
            if (devis.id_company.equals(_idConnected)) {
                if (devis.confirmed == false || devis.confirmed == undefined) {
                    const paymentIntent = await stripe.paymentIntents.create({
                        amount: devis.totalTva * 0.3 * 100,
                        currency: "eur",
                    })
                    const payment = new Payment({
                        id_company: devis.id_company,
                        id_mission: devis.id_mission,
                        amount: devis.totalTva * 0.3,
                        totalTva: devis.totalTva,
                        totalTvaUnpayed: devis.totalTva * 0.7
                    });
                    if (devis.id_freelance) payment.id_freelance = devis.id_freelance
                    else if (devis.id_agence) payment.id_agence = devis.id_agence
                    await Payment.create(payment)
                    return res.status(200).json({secret: paymentIntent.client_secret})
                } else {
                    const paymentIntent = await stripe.paymentIntents.create({
                        amount: amount * 100,
                        currency: "eur",
                    })
                    const payment = new Payment({
                        id_company: devis.id_company,
                        id_mission: devis.id_mission,
                        amount: amount,
                        totalTva: devis.totalTva,
                    });
                    if (devis.id_freelance) payment.id_freelance = devis.id_freelance
                    else if (devis.id_agence) payment.id_agence = devis.id_agence
                    await Payment.create(payment)
                    return res.status(200).json({secret: paymentIntent.client_secret})
                }

            } else return res.status(401).json({message: 'Action non autorisée.'})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllFreelancePayment: async (req, res) => {
        loggerFile.info("in findAllFreelancePayment demand")
        let id = req.auth._idConnected;
        try {
            let messages = []
            let payments = await Payment.find({id_freelance: id});
            payments.forEach(async function (item, index, array) {
                let mission = await Mission.findById(item.id_mission)
                let company = await Company.findById(item.id_company)
                await messages.push({mission: mission.name, company: company.firstName + " " + company.lastName})
                if (index == array.length - 1) {
                    return res.status(200).json({payments: messages})
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }, findAllAgencePayment: async (req, res) => {
        loggerFile.info("in findAllAgencePayment demand")
        let id = req.auth._idConnected;
        try {
            let messages = []
            let payments = await Payment.find({id_agence: id});
            payments.forEach(async function (item, index, array) {
                let mission = await Mission.findById(item.id_mission)
                let company = await Company.findById(item.id_company)
                await messages.push({mission: mission.name, company: company.firstName + " " + company.lastName})
                if (index == array.length - 1) {
                    return res.status(200).json({payments: messages})
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getPayment: async (req, res) => {
        loggerFile.info("in getPayment")
        let id = req.auth._idConnected;
        try {
            let payments = []
            const {id_freelance, id_agence, id_mission} = req.body
            let amount = 0;
            if (id_freelance) {
                payments = await Payment.find({
                    id_company: id,
                    id_freelance: id_freelance,
                    id_mission: id_mission
                });
            } else {
                payments = await Payment.find({
                    id_company: id,
                    id_agence: id_agence,
                    id_mission: id_mission
                });
            }
            payments.forEach(async function (item, index, array) {
                amount += item.amount
                if (index == array.length - 1) {
                    return res.status(200).json({unpayed: item.totalTva - amount})
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
