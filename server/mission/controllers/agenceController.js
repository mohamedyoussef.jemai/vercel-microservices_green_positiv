require('dotenv').config();
const Devis = require('../models/devis.model');
const Mission = require('../models/mission.model');
const Offer = require('../models/offer.model');
const Payment = require('../models/payment.model');
const loggerFile = require("../config/logger");
const utils = require("../config/utils");

module.exports = {
    sendDevis: async (req, res) => {
        loggerFile.info('in sendDevis');
        let id_agence = req.auth._idConnected;
        let {
            id_company, id_mission, dateBegin, dateEnd, tasks, total,
            totalTva,
            tva,
            totalGreen,
            totalGreenTva,
            totalUser,
            offer,
            id_offer
        } = req.body;
        try {
            let oldDevis = await Devis.find({
                id_company: id_company,
                id_mission: id_mission,
                id_agence: id_agence,
                confirmed: false,
                state: "terminé",
                id_offer: id_offer
            });
            if (oldDevis.length > 0) {
                let newDevis = {
                    id_company, id_agence, id_mission, dateBegin, dateEnd, tasks, total,
                    totalTva,
                    tva,
                    totalGreen,
                    totalGreenTva,
                    totalUser,
                    offer,
                    id_offer
                };
                newDevis.state = "en cours"
                newDevis.total = total
                await Devis.findByIdAndUpdate(oldDevis[0]._id, newDevis);
                return res.status(200).json({message: "Devis réenvoyé"});
            }
            let devis = new Devis({
                id_company, id_agence, id_mission, dateBegin, dateEnd, tasks, total,
                totalTva,
                tva,
                totalGreen,
                totalGreenTva,
                totalUser,
                offer,
                id_offer
            });
            await Devis.create(devis);
            return res.status(200).json({message: "Devis envoyé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisAgence: async (req, res) => {
        loggerFile.info('in getDevisAgence');
        let id = req.auth._idConnected;
        let missions = []
        let i = 0;
        let unpayedAmounts = [];
        try {
            let devises = await Devis.find({id_agence: id});
            if (devises.length > 0) {
                while (devises.length != unpayedAmounts.length) {
                    let mission = await Mission.findById(devises[i].id_mission);
                    await missions.push(mission)
                    if (devises[i].confirmed && devises[i].confirmed == true) {
                        var amount = 0
                        let payments = await Payment.find({id_mission: mission._id, id_agence: devises[i].id_agence})
                        payments.forEach(async function (item2, index2, array2) {
                            amount += item2.amount
                            if (index2 + 1 == array2.length) {
                                amount = (amount * 100) / item2.totalTva
                                unpayedAmounts.push(Math.round(amount))
                                i++
                            }
                        });
                    } else {
                        unpayedAmounts.push(null)
                    }
                }
                return res.status(200).json({
                    devises: devises, missions: missions.map(el => {
                        return {
                            _id: el._id,
                            name: el.name,
                            id_company: el.id_company
                        }
                    }),
                    unpayedAmounts: unpayedAmounts
                })
            } else {
                return res.status(200).json([])
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getOfferAgence: async (req, res) => {
        loggerFile.info('in getOfferAgence');
        let id = req.auth._idConnected;
        try {
            let offers = await Offer.find({id_agence: id, documents_val: true});
            return res.status(200).json(
                offers.map(el => {
                    return {
                        _id: el._id,
                        name: el.name,
                        price: el.price,
                        description: el.description,
                        documents: el.documents,
                    }
                })
            );
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
