const nodemailer = require("nodemailer");
const loggerFile = require("../config/logger");

const sendEmail = async (email, subject, text) => {
    try {
        const transporter = nodemailer.createTransport({
            host: "smtp-mail.outlook.com",
            secureConnection: false,
            port: 587,
            tls: {
                ciphers:'SSLv3'
            },
            auth: {
                user: process.env.EMAIL_SEND,
                pass: process.env.EMAIL_PASSWORD,
            }
        });
        await transporter.sendMail({
            from: process.env.EMAIL_SEND,
            to: email,
            subject: subject,
            text: text,
        });
    } catch (error) {
        loggerFile.error("email not sent")
    }
};

module.exports = sendEmail;
