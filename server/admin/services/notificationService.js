require('dotenv').config()
const Admin = require('../models/Admin.model');
const Post = require('../models/post.model');
const loggerFile = require("../config/logger");
const notificationServerURL = process.env.URL_NOTIFICATION;
const axios = require('axios').default;
const freelanceServerURL =process.env.URL_FREELANCER

module.exports = {
    postCreatedNotif: async (id, body) => {
        loggerFile.info("in postCreatedNotif");
        try {
            let user = await Admin.findById(id);
            let changes = {};
            changes.title = body.title;
            changes.content = body.content;
            changes.url_fb = body.url_fb;
            changes.url_instagram = body.url_instagram;
            changes.url_twitter = body.url_twitter;
            changes.url_linkedin = body.url_linkedin;
            changes.resume = body.resume;
            changes.author = body.author;
            changes.image = body.image;
            changes.state = 'created';
            changes.idPost = body._id;

            let username = user.username;
            let role = user.role;

            await axios.post(`${notificationServerURL}/admin/create`, {
                username: username,
                role: role,
                changes: changes
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('notification post create done');
                } else {
                    loggerFile.debug('problem notification post create');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            loggerFile.debug('postCreated data done')

        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    updatePostNotif: async (id, idPost, body) => {
        loggerFile.info("in updatePostNotif");
        let changes = {};
        try {
            let user = await Admin.findById(id);
            let post = await Post.findById(idPost);
            if (post.title != body.title) {
                changes.title = body.title;
            }
            if (post.content != body.content) {
                changes.content = "modifié";
            }
            if (post.image != body.image) {
                changes.image = body.image;
            }
            if (post.url_fb != body.url_fb) {
                changes.url_fb = body.url_fb;
            }
            if (post.url_instagram != body.url_instagram) {
                changes.url_instagram = body.url_instagram;
            }
            if (post.url_twitter != body.url_twitter) {
                changes.url_twitter = body.url_twitter;
            }
            if (post.url_linkedin != body.url_linkedin) {
                changes.url_linkedin = body.url_linkedin;
            }
            if (post.resume != body.resume) {
                changes.resume = body.resume;
            }
            let username = user.username;
            let role = user.role;
            changes.state = "updated"
            changes.idPost = post._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/admin/update`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification post update done');
                    } else {
                        loggerFile.debug('problem notification post update');
                        res.status(401).json({message: response2.data.message});
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                });
                loggerFile.debug('updatePost data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    validatedPostNotif: async (idPost) => {
        loggerFile.info("in validatedPostNotif");
        let post = await Post.findById(idPost);
        let username = post.author;
        let changes = {};
        changes.state = "published"
        changes.message = "l'article " + post.title + " a été publié"
        changes.idPost = post._id;
        try {
            await axios.patch(`${notificationServerURL}/admin/validate`, {
                username: username,
                changes: changes
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('notification post validate done');
                } else {
                    loggerFile.debug('problem notification post validate');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
            loggerFile.debug('validatedPostNotif data done')

        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    unvalidatedPostNotif: async (idPost) => {
        loggerFile.info("in unvalidatedPostNotif");
        let post = await Post.findById(idPost);
        let username = post.author;
        let changes = {};
        changes.message = "l'article " + post.title + " a été bloqué"
        changes.state = "bloqued"
        changes.idPost = post._id;
        try {
            await axios.patch(`${notificationServerURL}/admin/unvalidate`, {
                username: username,
                changes: changes
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('notification post unvalidate done');
                } else {
                    loggerFile.debug('problem notification post unvalidate');
                    res.status(401).json({message: response2.data.message});
                }
            }).catch(function (err) {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
            loggerFile.debug('unvalidatedPostNotif data done')

        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createDetailsContactNotif: async (id, body) => {
        loggerFile.info("in createDetailsContactNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntreprise.findOne({id_freelancer: id});
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/get/${id}`);

            let {name, address, address_plus, legal_form} = profile;
            if (name != body.name) {
                changes.name = body.name;
            }
            if (address != body.address) {
                changes.address = body.address;
            }
            if (address_plus != body.address_plus) {
                changes.address_plus = body.address_plus;
            }
            if (legal_form != body.legal_form) {
                changes.legal_form = body.legal_form;
            }
            changes.state = "details";
            changes.idFreelance = freelancer._id;
            let username = freelancer.username;
            let role = freelancer.role;
            await axios.patch(`${notificationServerURL}/freelance/create-details-contact`, {
                username: username,
                role: role,
                changes: changes
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('notification create details contact done');
                    test = true;
                } else {
                    loggerFile.debug('problem notification post create');
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            loggerFile.debug('createDetailsContactNotif data done')
            return test;
        } catch (error) {
            loggerFile.error(error)
        }
    },
    createLegalRepresentativeNotif: async (id, body) => {
        loggerFile.info("in createLegalRepresentativeNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntreprise.findOne({id_freelancer: id});
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/find/${id}`);
            let {lastname, firstname, birthday, postal, city_of_birth, country_of_birth, nationality} = profile;
            if (lastname != body.lastname) {
                changes.lastname = body.lastname;
            }
            if (firstname != body.firstname) {
                changes.firstname = body.firstname;
            }
            if (birthday != body.birthday) {
                changes.birthday = body.birthday;
            }
            if (postal != body.postal) {
                changes.postal = body.postal;
            }
            if (city_of_birth != body.city_of_birth) {
                changes.city_of_birth = body.city_of_birth;
            }
            if (country_of_birth != body.country_of_birth) {
                changes.country_of_birth = body.country_of_birth;
            }
            if (nationality != body.nationality) {
                changes.nationality = body.nationality;
            }
            let username = freelancer.username;
            let role = freelancer.role;
            changes.state = "representation";
            changes.idFreelance = freelancer._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/freelance/create-legal-representative`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create legal representative done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification legal reprensetative create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createLegalRepresentativeNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return true;
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createTaxeNotif: async (id, body) => {
        loggerFile.info("in createTaxeNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntreprise.findOne({id_freelancer: id});
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/find/${id}`);
            let {taxe} = profile;
            if (taxe != body.taxe) {
                changes.taxe = body.taxe;
            }
            changes.state = "taxe";
            changes.idFreelance = freelancer._id;
            let username = freelancer.username;
            let role = freelancer.role;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/freelance/taxe`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create taxe done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification taxe create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createTaxeNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return true;
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createLegalMentionNotif: async (id, body) => {
        loggerFile.info("in createLegalMentionNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntreprise.findOne({id_freelancer: id});
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/find/${id}`);
            let {sas, siret, rcs, naf, tva_intracom, days} = profile;
            if (sas != body.sas) {
                changes.sas = body.sas;
            }
            if (siret != body.siret) {
                changes.siret = body.siret;
            }
            if (rcs != body.rcs) {
                changes.rcs = body.rcs;
            }
            if (naf != body.naf) {
                changes.naf = body.naf;
            }
            if (tva_intracom != body.tva_intracom) {
                changes.tva_intracom = body.tva_intracom;
            }
            if (days != body.days) {
                changes.days = body.days;
            }
            let username = freelancer.username;
            let role = freelancer.role;
            changes.state = "mention";
            changes.idFreelance = freelancer._id;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/freelance/legal-mention`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create legal mention done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification legal mention create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createLegalMentionNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return test;
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createIbanNotif: async (id, body) => {
        loggerFile.info("in createIbanNotif");
        let changes = {};
        try {
            let profile = await profileEntreprise.findOne({id_freelancer: id});
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/find/${id}`);
            let test = false;
            switch (body.type_iban) {
                case "iban": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_iban
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_iban != body.cb_iban_iban) {
                        changes.cb_iban_iban = body.cb_iban_iban;
                    }
                    changes.state = "iban";
                    changes.idFreelance = freelancer._id;
                }
                    break;
                case "iban-us": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_aba_transit_number,
                        cb_iban_account_type
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_aba_transit_number != body.cb_iban_aba_transit_number) {
                        changes.cb_iban_aba_transit_number = body.cb_iban_aba_transit_number;
                    }
                    if (cb_iban_account_type != body.cb_iban_account_type) {
                        changes.cb_iban_account_type = body.cb_iban_account_type;
                    }
                    changes.state = "iban-us";
                    changes.idFreelance = freelancer._id;
                }
                    break;
                case "iban-ca": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_bank_name,
                        cb_iban_branch_code,
                        cb_iban_number_institution
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_bank_name != body.cb_iban_bank_name) {
                        changes.cb_iban_bank_name = body.cb_iban_bank_name;
                    }
                    if (cb_iban_branch_code != body.cb_iban_branch_code) {
                        changes.cb_iban_branch_code = body.cb_iban_branch_code;
                    }
                    if (cb_iban_number_institution != body.cb_iban_number_institution) {
                        changes.cb_iban_number_institution = body.cb_iban_number_institution;
                    }
                    changes.state = "iban-ca";
                    changes.idFreelance = freelancer._id;
                }
                    break;
                case "others": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_bic_swift,
                        cb_iban_account_country
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_bic_swift != body.cb_iban_bic_swift) {
                        changes.cb_iban_bic_swift = body.cb_iban_bic_swift;
                    }
                    if (cb_iban_account_country != body.cb_iban_account_country) {
                        changes.cb_iban_account_country = body.cb_iban_account_country;
                    }
                    changes.state = "iban-others";
                    changes.idFreelance = freelancer._id;
                }
                    break;
            }

            let username = freelancer.username;
            let role = freelancer.role;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/freelance/iban`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create iban done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification iban create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createIbanNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return true;
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createUploadDocumentNotif: async (id, filename) => {
        loggerFile.info("in createUploadDocumentNotif");
        let changes = {};
        try {
            let freelancer = await axios.get(`${freelanceServerURL}/freelancer/find/${id}`);
            changes.filename = filename;
            let username = freelancer.username;
            let role = freelancer.role;
            changes.state = "document";
            changes.idFreelance = freelancer._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/freelance/upload-document`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create upload document done');
                    } else {
                        loggerFile.debug('problem notification upload document create');
                        res.status(401).json({message: response2.data.message});
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                });
                loggerFile.debug('createUploadDocumentNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
}


