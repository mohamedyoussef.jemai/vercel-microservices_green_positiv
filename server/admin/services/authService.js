require('dotenv').config();
const loggerFile = require("../config/logger");
const User = require("../models/user.model")
const utils = require('../config/utils');

module.exports = {
    block: async (id) => {
        loggerFile.info('in block');
        let test = false;
        try {
            await User.findOneAndUpdate({idUser: id}, {validated: false}).then(response => {
                if (response) {
                    loggerFile.debug("user blocked ")
                    test = true;
                }
            }).catch(err => {
                loggerFile.error(utils.getClientAddress("user inexistant", err))
                test = false
            });
        } catch (error) {
            loggerFile.error(error);
            test = false;
        }
        return test
    },
    unblock: async (id) => {
        loggerFile.info('in block');
        let test = false;
        try {
            await User.findOneAndUpdate({idUser: id}, {validated: true}).then(response => {
                if (response) {
                    loggerFile.debug("user blocked ")
                    test = true;
                }
            }).catch(err => {
                loggerFile.debug("user inéxistant ", err)
                test = false
            });
        } catch (error) {
            loggerFile.error(error);
            test = false;
        }
        return test
    },
    delete: async (id) => {
        loggerFile.info('in delete user')
        let test = false;
        try {
            await User.findOneAndDelete({idUser: id}).then(data => {
                if (data) test = true;
                else test = false
            }).catch((err) => {
                loggerFile.error(err)
                test = false
            });
            loggerFile.debug('User effacé')
            return test
        } catch (error) {
            loggerFile.error(error)
            return false
        }
    },
}
