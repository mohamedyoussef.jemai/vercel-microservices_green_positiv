const axios = require('axios').default;
const profileEntreprise = require("../models/profileEntreprise.model");
const loggerFile = require("../config/logger");

module.exports = {
    createContactDetails: async (data) => {
        loggerFile.info("in createContactDetails")
        try {
            let test = false;
            await axios.post(`${profilServerURL}/profil-entreprise/`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("createContactDetails data done")
                    test = true
                } else {
                    loggerFile.debug('problem in createContactDetails');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false
            });
            return test;
        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateContactDetails: async (data) => {
        loggerFile.info("in updateContactDetails")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise/`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('updateContactDetails data done')
                    test = true;
                } else {
                    loggerFile.debug('problem in updateContactDetails');
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err.message);
                test = false;
            });
            return test;
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    updateLegalRepresentative: async (data) => {
        loggerFile.info("in UpdateLegalRepresentative")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise/legal-representative`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("UpdateLegalRepresentative data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in UpdateLegalRepresentative');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateIbanAccount: async (data) => {
        loggerFile.info("in updateIbanAccount")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise/payment/iban`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("updateIbanAccount data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in updateIbanAccount');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {
            loggerFile.error(error)
        }

    },
    updateTaxe: async (data) => {
        loggerFile.info("in UpdateTaxe")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise/taxes`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("UpdateTaxe data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in UpdateTaxe');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {
            loggerFile.error(error)
        }
    },
    updateLegalMention: async (data) => {
        loggerFile.info('in UpdateLegalMention')
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise/legal-mention`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("UpdateLegalMention data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in UpdateLegalMention');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {
            loggerFile.error(error)
        }
    },
    delete: async (id) => {
        loggerFile.info('in delete profile');
        let test = false;
        try {
            await profileEntreprise.findOneAndDelete({id_freelancer: id}).then(data => {
                if(data) test= true
                else  test = false;
            }).catch((err)=>{
                test = false;
            });
            return test
        } catch (error) {
            return false
            loggerFile.error(error);
        }
    },
    validate: async (id, bool) => {
        loggerFile.info('in validate')
        try {
            await profileEntreprise.findOneAndUpdate({id_freelancer: id}, {validated: bool});
            loggerFile.debug("validate data done")
            return;
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
}
