require('dotenv').config();
const Freelancer = require('../models/freelancer.model')
const missionService = require('../services/missionService')
module.exports = {
    getRequiredProfiles: async (profile) => {
        let finalFreelancers = []
        let {title_profile, jobCat, level, skillsNeeded, skillsAppreciated, languages} = profile;
        let freelancers = await Freelancer.find({
            title_profile: title_profile,
            jobCat: jobCat,
            level: level,
            visibility: 1,
            validated: true
        });
        let freelancersNeeded = []
        let freelancersAppreciated = []
        await freelancers.forEach(async function (freelance, index, array2) {
            let testLanguage = await missionService.testLanguage(freelance, languages);
            if (testLanguage) finalFreelancers.push(freelance)
        })

        await freelancers.forEach(async function (freelance, index, array) {
            let testSkillsNeeded = await missionService.testSkillsNeeded(freelance, skillsNeeded);
            if (testSkillsNeeded) freelancersNeeded.push(freelance)
        })
        await freelancers.forEach(async function (freelance, index, array2) {
            let testSkillsAppreciated = await missionService.testSkillsNeeded(freelance, skillsAppreciated);
            if (testSkillsAppreciated) freelancersAppreciated.push(freelance)
        })

        return {
            data: {
                freelancers: finalFreelancers.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        languages: el.languages
                    }
                }),
                freelancersNeeded: freelancersNeeded.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                }),
                freelancersAppreciated: freelancersAppreciated.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                })
            }
        };
    },
}
