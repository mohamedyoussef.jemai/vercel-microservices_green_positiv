require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    create: async (data) => {
        loggerFile.info('in create');
        try {
            let test = false;
            await axios.post(`${process.env.URL_MAIL}/email/create`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('mail de création envoyé');
                    test = true;
                } else {
                    loggerFile.debug("probléme lors de l'envoi du mail ");
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            return test;
        } catch (error) {
            loggerFile.error(error);
        }
    },
    block: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/block`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('mail de blocage envoyé');
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email de blocage ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
    unblock: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/unblock`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('mail de déblocage envoyé');
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email de blocage ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
    unvalidateDocument: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/unvalidateDocument`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("mail d' invalidation document envoyé");
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email d'invalidation de document ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
    validateDocument: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/validateDocument`, {
                email: data.email
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug('mail de validation document envoyé');
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email de validation de document ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
    validateDocumentOffer: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/validateOffer`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("mail de validation document envoyé");
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email de validation de document ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
    unvalidateDocumentOffer: async (data) => {
        try {
            await axios.post(`${process.env.URL_MAIL}/email/unvalidateOffer`, data).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("mail d' invalidation document envoyé");
                } else {
                    loggerFile.debug("probléme lors de l'envoi dr l'email d'invalidation de document ");
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
        } catch (error) {
            loggerFile.error(error);
        }
    },
}
