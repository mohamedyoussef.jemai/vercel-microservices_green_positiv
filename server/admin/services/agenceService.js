require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");
const Agence = require('../models/agence.model')
const Offer = require('../models/offer.model')
const utils = require('../config/utils');

module.exports = {
    findAgence: async (id) => {
        try {
            let data = {}
            await axios.get(`${process.env.URL_AGENCE}/agence/get/${id}`).then(function (response) {
                if (response.data) {
                    loggerFile.debug('findAgence');
                    data = {status: 200, data: response.data};
                } else {
                    loggerFile.debug('problem in get agence by id ');
                    data = {status: 400};
                }
            }).catch(function (err) {
                loggerFile.error(utils.getClientAddress(req, err))
                data = {status: 400};
            });
            return data;
        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return data = {status: 400};
        }
    },
    getRequiredAgenceProfiles: async (profile,minPrice,maxPrice) => {
        loggerFile.info('in getProfiles');
        let data = {
            jobCat: profile.jobCat,
            minPrice: minPrice,
            maxPrice: maxPrice
        }
        try {
            let finalOffers = []
            let {jobCat} = profile;
            let agences = await Agence.find({
                jobCat: jobCat,
            });
            let agencesId = agences.map(el => {
                return el._id
            })

            finalOffers = await Offer.find({
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }, documents_val: true,
                id_agence: {$in: agencesId}
            });
            return {data:{
                    agences: agences.map(el => {
                        return {
                            _id: el._id,
                            username: el.username,
                            email: el.email,
                            nameAgence: el.nameAgence,
                            lastName: el.lastName,
                            firstName: el.firstName,
                            phone: el.phone,
                            image: el.image
                        }
                    }),
                    offers: finalOffers
                }};

        } catch (error) {
            loggerFile.error(utils.getClientAddress(req, error))
            return data;
        }
    },
}
