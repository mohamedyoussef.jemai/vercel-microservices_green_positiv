const axios = require('axios').default;
const profilServerURL = process.env.URL_PROFIL;
const profileEntrepriseCompany = require("../models/profileEntrepriseCompany.model")
const loggerFile = require("../config/logger");

module.exports = {
    delete: async (id) => {
        loggerFile.info('in delete profile');
        let test = false;
        try {
            await profileEntrepriseCompany.findOneAndDelete({id_company: id}).then(data => {
                if(data) test= true
                else  test = false;
            }).catch((err)=>{
                test = false;
            });
            return test
        } catch (error) {
            return false
            loggerFile.error(error);
        }
    },
    validate: async (id, bool) => {
        loggerFile.info('in validate')
        try {
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, {validated: bool});
            loggerFile.debug("validate data done")
            return;
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    updateFacturation: async (id, token, data) => {
        loggerFile.info("in updateFacturation")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise-company/admin/facturation/${id}`, data, {
                headers: {
                    token: token
                }
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("updateFacturation data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in updateFacturation');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {

            loggerFile.error(error)
        }
    },
    updateContact: async (id, token, data) => {
        loggerFile.info("in updateContact")
        try {
            let test = false;
            await axios.patch(`${profilServerURL}/profil-entreprise-company/admin/contact/${id}`, data, {
                headers: {
                    token: token
                }
            }).then(function (response) {
                if (response.status == 200) {
                    loggerFile.debug("updateContact data done")
                    test = true;

                } else {
                    loggerFile.debug('problem in updateContact');
                    test = false
                }
            }).catch(function (err) {
                loggerFile.error(err);
            });
            return test;

        } catch (error) {

            loggerFile.error(error)
        }
    }
}
