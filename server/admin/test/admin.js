//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

let mongoose = require("mongoose");
let Admin = require('../models/Admin.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let token = "";
let idAdmin = "";
let idEditor = "";
let idPost = "";
let idJob = "";
let idSkill = "";
chai.use(chaiHttp);
//Our parent block
describe('admins', () => {
    before((done) => { //Before each test we empty the database
        try {
            Admin.deleteMany({}, async (err) => {
                done();
            });
        } catch (err) {
            console.log("error before ", err.message)
        }

    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /POST root
        */
        describe('/POST Root', () => {
            it('it should create root', (done) => {
                let body = {username: "root", email: "root@gmail.com", password: "azerty123"}
                chai.request(server)
                    .post('/auth/addRoot')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('token');
                        token = res.body.token;
                        done();
                    });
            });
        });
        /*
         * Test the /POST login root
        */
        describe('/POST login root', () => {
            it('it should not POST a user without username and password', (done) => {
                let user = {username: "root", password: "azerty123"}
                chai.request(server)
                    .post('/auth/login')
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        });
        /*
         * Test the /POST create admin
        */
        describe('/POST create admin', () => {
            it('it should create admin', (done) => {
                let user = {
                    username: "admin",
                    password: "azerty123",
                    repeatPassword: "azerty123",
                    email: "youssefjoejemai16@gmail.com",
                    lastName: "admin",
                    firstName: "admin",
                    phone: "20162470333"
                }
                chai.request(server)
                    .post('/admin')
                    .set('token', token)
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql('Admin créé');
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 2 :  update and delete admin
    */
    describe('Step 2 : update and delete admin', () => {
        /*
         * Test the /POST LOGIN
        */
        describe('/GET admin id', () => {
            it('it should get admin id', (done) => {

                chai.request(server)
                    .get('/admin')
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(2);
                        idAdmin = res.body[1]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /UPDATE admin
        */
        describe('/UPDATE admin', () => {
            it('it should update admin', (done) => {
                let body = {
                    username: "admin2",
                    password: "azerty123",
                    email: "admin2@gmail.com",
                    lastName: "admin2",
                    firstName: "admin2",
                    phone: "2222222222"
                }
                chai.request(server)
                    .patch(`/admin/update/${idAdmin}`)
                    .set('token', token)
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Admin modifié");
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE admin
        */
        describe('/delete admin', () => {
            it('it should DELETE admin', (done) => {
                chai.request(server)
                    .delete(`/admin/${idAdmin}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Admin effacé");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 3 :  module editor
   */
    describe('Step 3 : module editor', () => {
        /*
         * Test the /POST add Editor
        */
        describe('/POST add Editor', () => {
            it('it should add Editor', (done) => {
                let user = {
                    username: "editor",
                    password: "azerty123",
                    repeatPassword: "azerty123",
                    email: "editor@gmail.com",
                    lastName: "editor",
                    firstName: "editor",
                    phone: "1234567890"
                }
                chai.request(server)
                    .post('/editor')
                    .set('token', token)
                    .send(user)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Editeur créé");
                        done();
                    });
            });
        });
        /*
         * Test the /GET editors
        */
        describe('/GET all editors', () => {
            it('it should get editors', (done) => {

                chai.request(server)
                    .get('/editor')
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idEditor = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /UPDATE editor
        */
        describe('/UPDATE editor', () => {
            it('it should update editor', (done) => {
                let body = {
                    username: "editor2",
                    password: "azerty123",
                    email: "editor2@gmail.com",
                    lastName: "editor2",
                    firstName: "editor2",
                    phone: "1234567890"
                }
                chai.request(server)
                    .patch(`/editor/${idEditor}`)
                    .set('token', token)
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Editeur modifié");
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE editor
        */
        describe('/delete editor', () => {
            it('it should DELETE editor', (done) => {
                chai.request(server)
                    .delete(`/editor/${idEditor}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Editeur effacé");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 4 :  module post
    */
    describe('Step 4 : module post', () => {
        /*
         * Test the /POST add post
        */
        describe('/POST add post', () => {
            it('it should add post', (done) => {
                let post = {
                    imageName: "test.png",
                    title: "test",
                    content: "test",
                    url_fb: "test",
                    url_instagram: "test",
                    url_twitter: "test",
                    url_linkedin: "test",
                    resume: "editor"
                }
                chai.request(server)
                    .post('/posts')
                    .set('token', token)
                    .send(post)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Article publié");
                        done();
                    });
            });
        });
        /*
         * Test the /GET posts
        */
        describe('/GET all posts', () => {
            it('it should get posts', (done) => {

                chai.request(server)
                    .get('/posts')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idPost = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /UPDATE post
        */
        describe('/UPDATE post', () => {
            it('it should update post', (done) => {
                let post = {
                    imageName: "test.png",
                    title: "test",
                    content: "test",
                    url_fb: "test",
                    url_instagram: "test",
                    url_twitter: "test",
                    url_linkedin: "test",
                    resume: "editor"
                }
                chai.request(server)
                    .patch(`/posts/${idPost}`)
                    .set('token', token)
                    .send(post)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Article modifié avec succés");
                        done();
                    });
            });
        });
        /*
        * Test the /UPDATE validate post
       */
        describe('/UPDATE validate post', () => {
            it('it should update validate post', (done) => {

                chai.request(server)
                    .patch(`/posts/validate/${idPost}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Article publié avec succés");
                        done();
                    });
            });
        });
        /*
       * Test the /UPDATE unvalidate post
      */
        describe('/UPDATE unvalidate post', () => {
            it('it should update unvalidate post', (done) => {

                chai.request(server)
                    .patch(`/posts/unvalidate/${idPost}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Article bloqué avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE post
        */
        describe('/delete post', () => {
            it('it should DELETE post', (done) => {
                chai.request(server)
                    .delete(`/posts/${idPost}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Article effacé avec succés");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 5 :  module job
    */
    describe('Step 5 : module job', () => {
        /*
         * Test the /POST add job
        */
        describe('/POST add job', () => {
            it('it should add job', (done) => {
                let job = {
                    name: "job test"
                }
                chai.request(server)
                    .post('/jobs/add-job')
                    .set('token', token)
                    .send(job)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Métier créé avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /GET jobs
        */
        describe('/GET all jobs', () => {
            it('it should get jobs', (done) => {

                chai.request(server)
                    .get('/jobs')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idJob = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /UPDATE job
        */
        describe('/UPDATE job', () => {
            it('it should update job', (done) => {
                let job = {
                    name: "test 2 job",
                }
                chai.request(server)
                    .patch(`/jobs/${idJob}`)
                    .set('token', token)
                    .send(job)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Métier modifié avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE job
        */
        describe('/delete job', () => {
            it('it should DELETE job', (done) => {
                chai.request(server)
                    .delete(`/jobs/${idJob}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Métier effacé avec succés");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 6 :  module skills
    */
    describe('Step 6 : module skills', () => {
        /*
         * Test the /POST add skill
        */
        describe('/POST add skill', () => {
            it('it should add skill', (done) => {
                let skill = {
                    name: "skill test"
                }
                chai.request(server)
                    .post('/skills/add-skill')
                    .set('token', token)
                    .send(skill)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Compétence créée avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /GET skills
        */
        describe('/GET all skills', () => {
            it('it should get skills', (done) => {

                chai.request(server)
                    .get('/skills')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        idSkill = res.body[0]._id;
                        done();
                    });
            });
        });
        /*
         * Test the /UPDATE skill
        */
        describe('/UPDATE skill', () => {
            it('it should update skill', (done) => {
                let skill = {
                    name: "test 2 skill",
                }
                chai.request(server)
                    .patch(`/skills/${idSkill}`)
                    .set('token', token)
                    .send(skill)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Compétence modifiée avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /DELETE skill
        */
        describe('/delete skill', () => {
            it('it should DELETE skill', (done) => {
                chai.request(server)
                    .delete(`/skills/${idSkill}`)
                    .set('token', token)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Compétence effacée avec succés");
                        done();
                    });
            });
        });
    });
});
