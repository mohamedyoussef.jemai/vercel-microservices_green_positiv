require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const cors = require('cors');
const loggerFile = require("./config/logger");

const app = express();
const port = process.env.PORT_ADMIN;

app.use(logger('dev'));

require("./models/jobs.model");
require("./models/certification.model");
require("./models/formation.model");
require("./models/experience.model");
require("./models/freelancer.model");
require("./models/skills.model");
require("./models/Admin.model");
require("./models/post.model");
require("./models/subscription.model");
require("./models/user.model");
require("./models/logs.model");

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));
app.use(express.static("uploads"));
require('./config/passport');

app.use(logger('combined'));

let jobsRouter = require('./routes/jobs.route');
let skillsRouter = require('./routes/skills.route');
let adminRouter = require('./routes/Admin.route');
let indexRouter = require('./routes/index.route');
let editorRouter = require('./routes/editor.route');
let postRouter = require('./routes/posts.route');
let freelancerRouter = require('./routes/freelancer.route');
let companyRouter = require('./routes/company.route');
let agenceRouter = require('./routes/agence.route');
let notificationFreelance = require('./routes/notificationFreelance.route');
let notificationAgence = require('./routes/notificationAgence.route');
let notificationCompany = require('./routes/notificationCompany.route');
let notificationAdmin = require('./routes/notificationAdmin.route');
let notificationPost = require('./routes/notificationPost.route');
let subscriptionRouter = require('./routes/subscription.route');
let apiRouter = require('./routes/api.route');
let missionRouter = require('./routes/mission.route');
let demandRouter = require('./routes/demand.route');
let profileFreelanceRouter = require('./routes/profilEntreprise.route');
let profileAgenceRouter = require('./routes/profilEntrepriseAgence.route');
let profileCompanyRouter = require('./routes/profilEntrepriseCompany.route');
let logsRouter = require('./routes/logs.route');

app.use('/auth', indexRouter);
app.use('/admin', adminRouter);
app.use('/skills', skillsRouter);
app.use('/editor', editorRouter);
app.use('/posts', postRouter);
app.use('/freelancer', freelancerRouter);
app.use('/company', companyRouter);
app.use('/agence', agenceRouter);
app.use('/notification-freelance', notificationFreelance);
app.use('/notification-agence', notificationAgence);
app.use('/notification-company', notificationCompany);
app.use('/notification-admin', notificationAdmin);
app.use('/notification-posts', notificationPost);
app.use('/jobs', jobsRouter);
app.use('/subscriptions', subscriptionRouter);
app.use('/api', apiRouter);
app.use('/missions', missionRouter);
app.use('/demands', demandRouter);
app.use('/profil-entreprise', profileFreelanceRouter);
app.use('/profil-entreprise-agence', profileAgenceRouter);
app.use('/profil-entreprise-company', profileCompanyRouter);
app.use('/logs', logsRouter);

mongoose.connect(process.env.DB_URI_ADMIN_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    loggerFile.info('database Connected');
    app.listen(port, () => {
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.error(err);
});

module.exports = app;
