const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NotificationSchema = new Schema({
    username: {
        type: String
    },
    role: {
        type: String
    },
    changes: {
        type: Object
    },
    date: {
        type: Date,
        default: Date.now()
    },
    checked: {
        type: Boolean,
        default: false
    }
}, {
    collection: 'notif-freelance',
    timestamps: true
})

const notificationFreelance = mongoose.model("notificationFreelance", NotificationSchema);
module.exports = notificationFreelance;

