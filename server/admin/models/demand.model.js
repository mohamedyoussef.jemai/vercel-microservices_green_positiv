const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let demandSchema = new Schema({
    idUser: {
        type: String,
    },
    id_mission: {
        type: Schema.Types.ObjectId,
        ref: 'Mission'
    },
    name: {
        type: String
    },
    search: {
        type: JSON
    },
    state: {
        type: String,
        enum: ["en cours", "validé", "refusé"],
        default: "en cours"
    },
}, {
    collection: 'demands',
    timestamps: true
})

const demand = mongoose.model("Demand", demandSchema);
module.exports = demand;

