const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SkillSchema = new Schema({
    name: {
        type: String,
        required: true
    },

}, {
    collection: 'skills',
    timestamps: true
})

const Skill = mongoose.model("Skill", SkillSchema);
module.exports = Skill;

