const LegalForm = function (
    index, name
) {
    this.index = index;
    this.name = name;
}

LegalForm.fromJson = (object) => {
    return new LegalForm(
        object["$"]["index"],
        object["name"][0],
        object["_"],
    )
}


module.exports = {LegalForm}
