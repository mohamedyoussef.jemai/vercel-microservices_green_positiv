const jwt = require('jsonwebtoken');
const crypto = require('crypto');
const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let AdminSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        required: true
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    lastName: {
        type: String,
    },
    firstName: {
        type: String,
    },
    hash: {
        type: String,
        required: true
    },
    salt: {
        type: String,
        required: true
    },
    phone: {
        type: String
    },
    lastConnection: {
        type: Date,
        default: Date.now
    },
    visibility: {
        type: Number,
        default: 1
    },
}, {
    collection: 'admins',
    timestamps: true
});

//set Password crypted
AdminSchema.methods.setPassword = function (password) {
    this.salt = crypto.randomBytes(16).toString('hex');
    this.hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
};
//validate password crypted
AdminSchema.methods.validPassword = function (password) {
    var hash = crypto.pbkdf2Sync(password, this.salt, 1000, 64, 'sha512').toString('hex');
    return this.hash === hash;
};
//generate jwt token
AdminSchema.methods.generateJwt = function () {
    var expiry = new Date();
    //30mn
    expiry.setDate(expiry.getDate() + 7);
    return jwt.sign({
        _idConnected: this._id,
        emailConnected: this.email,
        usernameConnected: this.username,
        role: this.role,
        exp: parseInt(expiry.getTime() / 1000),
    }, process.env.MY_SECRET); // DO NOT KEEP YOUR SECRET IN THE CODE!
};

const Admin = mongoose.model("Admin", AdminSchema);
module.exports = Admin;
