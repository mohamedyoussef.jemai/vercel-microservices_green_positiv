const Sector = function (
    index, name, code
) {
    this.index = index;
    this.code = code;
    this.name = name;

}

Sector.fromJson = (object) => {
    return new Sector(
        object["$"]["index"],
        object["name"][0],
        object["code"][0],
        object["_"],
    )
}


module.exports = {Sector}
