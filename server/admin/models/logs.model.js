const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let LogSchema = new Schema({
    timestamp: {
        type: Date,
    },
    level: {
        type: String,
    },
    message: {
        type: String,
    },
    meta: {
        type: JSON,
    },
    checked: {
        type: Boolean,
        default: false
    },
}, {
    collection: 'logs',
    timestamps: true
})

const Log = mongoose.model("Log", LogSchema);
module.exports = Log;

