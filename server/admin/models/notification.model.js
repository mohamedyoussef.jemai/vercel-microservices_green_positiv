const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NotificationSchema = new Schema({
    username: {
        type: String
    },
    role: {
        type: String
    },
    changes: {
        type: Object
    },
    date: {
        type: Date,
        default: Date.now()
    },
    checked: {
        type: Boolean,
        default: false
    }
}, {
    collection: 'notifications',
    timestamps: true
})

const Notification = mongoose.model("notification", NotificationSchema);
module.exports = Notification;

