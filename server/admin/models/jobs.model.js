const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let JobSchema = new Schema({
    name: {
        type: String,
        required: true
    },
}, {
    collection: 'jobs',
    timestamps: true
})

const Job = mongoose.model("Job", JobSchema);
module.exports = Job;

