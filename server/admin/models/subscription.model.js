const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let SubscriptionSchema = new Schema({
    email: {
        type: String,
        required: true
    },
    active: {
        type: Boolean,
        default: true
    }
}, {
    collection: 'subscriptions',
    timestamps: true
})

const Subscription = mongoose.model("Subscription", SubscriptionSchema);
module.exports = Subscription;

