const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NotificationSchema = new Schema({
    username: {
        type: String
    },
    role: {
        type: String
    },
    changes: {
        type: Object
    },
    date: {
        type: Date,
        default: Date.now()
    },
    checked: {
        type: Boolean,
        default: false
    }
}, {
    collection: 'notif-admins',
    timestamps: true
})

const notificationAdmin = mongoose.model("notificationAdmin", NotificationSchema);
module.exports = notificationAdmin;

