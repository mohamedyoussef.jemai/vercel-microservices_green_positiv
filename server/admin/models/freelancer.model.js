const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let freelancerSchema = new Schema({
    username: {
        type: String,
        required: true,
        unique: true
    },
    role: {
        type: String,
        default: "Freelancer"
    },
    email: {
        type: String,
        required: true,
        unique: true
    },
    lastName: {
        type: String,
    },
    firstName: {
        type: String,
    },
    phone: {
        type: String
    },
    lastConnection: {
        type: Date,
        default: Date.now
    },
    visibility: {
        type: Number,
        default: 1
    },
    jobCat: {
        type: String,
        required: true
    },
    title_profile: {
        type: String
    },
    localisation: {
        type: String,
    },
    level: {
        type: String,
        enum: ['Junior', 'Intermédiaire', 'Senior'],
        default: 'Junior',
        required: true
    },
    skills: {
        type: [{
            type: String,
        }],
    },
    price_per_day: {
        type: Number,
        required: true,
        validate: {
            validator: Number.isInteger,
            message: '{VALUE} doit être un entier'
        }
    },
    show_price: {
        type: Number,
        default: 0
    },
    email_verification: {
        type: Number,
        default: 0
    },
    code_verification: {
        type: String,
    },
    confidentiality: {
        type: Boolean,
        default: 0
    },
    documents: {
        type: [{
            type: String
        }],
        default: ["", "", "", ""]
    },
    documents_val: {
        type: Boolean,
        default: 0
    },
    description: {
        type: String
    },
    image: {
        type: String,
    },
    signed_client: {
        type: Boolean,
        default: 0
    },
    languages: {
        type: []
    },
    url_fb: {
        type: String,
        default: ''
    },
    url_github: {
        type: String,
        default: ''
    },
    url_twitter: {
        type: String,
        default: ''
    },
    url_linkedin: {
        type: String,
        default: ''
    },
    validated: {
        type: Boolean,
        required: true,
        Default: 0
    },
    disponibility: {
        type: Boolean,
        Default: 0
    },
    disponibility_freq: {
        type: Number,
        Default: 0
    },
    formations: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Formation'
        }],
    },
    experiences: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Experience'
        }],
    },
    certifications: {
        type: [{
            type: Schema.Types.ObjectId,
            ref: 'Certification'
        }],
    },
    interest: {
        type: [{type: String}]
    },
    passion: {
        type: String,
        default: ""
    },
    greenQuestion: {
        type: String,
        default: ""
    },
    cloudinary_id: {
        type: String
    },
    cloudinary_doc_id: {
        type: String
    },
    cloudinary_kabis_id: {
        type: String,
    },
    cloudinary_vigilance_id: {
        type: String,
    },
    cloudinary_sasu_id: {
        type: String,
    },
}, {
    collection: 'freelancers',
    timestamps: true
})

const Freelancer = mongoose.model("Freelancer", freelancerSchema);
module.exports = Freelancer;

