const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let profileEntrepriseSchema = new Schema({
    name: {
        type: String
    },
    address: {
        type: String,
    },
    address_plus: {
        type: String,
    },
    legal_form: {
        type: String
    },
    lastname: {
        type: String,
    },
    firstname: {
        type: String,
    },
    birthday: {
        type: Date,
    },
    postal: {
        type: String,
        default: ""
    },
    city_of_birth: {
        type: String,
        default: ""
    },
    country_of_birth: {
        type: String
    },
    nationality: {
        type: String,
    },
    validated: {
        type: Boolean,
        default: 0
    },
    id_freelancer: {
        type: String,
    },
    cb_iban_name_lastname: {
        type: String,
        default: ""
    },
    cb_iban_address_holder: {
        type: String,
        default: ""
    },
    cb_iban_postal: {
        type: String,
        default: ""
    },
    cb_iban_city: {
        type: String,
        default: ""
    },
    cb_iban_country: {
        type: String,
        default: ""
    },
    cb_iban_iban: {
        type: String,
        default: ""
    },
    cb_iban_region: {
        type: String,
        default: ""
    },
    cb_iban_account_number: {
        type: String,
        default: ""
    },
    cb_iban_aba_transit_number: {
        type: String,
        default: ""
    },
    cb_iban_account_type: {
        type: String,
        enum: ['Compte courant', 'épargne'],
        default: 'Compte courant'
    },
    cb_iban_bank_name: {
        type: String
    },
    cb_iban_branch_code: {
        type: String
    },
    cb_iban_number_institution: {
        type: String
    },
    cb_iban_bic_swift: {
        type: String,
        default:""
    },
    cb_iban_account_country: {
        type: String
    },
    type_iban: {
        type: String,
        enum: ['iban', 'iban-us', 'iban-ca', 'others', 'empty'],
        default: 'empty'
    },
    taxe: {
        type: Number,
        default: 0
    },
    sas: {
        type: Number,
        default: 0
    },
    siret: {
        type: String,
        default: ""
    },
    rcs: {
        type: String,
        default: ""
    },
    naf: {
        type: String,
        default: ""
    },
    tva_intracom: {
        type: String,
        default: ""
    },
    days: {
        type: Number,
        default: 0
    },
}, {
    collection: 'profileEntrepriseFreelancers',
    timestamps: true
})

const profileEntreprise = mongoose.model("profileEntreprise", profileEntrepriseSchema);
module.exports = profileEntreprise;

