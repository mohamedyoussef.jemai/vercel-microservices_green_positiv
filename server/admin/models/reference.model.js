const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let ReferenceSchema = new Schema({
    client: {
        type: String,
        required: true
    },
    domain: {
        type: String,
        required: true
    },
    title: {
        type: String,
        required: true
    },
    place: {
        type: String,
        required: true
    },
    dateBegin: {
        type: Date,
    },
    dateEnd: {
        type: Date,
    },
    id_agence: {
        type: String,
    },
    confidential: {
        type: Boolean,
        default: true
    },
    image: {
        type: String,
        default: ""
    },
    cloudinary_id: {
        type: String,
    }
}, {
    collection: 'references',
    timestamps: true
})

const Reference = mongoose.model("Reference", ReferenceSchema);
module.exports = Reference;

