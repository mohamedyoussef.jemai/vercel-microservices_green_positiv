const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let NotificationSchema = new Schema({
    username: {
        type: String
    },
    role: {
        type: String
    },
    changes: {
        type: Object
    },
    date: {
        type: Date,
        default: Date.now()
    },
    checked: {
        type: Boolean,
        default: false
    },
    offer_name: {
        type: String
    }
}, {
    collection: 'notif-agence',
    timestamps: true
})

const notificationAgence = mongoose.model("notificationAgence", NotificationSchema);
module.exports = notificationAgence;

