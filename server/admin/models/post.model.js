const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let PostSchema = new Schema({
    title: {
        type: String,
        required: true
    },
    content: {
        type: String,
        required: true
    },
    author: {
        type: String,
        required: true
    },
    image: {
        type: String
    },
    url_fb: {
        type: String
    },
    url_instagram: {
        type: String
    },
    url_twitter: {
        type: String
    },
    url_linkedin: {
        type: String
    },
    resume: {
        type: String
    },
    validated: {
        type: Boolean,
        default: 0
    },
    cloudinary_id: {
        type: String,
    }
}, {
    collection: 'posts',
    timestamps: true
})

const Post = mongoose.model("Post", PostSchema);
module.exports = Post;

