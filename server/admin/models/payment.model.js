const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let paymentSchema = new Schema({
    id_company: {
        type: Schema.Types.ObjectId,
        ref: 'Company',
        required: true
    },
    id_freelance: {
        type: Schema.Types.ObjectId,
        ref: 'Freelance',
    },
    id_agence: {
        type: Schema.Types.ObjectId,
        ref: 'Agence',
    },
    id_mission: {
        type: Schema.Types.ObjectId,
        ref: 'Mission',
    },
    amount: {
        type: Number,
    },
    totalTva: {
        type: Number,
    },
    totalTvaUnpayed: {
        type: Number,
    },
    state: {
        type: Boolean,
        default: false
    }
}, {
    collection: 'payments',
    timestamps: true
})

const payment = mongoose.model("Payment", paymentSchema);
module.exports = payment;
