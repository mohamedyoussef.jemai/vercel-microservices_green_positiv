const passport = require('passport');
const LocalStrategy = require('passport-local').Strategy;
const mongoose = require('mongoose');
const Admin = mongoose.model('Admin');
const loggerFile = require("../config/logger");

passport.use(new LocalStrategy({
        usernameField: 'username'
    },
    function (username, password, done) {
        loggerFile.info("in middleware passport admin")
        Admin.findOne({username: username}, function (err, user) {
            if (err) {
                return done(err);
            }
            if (!user) {
                loggerFile.debug("utilisateur inéxistant")
                return done(null, false, {
                    message: 'utilisateur inéxistant'
                });
            }
            if (!user.validPassword(password)) {
                loggerFile.debug("mot de passe erroné")
                return done(null, false, {
                    message: 'mot de passe erroné'
                });
            }
            return done(null, user);
        });
    }
));
