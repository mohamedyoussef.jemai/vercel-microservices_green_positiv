const Admin = require('../models/Admin.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create admin")
        const {username, email, lastName, firstName, phone, password} = req.body;
        try {
            const admin = new Admin({username, email, lastName, firstName, phone});
            admin.role = "Admin";
            admin.setPassword(password);
            await Admin.create(admin);
            loggerFile.debug("admin created")
            res.status(200).json({message: "Admin créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("findAll admins")
        try {
            const admins = await Admin.find();
            if (admins) {
                loggerFile.debug("get all admins done")
                return res.status(200).json(admins);
            } else {
                loggerFile.debug("admins inéxistants")
                return res.status(200).json({message: "admins inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("findOne admin")
        const {id} = req.params;
        try {
            const admin = await Admin.findById(id);
            if (admin) {
                loggerFile.debug("findOne admin done")
                return res.status(200).json(admin);
            } else {
                loggerFile.debug("Admin inéxistant")
                return res.json({message: "Admin inéxistant"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update admin")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        const newAdmin = req.body;
        try {
            if (id === req.auth._idConnected) {
                await Admin.findByIdAndUpdate(id, newAdmin);
                loggerFile.debug('update admin done')
                return res.status(200).json({message: "Admin modifié"});
            } else {
                loggerFile.debug('not allowed to update this account')
                return res.status(401).json({message: "Vous ne pouvez pas modifier ce compte"});

            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }

    },
    delete: async (req, res) => {
        loggerFile.info("in delete admin")

        const {id} = req.params;
        try {
            await Admin.findByIdAndDelete(id);
            loggerFile.debug('delete admin done')
            return res.status(200).json({message: "Admin effacé"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateByAdmin: async (req, res) => {
        loggerFile.info("in update admin by root")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        const newAdmin = req.body;
        try {
            await Admin.findByIdAndUpdate(id, newAdmin);
            loggerFile.debug('update admin done')
            return res.status(200).json({message: "Admin modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }

    },
}

