const notificationAdmin = require("../models/notificationAdmin.model")
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    postCreatedNotif: async (req, res) => {
        loggerFile.info("in postCreatedNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAdmin({username, role, changes});
            await notificationAdmin.create(notif);
            loggerFile.debug('postCreated data done');
            return res.status(200).json({message: "article créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    updatePostNotif: async (req, res) => {
        loggerFile.info("in updatePostNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAdmin({username, role, changes});
            await notificationAdmin.create(notif);
            loggerFile.debug('updatePost data done')
            return res.status(200).json({message: "article modifié"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    validatedPostNotif: async (req, res) => {
        loggerFile.info("in validatedPostNotif");
        try {
            const {username, changes} = req.body;
            const notif = new notificationAdmin({username, changes});
            await notificationAdmin.create(notif);
            loggerFile.debug('validatedPostNotif data done')
            return res.status(200).json({message: "article publié"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    unvalidatedPostNotif: async (req, res) => {
        loggerFile.info("in unvalidatedPostNotif");
        try {
            const {username, changes} = req.body;
            const notif = new notificationAdmin({username, changes});
            await notificationAdmin.create(notif);
            loggerFile.debug('unvalidatedPostNotif data done')
            return res.status(200).json({message: "article dépublié"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll notification Admin");
        try {
            let notifications = await notificationAdmin.find();
            loggerFile.debug('findAll data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },

}
