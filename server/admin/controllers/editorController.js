const Admin = require('../models/Admin.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create editor")

        const {username, email, lastName, firstName, phone, password} = req.body;
        try {
            const editor = new Admin({username, email, lastName, firstName, phone});
            editor.role = "Editor";
            editor.setPassword(password);
            await Admin.create(editor);
            loggerFile.debug("editor created")
            return res.status(200).json({message: "Editeur créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll editors")
        try {
            const editeurs = await Admin.find({role: "Editor"});
            if (editeurs) {
                loggerFile.debug("findAll editors done")
                return res.status(200).json(editeurs);
            } else {
                loggerFile.debug("Editeurs inéxistants")
                return res.status(200).json({message: "Editeurs inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("findOne editor")

        const {id} = req.params;
        try {
            const editeur = await Admin.findById(id);
            if (editeur) {
                loggerFile.debug("findOne editor done")
                return res.status(200).json(editeur);
            } else {
                loggerFile.debug("Editeur inéxistant")
                return res.status(400).json({message: "Editeur inéxistant"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update admin")

        if (!req.body) return res.status(400).send({message: "les informations sont vides"});

        let {id} = req.params;
        const newEditor = req.body;
        try {
            await Admin.findByIdAndUpdate(id, newEditor);
            loggerFile.debug("update editor done")
            return res.status(200).json({message: "Editeur modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in delete editor")
        const {id} = req.params;
        try {
            await Admin.findByIdAndDelete(id);
            loggerFile.debug("delete editor done")
            return res.status(200).json({message: "Editeur effacé"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

