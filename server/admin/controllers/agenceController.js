const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');
const loggerFile = require("../config/logger");
const Agence = require("../models/agence.model")
const Offer = require("../models/offer.model")
const Reference = require("../models/reference.model")
const Devis = require("../models/devis.model");
const Mission = require("../models/mission.model");
const Payment = require("../models/payment.model");
const profileEntrepriseAgence = require("../models/profileEntrepriseAgence.model")
const authService = require("../services/authService");
const profilEntrepriseServiceAgence = require("../services/profilEntrepriseServiceAgence");
const mailService = require("../services/mailingService");


module.exports = {
    getRequiredProfiles: async (req, res) => {
        let finalOffers = []
        let {jobCat, minPrice, maxPrice} = req.body;
        try {
            let agences = await Agence.find({
                jobCat: jobCat,
            });
            let agencesId = agences.map(el => {
                return el._id
            })

            finalOffers = await Offer.find({
                price: {
                    $gte: minPrice,
                    $lte: maxPrice
                }, documents_val: true,
                id_agence: {$in: agencesId}
            });
            return res.status(200).send({
                agences: agences.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        nameAgence: el.nameAgence,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image
                    }
                }),
                offers: finalOffers
            });
        } catch (error) {
            loggerFile.warn(utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadKabisDocuments: async (req, res) => {
        loggerFile.info('in uploadKabisDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let docs = [];
        let cloudinary_kabis_id = "";
        try {
            let agence = await Agence.findById(id);
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new kabis');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_kabis_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[1] = docs[0]
                        newAgence.cloudinary_kabis_id = cloudinary_kabis_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadVigilanceDocuments: async (req, res) => {
        loggerFile.info('in uploadVigilanceDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;

        let docs = [];
        let cloudinary_vigilance_id = "";
        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new vigilance');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_vigilance_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[2] = docs[0]
                        newAgence.cloudinary_vigilance_id = cloudinary_vigilance_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadSasuDocuments: async (req, res) => {
        loggerFile.info('in uploadSasuDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let docs = [];
        let cloudinary_sasu_id = "";
        try {
            let agence = await Agence.findById(id);
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_sasu_id = result.public_id;
                        let newAgence = req.body;
                        newAgence.documents = agence.documents;
                        newAgence.documents[3] = docs[0]
                        newAgence.cloudinary_sasu_id = cloudinary_sasu_id;
                        newAgence.documents_val = false;

                        await Agence.findByIdAndUpdate(id, newAgence);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newAgence = req.body;
                newAgence.documents = agence.documents;
                newAgence.documents_val = false;
                await Agence.findByIdAndUpdate(id, newAgence);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },

    deleteAgence: async (req, res) => {
        loggerFile.info("in deleteAgence by admin")
        const {id} = req.params;
        try {
            await Agence.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                    }
                    if (data.documents.length !== 0) {
                        await data.documents.forEach(async function callback(document, index) {
                            await cloudinary.uploader.destroy(data.cloudinary_doc_id);
                        });
                    }
                    await Agence.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseServiceAgence.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    loggerFile.debug('Freelancer effacé')
                    return res.status(200).json({message: "Freelancer effacé"})
                } else {
                    loggerFile.debug('freelancer inéxistant')
                    return res.status(400).json({message: "Freelancer inéxistant"})
                }
            }).catch(error => {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll agences");
        try {
            const agences = await Agence.find();
            if (agences) {
                loggerFile.debug('get all agences done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('aucun agences éxistants')
                return res.status(200).json({message: "agences inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find agence by ID");
        const {id} = req.params;
        try {
            const agence = await Agence.findById(id);
            if (agence) {
                let jobCat = "";
                await req.body.jobs.forEach(async function callback(job, index) {
                    if (agence.jobCat == job._id) {
                        jobCat = await job.name;
                    }
                });
                loggerFile.debug('get agence by id');
                return await res.status(200).json({
                    agence: agence,
                    offers: req.body.offers,
                    references: req.body.references,
                    jobCat: jobCat
                });
            } else {
                loggerFile.debug('agence inéxistante');
                return res.status(200).json({message: "agence inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "agence inéxistante"});
            } else {
                loggerFile.error(error, utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    getValidatedAgences: async (req, res) => {
        loggerFile.info("in getValidatedAgences")
        try {
            const agences = await Agence.find({validated: true});
            if (agences) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(agences);
            } else {
                loggerFile.debug("agences valides inéxistants")
                return res.status(200).json({message: "agences valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getUnvalidatedAgences: async (req, res) => {
        loggerFile.info("in getUnvalidatedAgences")
        try {
            const agences = await Agence.find({validated: false});
            if (agences) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(agences);
            } else {
                loggerFile.debug("agences valides inéxistants")
                return res.status(200).json({message: "agences valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    blockAgence: async (req, res) => {
        loggerFile.info("in blockAgence by admin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {validated: false}).then(async response => {
                if (response) {
                    let agence = await Agence.findById(id);
                    loggerFile.info("in send email after bloqued")
                    let testAuth = await authService.block(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    mailService.block({
                        email: agence.email
                    });
                    loggerFile.debug('agence bloqué')
                    return res.status(200).json({message: "Agence bloqué"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    unblockAgence: async (req, res) => {
        loggerFile.info("in unblockAgence by admin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {validated: true}).then(async response => {
                if (response) {
                    let agence = await Agence.findById(id);
                    loggerFile.info("in send email after bloqued")
                    let testAuth = await authService.unblock(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    mailService.unblock({
                        email: agence.email
                    });
                    loggerFile.debug('agence unbloqué')
                    return res.status(200).json({message: "Agence débloqué"});
                }
            }).catch(err => {
                loggerFile.error(err, utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentAgence: async (req, res) => {
        loggerFile.info("in validateDocumentAgence by admin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseServiceAgence.validate(id, true);
                    loggerFile.debug("validateDocumentByAdmin update done")
                    loggerFile.info("in send email after validated document agence")
                    mailService.validateDocument({email: data.email});
                    return res.status(200).json({message: "Documents de l'agence validés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la valdiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateDocumentAgence: async (req, res) => {
        loggerFile.info("in unvalidateDocumentAgence by admin")
        let {id} = req.params;
        try {
            await Agence.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseServiceAgence.validate(id, false);
                    loggerFile.debug("unvalidateDocumentByAdmin update done")
                    loggerFile.info("in send email after unvalidated document agence")
                    mailService.validateDocument({email: data.email});
                    return res.status(200).json({message: "Documents de l'agence non validés"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la dévaldiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateOffer: async (req, res) => {
        loggerFile.info("in validateOffer by admin")
        let {id} = req.params;
        try {
            await Offer.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    loggerFile.debug("validate offer")
                    loggerFile.info("in send email after validate offer")
                    let agence = await Agence.findById(data.id_agence);
                    mailService.validateDocumentOffer({
                        email: agence.email,
                        name: data.name
                    });
                    return res.status(200).json({message: "Offre validé"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la validation de l'offre"});
                }
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateOffer: async (req, res) => {
        loggerFile.info("in unvalidateOffer by admin")
        let {id} = req.params;
        try {
            await Offer.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    loggerFile.debug("devalidate offer")
                    loggerFile.info("in send email after unvalidate offer")
                    let agence = await Agence.findById(data.id_agence);
                    console.log('agence ',agence)
                    mailService.unvalidateDocumentOffer({
                        email: agence.email,
                        name: data.name
                    });
                    return res.status(200).json({message: "Offre dévalidé"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(err, utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la dévalidation de l'offre"});
                }
            });
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllDocumentValidated: async (req, res) => {
        loggerFile.info("in findAllDocumentValidated by admin")
        try {
            const agences = await Agence.find({documents_val: true});
            if (agences) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('agences valides inéxistants')
                return res.status(200).json({message: "agences valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllDocumentunValidated: async (req, res) => {
        loggerFile.info("in findAllDocumentUnValidated by admin")
        try {
            const agences = await Agence.find({documents_val: false});
            if (agences) {
                loggerFile.debug('findAllDocumentUnValidated data done')
                return res.status(200).json(agences);
            } else {
                loggerFile.debug('agences non valides inéxistants')
                return res.status(200).json({message: "agences non valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfAgences: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfAgences by admin")
        try {
            const profileEntrepriseAgences = await profileEntrepriseAgence.find();
            if (profileEntrepriseAgences) {
                loggerFile.debug('get all profileEntrepriseAgences data done')
                return res.status(200).json(profileEntrepriseAgences);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfAgenceById: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfAgenceById by admin")
        let {id} = req.params;
        let ibanModule = null;
        let ibanUsModule = null;
        let ibanCaModule = null;
        let ibanOthers = null;
        try {
            const OneprofileEntrepriseAgence = await profileEntrepriseAgence.findOne({id_agence: id});
            if (OneprofileEntrepriseAgence) {
                let contactDetails = {
                    name: OneprofileEntrepriseAgence.name,
                    address: OneprofileEntrepriseAgence.address,
                    address_plus: OneprofileEntrepriseAgence.address_plus,
                    legal_form: OneprofileEntrepriseAgence.legal_form,
                }
                let legalRepresentative = {
                    lastname: OneprofileEntrepriseAgence.lastname,
                    firstname: OneprofileEntrepriseAgence.firstname,
                    birthday: OneprofileEntrepriseAgence.birthday,
                    city: OneprofileEntrepriseAgence.city,
                    postal: OneprofileEntrepriseAgence.postal,
                    city_of_birth: OneprofileEntrepriseAgence.city_of_birth,
                    country_of_birth: OneprofileEntrepriseAgence.country_of_birth,
                    nationality: OneprofileEntrepriseAgence.nationality,
                }
                let legalMention = {
                    sas: OneprofileEntrepriseAgence.sas,
                    siret: OneprofileEntrepriseAgence.siret,
                    rcs: OneprofileEntrepriseAgence.rcs,
                    naf: OneprofileEntrepriseAgence.naf,
                    tva_intracom: OneprofileEntrepriseAgence.tva_intracom,
                    days: OneprofileEntrepriseAgence.days
                }
                switch (OneprofileEntrepriseAgence.type_iban) {
                    case 'empty': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban': {
                        ibanModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_iban: OneprofileEntrepriseAgence.cb_iban_iban
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-us': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_aba_transit_number: OneprofileEntrepriseAgence.cb_iban_aba_transit_number,
                            cb_iban_account_type: OneprofileEntrepriseAgence.cb_iban_account_type
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-ca': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_bank_name: OneprofileEntrepriseAgence.cb_iban_bank_name,
                            cb_iban_branch_code: OneprofileEntrepriseAgence.cb_iban_branch_code,
                            cb_iban_number_institution: OneprofileEntrepriseAgence.cb_iban_number_institution
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'others': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_bic_swift: OneprofileEntrepriseAgence.cb_iban_bic_swift,
                            cb_iban_account_country: OneprofileEntrepriseAgence.cb_iban_account_country,
                        }
                    }
                        break;
                }
                return res.status(200).json({
                    contactDetails: contactDetails,
                    legalRepresentative: legalRepresentative,
                    taxe: OneprofileEntrepriseAgence.taxe,
                    legalMention: legalMention,
                    type_iban: OneprofileEntrepriseAgence.type_iban,
                    ibanModule: ibanModule,
                    ibanUsModule: ibanUsModule,
                    ibanCaModule: ibanCaModule,
                    ibanOthers: ibanOthers
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newagence = req.body;
        try {
            await Agence.findByIdAndUpdate(id, newagence);
            loggerFile.debug('agence modifiée')
            return res.status(200).json({message: "Agence modifiée"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceContactDetailsByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceContactDetailsByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_agence = id;
            let testProfil = await profilEntrepriseServiceAgence.updateContactDetails(data);
            if (testProfil) {
                loggerFile.debug("updateAgenceContactDetailsByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateAgenceContactDetailsByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceLegalRepresentativeByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceLegalRepresentativeByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_agence = id;
            let testProfil = await profilEntrepriseServiceAgence.updateLegalRepresentative(data);
            if (testProfil) {
                loggerFile.debug("updateAgenceLegalRepresentativeByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateAgenceLegalRepresentativeByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceLegalMentionByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceLegalMentionByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_agence = id;
            let testProfil = await profilEntrepriseServiceAgence.updateLegalMention(data);
            if (testProfil) {
                loggerFile.debug("updateAgenceLegalMentionByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateAgenceLegalMentionByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceIbanByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceIbanByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_agence = id;
            let testProfil = await profilEntrepriseServiceAgence.updateIbanAccount(data);
            if (testProfil) {
                loggerFile.debug("updateAgenceIbanByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateAgenceIbanByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceTaxesByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceTaxesByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_agence = id;
            let testProfil = await profilEntrepriseServiceAgence.updateTaxe(data);
            if (testProfil) {
                loggerFile.debug("updateAgenceTaxesByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateAgenceTaxesByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceOfferByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceOfferByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newOffer = req.body;
        try {
            await Offer.findByIdAndUpdate(id, newOffer);
            loggerFile.debug('offre modifiée')
            return res.status(200).json({message: "offre modifiée"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAgenceOfferByAdmin: async (req, res) => {
        loggerFile.info("in getAgenceOfferByAdmin by admin")
        try {
            let {id} = req.params;
            const offer = await Offer.findById(id);
            if (offer) {
                loggerFile.debug('in findById offer done')
                return res.status(200).json(offer);
            } else {
                loggerFile.debug('aucune offre éxistante')
                return res.status(200).json({message: "offre inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateAgenceReferenceByAdmin: async (req, res) => {
        loggerFile.info("in updateAgenceReferenceByAdmin by admin")
        let {id} = req.params;
        let new_image = "";
        let cloudinary_id = "";
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let reference = await Reference.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    await cloudinary.uploader.destroy(reference.cloudinary_id);

                } catch (err) {
                    loggerFile.error(error, utils.getClientAddress(req, error))
                }
            } else {
                return res.status(400).json({message: "image inexistante"});
            }
            const newReference = req.body;
            newReference.image = new_image;
            newReference.cloudinary_id = cloudinary_id;
            await Reference.findByIdAndUpdate(id, newReference);
            loggerFile.debug('référence modifiée')
            return res.status(200).json({message: "référence modifiée"});
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAgenceReferenceByAdmin: async (req, res) => {
        loggerFile.info("in getAgenceReferenceByAdmin by admin")
        try {
            let {id} = req.params;
            const reference = await Reference.findById(id);
            if (reference) {
                loggerFile.debug('in findById Reference done')
                return res.status(200).json(reference);
            } else {
                loggerFile.debug('aucune référence éxistante')
                return res.status(200).json({message: "référence inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisAgence: async (req, res) => {
        loggerFile.info('in getDevisAgence');
        let {id} = req.params;
        let missions = []
        let i = 0;
        let unpayedAmounts = [];
        try {
            let devises = await Devis.find({id_agence: id});
            if (devises.length > 0) {
                while (devises.length != unpayedAmounts.length) {
                    let mission = await Mission.findById(devises[i].id_mission);
                    await missions.push(mission)
                    if (devises[i].confirmed && devises[i].confirmed == true) {
                        var amount = 0
                        let payments = await Payment.find({id_mission: mission._id, id_agence: devises[i].id_agence})
                        payments.forEach(async function (item2, index2, array2) {
                            amount += item2.amount
                            if (index2 + 1 == array2.length) {
                                amount = (amount * 100) / item2.totalTva
                                unpayedAmounts.push(Math.round(amount))
                                i++
                            }
                        });
                    } else {
                        unpayedAmounts.push(null)
                    }
                }
                return res.status(200).json({
                    devises: devises, missions: missions.map(el => {
                        return {
                            _id: el._id,
                            name: el.name,
                            id_company: el.id_company
                        }
                    }),
                    unpayedAmounts: unpayedAmounts
                })
            } else {
                return res.status(200).json([])
            }

        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getOfferAgence: async (req, res) => {
        loggerFile.info('in getOfferAgence');
        let {id} = req.params;
        try {
            let offers = await Offer.find({id_agence: id, documents_val: true});
            return res.status(200).json(
                offers.map(el => {
                    return {
                        _id: el._id,
                        name: el.name,
                        price: el.price,
                        description: el.description,
                        documents: el.documents,
                    }
                })
            );
        } catch (error) {
            loggerFile.error(error, utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

