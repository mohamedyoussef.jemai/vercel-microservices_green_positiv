const profileEntrepriseAgence = require('../models/profileEntrepriseAgence.model');
const notificationService = require("../services/notificationServiceAgence");
const loggerFile = require("../config/logger");
const utils = require('../config/utils');
module.exports = {
    //update
    update: async (req, res) => {
        loggerFile.info("in update")
        let {id} = req.params;
        try {
            await profileEntrepriseAgence.findByIdAndUpdate(id, req.body);
            loggerFile.debug("update profile data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateProfileFreelane: async (req, res) => {
        loggerFile.info("in validateProfileFreelane")
        let {id_agence, validated} = req.body;
        try {
            await profileEntrepriseAgence.findOneAndUpdate({id_agence: id_agence}, {validated: validated});
            loggerFile.debug("validate data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    createContactDetails: async (req, res) => {
        loggerFile.info("in createContactDetails")
        const {
            name,
            address,
            address_plus,
            legal_form,
            id_agence
        } = req.body;
        try {
            const newprofileEntrepriseAgence = new profileEntrepriseAgence({
                name,
                address,
                address_plus,
                legal_form,
                id_agence
            });
            await profileEntrepriseAgence.create(newprofileEntrepriseAgence);
            loggerFile.debug("createContactDetails data done")

            return res.status(200).json({message: "Coordonnées de votre entreprise ajoutés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateContactDetails: async (req, res) => {
        loggerFile.info("in updateContactDetails")
        try {
            let {id_agence} = req.body;
            await notificationService.createDetailsContactNotif(id_agence, req.body);
            await profileEntrepriseAgence.findOneAndUpdate({id_agence: id_agence}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Coordonnées de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateLegalRepresentative: async (req, res) => {
        loggerFile.info("in UpdateLegalRepresentative")

        try {
            let {id_agence} = req.body;
            await notificationService.createLegalRepresentativeNotif(id_agence, req.body);
            await profileEntrepriseAgence.findOneAndUpdate({id_agence: id_agence}, req.body);
            loggerFile.debug("UpdateLegalRepresentative data done")
            return res.status(200).json({message: "Représentant légale de votre entreprise ajouté"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateIbanAccount: async (req, res) => {
        loggerFile.info("in updateIbanAccount")
        try {
            let {id_agence} = req.body;
            let profileEntrepriseAgenceIban = await profileEntrepriseAgence.findOne({id_agence: id_agence});
            try {
                if (profileEntrepriseAgenceIban.type_iban === 'empty') {
                    await notificationService.createIbanNotif(id_agence, req.body);

                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, req.body);
                } else if (profileEntrepriseAgenceIban.type_iban === 'iban') {
                    await notificationService.createIbanNotif(id_agence, req.body);

                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, {cb_iban_iban: ""});
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, req.body);
                } else if (profileEntrepriseAgenceIban.type_iban === 'iban-us') {
                    await notificationService.createIbanNotif(id_agence, req.body);
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_aba_transit_number: "",
                        cb_iban_account_type: ""
                    });
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, req.body);
                } else if (profileEntrepriseAgenceIban.type_iban === 'iban-ca') {
                    await notificationService.createIbanNotif(id_agence, req.body);
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_branch_code: "",
                        cb_iban_number_institution: ""
                    });
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, req.body);
                } else {
                    await notificationService.createIbanNotif(id_agence, req.body);
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_bic_swift: "",
                        cb_iban_account_country: ""
                    });
                    await profileEntrepriseAgence.findByIdAndUpdate(profileEntrepriseAgenceIban._id, req.body);
                }
            } catch (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(200).json({message: "Erreur lors de l'ajout de l'iban"});

            }
            loggerFile.debug("updateIbanAccount data done");
            return res.status(200).json({message: "Coordonnées bancaires Ajoutés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll")
        try {
            const profileEntrepriseAgences = await profileEntrepriseAgence.find();
            if (profileEntrepriseAgences) {
                loggerFile.debug('get all profileEntrepriseAgences data done')
                return res.status(200).json(profileEntrepriseAgences);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne")

        let {id} = req.params;
        let ibanModule = null;
        let ibanUsModule = null;
        let ibanCaModule = null;
        let ibanOthers = null;
        try {
            const OneprofileEntrepriseAgence = await profileEntrepriseAgence.findOne({id_agence: id});
            if (OneprofileEntrepriseAgence) {
                let contactDetails = {
                    name: OneprofileEntrepriseAgence.name,
                    address: OneprofileEntrepriseAgence.address,
                    address_plus: OneprofileEntrepriseAgence.address_plus,
                    legal_form: OneprofileEntrepriseAgence.legal_form,
                }
                let legalRepresentative = {
                    lastname: OneprofileEntrepriseAgence.lastname,
                    firstname: OneprofileEntrepriseAgence.firstname,
                    birthday: OneprofileEntrepriseAgence.birthday,
                    city: OneprofileEntrepriseAgence.city,
                    postal: OneprofileEntrepriseAgence.postal,
                    city_of_birth: OneprofileEntrepriseAgence.city_of_birth,
                    country_of_birth: OneprofileEntrepriseAgence.country_of_birth,
                    nationality: OneprofileEntrepriseAgence.nationality,
                }
                let legalMention = {
                    sas: OneprofileEntrepriseAgence.sas,
                    siret: OneprofileEntrepriseAgence.siret,
                    rcs: OneprofileEntrepriseAgence.rcs,
                    naf: OneprofileEntrepriseAgence.naf,
                    tva_intracom: OneprofileEntrepriseAgence.tva_intracom,
                    days: OneprofileEntrepriseAgence.days
                }
                switch (OneprofileEntrepriseAgence.type_iban) {
                    case 'empty': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban': {
                        ibanModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_iban: OneprofileEntrepriseAgence.cb_iban_iban
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-us': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_aba_transit_number: OneprofileEntrepriseAgence.cb_iban_aba_transit_number,
                            cb_iban_account_type: OneprofileEntrepriseAgence.cb_iban_account_type
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-ca': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_bank_name: OneprofileEntrepriseAgence.cb_iban_bank_name,
                            cb_iban_branch_code: OneprofileEntrepriseAgence.cb_iban_branch_code,
                            cb_iban_number_institution: OneprofileEntrepriseAgence.cb_iban_number_institution
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'others': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: OneprofileEntrepriseAgence.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntrepriseAgence.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntrepriseAgence.cb_iban_postal,
                            cb_iban_city: OneprofileEntrepriseAgence.cb_iban_city,
                            cb_iban_country: OneprofileEntrepriseAgence.cb_iban_country,
                            cb_iban_region: OneprofileEntrepriseAgence.cb_iban_region,
                            cb_iban_account_number: OneprofileEntrepriseAgence.cb_iban_account_number,
                            cb_iban_bic_swift: OneprofileEntrepriseAgence.cb_iban_bic_swift,
                            cb_iban_account_country: OneprofileEntrepriseAgence.cb_iban_account_country,
                        }
                    }
                        break;
                }
                return res.status(200).json({
                    contactDetails: contactDetails,
                    legalRepresentative: legalRepresentative,
                    taxe: OneprofileEntrepriseAgence.taxe,
                    legalMention: legalMention,
                    type_iban: OneprofileEntrepriseAgence.type_iban,
                    ibanModule: ibanModule,
                    ibanUsModule: ibanUsModule,
                    ibanCaModule: ibanCaModule,
                    ibanOthers: ibanOthers
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateTaxe: async (req, res) => {
        loggerFile.info("in UpdateTaxe")
        try {
            let {id_agence} = req.body;

            if (!req.body.taxe) {
                loggerFile.debug("il faut une valeur de taxe")
                return res.status(400).json({message: "il faut une valeur de taxe"});
            }
            if (req.body.taxe < 0 || req.body.taxe > 100) {
                loggerFile.debug("le taxe doit être compris entre 0 et 100")
                return res.status(400).json({message: "le taxe doit être compris entre 0 et 100"});
            }
            await notificationService.createTaxeNotif(id_agence, req.body);

            await profileEntrepriseAgence.findOneAndUpdate({id_agence: id_agence}, req.body);
            loggerFile.debug("UpdateTaxe data done")
            return res.status(200).json({message: "taxe ajouté"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateLegalMention: async (req, res) => {
        loggerFile.info('in UpdateLegalMention')
        try {
            let {id_agence} = req.body;
            await notificationService.createLegalMentionNotif(id_agence, req.body);

            await profileEntrepriseAgence.findOneAndUpdate({id_agence: id_agence}, req.body);
            loggerFile.debug('mention légale ajoutée')
            return res.status(200).json({message: "mention légale ajoutée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete profile')
        const {id} = req.params;
        try {
            await profileEntrepriseAgence.findOneAndDelete({id_agence: id});
            loggerFile.debug('profile effacé')
            return res.status(200).json({message: "done"})

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    getprofileEntrepriseAgenceById: async (req, res) => {
        try {
            let {id} = req.body;
            try {
                let profile = await profileEntrepriseAgence.findOne({id_agence: id});
                return res.status(200).json(profile);
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

