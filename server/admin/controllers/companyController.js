const loggerFile = require("../config/logger");
const Company = require('../models/company.model');
const profileEntrepriseCompany = require('../models/profileEntrepriseCompany.model');
const authService = require("../services/authService");
const mailService = require("../services/mailingService");
const profilEntrepriseService = require("../services/profilEntrepriseServiceCompany");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');

module.exports = {
    findAll: async (req, res) => {
        loggerFile.info("in findAll companies");
        try {
            const companies = await Company.find({role: "Company"});
            if (companies) {
                loggerFile.debug('get all companies done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('aucun companies éxistants')
                return res.status(200).json({message: "Entreprises inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find company by ID");
        const {id} = req.params;
        let worked_with = [];
        let worked_in = req.worked_in;
        let favorites = req.favorites;
        try {
            const company = await Company.findById(id);
            if (company) {
                loggerFile.debug('get company by id');
                let size = company.worked_with.length;
                if (size != 0) {
                    await company.worked_with.forEach(async function callback(collabId, index) {
                        let collab = await Company.findById(collabId);
                        let model = {
                            _id: collab._id,
                            username: collab.username,
                            email: collab.email,
                            validated: collab.validated,
                            departement: collab.departement,
                            firstName: collab.firstName,
                            lastName: collab.lastName
                        };
                        await worked_with.push(model);
                        if (size - 1 == index) {
                            return await res.status(200).json({
                                company: company,
                                worked_with: worked_with,
                                worked_in: worked_in,
                                favorites: favorites
                            });
                        }
                    });
                } else {
                    return await res.status(200).json({
                        company: company,
                        worked_with: worked_with,
                        worked_in: worked_in,
                        favorites: favorites
                    });
                }
            } else {
                loggerFile.debug('company inéxistante');
                return res.status(200).json({message: "Entreprise inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "Entreprise inéxistante"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    updateCompanyByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newCompany = req.body;
        try {
            await Company.findByIdAndUpdate(id, newCompany);
            loggerFile.debug('Entreprise modifié')
            return res.status(200).json({message: "Entreprise modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteCompany: async (req, res) => {
        loggerFile.info("in deleteCompany by admin")
        const {id} = req.params;
        try {
            await Company.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                        await cloudinary.uploader.destroy(data.cloudinary_doc_id);
                    }
                    await Company.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseService.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});

                    loggerFile.debug('Entreprise effacée')
                    return res.status(200).json({message: "Entreprise effacée"})
                } else {
                    loggerFile.debug('Entreprise inéxistante')
                    return res.status(400).json({message: "Entreprise inéxistante"})
                }
            }).catch(error => {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    getDocumentValidatedCompanies: async (req, res) => {
        loggerFile.info("in getDocumentValidatedCompanies")
        try {
            const companies = await Company.find({documents_val: true});
            if (companies) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('companies valides inéxistants')
                return res.status(200).json({message: "Entreprises valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDocumentUnvalidatedCompanies: async (req, res) => {
        loggerFile.info("in getDocumentUnvalidatedCompanies")
        try {
            const companies = await Company.find({documents_val: false});
            if (companies) {
                loggerFile.debug('companies data done')
                return res.status(200).json(companies);
            } else {
                loggerFile.debug("companies invalides inexistants")
                return res.status(200).json({message: "entreprises invalides inexistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error});
        }
    },
    getValidatedCompanies: async (req, res) => {
        loggerFile.info("in getValidatedCompanies")
        try {
            const companies = await Company.find({validated: true});
            if (companies) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(companies);
            } else {
                loggerFile.debug("companies valides inéxistants")
                return res.status(200).json({message: "entreprises valides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getUnvalidatedCompanies: async (req, res) => {
        loggerFile.info("in getUnvalidatedCompanies")
        try {
            const companies = await Company.find({validated: false});
            if (companies) {
                loggerFile.debug("findAllUnvalidated data done")
                return res.status(200).json(companies);
            } else {
                loggerFile.debug('companies invalides inéxistants');
                return res.status(200).json({message: "entreprises invalides inéxistantes"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    blockCompany: async (req, res) => {
        let {id} = req.params;
        loggerFile.info("in blockCompany by admin")
        try {
            await Company.findByIdAndUpdate(id, {validated: false});
            let company = await Company.findById(id);
            loggerFile.info("in send email after bloqued")
            let testAuth = await authService.block(id);
            if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
            mailService.block({
                email: company.email
            });
            loggerFile.debug('entreprise bloqué')
            return res.status(200).json({message: "Entreprise bloquée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unblockCompany: async (req, res) => {
        loggerFile.info("in unblockCompany by admin")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {validated: true});
            let company = await Company.findById(id);
            loggerFile.info("in send email after unbloqued")
            let testAuth = await authService.unblock(id);
            if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
            mailService.unblock({
                email: company.email
            });
            loggerFile.debug('entreprise débloquée')
            return res.status(200).json({message: "entreprise débloquée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentOfCompany: async (req, res) => {
        loggerFile.info("in validateDocumentOfCompany by admin")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, true);
                    loggerFile.info("in send email after validate document")
                    await mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents de l'entreprise validées"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la valdiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateDocumentOfCompany: async (req, res) => {
        loggerFile.info("in unvalidateDocumentOfCompany admin")
        let {id} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, false);
                    loggerFile.info("in send email after unvalidate document")
                    await mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents de l'entreprise non validées"});
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(200).json({message: "un problème est survenu au niveau de l'invaldiation des documents"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfCompany: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfCompany by admin")
        try {
            const profileEntreprises = await profileEntrepriseCompany.find();
            if (profileEntreprises) {
                loggerFile.debug('get all profileEntreprises data done')
                return res.status(200).json(profileEntreprises);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfCompanyById: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfCompanyById by admin")
        let {id} = req.params;
        try {
            const OneprofileEntreprise = await profileEntrepriseCompany.findOne({id_company: id});
            if (OneprofileEntreprise) {
                let profile = {
                    name: OneprofileEntreprise.name,
                    size: OneprofileEntreprise.size,
                    sector_activity: OneprofileEntreprise.sector_activity,
                    logo: OneprofileEntreprise.logo,
                }
                let facturation = {
                    social_reason: OneprofileEntreprise.social_reason,
                    siret: OneprofileEntreprise.siret,
                    tva_intracom: OneprofileEntreprise.tva_intracom,
                    address: OneprofileEntreprise.address,
                    address_plus: OneprofileEntreprise.address_plus,
                }
                let contact = {
                    lastName: OneprofileEntreprise.lastName,
                    firstName: OneprofileEntreprise.firstName,
                    email: OneprofileEntreprise.email,
                    send_compta: OneprofileEntreprise.send_compta,
                }
                return res.status(200).json({
                    profile: profile,
                    facturation: facturation,
                    contact: contact
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateCompanyFacturationByAdmin: async (req, res) => {
        loggerFile.info("in updateCompanyFacturationByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            let {token} = req.headers;
            data.id_company = id;
            let testProfil = await profilEntrepriseService.updateFacturation(id, token, data);
            if (testProfil) {
                loggerFile.debug("updateCompanyFacturationByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateCompanyFacturationByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateCompanyContactByAdmin: async (req, res) => {
        loggerFile.info("in updateCompanyContactByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            let {token} = req.headers;
            data.id_company = id;
            let testProfil = await profilEntrepriseService.updateContact(id, token, data);
            if (testProfil) {
                loggerFile.debug("updateCompanyContactByAdmin done");
                return res.status(200).json({message: "profil entreprise modifié"});
            } else {
                loggerFile.debug("updateCompanyContactByAdmin error");
                return res.status(200).json({message: "probléme rencontré lors de la modification du profil entreprise"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    createCollaborator: async (req, res) => {
        loggerFile.debug('in createCollaborator');
        let {id} = req.params;
        let {username, departement, email, password, firstName, lastName} = req.body;
        try {
            let collab = new Company({
                username,
                email,
                departement,
                firstName,
                lastName
            });
            collab.role = "Collab";
            collab.validated = false;
            await Company.create(collab).then(async data => {
                if (data) {
                    await Company.findByIdAndUpdate(id, {$push: {worked_with: data._id}}, {
                        new: true,
                        upsert: true
                    }).then(async response => {
                        if (response) {
                            let testAuth = await authService.create({
                                username: username,
                                email: email,
                                password: password,
                                idUser: data._id,
                                role: "Collab"
                            });
                            if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la création de compte"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de l'ajout d'un collaborateur à la liste"});
                        }
                    });
                    return res.status(200).json({
                        message: "collaborateur créé",
                        url: `${process.env.URL_COMPANY}/company/add-collab/${id}/${data._id}`
                    });
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de la création d'un collaborateur"});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addCollaborator: async (req, res) => {
        loggerFile.debug('in addCollaborator');
        let {id, idCollab} = req.params;
        try {
            await Company.findById(id).then(async data => {
                if (data && data.worked_with.includes(idCollab)) {

                    await Company.findByIdAndUpdate(idCollab, {$push: {company_collaborator: id}, validated: true}, {
                        new: true,
                        upsert: true
                    }).then(async response => {
                        if (response) {
                            loggerFile.debug("confirmation compte collab step 2 update done")
                            return res.status(200).json({message: "confirmation du collaborateur effectué"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de la confirmation du collaborateur"});
                        }
                    })

                } else return res.status(400).json({message: "le collaborateur ne fait pas partie de l'entreprise"});
            }).catch(err => {
                if (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de l'ajout d'un collaborateur"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeCollaborator: async (req, res) => {
        loggerFile.debug('in removeCollaborator');
        let {id, idCollab} = req.params;
        try {
            await Company.findByIdAndUpdate(id, {$pull: {worked_with: new ObjectId(idCollab)}}, {
                new: true,
                upsert: true
            }).then(async data => {
                if (data) {
                    await Company.findByIdAndDelete(idCollab).then(async data2 => {
                        if (data2) {
                            await authService.delete(idCollab);
                            return res.status(200).json({message: "Collaborateur supprimé"});
                        }
                    }).catch(err => {
                        if (err) {
                            loggerFile.error(utils.getClientAddress(req, err))
                            return res.status(400).json({message: "un problème est survenu lors de la suppression d'un collaborateur"});
                        }
                    })
                }
            }).catch(err => {
                if (err) {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu lors de la suppression d'un collaborateur"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in addFavoriteFreelance');
        let id = req.params.idCompany;
        let idFreelance = req.params.id;
        try {
            if (req.auth.role === 'Collab') {
                const collab = await Company.findById(id);
                if (collab.company_collaborator[0]) {
                    const company = await Company.findById(collab.company_collaborator[0])
                    if (company.favorites.includes(idFreelance)) {
                        return res.status(401).json({message: "freelance éxistant"});
                    } else {
                        await Company.findByIdAndUpdate(company._id, {$push: {favorites: idFreelance}}, {
                            new: true,
                            upsert: true
                        }).then(async data => {
                            if (data) {
                                loggerFile.debug('addFavoriteFreelance done');
                                return res.status(200).json({message: "ajout du freelance à la liste des favoris effectué"});
                            }
                        }).catch(err => {
                            if (err) {
                                loggerFile.error(utils.getClientAddress(req, err))
                                return res.status(400).json({message: "un problème est survenu lors de l'ajout du freelance de la liste des favoris"});
                            }
                        })
                    }
                } else {
                    return res.status(200).json({message: "le collaborateur doit être validé avant"});
                }

            } else {
                await Company.findByIdAndUpdate(id, {$push: {favorites: idFreelance}}, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('addFavoriteFreelance done');
                        return res.status(200).json({message: "ajout du freelance à la liste des favoris effectué"});
                    }
                }).catch(err => {
                    if (err) {
                        loggerFile.error(utils.getClientAddress(req, err))
                        return res.status(400).json({message: "un problème est survenu lors de l'ajout du freelance de la liste des favoris"});
                    }
                })
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeFavoriteFreelance: async (req, res) => {
        loggerFile.debug('in removeFavoriteFreelance');
        let id = req.params.idCompany;
        let idFreelance = req.params.id;
        try {
            if (req.auth.role === 'Collab') {
                const collab = await Company.findById(id);
                if (collab.company_collaborator[0]) {
                    const company = await Company.findById(collab.company_collaborator[0])
                    if (!company.favorites.includes(idFreelance)) {
                        return res.status(401).json({message: "freelance inéxistant"});
                    } else {
                        await Company.findByIdAndUpdate(company._id, {$pull: {favorites: idFreelance}}, {
                            new: true,
                            upsert: true
                        }).then(async data => {
                            if (data) {
                                loggerFile.debug('removeFavoriteFreelance done');
                                return res.status(200).json({message: "suppression du freelance à la liste des favoris effectué"});
                            }
                        }).catch(err => {
                            if (err) {
                                loggerFile.error(utils.getClientAddress(req, err))
                                return res.status(400).json({message: "un problème est survenu lors de la suppression du freelance de la liste des favoris"});
                            }
                        })
                    }
                } else {
                    return res.status(200).json({message: "le collaborateur doit être validé avant"});
                }

            } else {
                await Company.findByIdAndUpdate(id, {$pull: {favorites: idFreelance}}, {
                    new: true,
                    upsert: true
                }).then(async data => {
                    if (data) {
                        loggerFile.debug('removeFavoriteFreelance done');
                        return res.status(200).json({message: "suppression du freelance de la liste des favoris effectué"});
                    }
                }).catch(err => {
                    if (err) {
                        loggerFile.error(utils.getClientAddress(req, err))
                        return res.status(400).json({message: "un problème est survenu lors de la suppression du freelance de la liste des favoris"});
                    }
                })
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
