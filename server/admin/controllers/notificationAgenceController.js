const notificationAgence = require("../models/notificationAgence.model")
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    createDetailsContactNotif: async (req, res) => {
        loggerFile.info("in createDetailsContactNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createDetailsContactNotif data done');
            return res.status(200).json({message: "notification créé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createLegalRepresentativeNotif: async (req, res) => {
        loggerFile.info("in createLegalRepresentativeNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createLegalRepresentativeNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createTaxeNotif: async (req, res) => {
        loggerFile.info("in createTaxeNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createTaxeNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createLegalMentionNotif: async (req, res) => {
        loggerFile.info("in createLegalMentionNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createLegalMentionNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createIbanNotif: async (req, res) => {
        loggerFile.info("in createIbanNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createIbanNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createUploadDocumentNotif: async (req, res) => {
        loggerFile.info("in createUploadDocumentNotif");
        try {
            const {username, role, changes} = req.body;
            const notif = new notificationAgence({username, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createUploadDocumentNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createUploadOfferDocumentNotif: async (req, res) => {
        loggerFile.info("in createUploadOfferDocumentNotif");
        try {
            const {username, role, offer_name, changes} = req.body;
            const notif = new notificationAgence({username, offer_name, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createUploadOfferDocumentNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error)
            return res.status(401).json({message: error.message});
        }
    },
    createAddOfferNotif: async (req, res) => {
        loggerFile.info("in createAddOfferNotif");
        try {
            const {username, role, offer_name, changes} = req.body;
            const notif = new notificationAgence({username, offer_name, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createAddOfferNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createUpdateOfferNotif: async (req, res) => {
        loggerFile.info("in createUpdateOfferNotif");
        try {
            const {username, role, offer_name, changes} = req.body;
            const notif = new notificationAgence({username, offer_name, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createUpdateOfferNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    createDeleteOfferNotif: async (req, res) => {
        loggerFile.info("in createDeleteOfferNotif");
        try {
            const {username, role, offer_name, changes} = req.body;
            const notif = new notificationAgence({username, offer_name, role, changes});
            await notificationAgence.create(notif);
            loggerFile.debug('createDeleteOfferNotif data done')
            return res.status(200).json({message: "notification créé"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
    checkNotification: async (req, res) => {
        loggerFile.info('in notification');
        let {id} = req.params;
        try {
            let notif = await notificationAgence.findById(id);
            if (notif) {
                await notificationAgence.findByIdAndUpdate(id, {checked: true});
                loggerFile.debug('checkNotification data done');
                return res.status(200).json({message: "notification checked"});
            } else return res.status(400).json({message: "notification inéxistante"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotification: async (req, res) => {
        loggerFile.info('in getAllNotification');
        try {
            let notifications = await notificationAgence.find();
            loggerFile.debug('getAllNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllCheckedNotification');
        try {
            let notifications = await notificationAgence.find({checked: true});
            loggerFile.debug('getAllCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotification: async (req, res) => {
        loggerFile.info('in getAllUnCheckedNotification');
        try {
            let notifications = await notificationAgence.find({checked: false});
            loggerFile.debug('getAllUnCheckedNotification data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllNotificationByPeriod');
        try {
            let notifications = await notificationAgence.find({
                date: {
                    $gte: dateBegin,
                    $lte: dateEnd
                }
            });
            loggerFile.debug('getAllNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllCheckedNotificationByPeriod');
        try {
            let notifications = await notificationAgence.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: true
            });
            loggerFile.debug('getAllCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getAllUnCheckedNotificationByPeriod: async (req, res) => {
        let {dateBegin, dateEnd} = req.body;
        loggerFile.info('in getAllUnCheckedNotificationByPeriod');
        try {
            let notifications = await notificationAgence.find({
                date: {
                    $gte: dateBegin,
                    $lt: dateEnd
                }, checked: false
            });
            loggerFile.debug('getAllUnCheckedNotificationByPeriod data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll notification Freelance");
        try {
            let notifications = await notificationAgence.find();
            loggerFile.debug('findAll data done');
            return res.status(200).json(notifications);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: error.message});
        }
    },
}
