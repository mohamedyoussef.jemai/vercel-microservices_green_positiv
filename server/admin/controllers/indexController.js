const mongoose = require('mongoose');
const passport = require('passport');
const Admin = mongoose.model('Admin');
const sendEmail = require("../utils/sendEmail");
const crypto = require('crypto');
const jwt = require('jsonwebtoken');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    login: async (req, res) => {
        loggerFile.info("in login");
        await passport.authenticate('local', async function (err, admin, info) {
            let token;
            if (err) {
                loggerFile.error(err);
                return res.status(404).json({message: err.message});
            }
            if (admin) {
                if (admin.visibility === 0) {
                    loggerFile.error("visibility 0");
                    return res.status(401).send({
                        message: "Votre compte a été bloqué"
                    });
                } else {
                    try {
                        token = admin.generateJwt();
                        res.status(200);
                        res.json({
                            "token": token,
                        });
                    } catch {
                        loggerFile.error("erreur de connexion");
                        return res.status(400).send({
                            message: "erreur de connexion"
                        });
                    }
                }
            } else {
                loggerFile.debug("not found");
                return res.status(401).json(info);
            }
        })(req, res);
    },
    register: async (req, res) => {
        loggerFile.info("in register");
        const {email, username, password} = req.body;
        const ExistRoot = await Admin.findOne({email: email});
        if (ExistRoot) {
            return res.status(401).json({message: "utilisateur éxistant"});
        } else {
            let root = new Admin({email, username});
            root.setPassword(password);
            root.role = "Root";
            try {
                await Admin.create(root);
                let token = root.generateJwt();
                loggerFile.debug("register done");
                return res.status(200).json({
                    "token": token
                });
            } catch (err) {
                loggerFile.error("accés non autorisé");
                return res.status(401).json({
                    message: "accés non autorisé"
                });
            }
        }
    },
    forgotPassword: async (req, res) => {
        loggerFile.info("in forgotPassword");
        try {
            let user = await Admin.findOne({email: req.body.email});
            if (!user) return
            {
                loggerFile.debug("administrateur inéxistant");
                return res.status(400).json({message: "administrateur inéxistant"});
            }
            try {
                let randomPwd = await crypto.randomBytes(16).toString('hex');
                user.salt = await crypto.randomBytes(16).toString('hex');
                user.hash = await crypto.pbkdf2Sync(randomPwd, user.salt, 1000, 64, 'sha512').toString('hex');
                await Admin.findByIdAndUpdate(user._id, user);
                await sendEmail(user.email, "Mot de pass oublié", "ci-joint le mot de passe : " + randomPwd);
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
            }
            return res.status(200).json({message: "lien de récupération envoyé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(401).json({message: "erreur lors du récupération du mot de passe"});
        }
    },
    checkedToken: async (req, res) => {
        const {token} = req.headers;
        jwt.verify(token, process.env.MY_SECRET, (err) => {
            if (err) {
                loggerFile.info("token expiré");
                return res.status(401).json({token: false, error: "token expiré"});
            }
            else return res.status(200).json({token: true, message: "continue"});
        });
    },
    updatePassword: async (req, res) => {
        loggerFile.info('in updatePassword');
        const {
            password,
            repeatPassword
        } = req.body;
        try {
            if (password.length < 8) {
                res.status(401).json({message: "le mot de passe doit être de longueur minimum 8"});
            }
            if (password != repeatPassword) {
                res.status(401).json({message: "les mots de passes ne sont pas égaux"});
            }
            const token = req.auth;
            await Admin.findOne({id: token._idConnected}).then(async response => {
                await response.setPassword(password);
                await Admin.findByIdAndUpdate(response._id, response).then(response2 => {
                    if (response2) {
                        loggerFile.info('user password updated');
                        return res.status(200).json({message: "mot de passe modifié"});
                    }
                }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                });
            }).catch(err2 => {
                loggerFile.debug("user inéxistant ", err2)
                return res.status(401).json({message: "utilisateur inéxistant"});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    },
}
