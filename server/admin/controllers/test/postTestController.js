const fs = require('fs');
const Post = require('../../models/post.model');
const loggerFile = require("../../config/logger");
module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create post")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {imageName, title, content, url_fb, url_instagram, url_twitter, url_linkedin, resume} = req.body;
        const post = new Post({title, content, url_fb, url_instagram, url_twitter, url_linkedin, resume});
        post.author = req.auth.usernameConnected;
        post.image = imageName;
        try {
            await Post.create(post).then(response => {
                if (response) {
                    loggerFile.debug("post created done")
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
            return res.status(200).json({message: "Article publié"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findALl posts")
        try {
            const posts = await Post.find();
            loggerFile.debug("findAll posts done")
            return res.status(200).json(posts);
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne post by ID")
        const {id} = req.params;
        try {
            const post = await Post.findById(id);
            if (post) {
                loggerFile.debug("findOne post done")

                return res.status(200).json(post);
            } else {
                loggerFile.debug("aucun Article éxistant")
                return res.status(400).json({message: "aucun Article éxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update post by ID")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        const newPost = req.body;
        newPost.author = req.auth.usernameConnected;
        newPost.image = req.body.new_image;
        try {
            await Post.findByIdAndUpdate(id, newPost);
            loggerFile.debug("Article modifié avec succés")
            res.status(200).json({message: "Article modifié avec succés"});
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in delete post by ID")
        const {id} = req.params;
        try {
            await Post.findByIdAndDelete(id);
            loggerFile.debug("delete post done")
            return res.status(200).json({message: "Article effacé avec succés"})
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    validatePost: async (req, res) => {
        loggerFile.info("in validatePost")
        let {id} = req.params;
        try {
            await Post.findByIdAndUpdate(id, {validated: true}).then(response => {
                if (response) {
                    loggerFile.debug("Article Publié avec succés")
                    res.status(200).json({message: "Article publié avec succés"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    unValidatePost: async (req, res) => {
        loggerFile.info("in unValidatePost")
        let {id} = req.params;
        try {
            await Post.findByIdAndUpdate(id, {validated: false}).then(response => {
                if (response) {
                    loggerFile.debug("Article bloqué avec succés")
                    res.status(200).json({message: "Article bloqué avec succés"});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
}

