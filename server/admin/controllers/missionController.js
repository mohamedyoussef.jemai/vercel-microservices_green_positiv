const Mission = require('../models/mission.model');
const requiredProfil = require('../models/requiredProfil.model');
const Company = require('../models/company.model');
const Devis = require('../models/devis.model');
const Freelancer = require('../models/freelancer.model');
const Agence = require('../models/agence.model');
const Payment = require('../models/payment.model');

const freelanceService = require('../services/freelanceService')
const agenceService = require('../services/agenceService')

const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const ObjectId = require('mongoose').Types.ObjectId;
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create mission")
        let id = req.auth._idConnected;
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let mission = req.mission;
            await Company.findByIdAndUpdate(id, {$push: {missions: mission._id}}, {new: true, upsert: true});
            loggerFile.debug('addMission data done')
            return res.status(200).json({message: "mission ajoutée"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info('in update Mission');
        let {id} = req.params;
        let new_doc = "";
        let cloudinary_doc_id = "";
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let mission = await Mission.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_doc = result.secure_url;
                cloudinary_doc_id = result.public_id;
                try {
                    if (req.body.old_document)
                        await cloudinary.uploader.destroy(mission.cloudinary_doc_id);
                } catch (err) {
                     loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement du document"})
                }
            } else {
                new_doc = req.body.old_doc;
            }
            let {
                name,
                description,
                city,
                objectif,
                dateBegin,
                dateEnd,
                price_per_day,
                supp_month,
                period_per_month,
                work_frequence,
                show_price,
                budget,
                telework,
                nb_days_telework,
                comments,
                local_city,
            } = req.body;
            let newMission = {
                name,
                description,
                city,
                objectif,
                dateBegin,
                price_per_day,
                supp_month,
                period_per_month,
                work_frequence,
                show_price,
                budget,
                telework,
                nb_days_telework,
                comments,
                local_city,
                dateEnd
            }
            newMission.document = await new_doc;
            newMission.cloudinary_doc_id = cloudinary_doc_id;
            await Mission.findByIdAndUpdate(id, newMission);
            loggerFile.debug('mission modifié avec succés')
            return res.status(200).json({message: "mission modifié avec succés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in deleteMission")
        try {
            let idMission = req.params.id;
            let mission = await Mission.findById(idMission);
            if (mission.cloudinary_doc_id) {
                await cloudinary.uploader.destroy(mission.cloudinary_doc_id);
            }
            await Mission.findByIdAndDelete(idMission);
            await requiredProfil.findByIdAndDelete(new ObjectId(mission.profilSearched))
            await Company.findByIdAndUpdate(req.auth._idConnected, {$pull: {missions: new ObjectId(idMission)}}, {
                new: true,
                upsert: true,
                multi: true
            }).then(async data => {
                if (data) {
                    loggerFile.debug('deleteMission done');
                    return res.status(200).json({message: "mission supprimée"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message})
            })

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne mission")
        try {
            let {id} = req.params;
            let mission = await Mission.findById(id);
            await requiredProfil.findById(mission.profilSearched).then(data => {
                let response = {
                    profile: data,
                    mission: mission
                }
                return res.status(200).json(response);
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisFromMission: async (req, res) => {
        loggerFile.info("in getDevisFromMission")
        let id = req.params.idCompany;
        try {
            let missions = await Mission.find({id_company: id});
            let array = [];
            let users = [];
            await missions.forEach(item => array.push(item._id));
            let devises = await Devis.find({id_mission: {$in: array}});
            if (devises.length > 0) {
                devises.forEach(async function (item, index, array) {
                    if (item.id_freelance) {
                        let freelancer = await Freelancer.findById(item.id_freelance);
                        await users.push(freelancer)
                        if (index + 1 == array.length) {
                            return res.status(200).json({devises: devises, users: users})
                        }
                    } else {
                        let agence = await Agence.findById(item.id_agence);
                        await users.push(agence)
                        if (index + 1 == array.length) {
                            return res.status(200).json({devises: devises, users: users})
                        }
                    }


                });

            } else {
                return res.status(200).json([])
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisFromMissionById: async (req, res) => {
        loggerFile.info("in getDevisFromMissionById")
        let {id, idCompany} = req.params;
        try {
            let mission = await Mission.findById(id);
            if (mission.id_company == idCompany) {
                let users = [];
                let i = 0;
                let unpayedAmounts = [];
                let devises = await Devis.find({id_mission: id});
                if (devises.length > 0) {
                    while (devises.length != unpayedAmounts.length) {
                        if (devises[i].id_freelance) {
                            let freelancer = await Freelancer.findById(devises[i].id_freelance);
                            await users.push(freelancer)
                            if (devises[i].confirmed && devises[i].confirmed == true) {
                                var amount = 0
                                let payments = await Payment.find({
                                    id_mission: id,
                                    id_freelance: devises[i].id_freelance
                                })
                                payments.forEach(async function (item2, index2, array2) {
                                    amount += item2.amount
                                    if (index2 + 1 == array2.length) {
                                        amount = (amount * 100) / item2.totalTva
                                        unpayedAmounts.push(Math.round(amount))
                                        i++
                                    }
                                });
                            } else {
                                unpayedAmounts.push(null)
                            }

                        } else if (devises[i].id_agence) {
                            let agence = await Agence.findById(devises[i].id_agence);
                            await users.push(agence)
                            if (devises[i].confirmed && devises[i].confirmed === true) {
                                var amount = 0
                                let payments = await Payment.find({id_mission: id, id_agence: devises.id_agence})
                                payments.forEach(async function (item2, index2, array2) {
                                    amount += item2.amount
                                    if (index2 + 1 == array2.length) {
                                        amount = (amount * 100) / item2.totalTva
                                        unpayedAmounts.push(Math.round(amount))
                                        i++
                                    }
                                });
                            } else {
                                unpayedAmounts.push(null)
                            }
                        }
                    }
                    return res.status(200).json({
                        devises: devises,
                        users: users,
                        unpayedAmounts: unpayedAmounts
                    })

                } else {
                    return res.status(200).json([])
                }
            } else {
                return res.status(400).json({message: "accés non autorisé"});

            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisById: async (req, res) => {
        loggerFile.info("in getDevisById")
        let idCompany = req.params.id;
        let {idMission} = req.params;
        try {
            let devis = await Devis.findById(idMission);
            if (devis.id_company == idCompany) {
                return res.status(200).json({devise: devis})
            } else {
                return res.status(400).json({message: "Accés non autorisé"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findFreelancedSearched: async (req, res) => {
        loggerFile.info("in findFreelancedSearched")
        try {
            let {id} = req.params;
            const mission = await Mission.findById(id);
            const requiredProfile = await requiredProfil.findById(mission.profilSearched)
            let response = await freelanceService.getRequiredProfiles(requiredProfile)
            return res.status(200).json({data: response.data});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAgenceSearched: async (req, res) => {
        loggerFile.info("in findAgenceSearched")
        let {id} = req.params;
        const {minPrice, maxPrice} = req.body;
        try {
            const mission = await Mission.findById(id);
            const requiredProfile = await requiredProfil.findById(mission.profilSearched)
            let response = await agenceService.getRequiredAgenceProfiles(requiredProfile, minPrice, maxPrice)
            return res.status(200).json({data: response.data});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifDevis: async (req, res) => {
        loggerFile.info("in verifDevis")
        let {idCompany, idMission} = req.body;
        try {
            let mission = await Mission.findById(idMission);
            let company = await Company.findById(idCompany);
            if (company && mission && mission.id_company.equals(company._id)) {
                return res.status(200).json({message: "Devis vérifié"})
            } else {
                return res.status(400).json({message: "les informations sont erronés"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    acceptFreelance: async (req, res) => {
        loggerFile.info("in acceptFreelance")
        let {id} = req.params;
        let {id_freelance} = req.body
        try {
            let devis = await Devis.findById(id);
            let mission = await Mission.findById(devis.id_mission)

            if (mission && !mission.freelancers.includes(id_freelance)) {
                await Mission.findByIdAndUpdate(devis.id_mission, {$push: {freelancers: id_freelance}}, {
                    new: true,
                    upsert: true
                });
                await Devis.findByIdAndUpdate(id, {confirmed: true, state: "terminé"});
                return res.status(200).json({message: "Freelance ajouté"});
            } else {
                return res.status(400).json({message: "Freelance éxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    refuseFreelance: async (req, res) => {
        loggerFile.info("in refuseFreelance")
        let {id} = req.params;
        try {
            await Devis.findByIdAndUpdate(id, {confirmed: false, state: "terminé"});
            return res.status(200).json({message: "Devis Freelance refusé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeFreelance: async (req, res) => {
        loggerFile.info("in removeFreelance")
        let idCompany = req.auth._idConnected;
        let {id_mission, id_freelance} = req.body
        let {id} = req.params;
        try {
            let mission = await Mission.findById(id_mission);
            if (mission && mission.id_company.equals(idCompany) && mission.freelancers.includes(id_freelance)) {
                await Mission.findByIdAndUpdate(id_mission, {$pull: {freelancers: id_freelance}}, {
                    new: true,
                    upsert: true
                });
                await Devis.findByIdAndUpdate(id, {confirmed: false, state: "supprimé"});
                return res.status(200).json({message: "Freelance supprimé"});
            } else {
                return res.status(400).json({message: "Freelance inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    acceptAgence: async (req, res) => {
        loggerFile.info("in acceptAgence")
        let {id} = req.params;
        let {id_agence} = req.body
        try {
            let devis = await Devis.findById(id);
            let mission = await Mission.findById(devis.id_mission)

            if (mission) {
                await Mission.findByIdAndUpdate(devis.id_mission, {id_agence: id_agence}, {
                    new: true,
                    upsert: true
                });
                await Devis.findByIdAndUpdate(id, {confirmed: true, state: "terminé"});
                return res.status(200).json({message: "Agence ajoutée"});
            } else {
                return res.status(400).json({message: "Agence éxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    refuseAgence: async (req, res) => {
        loggerFile.info("in refuseAgence")
        let {id} = req.params;
        try {
            await Devis.findByIdAndUpdate(id, {confirmed: false, state: "terminé"});
            return res.status(200).json({message: "Agence refusé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    removeAgence: async (req, res) => {
        loggerFile.info("in removeAgence")
        let idCompany = req.auth._idConnected;
        let {id} = req.params;
        let {id_mission, id_agence} = req.body
        try {
            let mission = await Mission.findById(id_mission);
            if (mission && mission.id_company.equals(idCompany) && mission.id_agence == id_agence) {
                await Mission.findByIdAndUpdate(id_mission, {id_agence: null}, {
                    new: true,
                    upsert: true
                });
                await Devis.findByIdAndUpdate(id, {confirmed: false, state: "supprimé"});
                return res.status(200).json({message: "Agence supprimé"});
            } else {
                return res.status(400).json({message: "Agence inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findCompanyMissions: async (req, res) => {
        loggerFile.info("findCompanyMissions");
        let {id} = req.params
        let missions = []
        try {
            let company = await Company.findById(id);
            if (company.missions.length > 0) {
                company.missions.forEach(async function (id, index, array) {
                    await Mission.findById(id).then(async data => {
                        let profile = await requiredProfil.findById(data.profilSearched)
                        let devis = await Devis.find({id_mission: id, state: 'en cours'});
                        await missions.push({mission: data, profile: profile, nbDevis: devis.length})
                        if (missions.length == array.length) {
                            return res.status(200).send(missions)
                        }
                    }).catch(err => {
                         loggerFile.error(err,utils.getClientAddress(req, err))
                    })
                });
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error});
        }
    },
    getDevisFreelance: async (req, res) => {
        loggerFile.info("getDevisFreelance");
        let id = req.auth._idConnected;
        let missions = []
        let i = 0;
        let unpayedAmounts = [];
        try {
            let devises = await Devis.find({id_freelance: id});
            if (devises.length > 0) {
                while (devises.length != unpayedAmounts.length) {
                    let mission = await Mission.findById(devises[i].id_mission);
                    await missions.push(mission)
                    if (devises[i].confirmed && devises[i].confirmed == true) {
                        var amount = 0
                        let payments = await Payment.find({
                            id_mission: mission._id,
                            id_freelance: devises[i].id_freelance
                        })
                        payments.forEach(async function (item2, index2, array2) {
                            amount += item2.amount
                            if (index2 + 1 == array2.length) {
                                amount = (amount * 100) / item2.totalTva
                                unpayedAmounts.push(Math.round(amount))
                                i++
                            }
                        });
                    } else {
                        unpayedAmounts.push(null)
                    }
                }
                return res.status(200).json({
                    devises: devises, missions: missions.map(el => {
                        return {
                            _id: el._id,
                            name: el.name,
                            id_company: el.id_company
                        }
                    }),
                    unpayedAmounts: unpayedAmounts
                })
            } else {
                return res.status(200).json([])
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
