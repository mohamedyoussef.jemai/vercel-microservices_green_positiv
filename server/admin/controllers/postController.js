const Post = require('../models/post.model');
const loggerFile = require("../config/logger");
const notificationService = require("../services/notificationService");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');
module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create post")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {title, content, url_fb, url_instagram, url_twitter, url_linkedin, resume} = req.body;
        const post = new Post({title, content, url_fb, url_instagram, url_twitter, url_linkedin, resume});
        try {
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                post.author = req.auth.usernameConnected;
                post.image = result.secure_url;
                post.cloudinary_id = result.public_id;

                await Post.create(post).then(async response => {
                    if (response) {
                        loggerFile.debug("post created done")
                        await notificationService.postCreatedNotif(req.auth._idConnected, post);
                        loggerFile.debug("post created notificaiton done")
                        return res.status(200).json({message: "Article publié"});
                    }
                }).catch(err => {
                    loggerFile.error(err);
                    return res.status(400).json({message: err.message});
                });

            } else {
                return res.status(400).json({message: "image inexistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findALl posts")
        try {
            const posts = await Post.find();
            loggerFile.debug("findAll posts done")
            return res.status(200).json(posts);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne post by ID")
        const {id} = req.params;
        try {
            const post = await Post.findById(id);
            if (post) {
                loggerFile.debug("findOne post done")

                return res.status(200).json(post);
            } else {
                loggerFile.debug("aucun Article éxistant")
                return res.status(400).json({message: "aucun Article éxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update post by ID")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let new_image = "";
        let cloudinary_id = "";
        try {
            let post = await Post.findById(id);
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_image = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    await cloudinary.uploader.destroy(post.cloudinary_id);

                } catch (err) {
                    loggerFile.error(err)
                }
            } else {
                return res.status(400).json({message: "image inexistante"});

            }
            const newPost = req.body;
            newPost.author = req.auth.usernameConnected;
            newPost.image = new_image;
            newPost.cloudinary_id = cloudinary_id;

            await notificationService.updatePostNotif(req.auth._idConnected, id, newPost);
            await Post.findByIdAndUpdate(id, newPost);
            loggerFile.debug("Article modifié avec succés")
            return res.status(200).json({message: "Article modifié avec succés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in delete post by ID")
        const {id} = req.params;
        try {
            let post = await Post.findById(id);
            if (post) {
                await cloudinary.uploader.destroy(post.cloudinary_id);
            } else {
                loggerFile.debug("Article inéxistant")
                return res.status(400).json({message: "Article inéxistant"})
            }
            await Post.findByIdAndDelete(id);
            loggerFile.debug("delete post done")
            return res.status(200).json({message: "Article effacé avec succés"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteAll: async (req, res) => {
        loggerFile.info("in deleteAll post")
        try {
            let posts = await Post.find();
            await posts.forEach(async function callback(post, index) {
                await cloudinary.uploader.destroy(post.cloudinary_id);
            });
            await Post.deleteMany().then(data => {
                loggerFile.debug('deleteAll posts done')
                return res.status(200).json({message: `${data.deletedCount} Articles effacés avec succés `})
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validatePost: async (req, res) => {
        loggerFile.info("in validatePost")
        let {id} = req.params;
        try {
            await Post.findByIdAndUpdate(id, {validated: true}).then(async response => {
                if (response) {
                    await notificationService.validatedPostNotif(id)
                    loggerFile.debug("Article Publié avec succés")
                    return res.status(200).json({message: "Article publié avec succés"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unValidatePost: async (req, res) => {
        loggerFile.info("in unValidatePost")
        let {id} = req.params;
        try {
            await Post.findByIdAndUpdate(id, {validated: false}).then(async response => {
                if (response) {
                    await notificationService.unvalidatedPostNotif(id)
                    loggerFile.debug("Article bloqué avec succés")
                    return res.status(200).json({message: "Article bloqué avec succés"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

