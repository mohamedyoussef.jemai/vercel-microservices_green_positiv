require('dotenv').config();
const cloudinary = require('../config/cloudinary');
const loggerFile = require("../config/logger");
const Freelancer = require("../models/freelancer.model");
const mailService = require("../services/mailingService");
const authService = require("../services/authService");
const profilEntrepriseService = require("../services/profilEntrepriseService");
const Certification = require("../models/certification.model");
const Formation = require("../models/formation.model");
const Experience = require("../models/experience.model");
const Devis = require("../models/devis.model");
const Mission = require("../models/mission.model");
const Payment = require("../models/payment.model");
const profileEntreprise = require("../models/profileEntreprise.model");
const missionService = require("../services/missionService")
const utils = require('../config/utils');

module.exports = {
    getRequiredProfiles: async (req, res) => {
        let finalFreelancers = []
        let {title_profile, jobCat, level, skillsNeeded, skillsAppreciated, languages} = req.body;
        try {
            let freelancers = await Freelancer.find({
                title_profile: title_profile,
                jobCat: jobCat,
                level: level,
                visibility: 1,
                validated: true
            });
            let freelancersNeeded = []
            let freelancersAppreciated = []
            await freelancers.forEach(async function (freelance, index, array2) {
                let testLanguage = await missionService.testLanguage(freelance, languages);
                if (testLanguage) finalFreelancers.push(freelance)
            })

            await freelancers.forEach(async function (freelance, index, array) {
                let testSkillsNeeded = await missionService.testSkillsNeeded(freelance, skillsNeeded);
                if (testSkillsNeeded) freelancersNeeded.push(freelance)
            })
            await freelancers.forEach(async function (freelance, index, array2) {
                let testSkillsAppreciated = await missionService.testSkillsNeeded(freelance, skillsAppreciated);
                if (testSkillsAppreciated) freelancersAppreciated.push(freelance)
            })

            return res.status(200).send({
                freelancers: finalFreelancers.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        languages: el.languages
                    }
                }),
                freelancersNeeded: freelancersNeeded.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                }),
                freelancersAppreciated: freelancersAppreciated.map(el => {
                    return {
                        _id: el._id,
                        username: el.username,
                        email: el.email,
                        lastName: el.lastName,
                        firstName: el.firstName,
                        phone: el.phone,
                        image: el.image,
                        level: el.level,
                        price_per_day: el.price_per_day,
                        show_price: el.show_price,
                        skills: el.skills
                    }
                })
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    uploadKabisDocuments: async (req, res) => {
        loggerFile.info('in uploadKabisDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let docs = [];
        let cloudinary_kabis_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new kabis');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_kabis_id = result.public_id;
                        let newFreelancer = req.body;
                        newFreelancer.documents = freelancer.documents;
                        newFreelancer.documents[1] = docs[0]
                        newFreelancer.cloudinary_kabis_id = cloudinary_kabis_id;
                        newFreelancer.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFreelancer);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFreelance = req.body;
                newFreelance.documents = freelancer.documents;
                newFreelance.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFreelance);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadVigilanceDocuments: async (req, res) => {
        loggerFile.info('in uploadVigilanceDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let docs = [];
        let cloudinary_vigilance_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new vigilance');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_vigilance_id = result.public_id;
                        let newFreelance = req.body;
                        newFreelance.documents = freelancer.documents;
                        newFreelance.documents[2] = docs[0]
                        newFreelance.cloudinary_vigilance_id = cloudinary_vigilance_id;
                        newFreelance.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFreelance);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFreelance = req.body;
                newFreelance.documents = frelancer.documents;
                newFreelance.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFreelance);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    uploadSasuDocuments: async (req, res) => {
        loggerFile.info('in uploadSasuDocuments');
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        let docs = [];
        let cloudinary_sasu_id = "";
        let freelancer = await Freelancer.findById(id);

        try {
            if (req.files) {
                await req.files.forEach(async function callback(file, index) {
                    loggerFile.debug('get new documents');
                    await cloudinary.uploader.upload(file.path).then(async result => {
                        docs.push(result.secure_url);
                        cloudinary_sasu_id = result.public_id;
                        let newFrelancer = req.body;
                        newFrelancer.documents = freelancer.documents;
                        newFrelancer.documents[3] = docs[0]
                        newFrelancer.cloudinary_sasu_id = cloudinary_sasu_id;
                        newFrelancer.documents_val = false;

                        await Freelancer.findByIdAndUpdate(id, newFrelancer);
                        loggerFile.debug("Documents ajoutés");
                        return res.status(200).json({message: "Documents ajoutés"});
                    });
                });
            } else {
                loggerFile.debug('get old documents');
                docs.push(req.body.old_documents);
                let newFrelancer = req.body;
                newFrelancer.documents = freelancer.documents;
                newFrelancer.documents_val = false;
                await Freelancer.findByIdAndUpdate(id, newFrelancer);
                loggerFile.debug("Documents ajoutés");
                return res.status(200).json({message: "Documents ajoutés"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll freelancers");
        try {
            const freelancers = await Freelancer.find();
            if (freelancers) {
                loggerFile.debug('get all freelancers done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('aucun freelancers éxistants')
                return res.status(200).json({message: "freelancers inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in find freelancer by ID");
        const {id} = req.params;
        try {
            const freelancer = await Freelancer.findById(id);
            if (freelancer) {
                let jobCat = "";
                await req.body.jobs.forEach(async function callback(job, index) {
                    if (freelancer.jobCat == job._id) {
                        jobCat = await job.name;
                    }
                });
                loggerFile.debug('get freelancer by id');
                return await res.status(200).json({
                    freelancer: freelancer,
                    certifications: req.body.certifications,
                    formations: req.body.formations,
                    experiences: req.body.experiences,
                    jobCat: jobCat,
                    skills: req.body.allSkills
                });
            } else {
                loggerFile.debug('freelancer inéxistant');
                return res.status(200).json({message: "freelancer inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error);
            if (error.name === 'CastError') {
                return res.status(400).json({message: "freelancer inéxistant"});
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error});
            }
        }
    },
    deleteFreelancer: async (req, res) => {
        loggerFile.info("in deleteFreelancer by admin")
        const {id} = req.params;
        try {
            await Freelancer.findById(id).then(async data => {
                if (data !== null) {
                    if (data.image) {
                        await cloudinary.uploader.destroy(data.cloudinary_id);
                    }
                    if (data.documents.length !== 0) {
                        await data.documents.forEach(async function callback(document, index) {
                            await cloudinary.uploader.destroy(data.cloudinary_doc_id);
                        });
                    }
                    await Freelancer.findByIdAndDelete(id);
                    let testProfil = await profilEntrepriseService.delete(id);
                    if (!testProfil) return res.status(400).json({message: "Un probléme est survenu lors de la suppression du profil"});

                    let testAuth = await authService.delete(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    loggerFile.debug('Freelancer effacé')
                    return res.status(200).json({message: "Freelancer effacé"})
                } else {
                    loggerFile.debug('freelancer inéxistant')
                    return res.status(400).json({message: "Freelancer inéxistant"})
                }
            }).catch(error => {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    getDocumentValidatedFreelancers: async (req, res) => {
        loggerFile.info("in getDocumentValidatedFreelancers")
        try {
            const freelancers = await Freelancer.find({documents_val: true});
            if (freelancers) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('freelancers valides inéxistants')
                return res.status(200).json({message: "freelancers valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDocumentUnvalidatedFreelancers: async (req, res) => {
        loggerFile.info("in getDocumentUnvalidatedFreelancers")

        try {
            const freelancers = await Freelancer.find({documents_val: false});
            if (freelancers) {
                loggerFile.debug('findAllDocumentValidated data done')
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug('freelancers valides inéxistants')
                return res.status(200).json({message: "freelancers valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getValidatedFreelancers: async (req, res) => {
        loggerFile.info("in getValidatedFreelancers")
        try {
            const freelancers = await Freelancer.find({validated: true});
            if (freelancers) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug("freelancers valides inéxistants")
                return res.status(200).json({message: "freelancers valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getUnvalidatedFreelancers: async (req, res) => {
        loggerFile.info("in getUnvalidatedFreelancers")
        try {
            const freelancers = await Freelancer.find({validated: false});
            if (freelancers) {
                loggerFile.debug("findAllValidated data done")
                return res.status(200).json(freelancers);
            } else {
                loggerFile.debug("freelancers valides inéxistants")
                return res.status(200).json({message: "freelancers valides inéxistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    blockFreelancer: async (req, res) => {
        loggerFile.info("in blockFreelancer")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {validated: false}).then(async response => {
                if (response) {
                    let freelancer = await Freelancer.findById(id);
                    loggerFile.info("in send email after bloqued")
                    let testAuth = await authService.block(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    mailService.block({
                        email: freelancer.email
                    });
                    loggerFile.debug('freelancer bloqué')
                    return res.status(200).json({message: "Freelancer bloqué"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unblockFreelancer: async (req, res) => {
        loggerFile.info("in unblockFreelancer")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {validated: true}).then(async response => {
                if (response) {
                    let freelancer = await Freelancer.findById(id);
                    loggerFile.info("in send email after unbloqued")
                    let testAuth = await authService.unblock(id);
                    if (!testAuth) return res.status(400).json({message: "Un probléme est survenu lors de la suppression de compte"});
                    mailService.unblock({
                        email: freelancer.email
                    });
                    loggerFile.debug('freelancer débloqué')
                    return res.status(200).json({message: "Freelancer débloqué"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDocumentOfFreelancer: async (req, res) => {
        loggerFile.info("in valdiateDocumentOfFreelancer by admin")
        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {documents_val: true}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, true);
                    loggerFile.info("in send email after validate document")
                    mailService.validateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents du Freelancer validés"});
                }
            }).catch(err => {
                if (err) {
                     loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de la validiation des documents"});
                }

            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unvalidateDocumentOfFreelancer: async (req, res) => {
        loggerFile.info("in unvaldiateDocumentOfFreelancer admin")

        let {id} = req.params;
        try {
            await Freelancer.findByIdAndUpdate(id, {documents_val: false}).then(async data => {
                if (data) {
                    await profilEntrepriseService.validate(id, false);
                    loggerFile.debug("unValidateDocumentByAdmin update done");
                    loggerFile.info("in send email after unvalidate document")
                    mailService.unvalidateDocument({
                        email: data.email
                    });
                    return res.status(200).json({message: "Documents du Freelancer non validés"});
                }
            }).catch(err => {
                if (err) {
                     loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "un problème est survenu au niveau de l'invalidiation des documents"});
                }
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfFreelancer: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfFreelancer by admin")
        try {
            const profileEntreprises = await profileEntreprise.find();
            if (profileEntreprises) {
                loggerFile.debug('get all profileEntreprises data done')
                return res.status(200).json(profileEntreprises);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getProfileEntrepriseOfFreelancerById: async (req, res) => {
        loggerFile.info("in getProfileEntrepriseOfFreelancerById by admin")
        let {id} = req.params;
        let ibanModule = null;
        let ibanUsModule = null;
        let ibanCaModule = null;
        let ibanOthers = null;
        try {
            const OneprofileEntreprise = await profileEntreprise.findOne({id_freelancer: id});
            if (OneprofileEntreprise) {
                let contactDetails = {
                    name: OneprofileEntreprise.name,
                    address: OneprofileEntreprise.address,
                    address_plus: OneprofileEntreprise.address_plus,
                    legal_form: OneprofileEntreprise.legal_form,
                }
                let legalRepresentative = {
                    lastname: OneprofileEntreprise.lastname,
                    firstname: OneprofileEntreprise.firstname,
                    birthday: OneprofileEntreprise.birthday,
                    city: OneprofileEntreprise.city,
                    postal: OneprofileEntreprise.postal,
                    city_of_birth: OneprofileEntreprise.city_of_birth,
                    country_of_birth: OneprofileEntreprise.country_of_birth,
                    nationality: OneprofileEntreprise.nationality,
                }
                let legalMention = {
                    sas: OneprofileEntreprise.sas,
                    siret: OneprofileEntreprise.siret,
                    rcs: OneprofileEntreprise.rcs,
                    naf: OneprofileEntreprise.naf,
                    tva_intracom: OneprofileEntreprise.tva_intracom,
                    days: OneprofileEntreprise.days
                }
                switch (OneprofileEntreprise.type_iban) {
                    case 'empty': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban': {
                        ibanModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_iban: OneprofileEntreprise.cb_iban_iban
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-us': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_aba_transit_number: OneprofileEntreprise.cb_iban_aba_transit_number,
                            cb_iban_account_type: OneprofileEntreprise.cb_iban_account_type
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-ca': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_bank_name: OneprofileEntreprise.cb_iban_bank_name,
                            cb_iban_branch_code: OneprofileEntreprise.cb_iban_branch_code,
                            cb_iban_number_institution: OneprofileEntreprise.cb_iban_number_institution
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'others': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_bic_swift: OneprofileEntreprise.cb_iban_bic_swift,
                            cb_iban_account_country: OneprofileEntreprise.cb_iban_account_country,
                        }
                    }
                        break;
                }
                return res.status(200).json({
                    contactDetails: contactDetails,
                    legalRepresentative: legalRepresentative,
                    taxe: OneprofileEntreprise.taxe,
                    legalMention: legalMention,
                    type_iban: OneprofileEntreprise.type_iban,
                    ibanModule: ibanModule,
                    ibanUsModule: ibanUsModule,
                    ibanCaModule: ibanCaModule,
                    ibanOthers: ibanOthers
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFreelancer = req.body;
        try {
            await Freelancer.findByIdAndUpdate(id, newFreelancer);
            loggerFile.debug('freelancer modifié')
            return res.status(200).json({message: "Freelancer modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerContactDetailsByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerContactDetailsByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_freelancer = id;
            let {id_freelancer} = req.body;
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Coordonnées de votre entreprise modifiés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerLegalRepresentativeByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerLegalRepresentativeByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_freelancer = id;
            let {id_freelancer} = req.body;
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug("UpdateLegalRepresentative data done")
            return res.status(200).json({message: "Représentant légale de votre entreprise ajouté"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerLegalMentionByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerLegalMentionByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_freelancer = id;
            let {id_freelancer} = req.body;
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug('mention légale ajoutée')
            return res.status(200).json({message: "mention légale ajoutée"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerTaxesByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerTaxesByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_freelancer = id;
            let {id_freelancer} = req.body;

            if (!req.body.taxe) {
                loggerFile.debug("il faut une valeur de taxe")
                return res.status(400).json({message: "il faut une valeur de taxe"});
            }
            if (req.body.taxe < 0 || req.body.taxe > 100) {
                loggerFile.debug("le taxe doit être compris entre 0 et 100")
                return res.status(400).json({message: "le taxe doit être compris entre 0 et 100"});
            }
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug("UpdateTaxe data done")
            return res.status(200).json({message: "taxe ajouté"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerIbanByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerIbanByAdmin by admin")
        try {
            let {id} = req.params;
            let data = req.body;
            data.id_freelancer = id;
            let {id_freelancer} = req.body;
            let profileEntrepriseIban = await profileEntreprise.findOne({id_freelancer: id_freelancer});
            try {
                if (profileEntrepriseIban.type_iban === 'empty') {
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban') {
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {cb_iban_iban: ""});
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban-us') {
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_aba_transit_number: "",
                        cb_iban_account_type: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban-ca') {
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_branch_code: "",
                        cb_iban_number_institution: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else {
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_bic_swift: "",
                        cb_iban_account_country: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                }
            } catch (err) {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(200).json({message: "Erreur lors de l'ajout de l'iban"});
            }
            loggerFile.debug("updateIbanAccount data done");
            return res.status(200).json({message: "Coordonnées bancaires Ajoutés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerCertificationByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerCertificationByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newCertification = req.body;
        try {
            await Certification.findByIdAndUpdate(id, newCertification);
            loggerFile.debug('certification modifiée')
            return res.status(200).json({message: "certification modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFreelancerCertificationByAdmin: async (req, res) => {
        loggerFile.info("in getFreelancerCertificationByAdmin by admin")
        try {
            let {id} = req.params;
            const certification = await Certification.findById(id);
            if (certification) {
                loggerFile.debug('in findById certification done')
                return res.status(200).json(certification);
            } else {
                loggerFile.debug('aucune certification éxistante')
                return res.status(200).json({message: "certification inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerFormationByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerFormationByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newFormation = req.body;
        try {
            await Formation.findByIdAndUpdate(id, newFormation);
            loggerFile.debug('formation modifiée')
            return res.status(200).json({message: "formation modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFreelancerFormationByAdmin: async (req, res) => {
        loggerFile.info("in getFreelancerFormationByAdmin by admin")
        try {
            let {id} = req.params;
            const formation = await Formation.findById(id);
            if (formation) {
                loggerFile.debug('in findById formation done')
                return res.status(200).json(formation);
            } else {
                loggerFile.debug('aucune formation éxistante')
                return res.status(200).json({message: "formation inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFreelancerExperienceByAdmin: async (req, res) => {
        loggerFile.info("in updateFreelancerExperienceByAdmin by admin")
        let {id} = req.params;
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let newExperience = req.body;
        try {
            await Experience.findByIdAndUpdate(id, newExperience);
            loggerFile.debug('experience modifiée')
            return res.status(200).json({message: "experience modifiée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getFreelancerExperienceByAdmin: async (req, res) => {
        loggerFile.info("in getFreelancerExperienceByAdmin by admin")
        try {
            let {id} = req.params;
            const experience = await Experience.findById(id);
            if (experience) {
                loggerFile.debug('in findById experience done')
                return res.status(200).json(experience);
            } else {
                loggerFile.debug('aucune experience éxistante')
                return res.status(200).json({message: "experience inéxistante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisFreelance: async (req, res) => {
        loggerFile.info("getDevisFreelance");
        let id = req.auth._idConnected;
        let missions = []
        let i = 0;
        let unpayedAmounts = [];
        try {
            let devises = await Devis.find({id_freelance: id});
            if (devises.length > 0) {
                while (devises.length != unpayedAmounts.length) {
                    let mission = await Mission.findById(devises[i].id_mission);
                    await missions.push(mission)
                    if (devises[i].confirmed && devises[i].confirmed == true) {
                        var amount = 0
                        let payments = await Payment.find({
                            id_mission: mission._id,
                            id_freelance: devises[i].id_freelance
                        })
                        payments.forEach(async function (item2, index2, array2) {
                            amount += item2.amount
                            if (index2 + 1 == array2.length) {
                                amount = (amount * 100) / item2.totalTva
                                unpayedAmounts.push(Math.round(amount))
                                i++
                            }
                        });
                    } else {
                        unpayedAmounts.push(null)
                    }
                }
                return res.status(200).json({
                    devises: devises, missions: missions.map(el => {
                        return {
                            _id: el._id,
                            name: el.name,
                            id_company: el.id_company
                        }
                    }),
                    unpayedAmounts: unpayedAmounts
                })
            } else {
                return res.status(200).json([])
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getDevisFreelance: async (req, res) => {
        loggerFile.info("getDevisFreelance");
        let {id} = req.params
        let missions = []
        let i = 0;
        let unpayedAmounts = [];
        try {
            let devises = await Devis.find({id_freelance: id});
            if (devises.length > 0) {
                while (devises.length != unpayedAmounts.length) {
                    let mission = await Mission.findById(devises[i].id_mission);
                    await missions.push(mission)
                    if (devises[i].confirmed && devises[i].confirmed == true) {
                        var amount = 0
                        let payments = await Payment.find({
                            id_mission: mission._id,
                            id_freelance: devises[i].id_freelance
                        })
                        payments.forEach(async function (item2, index2, array2) {
                            amount += item2.amount
                            if (index2 + 1 == array2.length) {
                                amount = (amount * 100) / item2.totalTva
                                unpayedAmounts.push(Math.round(amount))
                                i++
                            }
                        });
                    } else {
                        unpayedAmounts.push(null)
                    }
                }
                return res.status(200).json({
                    devises: devises, missions: missions.map(el => {
                        return {
                            _id: el._id,
                            name: el.name,
                            id_company: el.id_company
                        }
                    }),
                    unpayedAmounts: unpayedAmounts
                })
            } else {
                return res.status(200).json([])
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

