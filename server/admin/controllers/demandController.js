const Demand = require('../models/demand.model');
const Mission = require('../models/mission.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create demand")
        let idUser = req.auth._idConnected;
        let id_mission = req.params.id
        try {
            const mission = await Mission.findById(id_mission);
            if (mission) {
                let name = mission.name
                const demandOne = await Demand.find({iduser: idUser, id_mission: id_mission});
                if (demandOne.length === 0) {
                    let demand = new Demand({idUser, id_mission, name});
                    await Demand.create(demand)
                    loggerFile.debug('create demand data done')
                    return res.status(200).json({message: "demande de recherche envoyé"});
                } else return res.status(200).json({message: "demande éxistante"});
            } else return res.status(200).json({message: "mission inéxistante"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateSearch: async (req, res) => {
        loggerFile.info("in updateSearch demand")
        const {search} = req.body;
        let {id} = req.params
        try {
            if (search.freelancers && search.freelancersNeeded && search.freelancersAppreciated) {
                await Demand.findByIdAndUpdate(id, {search: search, state: 'en cours'})
                loggerFile.debug('update search demand done')
                return res.status(200).json({message: "recherches ajouté à la demande"});
            } else return res.status(200).json({message: "format incohérent"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllByIdUser: async (req, res) => {
        loggerFile.info("in findAllByIdUser demand")
        let idUser = req.auth._idConnected;
        try {
            let demands = await Demand.find({idUser: idUser});

            loggerFile.debug('findAllByIdUser data done')
            return res.status(200).json(demands);

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }, findDemandByIdAndByIdUser: async (req, res) => {
        loggerFile.info("in findDemandByIdAndByIdUser demand")
        let {id} = req.params;
        try {
            let demand = await Demand.findById(id);
            loggerFile.debug('findDemandByIdAndByIdUser data done')
            return res.status(200).json(demand);

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateDemand: async (req, res) => {
        loggerFile.info("in validateDemand")
        let {id} = req.params;
        let {_idConnected} = req.auth
        try {
            let demand = await Demand.findById(id);
            if (demand.state !== 'validé') {
                if (demand && _idConnected === demand.idUser) {
                    await Demand.findByIdAndUpdate(id, {state: "validé"});
                    return res.status(200).json({message: "demande validée"});
                } else {
                    return res.status(400).json({message: "Demande inéxistante"});
                }
            } else {
                return res.status(400).json({message: "Demande validé précédement"});

            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unValidateDemand: async (req, res) => {
        loggerFile.info("in unValidateDemand")
        let {id} = req.params;
        let {_idConnected} = req.auth
        try {
            let demand = await Demand.findById(id);
            if (demand.state !== 'refusé') {
                if (demand && _idConnected === demand.idUser) {
                    await Demand.findByIdAndUpdate(id, {state: "refusé"});
                    return res.status(200).json({message: "demande refusée"});
                } else {
                    return res.status(400).json({message: "Demande inéxistante"});
                }
            } else {
                return res.status(400).json({message: "Demande validé précédement"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllInProcess: async (req, res) => {
        loggerFile.info("in findAllInProcess demand")
        try {
            let demands = await Demand.find({state: "en cours"});

            loggerFile.debug('findAllInProcess data done')
            return res.status(200).json({demands: demands});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll")
        try {
            let demands = await Demand.find();

            loggerFile.debug('findAll data done')
            return res.status(200).json({demands: demands});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
