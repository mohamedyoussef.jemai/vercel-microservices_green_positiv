const Job = require('../models/jobs.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create job")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }

        let {name} = req.body;
        const job = new Job({name});
        try {
            await Job.create(job).then(data => {
                if (data) {
                    loggerFile.debug("create job done")
                    return res.status(200).json({message: "Métier créé avec succés", data: data});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll jobs")
        try {
            const jobs = await Job.find();
            loggerFile.debug("findAll jobs done")

            return res.status(200).json(jobs);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne job")

        const {id} = req.params;
        try {
            const job = await Job.findById(id);
            if (job) {
                loggerFile.debug('find job done')
                return res.status(200).json(job);
            } else {
                loggerFile.error("aucun Métier éxistant");
                return res.status(400).json({message: "aucun Métier éxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update job")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        const newJob = req.body;
        try {
            await Job.findByIdAndUpdate(id, newJob);
            loggerFile.info("update job done")
            return res.status(200).json({message: "Métier modifié avec succés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in delete job")
        const {id} = req.params;
        try {
            await Job.findByIdAndDelete(id);
            loggerFile.debug('delete job done')
            return res.status(200).json({message: "Métier effacé avec succés"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteAll: async (req, res) => {
        loggerFile.info("in create admin")
        try {
            await Job.deleteMany().then(data => {
                loggerFile.debug('delete all jobs done')
                return res.status(200).json({message: `${data.deletedCount} Métiers effacés avec succés `})
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAllAdmin: async (req, res) => {
        loggerFile.info("in findAllAdmin jobs")
        try {
            let jobsAdmin = [];
            const jobs = await Job.find();
            await jobs.forEach(async function callback(job, index) {
                let model = {text: job.name, value: job._id};
                await jobsAdmin.push(model);
            });
            loggerFile.debug("findAllAdmin jobs done")
            return res.status(200).json(jobsAdmin);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

