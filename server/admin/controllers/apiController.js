require('dotenv').config();
const fs = require('fs');
const xml = require("xml2js");
const path = require("path");
const loggerFile = require("../config/logger");
const utils = require('../config/utils');
const {Sector} = require("../models/sector.model");
const {Language} = require('../models/language.model');
const {LegalForm} = require('../models/legalForm.model');
const {IbanCountry} = require('../models/ibanCountry.model');

module.exports = {
    getLanguages: async (req, res) => {
        loggerFile.info('in getLanguages');
        try {
            fs.readFile(path.join(__dirname, "../ressources/languages.xml"), async (err, buffer) => {
                if (!err) {
                    let arrayLanguages = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arrayLanguages = result.list.record;
                        arrayLanguages = arrayLanguages.map((el) => {
                            return Language.fromJson(el);
                        });
                    });
                    loggerFile.debug(`getLanguages data done`);
                    return res.status(200).json(arrayLanguages);
                } else {
                    loggerFile.error(error,utils.getClientAddress(req, error))
                    return res.status(400).json({message: err.message});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getLegalForm: async (req, res) => {
        loggerFile.info('in getLegalForm');
        try {
            fs.readFile(path.join(__dirname, "../ressources/legal_form.xml"), async (err, buffer) => {
                if (!err) {
                    let arrayLegalForm = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arrayLegalForm = result.list.record;
                        arrayLegalForm = arrayLegalForm.map((el) => {
                            return LegalForm.fromJson(el);
                        });
                    });
                    loggerFile.debug(`getLegalForm data done`);
                    return res.status(200).json(arrayLegalForm);
                } else {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});

                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }

    },
    getCountry: async (req, res) => {
        loggerFile.info('in getCountry');
        try {
            fs.readFile(path.join(__dirname, "../ressources/countries.xml"), async (err, buffer) => {
                if (!err) {
                    let arrayCountry = [];
                    let arrayCountryResult = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arrayCountry = result.list.record;
                        arrayCountry = arrayCountry.map((el) => {
                            arrayCountryResult.push(el.name[0]);
                            return Language.fromJson(el);
                        });
                    });
                    loggerFile.debug(`getLegalForm data done`);
                    return res.status(200).json(arrayCountryResult);
                } else {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getIbanCountry: async (req, res) => {
        loggerFile.info('in getIbanCountry');
        try {
            fs.readFile(path.join(__dirname, "../ressources/ibanCountries.xml"), async (err, buffer) => {
                if (!err) {
                    let arrayibanCountries = [];
                    let ibanArray = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arrayibanCountries = result.list.record;
                        arrayibanCountries = arrayibanCountries.map((el) => {
                            if (el.type == 'iban') {
                                ibanArray.push(IbanCountry.fromJson(el));
                            }
                            return IbanCountry.fromJson(el);
                        });
                    });
                    loggerFile.debug(`getIbanCountry data done`);
                    return res.status(200).json(ibanArray);
                } else {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getIbanUsCaOtherCountry: async (req, res) => {
        loggerFile.info('in getIbanUsCaOtherCountry');
        try {
            fs.readFile(path.join(__dirname, "../ressources/ibanCountries.xml"), async (err, buffer) => {
                if (!err) {
                    let arrayibanCountries = [];
                    let ibanUsUCOthersArray = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arrayibanCountries = result.list.record;
                        arrayibanCountries = arrayibanCountries.map((el) => {
                            if (el.type == 'iban-us-ca-o') {
                                ibanUsUCOthersArray.push(IbanCountry.fromJson(el));
                            }
                            return IbanCountry.fromJson(el);

                        });
                    });
                    loggerFile.debug(`getIbanUsCaOtherCountry data done`);
                    return res.status(200).json(ibanUsUCOthersArray);
                } else {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getSectorActivity: async (req, res) => {
        loggerFile.info(" in getSectorActivity");
        try {
            fs.readFile(path.join(__dirname, "../ressources/sector_activity.xml"), async (err, buffer) => {
                if (!err) {
                    let arraySector = [];
                    await xml.parseString(buffer, (err3, result) => {
                        arraySector = result.list.record;
                        arraySector = arraySector.map((el) => {
                            return Sector.fromJson(el);
                        });
                    });
                    loggerFile.debug(`getSectorActivity data done`);
                    return res.status(200).send(arraySector);
                } else {
                    loggerFile.error(utils.getClientAddress(req, err))
                    return res.status(400).json({message: err.message});
                }
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

