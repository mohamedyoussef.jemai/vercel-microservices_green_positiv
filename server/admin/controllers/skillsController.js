const Skill = require('../models/skills.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create skill")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {name} = req.body;
        const skill = new Skill({name});
        try {
            await Skill.create(skill).then(data => {
                if (data) {
                    loggerFile.debug("create skill done")
                    return res.status(200).json({message: "Compétence créée avec succés", data: data});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll skills")
        try {
            const skills = await Skill.find();
            loggerFile.debug(" findAll skill done")
            return res.status(200).json(skills);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne skill")
        const {id} = req.params;
        try {
            const skill = await Skill.findById(id);
            if (skill) {
                loggerFile.debug(" findOne skill done")
                return res.status(200).json(skill);
            } else {
                loggerFile.debug(" aucune Compétence existante")
                return res.status(200).json({message: "aucune Compétence existante"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update skill")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {id} = req.params;
        const newSkill = req.body;
        try {
            await Skill.findByIdAndUpdate(id, newSkill);
            loggerFile.debug('update skill done')
            return res.status(200).json({message: "Compétence modifiée avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info("in delete skill")
        const {id} = req.params;
        try {
            await Skill.findByIdAndDelete(id);
            loggerFile.debug('delete skill done')
            return res.status(200).json({message: "Compétence effacée avec succés"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    deleteAll: async (req, res) => {
        loggerFile.info("in deleteAll skills")
        try {
            await Skill.deleteMany().then(data => {
                loggerFile.debug('deleteAll skill done')
                return res.status(200).json({message: `${data.deletedCount} Compétences effacées avec succés`})
            })
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}

