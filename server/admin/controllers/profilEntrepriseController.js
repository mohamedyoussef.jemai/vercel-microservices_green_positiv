const profileEntreprise = require('../models/profileEntreprise.model');
const notificationService = require("../services/notificationService");
const loggerFile = require("../config/logger");
const utils = require('../config/utils');
module.exports = {
    update: async (req, res) => {
        loggerFile.info("in update")
        let {id} = req.params;
        try {
            await profileEntreprise.findByIdAndUpdate(id, req.body);
            loggerFile.debug("update profile data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateProfileFreelane: async (req, res) => {
        loggerFile.info("in validateProfileFreelane")
        let {id_freelancer, validated} = req.body;
        try {
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, {validated: validated});
            loggerFile.debug("validate data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    createContactDetails: async (req, res) => {
        loggerFile.info("in createContactDetails")
        const {
            name,
            address,
            address_plus,
            legal_form,
            id_freelancer
        } = req.body;
        try {
            const newProfileEntreprise = new profileEntreprise({
                name,
                address,
                address_plus,
                legal_form,
                id_freelancer
            });
            await profileEntreprise.create(newProfileEntreprise);
            loggerFile.debug("createContactDetails data done")

            return res.status(200).json({message: "Coordonnées de votre entreprise ajoutés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateContactDetails: async (req, res) => {
        loggerFile.info("in updateContactDetails")
        try {
            let {id_freelancer} = req.body;
            await notificationService.createDetailsContactNotif(id_freelancer, req.body);
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Coordonnées de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateLegalRepresentative: async (req, res) => {
        loggerFile.info("in UpdateLegalRepresentative")

        try {
            let {id_freelancer} = req.body;
            await notificationService.createLegalRepresentativeNotif(id_freelancer, req.body);
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug("UpdateLegalRepresentative data done")
            return res.status(200).json({message: "Représentant légale de votre entreprise ajouté"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateIbanAccount: async (req, res) => {
        loggerFile.info("in updateIbanAccount")
        try {
            let {id_freelancer} = req.body;

            let profileEntrepriseIban = await profileEntreprise.findOne({id_freelancer: id_freelancer});
            try {
                if (profileEntrepriseIban.type_iban === 'empty') {
                    await notificationService.createIbanNotif(id_freelancer, req.body);
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban') {
                    await notificationService.createIbanNotif(id_freelancer, req.body);
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {cb_iban_iban: ""});
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban-us') {
                    await notificationService.createIbanNotif(id_freelancer, req.body);
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_aba_transit_number: "",
                        cb_iban_account_type: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else if (profileEntrepriseIban.type_iban === 'iban-ca') {
                    await notificationService.createIbanNotif(id_freelancer, req.body);
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_branch_code: "",
                        cb_iban_number_institution: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                } else {
                    await notificationService.createIbanNotif(id_freelancer, req.body);
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, {
                        cb_iban_region: "",
                        cb_iban_account_number: "",
                        cb_iban_bic_swift: "",
                        cb_iban_account_country: ""
                    });
                    await profileEntreprise.findByIdAndUpdate(profileEntrepriseIban._id, req.body);
                }
            } catch (err) {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(200).json({message: "Erreur lors de l'ajout de l'iban"});

            }
            loggerFile.debug("updateIbanAccount data done");
            return res.status(200).json({message: "Coordonnées bancaires Ajoutés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll")
        try {
            const profileEntreprises = await profileEntreprise.find();
            if (profileEntreprises) {
                loggerFile.debug('get all profileEntreprises data done')
                return res.status(200).json(profileEntreprises);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne")

        let {id} = req.params;
        let ibanModule = null;
        let ibanUsModule = null;
        let ibanCaModule = null;
        let ibanOthers = null;
        try {
            const OneprofileEntreprise = await profileEntreprise.findOne({id_freelancer: id});
            if (OneprofileEntreprise) {
                let contactDetails = {
                    name: OneprofileEntreprise.name,
                    address: OneprofileEntreprise.address,
                    address_plus: OneprofileEntreprise.address_plus,
                    legal_form: OneprofileEntreprise.legal_form,
                }
                let legalRepresentative = {
                    lastname: OneprofileEntreprise.lastname,
                    firstname: OneprofileEntreprise.firstname,
                    birthday: OneprofileEntreprise.birthday,
                    city: OneprofileEntreprise.city,
                    postal: OneprofileEntreprise.postal,
                    city_of_birth: OneprofileEntreprise.city_of_birth,
                    country_of_birth: OneprofileEntreprise.country_of_birth,
                    nationality: OneprofileEntreprise.nationality,
                }
                let legalMention = {
                    sas: OneprofileEntreprise.sas,
                    siret: OneprofileEntreprise.siret,
                    rcs: OneprofileEntreprise.rcs,
                    naf: OneprofileEntreprise.naf,
                    tva_intracom: OneprofileEntreprise.tva_intracom,
                    days: OneprofileEntreprise.days
                }
                switch (OneprofileEntreprise.type_iban) {
                    case 'empty': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban': {
                        ibanModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_iban: OneprofileEntreprise.cb_iban_iban
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-us': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_aba_transit_number: OneprofileEntreprise.cb_iban_aba_transit_number,
                            cb_iban_account_type: OneprofileEntreprise.cb_iban_account_type
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'iban-ca': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_bank_name: OneprofileEntreprise.cb_iban_bank_name,
                            cb_iban_branch_code: OneprofileEntreprise.cb_iban_branch_code,
                            cb_iban_number_institution: OneprofileEntreprise.cb_iban_number_institution
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bic_swift: "",
                            cb_iban_account_country: "",
                        }
                    }
                        break;
                    case 'others': {
                        ibanModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_iban: ""
                        }
                        ibanUsModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_country: "",
                            cb_iban_region: "",
                            cb_iban_account_number: "",
                            cb_iban_aba_transit_number: "",
                            cb_iban_account_type: ""
                        }
                        ibanCaModule = {
                            cb_iban_name_lastname: "",
                            cb_iban_address_holder: "",
                            cb_iban_postal: "",
                            cb_iban_city: "",
                            cb_iban_region: "",
                            cb_iban_country: "",
                            cb_iban_account_number: "",
                            cb_iban_bank_name: "",
                            cb_iban_branch_code: "",
                            cb_iban_number_institution: ""
                        }
                        ibanOthers = {
                            cb_iban_name_lastname: OneprofileEntreprise.cb_iban_name_lastname,
                            cb_iban_address_holder: OneprofileEntreprise.cb_iban_address_holder,
                            cb_iban_postal: OneprofileEntreprise.cb_iban_postal,
                            cb_iban_city: OneprofileEntreprise.cb_iban_city,
                            cb_iban_country: OneprofileEntreprise.cb_iban_country,
                            cb_iban_region: OneprofileEntreprise.cb_iban_region,
                            cb_iban_account_number: OneprofileEntreprise.cb_iban_account_number,
                            cb_iban_bic_swift: OneprofileEntreprise.cb_iban_bic_swift,
                            cb_iban_account_country: OneprofileEntreprise.cb_iban_account_country,
                        }
                    }
                        break;
                }
                return res.status(200).json({
                    contactDetails: contactDetails,
                    legalRepresentative: legalRepresentative,
                    taxe: OneprofileEntreprise.taxe,
                    legalMention: legalMention,
                    type_iban: OneprofileEntreprise.type_iban,
                    ibanModule: ibanModule,
                    ibanUsModule: ibanUsModule,
                    ibanCaModule: ibanCaModule,
                    ibanOthers: ibanOthers
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateTaxe: async (req, res) => {
        loggerFile.info("in UpdateTaxe")
        try {
            let {id_freelancer} = req.body;

            if (!req.body.taxe) {
                loggerFile.debug("il faut une valeur de taxe")
                return res.status(400).json({message: "il faut une valeur de taxe"});
            }
            if (req.body.taxe < 0 || req.body.taxe > 100) {
                loggerFile.debug("le taxe doit être compris entre 0 et 100")
                return res.status(400).json({message: "le taxe doit être compris entre 0 et 100"});
            }
            await notificationService.createTaxeNotif(id_freelancer, req.body);
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug("UpdateTaxe data done")
            return res.status(200).json({message: "taxe ajouté"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    UpdateLegalMention: async (req, res) => {
        loggerFile.info('in UpdateLegalMention')
        try {
            let {id_freelancer} = req.body;
            await notificationService.createLegalMentionNotif(id_freelancer, req.body);
            await profileEntreprise.findOneAndUpdate({id_freelancer: id_freelancer}, req.body);
            loggerFile.debug('mention légale ajoutée')
            return res.status(200).json({message: "mention légale ajoutée"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete profile')
        const {id} = req.params;
        try {
            await profileEntreprise.findOneAndDelete({id_freelancer: id});
            loggerFile.debug('profile effacé')
            return res.status(200).json({message: "done"})

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    getProfileEntrepriseById: async (req, res) => {
        try {
            let {id} = req.body;
            try {
                let profile = await profileEntreprise.findOne({id_freelancer: id});
                return res.status(200).json(profile);
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

