const Subscription = require('../models/subscription.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create subscription")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }

        let {email} = req.body;
        const subscription = new Subscription({email});
        try {
            await Subscription.create(subscription).then(data => {
                if (data) {
                    loggerFile.debug("create subscription done")
                    return res.status(200).json({message: "Abonnement créé avec succés", data: data});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll subscriptions")
        try {
            const subscriptions = await Subscription.find();
            loggerFile.debug("findAll subscriptions done")

            return res.status(200).json(subscriptions);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    update: async (req, res) => {
        loggerFile.info("in update subscription")
        if (!req.body) {
            loggerFile.debug('les informations sont vides')
            return res.status(400).send({message: "les informations sont vides"});
        }
        const {email, active} = req.body;

        try {
            const subsciption = await Subscription.findOne({email: email});
            if (subsciption) {
                await Subscription.findByIdAndUpdate(subsciption._id, {active: active});
                loggerFile.info("update subscription done")
                return res.status(200).json({message: "Abonnement modifié avec succés"});
            } else return res.status(400).json({message: "Abonnement non trouvé"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

