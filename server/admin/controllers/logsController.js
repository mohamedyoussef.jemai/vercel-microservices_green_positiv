const Logs = require('../models/logs.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    findAll: async (req, res) => {
        loggerFile.info("in findAll logs")
        try {
            const logs = await Logs.find();
            loggerFile.debug("findAll logs done")
            return res.status(200).json(logs);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findUnchecked: async (req, res) => {
        loggerFile.info("in findUnchecked logs")
        try {
            const logs = await Logs.find({checked: false});
            loggerFile.debug("findUnchecked logs done")

            return res.status(200).json(logs);
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    checkLog: async (req, res) => {
        loggerFile.info("in checkLog")
        let {id} = req.params;
        try {
            await Logs.findByIdAndUpdate(id, {checked: true}).then(async response => {
                if (response) {
                    loggerFile.debug("Log rectifié")
                    return res.status(200).json({message: "Log checked"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    unCheckLog: async (req, res) => {
        loggerFile.info("in checkLog")
        let {id} = req.params;
        try {
            await Logs.findByIdAndUpdate(id, {checked: false}).then(async response => {
                if (response) {
                    loggerFile.debug("Log rectifié")
                    return res.status(200).json({message: "Log checked"});
                }
            }).catch(err => {
                 loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}

