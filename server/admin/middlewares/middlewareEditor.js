require('dotenv/config')
const Admin = require("../models/Admin.model");
const jwt = require('jsonwebtoken');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareValidityCreationUser: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationUser")
        const {username, email, lastName, firstName, phone, password, repeatPassword} = req.body;
        try {
            if (username.trim().length === 0) {
                loggerFile.debug("creation Editor data : problem in username")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (email.trim().length === 0) {
                loggerFile.debug("creation Editor data : problem in email")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (lastName.trim().length === 0) {
                loggerFile.debug("creation Editor data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName.trim().length === 0) {
                loggerFile.debug("creation Editor data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (password.trim().length === 0 || password.length < 8) {
                loggerFile.debug("creation Editor data : problem in password")
                return res.status(400).send({message: "le mot de passe est trop court (8 caractéres requis)"});
            }
            if (password != repeatPassword) {
                loggerFile.debug("creation Editor data : problem in equals passwords")
                return res.status(400).send({message: "les mots de passe ne sont pas égaux"});
            }
            if (!/^\d+$/.test(phone) || phone.length !== 10) {
                loggerFile.debug("creation Editor data : problem in phone")
                return res.status(400).send({message: "le numéro de téléphone saisi est erroné"});
            } else return next();
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistUsernameEditor: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameEditor")
        let {username} = req.body;
        try {
            await Admin.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsernameEditor done");
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailEditor: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailEditor")
        let {email} = req.body;
        try {
            await Admin.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else {
                    loggerFile.debug("middlewareExistEmailEditor done");
                    return next();
                }
            }).catch(err => {
                lloggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenAdminOrRootOrEditor: async (req, res, next) => {
        loggerFile.info("in middlewareTokenAdminOrRootOrEditor")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, message: "besoin de token"});
        }
        let decoded = jwt.decode(token);
        if (decoded) {
            const _id = decoded._idConnected;
            const user = await Admin.findById(_id);
            jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                if (err) return res.status(401).json({user: false, error: "token invalide"});
                if (user && (user.role === "Admin" || user.role === "Root" || user.role === "Editor")) {
                    req.auth = decoded2;
                    return next();
                } else {
                    return res.status(401).json({user: false, message: "accés non autorisé"});
                }
            });
        } else {
            return res.status(401).json({user: false, message: "token invalide"});
        }
    },
}
