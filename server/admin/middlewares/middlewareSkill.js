const Skill = require('../models/skills.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareValidityCreationSkill: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationSkill")
        const {name} = req.body;
        try {
            if (!name) {
                loggerFile.debug("les informations sont vides")
                return res.status(400).send({message: "les informations sont vides"});
            } else {
                loggerFile.debug("middlewareValidityCreationSkill done")
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareSkillExist: async (req, res, next) => {
        loggerFile.info("in middlewareSkillExist")
        let skills = await Skill.find();
        let test = false;
        let {name} = req.body;
        try {
            skills.map((el) => {
                if (name.toUpperCase() == el.name.toUpperCase()) {
                    loggerFile.debug("skill exist")
                    test = true
                } else {
                    test = false;
                }
            });
            if (test) {
                loggerFile.debug("skill exist")
                return res.status(400).json({message: "Cette compétence éxiste"});
            } else {
                next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareUpdateSkill: async (req, res, next) => {
        loggerFile.info("in middlewareUpdateSkill")
        const {name} = req.body;
        if (!name) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let skills = await Skill.find();
        let test = false;

        try {
            skills.map((el) => {
                if (name.toUpperCase() == el.name.toUpperCase()) {
                    loggerFile.debug("skill exist")
                    test = true;
                }
            });
            if (test == true) {
                return res.status(400).json({message: "Cette compétence existe"});
            } else {
                next();
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
    ,
}
