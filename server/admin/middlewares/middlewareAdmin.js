require('dotenv/config')
const Admin = require("../models/Admin.model");
const Subscription = require("../models/subscription.model");
const jwt = require('jsonwebtoken');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareValidityCreationUser: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationUser")
        const {username, email, lastName, firstName, phone, password, repeatPassword} = req.body;
        try {
            if (username && username.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in username")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (email && email.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in email")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (lastName && lastName.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName && firstName.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (phone && phone.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in phone")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (password && password.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in password")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (repeatPassword && repeatPassword.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in repeatPassword")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (password != repeatPassword) {
                loggerFile.debug("creation Freelancer data : equal passwords")
                return res.status(400).send({message: "les mots de passe ne sont pas égaux"});
            } else return next();
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }

    },
    middlewareExistUsernameAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameAdmin")
        let {username} = req.body;
        let {id} = req.params;
        try {
            const admin = await Admin.findById(id)
            await Admin.countDocuments({username: username}).then(count => {
                if (count >= 1 && admin.username != username) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsernameAdmin done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailAdmin")
        let {email} = req.body;
        let {id} = req.params;
        try {
            const admin = await Admin.findById(id)
            await Admin.countDocuments({email: email}).then(count => {
                if (count >= 1 && admin.email != email) {
                    loggerFile.info("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else {
                    loggerFile.info("middlewareExistEmailAdmin done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenAdminOrRoot: async (req, res, next) => {
        loggerFile.info("in middlewareTokenAdminOrRoot")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, message: "besoin de token"});
        }
        try {
            let decoded = jwt.decode(token);
            if (decoded) {
                const _id = decoded._idConnected;
                const user = await Admin.findById(_id);
                jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                    if (err) return res.status(401).json({user: false, error: "token invalide"});
                    if (user && (user.role === "Admin" || user.role === "Root")) {
                        req.auth = decoded2;
                        loggerFile.debug("token valide")
                        return next();
                    } else {
                        return res.status(401).json({user: false, message: "accés non autorisé"});
                    }
                });
            } else {
                return res.status(401).json({user: false, error: "token invalide"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenAdminOrRootOrEditor: async (req, res, next) => {
        loggerFile.info("in middlewareTokenAdminOrRoot")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, message: "besoin de token"});
        }
        try {
            let decoded = jwt.decode(token);
            if (decoded) {
                const _id = decoded._idConnected;
                const user = await Admin.findById(_id);
                jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                    if (err) return res.status(401).json({user: false, error: "token invalide"});
                    if (user && (user.role === "Admin" || user.role === "Root" || user.role === "Editor")) {
                        req.auth = decoded2;
                        loggerFile.debug("token valide")
                        return next();
                    } else {
                        return res.status(401).json({user: false, message: "accés non autorisé"});
                    }
                });
            } else {
                return res.status(401).json({user: false, error: "token invalide"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareSubscriptionEmailExist: async (req, res, next) => {
        loggerFile.info("in middlewareSubscriptionEmailExist")
        let {email} = req.body;
        try {
            await Subscription.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.info("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else {
                    loggerFile.info("middlewareSubscriptionEmailExist done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenUser: async (req, res, next) => {
        loggerFile.info("in middlewareTokenUser")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            let decoded = jwt.decode(token);
            if (decoded) {
                jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                    if (err) return res.status(401).json({user: false, error: "token invalide"});
                    if (decoded2.role === "Freelancer" || decoded2.role === "Agence" || decoded2.role === "Company" || decoded2.role === "Collab") {
                        loggerFile.debug("middlewareTokenUser done")
                        return next();
                    } else {
                        loggerFile.debug("accés non autorisé")
                        return res.status(401).json({user: false, error: "accés non autorisé"});
                    }
                });

            } else {
                loggerFile.debug("token invalide")
                return res.status(401).json({user: false, error: "token invalide"});
            }
        }
    }
}
