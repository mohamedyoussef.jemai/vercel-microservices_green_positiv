require('dotenv/config')
const axios = require('axios').default;

const loggerFile = require("../config/logger");

const Company = require("../models/company.model");
const Freelancer = require("../models/freelancer.model");
const utils = require('../config/utils');


module.exports = {
    verifFreelanceExist: async (req, res, next) => {
        loggerFile.info("in verifFreelanceExist")
        try {
            let {id} = req.params;
            let freelancer = await Freelancer.findById(id)
            if(freelancer) next()
            else res.status(400).json({message:"Freelancer inéxistant"})
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifyFavoriteFreelanceExist: async (req, res, next) => {
        loggerFile.info("in verifyFavoriteFreelanceExist")
        try {
            let id = req.params.idCompany;
            let idFreelance = req.params.id
            let company = await Company.findById(id);
            if (company.favorites.includes(idFreelance)) {
                return res.status(400).json({message: "Freelancer déja existant dans la liste des favoris"})
            } else {
                next()
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifyAccessCollab: async (req, res, next) => {
        loggerFile.info("in verifyAccessCollab")
        let {id} = req.params;
        try {
            let company = await Company.findById(id);
            if (company.active_collab) {
                loggerFile.debug("in verifyAccessCollab done")
                next();
            } else {
                loggerFile.debug("in verifyAccessCollab refuse done")
                return res.status(400).json({message: "lien de partage inactif"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareValidityCollaborator: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCollaborator")
        const {
            username,
            email,
            password,
            departement,
            lastName,
            firstName
        } = req.body;
        try {
            if (username.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in username")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (email.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in email")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (firstName.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in firstName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (lastName.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in lastName")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (password.trim().length === 0 || password.length < 8) {
                loggerFile.debug("creation Company data : problem in password")
                return res.status(400).send({message: "le mot de passe est erroné"});
            }
            if (!departements.includes(departement) || departement.trim().length === 0) {
                loggerFile.debug("creation Company data : problem in departement")
                return res.status(400).send({message: "le département n'éxiste pas"});
            } else {
                loggerFile.debug('validate creationCollaborator done')
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: "réessayez l'action"});
        }
    },
    middlewareExistUsernameCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistUsernameCompany")
        let {username} = req.body;
        try {
            await Company.countDocuments({username: username}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("l'utilisateur existe, essayez un autre identifiant")
                    return res.status(401).json({
                        message: "l'utilisateur existe, essayez un autre identifiant"
                    });
                } else {
                    loggerFile.debug("middlewareExistUsernameCompany done")
                    return next();
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareExistEmailCompany: async (req, res, next) => {
        loggerFile.info("in middlewareExistEmailCompany")
        let {email} = req.body;
        try {
            await Company.countDocuments({email: email}).then(count => {
                if (count >= 1) {
                    loggerFile.debug("cette adresse mail existe, essayez une autre")
                    return res.status(401).json({
                        message: "cette adresse mail existe, essayez une autre"
                    });
                } else return next();
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(401).json({
                    message: err.message
                });
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    getWorkedInFreelancers: async (req, res, next) => {
        loggerFile.info("in getWorkedInFreelancers")
        try {
            let {id} = req.params;
            let company = await Company.findById(id);
            let array = company.worked_in;
            let size = array.length;
            let worked_in = [];
                if (size != 0) {
                    await array.forEach(async function callback(idFreelance, index) {
                        let freelance = await Freelancer.findById(idFreelance);
                        let model = {
                            firstName: freelance.firstName,
                            lastName: freelance.lastName,
                            email: freelance.email,
                            phone: freelance.phone,
                            title_profile: freelance.title_profile
                        }
                        await worked_in.push(model);
                        if (size - 1 == index) {
                            req.worked_in = worked_in;
                            next()
                        }
                    });
                } else {
                    req.worked_in = [];
                    next()
                }
            } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
    },
    getFavoriteFreelancers: async (req, res, next) => {
        loggerFile.info("in getFavoriteFreelancers")
        let favorites = [];
        try {
            let {id} = req.params;
            let company = await Company.findById(id);
            let array = company.favorites;
            let size = array.length
            var itemsProcessed = 0;
            if (size != 0) {
                array.forEach(async function callback(idFreelance, index) {
                    await Freelancer.findById(idFreelance).then(async data => {
                        let model = {
                            _id: data._id,
                            firstName: data.firstName,
                            lastName: data.lastName,
                            email: data.email,
                            phone: data.phone,
                            image: data.image,
                            title_profile: data.title_profile
                        }
                        await favorites.push(model);
                        itemsProcessed++;
                        if (itemsProcessed === size) {
                            req.favorites= favorites
                            next()
                        }
                    }).catch(err => {
                        loggerFile.error(err,utils.getClientAddress(req, err))
                    });
                });

            } else {
                req.favorites = []
                next()
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareTokenCompany: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompany")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await axios.get(`${process.env.URL_COMPANY}/company/get/${_id}`);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err);
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.data.company.role === "Company") {
                            req.auth = decoded2
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenRootOrAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareTokenRootOrAdmin")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err);
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (decoded2.role === "Root" || decoded2.role === "Admin") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    ExistParamCompany: async (req, res, next) => {
        loggerFile.info("in ExistParamCompany")
        try {
            let {id} = req.params;
            const company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("company trouvé")
                return next();
            } else {
                loggerFile.debug("company introuvable")
                return res.status(400).json({message: "entreprise introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateFacturationDetails: async (req, res, next) => {
        loggerFile.info("in validateFacturationDetails")
        try {
            let {social_reason, siret, tva_intracom, address, address_plus} = req.body;
            if (!social_reason || typeof social_reason !== "string") {
                loggerFile.debug("social_reason non conforme")
                return res.status(400).json({message: "social_reason non conforme"});
            }
            if (!address || typeof address !== "string") {
                loggerFile.debug("address non conforme")
                return res.status(400).json({message: "address non conforme"});
            }
            if (!address_plus || typeof address_plus !== "string") {
                loggerFile.debug("address_plus non conforme")
                return res.status(400).json({message: "address_plus non conforme"});
            }
            if (!siret || typeof siret !== "string" || siret.length !== 14 || !/^\d+$/.test(siret)) {
                loggerFile.debug("siret non conforme")
                return res.status(400).json({message: "siret non conforme"});
            }
            let tva_numbers = tva_intracom.slice(2, tva_intracom.length);
            if (!tva_intracom || typeof tva_intracom !== "string" || !tva_intracom[0].match("F") || !tva_intracom[1].match("R") || !/^\d+$/.test(tva_numbers)) {
                loggerFile.debug("tva_intracom non conforme")
                return res.status(400).json({message: "tva_intracom non conforme"});
            } else {
                loggerFile.debug("validateFacturationDetails done")
                next();
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    middlewareCompanyExist: async (req, res, next) => {
        loggerFile.info("in middlewareCompanyExist")
        try {
            let id = req.auth._idConnected;
            let company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("middlewareCompanyExist done")
                return next();
            } else {
                loggerFile.debug("company not found")
                return res.status(400).json({message: "company introuvable"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareCompanyExistByAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareCompanyExistByAdmin")
        try {
            let {id} = req.params;
            let company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("middlewareCompanyExistByAdmin done")
                return next();
            } else {
                loggerFile.debug("company not found")
                return res.status(400).json({message: "company introuvable"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
