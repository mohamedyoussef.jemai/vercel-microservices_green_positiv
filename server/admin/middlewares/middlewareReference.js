const Reference = require("../models/reference.model");
const Agence = require('../models/agence.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    getReferences: async (req, res, next) => {
        loggerFile.info("in getReferences")
        var references = [];
        const idAgence = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence != null && agence.references.length > 0) {
                await agence.references.forEach(async function callback(referenceId, index) {
                    let reference = await Reference.findById(referenceId);
                    await references.push(reference);
                });
                loggerFile.debug('getReferences done')
                req.body.references = references;
                return next();
            } else {
                loggerFile.debug('getReferences done')
                req.body.references = references;
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "référence client inéxistante"});
            } else return res.status(400).json({message: err.message});
        }
    },
}

