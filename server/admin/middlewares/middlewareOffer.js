const Offer = require("../models/offer.model");
const Agence = require('../models/agence.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

let sector_activity =
    [
        "AVIATION_AEROSPACE",
        "IT",
        "FOOD",
        "ARCHITECTURE",
        "CRAFTS",
        "CIVIC_SOCIAL",
        "AUTOMOBILE",
        "BANKING_INSURANCE",
        "BIOTECH",
        "CIVIL_ENGINEERING",
        "RESEARCH",
        "CHEMICAL",
        "FILM",
        "SMALL_RETAIL",
        "CONSULTING",
        "CULTURE",
        "MILITARY",
        "LEISURE",
        "E_COMMERCE",
        "PUBLISHING",
        "EDUCATION",
        "SOFTWARE",
        "ENERGY",
        "ENVIRONMENT",
        "RETAIL",
        "HIGH_TECH",
        "HOSPITALITY",
        "REAL_ESTATE",
        "IMPORT_EXPORT",
        "MECHANICAL",
        "PRIMARY",
        "PHARMA",
        "GAMES",
        "LUXURY",
        "COSMETICS",
        "NANOTECH",
        "IOT",
        "PRESS",
        "SOCIAL",
        "RH",
        "RESTAURANTS",
        "HEALTH",
        "MEDICAL",
        "PUBLIC",
        "SAFETY",
        "SPORTS",
        "TELECOM",
        "TRANSPORT",
        "LOGISTIC",
        "TRAVEL",
        "WINE"];

module.exports = {
    getOffers: async (req, res, next) => {
        loggerFile.info("in getOffers")
        var offers = [];
        const idAgence = req.params.id;
        try {
            const agence = await Agence.findById(idAgence);
            if (agence != null && agence.offers.length > 0) {
                await agence.offers.forEach(async function callback(offerId, index) {
                    let offer = await Offer.findById(offerId);
                    await offers.push(offer);
                });
                loggerFile.debug('getOffers done')
                req.body.offers = offers;
                return next();
            } else {
                loggerFile.debug('getOffers done')
                req.body.offers = offers;
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            if (error.name === 'CastError') {
                return res.status(400).json({message: "offre inéxistante"});
            } else return res.status(400).json({message: error.message});
        }
    }
}

