require('dotenv/config')
const Admin = require("../models/Admin.model");
const jwt = require('jsonwebtoken');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareTokenRoot: async (req, res, next) => {
        loggerFile.info("in middlewareTokenRoot")
        const {token} = req.headers;
        if (!token) {
            loggerFile.debug("besoin de token")
            return res.status(401).json({user: false, message: "besoin token"});
        } else {
            var decoded = jwt.decode(token);
            if (decoded) {
                const _id = decoded._idConnected;
                const user = await Admin.findById(_id);
                jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                    if (err) return res.status(401).json({user: false, message: "token invalide"});
                    if (user && (user.role === "Root")) {
                        req.auth = decoded2;
                        return next();
                    } else {
                        return res.status(401).json({user: false, message: "accés non autorisé"});
                    }
                });
            } else {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(401).json({user: false, message: "token invalide"});
            }
        }
    },
}
