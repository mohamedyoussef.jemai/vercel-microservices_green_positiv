const Mission = require("../models/mission.model");
const Company = require('../models/company.model');
const requiredProfil = require('../models/requiredProfil.model');
const adminService = require("../services/adminService");
const freelanceService = require("../services/freelanceService");
const agenceService = require("../services/agenceService");

const loggerFile = require("../config/logger");
const cloudinary = require('../config/cloudinary');
const utils = require('../config/utils');

const levels = ['Junior', 'Intermédiaire', 'Senior'];
const ObjectId = require('mongoose').Types.ObjectId;

module.exports = {
    addRequiredProfil: async (req, res, next) => {
        loggerFile.info('in addRequiredProfil')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let {title_profile, level, jobCat, skillsNeeded, skillsAppreciated, languages} = req.body;
        try {
            if (title_profile && typeof title_profile !== "string") {
                loggerFile.debug("creation requiredProfil data : problem in title_profile")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (!levels.includes(level) || level.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in level")
                return res.status(400).send({message: "le niveau est inapproprié"});
            } else {
                skillsNeeded = skillsNeeded.split(',')
                skillsAppreciated = skillsAppreciated.split(',')
                languages = languages.split(',')


                let profile = new requiredProfil({
                    title_profile,
                    level,
                    jobCat,
                    skillsNeeded,
                    skillsAppreciated,
                    languages
                });
                await requiredProfil.create(profile).then(data => {
                    if (data) {
                        loggerFile.debug("add requiredProfile data done")
                        req.profile = data;
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de l'ajout du profil recherché"});
                });
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: "réessayez l'action"});
        }
    },
    middlewareJobExist: async (req, res, next) => {
        loggerFile.info("in middlewareJobExist")
        try {
            let {jobCat} = req.body;
            await adminService.getJobs().then(response => {
                if (response.status == 200) {
                    let job = response.data.find(subject => subject._id === jobCat);
                    if (job) {
                        loggerFile.debug("middlewareJobExist done step 2")
                        return next();
                    } else {
                        loggerFile.debug("le métier n'éxiste pas")
                        return res.status(400).json({message: `le métier n'éxiste pas`});
                    }
                } else {
                    loggerFile.debug("un probléme est survenu")
                    return res.status(400).json({message: `un probléme est survenu`});
                }
            }).catch(err => {
                loggerFile.error(err,utils.getClientAddress(req, err))
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    addMission: async (req, res, next) => {
        loggerFile.info('in addMission')
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let id = req.auth._idConnected;
            let {
                name,
                description,
                objectif,
                dateBegin,
                dateEnd,
                price_per_day,
                supp_month,
                period_per_month,
                work_frequence,
                show_price,
                budget,
                telework,
                nb_days_telework,
                comments,
                local_city,
            } = req.body;

            if (name && typeof name !== "string") {
                loggerFile.debug("creation mission data : problem in name")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (comments && typeof comments !== "string") {
                loggerFile.debug("creation mission data : problem in comments")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (description && typeof description !== "string") {
                loggerFile.debug("creation mission data : problem in description")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (objectif && typeof objectif !== "string") {
                loggerFile.debug("creation mission data : problem in objectif")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (Date.parse(dateBegin) > Date.parse(dateEnd)) {
                loggerFile.debug("Changer la période, l'intervalle est erroné")
                return res.status(400).json({message: "Changer la période, l'intervalle est erroné"});
            }
            if (supp_month) {
                if (period_per_month && typeof period_per_month !== "number" && period_per_month < 1) {
                    loggerFile.debug("creation mission data : problem in period_per_month")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (period_per_month)"});
                }
                if (price_per_day && typeof price_per_day !== "number" && period_per_month <= 0) {
                    loggerFile.debug("creation mission data : problem in price_per_day")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (price_per_day)"});
                }
                if (show_price && show_price != "true" && show_price != "false") {
                    loggerFile.debug("creation mission data : problem in show_price")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (show_price)"});
                }
                if (work_frequence && typeof parseInt(work_frequence) !== "number" || (parseInt(work_frequence) < 0 || parseInt(work_frequence) > 4)) {
                    loggerFile.debug("creation mission data : problem in work_frequence")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (work_frequence)"});
                }
            } else {
                if (budget && typeof budget !== "number" && budget <= 0) {
                    loggerFile.debug("creation mission data : problem in budget")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (budget)"});
                }
            }
            if (telework && telework != "true" && telework != "false") {
                loggerFile.debug("creation mission data : problem in telework")
                return res.status(400).send({message: "les informations ne peuvent pas être vides (telework)"});
            } else {
                if (telework == "true" && (parseInt(nb_days_telework) && typeof parseInt(nb_days_telework) !== "number" || (parseInt(nb_days_telework) < 0 || parseInt(nb_days_telework) > 4))) {
                    loggerFile.debug("creation mission data : problem in nb_days_telework")
                    return res.status(400).send({message: "les informations ne peuvent pas être vides (nb_days_telework)"});
                }
            }
            if (local_city && typeof local_city !== "string") {
                loggerFile.debug("creation mission data : problem in local_city")
                return res.status(400).send({message: "les informations ne peuvent pas être vides (local_city)"});
            } else {

                let mission = new Mission({
                    name,
                    description,
                    objectif,
                    dateBegin,
                    dateEnd,
                    price_per_day,
                    supp_month,
                    period_per_month,
                    work_frequence,
                    show_price,
                    budget,
                    telework,
                    nb_days_telework,
                    comments,
                    local_city,
                });

                mission.id_company = id;
                mission.profilSearched = req.profile._id;
                if (req.file) {
                    const result = await cloudinary.uploader.upload(req.file.path);
                    mission.document = result.secure_url;
                    mission.cloudinary_doc_id = result.public_id;
                    await Mission.create(mission).then(data => {
                        if (data) {
                            loggerFile.debug("addMission data done")
                            req.mission = data;
                            return next();
                        }
                    }).catch(err => {
                        loggerFile.debug(err)
                        return res.status(400).json({message: "erreur lors de l'ajout de la mission"});
                    });
                } else {
                    return res.status(400).json({message: "document inexistant"});
                }
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifiyMissionCompany: async (req, res, next) => {
        loggerFile.info('in verifiyMissionCompany')
        const idCompany = req.auth._idConnected;
        const idMission = req.params.id;
        try {
            const company = await Company.findById(idCompany);
            if (company.missions.includes(idMission)) {
                loggerFile.debug("verifyFormationFreelancer done")
                return next();
            } else {
                loggerFile.debug("mission non excistante pour cette entreprise")
                return res.status(400).json({message: "mission non excistante pour cette entreprise"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateRequiredProfil: async (req, res, next) => {
        loggerFile.info('in updateRequiredProfil');
        if (!req.body) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        try {
            let idMission = req.params;

            let {title_profile, jobCat, level, skillsNeeded, skillsAppreciated, languages} = req.body;

            if (title_profile && typeof title_profile !== "string") {
                loggerFile.debug("creation requiredProfil data : problem in title_profile")
                return res.status(400).send({message: "les informations ne peuvent pas être vides"});
            }
            if (!levels.includes(level) || level.trim().length === 0) {
                loggerFile.debug("creation Freelancer data : problem in level")
                return res.status(400).send({message: "le niveau est inapproprié"});
            } else {
                skillsNeeded = skillsNeeded.split(',')
                skillsAppreciated = skillsAppreciated.split(',')
                languages = languages.split(',')

                let newProfile = {
                    title_profile,
                    level,
                    jobCat,
                    skillsNeeded,
                    skillsAppreciated,
                    languages
                };
                let mission = await Mission.findById(new ObjectId(idMission));
                await requiredProfil.findByIdAndUpdate(mission.profilSearched, newProfile).then(data => {
                    if (data) {
                        loggerFile.debug("add requiredProfile data done")
                        req.profile = data;
                        return next();
                    }
                }).catch(err => {
                    loggerFile.error(err,utils.getClientAddress(req, err))
                    return res.status(400).json({message: "erreur lors de la mise à jour du profil recherché"});
                });
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    verifFreelanceExist: async (req, res, next) => {
        loggerFile.info('in verifFreelanceExist');
        let {id_freelance} = req.body;
        let response = await freelanceService.findFreelance(id_freelance);
        if (response.status == 200 && response.data) {
            next()
        } else {
            return res.status(400).json({message: "freelance inéxistant"});
        }
    },
    verifAgenceExist: async (req, res, next) => {
        loggerFile.info('in verifAgenceExist');
        let {id_agence} = req.body;
        try {
            let response = await agenceService.findAgence(id_agence);
            if (response.status == 200 && response.data) {
                next()
            } else {
                return res.status(400).json({message: "Agence inéxistante"});
            }
        } catch(error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
        }
    },
    verifInvite: async (req, res, next) => {
        loggerFile.info('in verifInviteFreelance');

        let {id_company, id_mission, price_per_day, dateBegin, dateEnd, budget} = req.body;
        if (Date.parse(dateBegin) > Date.parse(dateEnd)) {
            loggerFile.debug("Changer la période, l'intervalle est erroné")
            return res.status(400).json({message: "Changer la période, l'intervalle est erroné"});
        }
        try {
            let mission = await Mission.findById(id_mission);
            if (mission.supp_month) {
                if (price_per_day && typeof price_per_day !== "number") {
                    loggerFile.debug("invit Freelancer verif : problem in price_per_day")
                    return res.status(400).send({message: "le prix est inapproprié"});
                }
            } else if (budget && typeof budget !== "number") {
                loggerFile.debug("invit Freelancer verif : problem in budget")
                return res.status(400).send({message: "le budget est inapproprié"});
            }

            let company = await Company.findById(id_company);
            if (company && mission && mission.id_company.equals(company._id)) {
                return next()
            } else {
                return res.status(400).json({message: "l'envoi du devis à échouer"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}



