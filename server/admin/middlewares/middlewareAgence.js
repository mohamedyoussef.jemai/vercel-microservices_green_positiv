const loggerFile = require("../config/logger");
const Job = require('../models/jobs.model')
const Agence = require('../models/agence.model')
const utils = require('../config/utils');

module.exports = {
    getJobs: async (req, res, next) => {
        loggerFile.info("in getJobs")
        try {
            let response = await Job.find();
            if (response) {
                req.body.jobs = response;
                loggerFile.debug("getJobs done")
                return next();
            } else {
                loggerFile.debug("erreur lors du chargement des jobs")
                return res.status(400).send({message: `erreur lors du chargement des jobs`});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    ExistParamAgence: async (req, res, next) => {
        loggerFile.info("in ExistParamAgence")
        try {
            let {id} = req.params;
            let agence = await Agence.findById(id)

            if (agence) {
                loggerFile.debug("freelancer trouvé")
                return next();
            } else {
                loggerFile.debug("freelancer introuvable")
                return res.status(400).json({message: "freelancer introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
}
