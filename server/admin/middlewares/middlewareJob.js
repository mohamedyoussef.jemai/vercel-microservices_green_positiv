const Job = require('../models/jobs.model');
const loggerFile = require("../config/logger");
const utils = require('../config/utils');

module.exports = {
    middlewareValidityCreationJob: async (req, res, next) => {
        loggerFile.info("in middlewareValidityCreationJob")
        const {name} = req.body;
        try {
            if (!name) {
                loggerFile.debug("les informations sont vides")
                return res.status(400).send({message: "les informations sont vides"});
            } else {
                loggerFile.debug("middlewareValidityCreationJob")
                return next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareJobExist: async (req, res, next) => {
        loggerFile.info("in middlewareJobExist")
        let jobs = await Job.find();
        let test = false;
        let {name} = req.body;

        try {
            jobs.map((el) => {
                if (name.toUpperCase() == el.name.toUpperCase()) {
                    loggerFile.debug("job exist")
                    test = true
                } else {
                    test = false;
                }
            });
            if (test) {
                loggerFile.debug("job exist")
                return res.status(400).json({message: "Ce métier éxiste"});
            } else {
                next();
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareUpdateJob: async (req, res, next) => {
        loggerFile.info("in middlewareUpdateJob")
        const {name} = req.body;
        if (!name) {
            loggerFile.debug("les informations sont vides")
            return res.status(400).send({message: "les informations sont vides"});
        }
        let jobs = await Job.find();
        let test = false;

        try {
            jobs.map((el) => {
                if (name.toUpperCase() == el.name.toUpperCase()) {
                    loggerFile.debug("job exist")
                    test = true;
                }
            });
            if (test == true) {
                return res.status(400).json({message: "Ce métier existe"});
            } else {
                next();
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    }
}
