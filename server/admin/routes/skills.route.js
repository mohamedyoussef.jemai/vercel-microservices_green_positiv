const express = require('express');
const router = express.Router();
const skillsController = require('../controllers/skillsController');
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')
const {
    middlewareValidityCreationSkill,
    middlewareSkillExist,
    middlewareUpdateSkill
} = require('../middlewares/middlewareSkill')

router.get('/', skillsController.findAll);
router.get('/:id', middlewareTokenAdminOrRoot, skillsController.findOne);
router.post('/add-skill', middlewareTokenAdminOrRoot, middlewareValidityCreationSkill, middlewareSkillExist, skillsController.create);
router.patch('/:id', middlewareTokenAdminOrRoot, middlewareUpdateSkill, skillsController.update);
router.delete('/:id', middlewareTokenAdminOrRoot, skillsController.delete);
router.delete('/', middlewareTokenAdminOrRoot, skillsController.deleteAll);

module.exports = router;
