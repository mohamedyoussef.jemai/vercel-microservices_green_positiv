const express = require('express');
const router = express.Router();
const profilEntrepriseAgenceController = require('../controllers/profilEntrepriseAgenceController');
const {
    validateLegalForms,
    validateIbanTypeAndData,
    validateCountryLegalRepresentative,
    validateIbanCountry,
    verifyMentionLegalCreation
} = require('../middlewares/middlewareFreelancer');
const {
    ExistParamAgence
} = require('../middlewares/middlewareAgence');

router.patch('/validate', profilEntrepriseAgenceController.validateProfileFreelane);
router.patch('/update/:id', profilEntrepriseAgenceController.update);
router.post('/', validateLegalForms, profilEntrepriseAgenceController.createContactDetails);
router.patch('/', validateLegalForms, profilEntrepriseAgenceController.updateContactDetails);
router.patch('/legal-representative', validateCountryLegalRepresentative, profilEntrepriseAgenceController.UpdateLegalRepresentative);
router.patch('/payment/iban', validateIbanTypeAndData, validateIbanCountry, profilEntrepriseAgenceController.updateIbanAccount);
router.patch('/taxes', profilEntrepriseAgenceController.UpdateTaxe);
router.patch('/legal-mention', verifyMentionLegalCreation, profilEntrepriseAgenceController.UpdateLegalMention);
router.delete('/delete/:id', profilEntrepriseAgenceController.delete);

router.get('/', profilEntrepriseAgenceController.findAll);
router.get('/:id', ExistParamAgence, profilEntrepriseAgenceController.findOne);

module.exports = router;
