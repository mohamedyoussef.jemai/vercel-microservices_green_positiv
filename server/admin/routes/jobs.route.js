const express = require('express');
const router = express.Router();
const jobsController = require('../controllers/jobsController');
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')
const {middlewareValidityCreationJob, middlewareJobExist, middlewareUpdateJob} = require('../middlewares/middlewareJob')

router.get('/', jobsController.findAll);
router.get('/admin', middlewareTokenAdminOrRoot, jobsController.findAllAdmin);
router.get('/:id', middlewareTokenAdminOrRoot, jobsController.findOne);
router.post('/add-job', middlewareTokenAdminOrRoot, middlewareValidityCreationJob, middlewareJobExist, jobsController.create);
router.patch('/:id', middlewareTokenAdminOrRoot, middlewareUpdateJob, jobsController.update);
router.delete('/:id', middlewareTokenAdminOrRoot, jobsController.delete);
router.delete('/', middlewareTokenAdminOrRoot, jobsController.deleteAll);

module.exports = router;
