const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationPostController');

const {middlewareTokenAdminOrRootOrEditor} = require('../middlewares/middlewareEditor')
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')
router.patch('/check/:id', middlewareTokenAdminOrRoot, notificationController.checkNotification);
router.patch('/uncheck/:id', middlewareTokenAdminOrRoot, notificationController.uncheckNotification);
router.get('/', middlewareTokenAdminOrRoot, notificationController.getAllNotification);
router.get('/checked', middlewareTokenAdminOrRoot, notificationController.getAllCheckedNotification);
router.get('/unchecked', middlewareTokenAdminOrRoot, notificationController.getAllUnCheckedNotification);
router.post('/period', middlewareTokenAdminOrRoot, notificationController.getAllNotificationByPeriod);
router.post('/period/checked', middlewareTokenAdminOrRoot, notificationController.getAllCheckedNotificationByPeriod);
router.post('/period/unchecked', middlewareTokenAdminOrRoot, notificationController.getAllUnCheckedNotificationByPeriod);
router.get('/editor', middlewareTokenAdminOrRootOrEditor, notificationController.getPostEditor);
router.post('/editor-period', middlewareTokenAdminOrRootOrEditor, notificationController.getPostEditorPeriod);

module.exports = router;
