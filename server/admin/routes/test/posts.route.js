const express = require('express');
const multer = require('multer');
const router = express.Router();
const postController = require('../../controllers/test/postTestController');
const {middlewareTokenAdminOrRootOrEditor} = require('../../middlewares/middlewareEditor')
const {middlewareTokenAdminOrRoot} = require('../../middlewares/middlewareAdmin')


router.get('', postController.findAll);
router.get('/:id', postController.findOne);
router.post('/', middlewareTokenAdminOrRootOrEditor, postController.create);
router.patch('/:id', middlewareTokenAdminOrRootOrEditor, postController.update);
router.delete('/:id', middlewareTokenAdminOrRootOrEditor, postController.delete);
router.patch('/validate/:id', middlewareTokenAdminOrRoot, postController.validatePost);
router.patch('/unvalidate/:id', middlewareTokenAdminOrRoot, postController.unValidatePost);

module.exports = router;
