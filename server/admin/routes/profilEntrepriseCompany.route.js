const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const profileEntrepriseCompanyController = require('../controllers/profilEntrepriseCompanyController');

const {
    middlewareTokenCompany,
    middlewareCompanyExist,
    validateFacturationDetails,
    ExistParamCompany,
    middlewareTokenRootOrAdmin,
    middlewareCompanyExistByAdmin
} = require('../middlewares/middlewareCompany');

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        cb(null, true)
    },
}).single("logo");

router.post('/', profileEntrepriseCompanyController.create);
router.patch('/', middlewareTokenCompany, middlewareCompanyExist, upload, profileEntrepriseCompanyController.createProfileEntreprise);
router.patch('/facturation', middlewareTokenCompany, middlewareCompanyExist, validateFacturationDetails, profileEntrepriseCompanyController.updateFacturationDetails);
router.patch('/contact', middlewareTokenCompany, middlewareCompanyExist, profileEntrepriseCompanyController.updateContactCompta);
router.get('/', profileEntrepriseCompanyController.findAll);
router.get('/:id', ExistParamCompany, profileEntrepriseCompanyController.findOne);
router.patch('/validate', profileEntrepriseCompanyController.validateProfileCompany);
router.delete('/delete/:id', profileEntrepriseCompanyController.delete);

router.patch('/admin/:id', middlewareTokenRootOrAdmin,middlewareCompanyExistByAdmin, upload, profileEntrepriseCompanyController.createProfileEntrepriseByAdmin);
router.patch('/admin/facturation/:id', middlewareTokenRootOrAdmin, middlewareCompanyExistByAdmin,validateFacturationDetails, profileEntrepriseCompanyController.updateFacturationDetailsByAdmin);
router.patch('/admin/contact/:id', middlewareTokenRootOrAdmin, middlewareCompanyExistByAdmin, profileEntrepriseCompanyController.updateContactComptaByAdmin);

module.exports = router;
