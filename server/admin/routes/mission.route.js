const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const missionController = require('../controllers/missionController');

const {
    middlewareTokenCompany
} = require('../middlewares/middlewareUser');
const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');
const {
    addMission,
    verifiyMissionCompany,
    addRequiredProfil,
    updateRequiredProfil,
    verifFreelanceExist,
    verifAgenceExist,
} = require('../middlewares/middlewareMission');
const {
    middlewareJobExist,
} = require('../middlewares/middlewareJob');

let uploadOneDocument = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        cb(null, true)
    },
}).single("document");

router.patch("/:id", uploadOneDocument, middlewareTokenAdminOrRoot, updateRequiredProfil, missionController.update);
router.delete("/:id", middlewareTokenAdminOrRoot, missionController.delete);
router.get("/get/:id",middlewareTokenAdminOrRoot, missionController.findOne);
router.get("/profiles/:id", middlewareTokenAdminOrRoot, missionController.findFreelancedSearched);
router.post("/profiles-agences/:id", middlewareTokenAdminOrRoot, missionController.findAgenceSearched);

router.get("/devis/:idCompany/:id", middlewareTokenAdminOrRoot, missionController.getDevisFromMission);
router.get("/devis-by-id/:idCompany/:id", middlewareTokenAdminOrRoot, missionController.getDevisFromMissionById);
router.get("/get-devis/:id", middlewareTokenAdminOrRoot, missionController.getDevisById);
router.patch("/freelance/accept/:id", middlewareTokenAdminOrRoot, verifFreelanceExist, missionController.acceptFreelance);
router.patch("/freelance/refuse/:id", middlewareTokenAdminOrRoot, verifFreelanceExist, missionController.refuseFreelance);
router.post("/freelance/remove/:id",middlewareTokenAdminOrRoot, verifFreelanceExist, missionController.removeFreelance);

router.patch("/agence/accept/:id", middlewareTokenAdminOrRoot, verifAgenceExist, missionController.acceptAgence);
router.patch("/agence/refuse/:id", middlewareTokenAdminOrRoot, verifAgenceExist, missionController.refuseAgence);
router.post("/agence/remove/:id", middlewareTokenAdminOrRoot, verifAgenceExist, missionController.removeAgence);

router.get('/all/:id', middlewareTokenAdminOrRoot, missionController.findCompanyMissions);

module.exports = router;
