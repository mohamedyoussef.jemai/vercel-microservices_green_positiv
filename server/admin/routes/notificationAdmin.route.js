const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationAdminController');

router.post('/create', notificationController.postCreatedNotif);
router.patch('/update', notificationController.updatePostNotif);
router.patch('/validate', notificationController.validatedPostNotif);
router.patch('/unvalidate', notificationController.unvalidatedPostNotif);
router.get('/', notificationController.findAll);

module.exports = router;
