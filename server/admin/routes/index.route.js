const express = require('express');
const router = express.Router();
const indexController = require('../controllers/indexController');
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')

router.post('/login', indexController.login);
router.post('/forgot-password', indexController.forgotPassword);
router.post('/check', middlewareTokenAdminOrRoot, indexController.checkedToken);
router.patch('/update-password', middlewareTokenAdminOrRoot, indexController.updatePassword);

module.exports = router;
