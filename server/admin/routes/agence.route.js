const express = require('express');
const router = express.Router();
const multer = require('multer');
const path = require('path');
const agenceController = require('../controllers/agenceController');


const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');

const {
    getJobs,
} = require('../middlewares/middlewareAgence');
const {
    getOffers,
} = require('../middlewares/middlewareOffer');

const {
    getReferences,
} = require('../middlewares/middlewareReference');

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        console.log("extension ", ext)
        cb(null, true)
    },
}).single("image");
let uploadDocuments = multer({
    storage: multer.diskStorage({}),
}).array("documents", 4);

router.get('/all', agenceController.findAll);
router.get('/get/:id', getOffers, getReferences, getJobs, agenceController.findOne);

router.patch('/delete-agence/:id', middlewareTokenAdminOrRoot, agenceController.deleteAgence);
router.get('/validated-agences', middlewareTokenAdminOrRoot, agenceController.getValidatedAgences);
router.get('/unvalidated-agences', middlewareTokenAdminOrRoot, agenceController.getUnvalidatedAgences);
router.patch('/block-agence/:id', middlewareTokenAdminOrRoot, agenceController.blockAgence);
router.patch('/unblock-agence/:id', middlewareTokenAdminOrRoot, agenceController.unblockAgence);
router.patch('/validate-document-agence/:id', middlewareTokenAdminOrRoot, agenceController.validateDocumentAgence);
router.patch('/unvalidate-document-agence/:id', middlewareTokenAdminOrRoot, agenceController.unvalidateDocumentAgence);
router.patch('/validate-offer/:id', middlewareTokenAdminOrRoot, agenceController.validateOffer);
router.patch('/unvalidate-offer/:id', middlewareTokenAdminOrRoot, agenceController.unvalidateOffer);
router.get('/agence-documents-validated', middlewareTokenAdminOrRoot, agenceController.findAllDocumentValidated);
router.get('/agence-documents-unvalidated', middlewareTokenAdminOrRoot, agenceController.findAllDocumentunValidated);
router.get('/profil-entreprise/', middlewareTokenAdminOrRoot, agenceController.getProfileEntrepriseOfAgences);
router.get('/profil-entreprise/:id', middlewareTokenAdminOrRoot, agenceController.getProfileEntrepriseOfAgenceById);

router.patch('/update-agence/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceByAdmin);
router.patch('/update-agence-contact/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceContactDetailsByAdmin);
router.patch('/update-agence-taxes/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceTaxesByAdmin);
router.patch('/update-agence-legal-representative/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceLegalRepresentativeByAdmin);
router.patch('/update-agence-legal-mention/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceLegalMentionByAdmin);
router.patch('/update-agence-iban/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceIbanByAdmin);
router.patch('/update-agence-offer/:id', middlewareTokenAdminOrRoot, agenceController.updateAgenceOfferByAdmin);
router.post('/get-agence-offer/:id', middlewareTokenAdminOrRoot, agenceController.getAgenceOfferByAdmin);
router.patch('/update-agence-reference/:id', middlewareTokenAdminOrRoot, upload, agenceController.updateAgenceReferenceByAdmin);
router.post('/get-agence-reference/:id', middlewareTokenAdminOrRoot, agenceController.getAgenceReferenceByAdmin);

router.patch('/kabis-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, agenceController.uploadKabisDocuments);
router.patch('/vigilance-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, agenceController.uploadVigilanceDocuments);
router.patch('/sasu-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, agenceController.uploadSasuDocuments);

router.get("/devis/:id", middlewareTokenAdminOrRoot, agenceController.getDevisAgence);
router.get("/offers/:id", middlewareTokenAdminOrRoot, agenceController.getOfferAgence);

router.post("/required-profile", middlewareTokenAdminOrRoot, agenceController.getRequiredProfiles);

module.exports = router;
