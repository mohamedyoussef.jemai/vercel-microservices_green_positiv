const express = require('express');
const router = express.Router();
const subscriptionController = require('../controllers/subscriptionController');
const {
    middlewareTokenAdminOrRoot,
    middlewareSubscriptionEmailExist,
    middlewareTokenUser
} = require('../middlewares/middlewareAdmin')

router.get('/', middlewareTokenAdminOrRoot, subscriptionController.findAll);
router.post('/', middlewareSubscriptionEmailExist, subscriptionController.create);
router.patch('/', middlewareTokenUser, subscriptionController.update);

module.exports = router;
