const express = require('express');
const router = express.Router();
const adminController = require('../controllers/adminController');

const {
    middlewareValidityCreationUser,
    middlewareExistUsernameAdmin,
    middlewareExistEmailAdmin,
    middlewareTokenAdminOrRoot,
    middlewareTokenAdminOrRootOrEditor
} = require('../middlewares/middlewareAdmin');
const {middlewareTokenRoot} = require('../middlewares/middlewareRoot')

router.get('/', middlewareTokenRoot, adminController.findAll);
router.get('/:id', middlewareTokenAdminOrRootOrEditor, adminController.findOne);
router.post('/', middlewareTokenRoot, middlewareValidityCreationUser, middlewareExistUsernameAdmin, middlewareExistEmailAdmin, adminController.create);
router.patch('/:id', middlewareTokenAdminOrRoot, middlewareExistUsernameAdmin, middlewareExistEmailAdmin, adminController.update);
router.patch('/update/:id', middlewareTokenRoot, middlewareExistUsernameAdmin, middlewareExistEmailAdmin, adminController.updateByAdmin);
router.delete('/:id', middlewareTokenRoot, adminController.delete);

module.exports = router;
