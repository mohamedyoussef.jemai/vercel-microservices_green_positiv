const express = require('express');
const router = express.Router();
const profileEntrepriseController = require('../controllers/profilEntrepriseController');
const {
    validateLegalForms,
    validateIbanTypeAndData,
    validateCountryLegalRepresentative,
    validateIbanCountry,
    verifyMentionLegalCreation,
    ExistParamFreelancer
} = require('../middlewares/middlewareFreelancer');

router.patch('/validate', profileEntrepriseController.validateProfileFreelane);
router.patch('/update/:id', profileEntrepriseController.update);
router.post('/', validateLegalForms, profileEntrepriseController.createContactDetails);
router.patch('/', validateLegalForms, profileEntrepriseController.updateContactDetails);
router.patch('/legal-representative', validateCountryLegalRepresentative, profileEntrepriseController.UpdateLegalRepresentative);
router.patch('/payment/iban', validateIbanTypeAndData, validateIbanCountry, profileEntrepriseController.updateIbanAccount);
router.patch('/taxes', profileEntrepriseController.UpdateTaxe);
router.patch('/legal-mention', verifyMentionLegalCreation, profileEntrepriseController.UpdateLegalMention);
router.delete('/delete/:id', profileEntrepriseController.delete);

router.get('/', profileEntrepriseController.findAll);
router.get('/:id',ExistParamFreelancer , profileEntrepriseController.findOne);

module.exports = router;
