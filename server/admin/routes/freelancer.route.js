const express = require('express');
const router = express.Router();
const multer = require('multer');
const freelancerController = require('../controllers/freelancerController');


const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');
const {
    getFormation,
    getCertification,
    getSkills,
    getExperiences,
    getJobs,
} = require('../middlewares/middlewareFreelancer');

let uploadDocuments = multer({
    storage: multer.diskStorage({}),
}).array("documents", 4);

router.get('/all', freelancerController.findAll);
router.get('/get/:id', middlewareTokenAdminOrRoot, getCertification, getFormation, getExperiences, getJobs, getSkills, freelancerController.findOne);
router.patch('/delete-freelancer/:id', middlewareTokenAdminOrRoot, freelancerController.deleteFreelancer);
router.get('/documents-validated-freelancers', middlewareTokenAdminOrRoot, freelancerController.getDocumentValidatedFreelancers);
router.get('/documents-unvalidated-freelancers', middlewareTokenAdminOrRoot, freelancerController.getDocumentUnvalidatedFreelancers);
router.get('/validated-freelancers', middlewareTokenAdminOrRoot, freelancerController.getValidatedFreelancers);
router.get('/unvalidated-freelancers', middlewareTokenAdminOrRoot, freelancerController.getUnvalidatedFreelancers);
router.patch('/documents-validated/:id', middlewareTokenAdminOrRoot, freelancerController.validateDocumentOfFreelancer);
router.patch('/documents-unvalidated/:id', middlewareTokenAdminOrRoot, freelancerController.unvalidateDocumentOfFreelancer);
router.patch('/block-freelancers/:id', middlewareTokenAdminOrRoot, freelancerController.blockFreelancer);
router.patch('/unblock-freelancers/:id', middlewareTokenAdminOrRoot, freelancerController.unblockFreelancer);
router.get('/profil-entreprise', middlewareTokenAdminOrRoot, freelancerController.getProfileEntrepriseOfFreelancer);
router.get('/profil-entreprise/:id', middlewareTokenAdminOrRoot, freelancerController.getProfileEntrepriseOfFreelancerById);

router.patch('/update-freelance/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerByAdmin);
router.patch('/update-freelance-contact/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerContactDetailsByAdmin);
router.patch('/update-freelance-legal-representative/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerLegalRepresentativeByAdmin);
router.patch('/update-freelance-taxes/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerTaxesByAdmin);
router.patch('/update-freelance-legal-mention/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerLegalMentionByAdmin);
router.patch('/update-freelance-iban/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerIbanByAdmin);
router.patch('/update-freelance-certification/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerCertificationByAdmin);
router.post('/get-freelance-certification/:id', middlewareTokenAdminOrRoot, freelancerController.getFreelancerCertificationByAdmin);
router.patch('/update-freelance-formation/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerFormationByAdmin);
router.post('/get-freelance-formation/:id', middlewareTokenAdminOrRoot, freelancerController.getFreelancerFormationByAdmin);
router.patch('/update-freelance-experience/:id', middlewareTokenAdminOrRoot, freelancerController.updateFreelancerExperienceByAdmin);
router.post('/get-freelance-experience/:id', middlewareTokenAdminOrRoot, freelancerController.getFreelancerExperienceByAdmin);

router.patch('/kabis-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, freelancerController.uploadKabisDocuments);
router.patch('/vigilance-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, freelancerController.uploadVigilanceDocuments);
router.patch('/sasu-documents/:id', middlewareTokenAdminOrRoot, uploadDocuments, freelancerController.uploadSasuDocuments);

router.get("/devis/:id", middlewareTokenAdminOrRoot, freelancerController.getDevisFreelance);

router.post("/required-profile", middlewareTokenAdminOrRoot, freelancerController.getRequiredProfiles);

module.exports = router;
