const express = require('express');
const router = express.Router();
const demandController = require('../controllers/demandController');

const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');

router.get("/get/:id", middlewareTokenAdminOrRoot, demandController.findAllByIdUser);
router.get("/:id", middlewareTokenAdminOrRoot, demandController.findDemandByIdAndByIdUser);

router.patch("/:id", middlewareTokenAdminOrRoot, demandController.updateSearch);
router.get("/all", middlewareTokenAdminOrRoot, demandController.findAllInProcess);
router.get("/", middlewareTokenAdminOrRoot, demandController.findAll);

module.exports = router;
