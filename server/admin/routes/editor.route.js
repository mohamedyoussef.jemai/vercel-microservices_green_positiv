const express = require('express');
const router = express.Router();
const editorController = require('../controllers/editorController');
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')
const {
    middlewareValidityCreationUser,
    middlewareExistUsernameEditor,
    middlewareExistEmailEditor,
} = require('../middlewares/middlewareEditor')

router.get('/', middlewareTokenAdminOrRoot, editorController.findAll);
router.get('/:id', middlewareTokenAdminOrRoot, editorController.findOne);
router.post('/', middlewareTokenAdminOrRoot, middlewareExistUsernameEditor, middlewareExistEmailEditor, middlewareValidityCreationUser, editorController.create);
router.patch('/:id', middlewareTokenAdminOrRoot, editorController.update);
router.delete('/:id', middlewareTokenAdminOrRoot, editorController.delete);

module.exports = router;
