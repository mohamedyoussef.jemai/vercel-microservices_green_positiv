const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationPostController');

const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');

const {
    middlewareTokenAdminOrRootOrEditor
} = require('../middlewares/middlewareEditor');

router.patch('/check/:id', middlewareTokenAdminOrRootOrEditor, notificationController.checkNotification);
router.get('/', middlewareTokenAdminOrRoot, notificationController.getAllNotification);
router.get('/checked', middlewareTokenAdminOrRoot, notificationController.getAllCheckedNotification);
router.get('/unchecked', middlewareTokenAdminOrRoot, notificationController.getAllUnCheckedNotification);
router.post('/period', middlewareTokenAdminOrRoot, notificationController.getAllNotificationByPeriod);
router.post('/period/checked', middlewareTokenAdminOrRoot, notificationController.getAllCheckedNotificationByPeriod);
router.post('/period/unchecked', middlewareTokenAdminOrRoot, notificationController.getAllUnCheckedNotificationByPeriod);
router.get('/editor', middlewareTokenAdminOrRootOrEditor, notificationController.getPostEditor);

module.exports = router;
