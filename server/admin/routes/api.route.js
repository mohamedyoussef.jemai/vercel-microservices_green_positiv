const express = require('express');
const router = express.Router();
const apiController = require('../controllers/apiController');

router.get('/languages', apiController.getLanguages);
router.get('/legal-forms', apiController.getLegalForm);
router.get('/countries', apiController.getCountry);
router.get('/iban-countries', apiController.getIbanCountry);
router.get('/iban-us-ca-others-countries', apiController.getIbanUsCaOtherCountry);
router.get('/sector-activity', apiController.getSectorActivity);

module.exports = router;
