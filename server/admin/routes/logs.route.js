const express = require('express');
const router = express.Router();
const logsController = require('../controllers/logsController');

router.get('/', logsController.findAll);
router.get('/unchecked', logsController.findUnchecked);
router.patch('/check/:id', logsController.checkLog);
router.patch('/uncheck/:id', logsController.unCheckLog);

module.exports = router;
