const express = require('express');
const router = express.Router();
const notificationController = require('../controllers/notificationCompanyController');

router.patch('/upload-document', notificationController.createUploadDocumentNotif);
router.patch('/profil', notificationController.createProfilEntrepriseNotif);
router.patch('/facture', notificationController.createFacturationDetailsNotif);
router.patch('/contact', notificationController.createContactComptaNotif);
router.get('/', notificationController.findAll);
router.patch('/check/:id', notificationController.checkNotification);
router.get('/', notificationController.getAllNotification);
router.get('/checked', notificationController.getAllCheckedNotification);
router.get('/unchecked', notificationController.getAllUnCheckedNotification);
router.post('/period', notificationController.getAllNotificationByPeriod);
router.post('/period/checked', notificationController.getAllCheckedNotificationByPeriod);
router.post('/period/unchecked', notificationController.getAllUnCheckedNotificationByPeriod);

module.exports = router;
