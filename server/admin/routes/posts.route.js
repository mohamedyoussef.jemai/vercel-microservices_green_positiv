const express = require('express');
const multer = require('multer');
const path = require('path');
const router = express.Router();
const postController = require('../controllers/postController');
const {middlewareTokenAdminOrRootOrEditor} = require('../middlewares/middlewareEditor')
const {middlewareTokenAdminOrRoot} = require('../middlewares/middlewareAdmin')

let upload = multer({
    storage: multer.diskStorage({}),
    fileFilter: (req, file, cb) => {
        let ext = path.extname(file.originalname);
        if (ext !== ".jpg" && ext !== ".jpeg" && ext !== ".png") {
            req.validityFormat = false;
        } else {
            req.validityFormat = true;
        }
        cb(null, true)
    },
}).single("image");

router.get('/', postController.findAll);
router.get('/:id', postController.findOne);
router.post('/', middlewareTokenAdminOrRootOrEditor, upload, postController.create);
router.patch('/:id', middlewareTokenAdminOrRootOrEditor, upload, postController.update);
router.delete('/:id', middlewareTokenAdminOrRootOrEditor, postController.delete);
router.delete('/', middlewareTokenAdminOrRootOrEditor, postController.deleteAll);
router.patch('/validate/:id', middlewareTokenAdminOrRoot, postController.validatePost);
router.patch('/unvalidate/:id', middlewareTokenAdminOrRoot, postController.unValidatePost);

module.exports = router;
