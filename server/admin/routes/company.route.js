const express = require('express');
const router = express.Router();
const companyController = require('../controllers/companyController');

const {
    middlewareTokenAdminOrRoot
} = require('../middlewares/middlewareAdmin');
const {
    getWorkedInFreelancers,
    getFavoriteFreelancers,
    middlewareExistUsernameCompany,
    middlewareExistEmailCompany,
    middlewareValidityCollaborator,
    verifyAccessCollab,
    verifFreelanceExist,
    verifyFavoriteFreelanceExist,
} = require('../middlewares/middlewareCompany');

router.get('/all', companyController.findAll);
router.get('/get/:id', middlewareTokenAdminOrRoot, getWorkedInFreelancers, getFavoriteFreelancers, companyController.findOne);
router.patch('/update-company/:id', middlewareTokenAdminOrRoot, companyController.updateCompanyByAdmin);

router.patch('/delete-company/:id', middlewareTokenAdminOrRoot, companyController.deleteCompany);
router.get('/documents-validated-companies', middlewareTokenAdminOrRoot, companyController.getDocumentValidatedCompanies);
router.get('/documents-unvalidated-companies', middlewareTokenAdminOrRoot, companyController.getDocumentUnvalidatedCompanies);
router.get('/validated-companies', middlewareTokenAdminOrRoot, companyController.getValidatedCompanies);
router.get('/unvalidated-companies', middlewareTokenAdminOrRoot, companyController.getUnvalidatedCompanies);
router.patch('/documents-validated/:id', middlewareTokenAdminOrRoot, companyController.validateDocumentOfCompany);
router.patch('/documents-unvalidated/:id', middlewareTokenAdminOrRoot, companyController.unvalidateDocumentOfCompany);
router.patch('/block-company/:id', middlewareTokenAdminOrRoot, companyController.blockCompany);
router.patch('/unblock-company/:id', middlewareTokenAdminOrRoot, companyController.unblockCompany);
router.get('/profil-entreprise/', middlewareTokenAdminOrRoot, companyController.getProfileEntrepriseOfCompany);
router.get('/profil-entreprise/:id', middlewareTokenAdminOrRoot, companyController.getProfileEntrepriseOfCompanyById);
router.patch('/profil-entreprise/facturation/:id', middlewareTokenAdminOrRoot, companyController.updateCompanyFacturationByAdmin);
router.patch('/profil-entreprise/contact/:id', middlewareTokenAdminOrRoot, companyController.updateCompanyContactByAdmin);

router.post('/create-collab/:id', middlewareTokenAdminOrRoot, middlewareExistUsernameCompany, middlewareExistEmailCompany, middlewareValidityCollaborator, companyController.createCollaborator)
router.get('/add-collab/:id/:idCollab', middlewareTokenAdminOrRoot, verifyAccessCollab, companyController.addCollaborator);
router.patch('/remove-collab/:id/:idCollab', middlewareTokenAdminOrRoot, companyController.removeCollaborator);

router.patch('/add-favoris-freelance/:id/:idCompany', middlewareTokenAdminOrRoot, verifFreelanceExist, verifyFavoriteFreelanceExist, companyController.addFavoriteFreelance);
router.patch('/remove-favoris-freelance/:id/:idCompany', middlewareTokenAdminOrRoot, verifFreelanceExist, companyController.removeFavoriteFreelance);

module.exports = router;
