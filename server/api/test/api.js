//During the test the env variable is set to test
process.env.NODE_ENV = 'test';

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();


chai.use(chaiHttp);
//Our parent block
describe('API', () => {
    /*
     * Test Step 1 : GET api
    */
    describe('Step 1 : GET api', () => {
        /*
         * Test the /GET route
        */
        describe('/GET all languages', () => {
            it('it should GET all the 184 languages', (done) => {
                chai.request(server)
                    .get('/api/languages')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(184);
                        done();
                    });
            });
        });
        /*
         * Test the /GET route
        */
        describe('/GET all languages', () => {
            it('it should GET all the 19 legal forms', (done) => {
                chai.request(server)
                    .get('/api/legal-forms')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(19);
                        done();
                    });
            });
        });
        /*
         * Test the /GET route
        */
        describe('/GET all countries', () => {
            it('it should GET all the 252 countries', (done) => {
                chai.request(server)
                    .get('/api/countries')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(252);
                        done();
                    });
            });
        });
        /*
         * Test the /GET route
        */
        describe('/GET all iban countries', () => {
            it('it should GET all the 39 iban countries', (done) => {
                chai.request(server)
                    .get('/api/iban-countries')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(39);
                        done();
                    });
            });
        });
        /*
         * Test the /GET route
        */
        describe('/GET all iban us ca others countries', () => {
            it('it should GET all the 72 iban us ca others countries', (done) => {
                chai.request(server)
                    .get('/api/iban-us-ca-others-countries')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(72);
                        done();
                    });
            });
        });
        /*
         * Test the /GET route
        */
        describe('/GET all sector activity', () => {
            it('it should GET all the 51 sector activity', (done) => {
                chai.request(server)
                    .get('/api/sector-activity')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(51);
                        done();
                    });
            });
        });
    });
});
