const IbanCountry = function (
    index, name, code, type
) {
    this.index = index;
    this.code = code;
    this.name = name;
    this.type = type;


}

IbanCountry.fromJson = (object) => {
    return new IbanCountry(
        object["$"]["index"],
        object["name"][0],
        object["code"][0],
        object["type"][0],
        object["_"],
    )
}


module.exports = {IbanCountry}
