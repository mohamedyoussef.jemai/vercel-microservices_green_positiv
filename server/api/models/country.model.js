const Country = function (
    index, name, code
) {
    this.index = index;
    this.code = code;
    this.name = name;

}

Country.fromJson = (object) => {
    return new Country(
        object["$"]["index"],
        object["name"],
        object["code"],
        object["_"],
    )
}


module.exports = {Country}
