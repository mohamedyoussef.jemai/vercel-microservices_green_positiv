const Language = function (
    index, name, code
) {
    this.index = index;
    this.code = code;
    this.name = name;
}

Language.fromJson = (object) => {
    return new Language(
        object["$"]["index"],
        object["name"][0],
        object["code"][0],
        object["_"],
    )
}


module.exports = {Language}
