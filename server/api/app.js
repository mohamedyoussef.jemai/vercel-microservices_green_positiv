require('dotenv').config();
const express = require('express');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

let apiRouter = require('./routes/api.route');

app.use('/api', apiRouter);

app.listen(port, () => {
    loggerFile.info(`server running on port ${port}`);
})

module.exports = app;
