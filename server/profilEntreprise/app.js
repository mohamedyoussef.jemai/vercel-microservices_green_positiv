require('dotenv').config();
const express = require('express');
const mongoose = require('mongoose');
const logger = require('morgan');
const loggerFile = require("./config/logger");
const cors = require('cors');

const app = express();
const port = process.env.PORT;

app.use(logger('dev'));

require('./models/profileEntreprise.model');

app.use(cors());
app.use(express.json());
app.use(express.urlencoded({extended: false}));

app.use(logger('combined'));

let freelanceRouter = require('./routes/profilEntreprise.route');
let agenceRouter = require('./routes/profilEntrepriseAgence.route');
let companyRouter = require('./routes/profilEntrepriseCompany.route');

app.use('/profil-entreprise', freelanceRouter);
app.use('/profil-entreprise-agence', agenceRouter);
app.use('/profil-entreprise-company', companyRouter);

mongoose.connect(process.env.DB_URI_PROFIL_LOCAL_PROD, {
    useUnifiedTopology: true,
    useNewUrlParser: true
}).then(() => {
    app.listen(port, () => {
        loggerFile.info(`server running on port ${port}`);
    })
}).catch((err) => {
    loggerFile.info(err);
});

module.exports = app;
