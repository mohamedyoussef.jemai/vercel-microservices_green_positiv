//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let profilEntreprise = require('../models/profileEntreprise.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idFreelancer = "6220ea0b569c520f389d1f76";

chai.use(chaiHttp);
//Our parent block
describe('profilEntreprise Freelance', () => {
    before((done) => { //Before each test we empty the database
        profilEntreprise.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /POST contact details
        */
        describe('/POST all updateContactDetails', () => {
            it('it should UPDATE contact details', (done) => {
                let body = {
                    name: "test 2 entreprise",
                    address: "Paris",
                    address_plus: "14éme",
                    legal_form: "SARL à capital variable",
                    id_freelancer: idFreelancer
                }
                chai.request(server)
                    .post('/profil-entreprise')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées de votre entreprise ajoutés");
                        done();
                    });
            });
        });
        /*
         * Test the /POST contact details
        */
        describe('/POST all updateContactDetails', () => {
            it('it should UPDATE contact details', (done) => {
                let body = {
                    name: "test 2 entreprise",
                    address: "Paris",
                    address_plus: "14éme",
                    legal_form: "SARL à capital variable",
                    id_freelancer: idFreelancer
                }
                chai.request(server)
                    .patch('/profil-entreprise')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées de votre entreprise modifiés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH contact details
        */
        describe('/PATCH all updateContactDetails', () => {
            it('it should UPDATE contact details', (done) => {
                let body = {
                    name: "test 2 entreprise",
                    address: "Paris",
                    address_plus: "14éme",
                    legal_form: "SARL à capital variable",
                    id_freelancer: idFreelancer
                }
                chai.request(server)
                    .patch('/profil-entreprise')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées de votre entreprise modifiés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH legal representative
        */
        describe('/PATCH all UpdateLegalRepresentative', () => {
            it('it should UPDATE legal representative', (done) => {
                let body = {
                    lastname: "jemai",
                    firstname: "youssef",
                    birthday: "1998-01-16",
                    postal: "2036",
                    city_of_birth: "Ariana",
                    country_of_birth: "Tunisie",
                    nationality: "Tunisie",
                    id_freelancer: idFreelancer
                }
                chai.request(server)
                    .patch('/profil-entreprise/legal-representative')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Représentant légale de votre entreprise ajouté");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH taxe
        */
        describe('/PATCH all UpdateTaxe', () => {
            it('it should UPDATE taxe', (done) => {
                let body = {
                    taxe: 13,
                    id_freelancer: idFreelancer
                }
                chai.request(server)
                    .patch('/profil-entreprise/taxes')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("taxe ajouté");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH legal mention
        */
        describe('/PATCH all UpdateLegalMention', () => {
            it('it should UPDATE legal mention', (done) => {
                let body = {
                    sas: 20,
                    siret: "84492526300015",
                    rcs: "test",
                    naf: "1234A",
                    tva_intracom: "FR28844925263",
                    days: 0,
                    id_freelancer: idFreelancer

                }
                const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWRDb25uZWN0ZWQiOiI2MjFjOWE5Y2Y5ZjQ1NmM5OWVlZmEzYTMiLCJlbWFpbENvbm5lY3RlZCI6InlvdXNzZWZqb2VqZW1haTE2QGdtYWlsLmNvbSIsInVzZXJuYW1lQ29ubmVjdGVkIjoiam9lIiwicm9sZSI6IkZyZWVsYW5jZXIiLCJleHAiOjE2NDY4MTYyNTQsImlhdCI6MTY0NjIxMTQ1NH0.gCUxBk-zD1PuIR6BrEt8qt8whCz7tyf3qVc34s-UWfo";
                chai.request(server)
                    .patch('/profil-entreprise/legal-mention')
                    .set({'token': token})
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("mention légale ajoutée");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH iban
        */
        describe('/PATCH all updateIbanAccount', () => {
            it('it should UPDATE iban', (done) => {
                let body = {
                    type_iban: "iban",
                    cb_iban_name_lastname: "youssef jemai",
                    cb_iban_address_holder: "youssef",
                    cb_iban_postal: "20365",
                    cb_iban_city: "Tunisie",
                    cb_iban_country: "Norvège",
                    cb_iban_iban: "58965896587458",
                    id_freelancer: idFreelancer

                }
                const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWRDb25uZWN0ZWQiOiI2MjFjOWE5Y2Y5ZjQ1NmM5OWVlZmEzYTMiLCJlbWFpbENvbm5lY3RlZCI6InlvdXNzZWZqb2VqZW1haTE2QGdtYWlsLmNvbSIsInVzZXJuYW1lQ29ubmVjdGVkIjoiam9lIiwicm9sZSI6IkZyZWVsYW5jZXIiLCJleHAiOjE2NDY4MTYyNTQsImlhdCI6MTY0NjIxMTQ1NH0.gCUxBk-zD1PuIR6BrEt8qt8whCz7tyf3qVc34s-UWfo";
                chai.request(server)
                    .patch('/profil-entreprise/payment/iban')
                    .set({'token': token})
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées bancaires Ajoutés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH iban-us
        */
        describe('/PATCH all updateIbanAccount', () => {
            it('it should UPDATE iban-us', (done) => {
                let body = {
                    type_iban: "iban-us",
                    cb_iban_name_lastname: "yosusef jemai",
                    cb_iban_address_holder: "youssef",
                    cb_iban_postal: "20365",
                    cb_iban_city: "california",
                    cb_iban_region: "sud",
                    cb_iban_country: "Turquie",
                    cb_iban_account_number: "12345678",
                    cb_iban_aba_transit_number: "123456789",
                    cb_iban_account_type: "epargne",
                    id_freelancer: idFreelancer

                }
                const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWRDb25uZWN0ZWQiOiI2MjFjOWE5Y2Y5ZjQ1NmM5OWVlZmEzYTMiLCJlbWFpbENvbm5lY3RlZCI6InlvdXNzZWZqb2VqZW1haTE2QGdtYWlsLmNvbSIsInVzZXJuYW1lQ29ubmVjdGVkIjoiam9lIiwicm9sZSI6IkZyZWVsYW5jZXIiLCJleHAiOjE2NDY4MTYyNTQsImlhdCI6MTY0NjIxMTQ1NH0.gCUxBk-zD1PuIR6BrEt8qt8whCz7tyf3qVc34s-UWfo";
                chai.request(server)
                    .patch('/profil-entreprise/payment/iban')
                    .set({'token': token})
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées bancaires Ajoutés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH iban-ca
        */
        describe('/PATCH all updateIbanAccount', () => {
            it('it should UPDATE iban-ca', (done) => {
                let body = {
                    type_iban: "iban-ca",
                    cb_iban_name_lastname: "yosusef jemai",
                    cb_iban_address_holder: "youssef",
                    cb_iban_postal: "20365",
                    cb_iban_city: "Tunisie",
                    cb_iban_region: "test",
                    cb_iban_country: "Canada",
                    cb_iban_account_number: "12345678",
                    cb_iban_branch_code: "12345",
                    cb_iban_number_institution: "614",
                    cb_iban_bank_name: "bank test",
                    id_freelancer: idFreelancer

                }
                const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWRDb25uZWN0ZWQiOiI2MjFjOWE5Y2Y5ZjQ1NmM5OWVlZmEzYTMiLCJlbWFpbENvbm5lY3RlZCI6InlvdXNzZWZqb2VqZW1haTE2QGdtYWlsLmNvbSIsInVzZXJuYW1lQ29ubmVjdGVkIjoiam9lIiwicm9sZSI6IkZyZWVsYW5jZXIiLCJleHAiOjE2NDY4MTYyNTQsImlhdCI6MTY0NjIxMTQ1NH0.gCUxBk-zD1PuIR6BrEt8qt8whCz7tyf3qVc34s-UWfo";
                chai.request(server)
                    .patch('/profil-entreprise/payment/iban')
                    .set({'token': token})
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées bancaires Ajoutés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH iban others
        */
        describe('/PATCH all updateIbanAccount', () => {
            it('it should UPDATE others', (done) => {
                let body = {
                    type_iban: "others",
                    cb_iban_name_lastname: "yosusef jemai",
                    cb_iban_address_holder: "youssef",
                    cb_iban_postal: "20365",
                    cb_iban_city: "Tunisie",
                    cb_iban_region: "tozeur",
                    cb_iban_country: "Argentine",
                    cb_iban_account_number: "12345678",
                    cb_iban_bic_swift: "TNATBTTN",
                    cb_iban_account_country: "Canada",
                    id_freelancer: idFreelancer

                }
                const token = "eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJfaWRDb25uZWN0ZWQiOiI2MjFjOWE5Y2Y5ZjQ1NmM5OWVlZmEzYTMiLCJlbWFpbENvbm5lY3RlZCI6InlvdXNzZWZqb2VqZW1haTE2QGdtYWlsLmNvbSIsInVzZXJuYW1lQ29ubmVjdGVkIjoiam9lIiwicm9sZSI6IkZyZWVsYW5jZXIiLCJleHAiOjE2NDY4MTYyNTQsImlhdCI6MTY0NjIxMTQ1NH0.gCUxBk-zD1PuIR6BrEt8qt8whCz7tyf3qVc34s-UWfo";
                chai.request(server)
                    .patch('/profil-entreprise/payment/iban')
                    .set({'token': token})
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Coordonnées bancaires Ajoutés");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 2 : GET ALL
   */
    describe('Step 1 : GET ALL', () => {
        /*
         * Test the /GET all profil entreprise
        */
        describe('/GET all profil entreprise', () => {
            it('it should GET  all profil entreprise', (done) => {
                chai.request(server)
                    .get('/profil-entreprise')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
    });
});
