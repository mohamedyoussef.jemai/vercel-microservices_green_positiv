//During the test the env variable is set to test
process.env.NODE_ENV = 'test';
let profilEntrepriseCompany = require('../models/profileEntrepriseCompany.model');

//Require the dev-dependencies
let chai = require('chai');
let chaiHttp = require('chai-http');
let server = require('../app');
let should = chai.should();
let idCompany = "6220ea0b569c520f389d1f76";

chai.use(chaiHttp);
//Our parent block
describe('profilEntreprise Company', () => {
    before((done) => { //Before each test we empty the database
        profilEntrepriseCompany.remove({}, (err) => {
            done();
        });
    });
    /*
     * Test Step 1 : Creation
    */
    describe('Step 1 : Creation', () => {
        /*
         * Test the /POST profil entreprise
        */
        describe('/POST all updateProfileEntreprise', () => {
            it('it should UPDATE contact details', (done) => {
                let body = {
                    name: "test ",
                    size: "2",
                    id_company: idCompany
                }
                chai.request(server)
                    .post('/profil-entreprise-company')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("profil ajouté avec succés");
                        done();
                    });
            });
        });
        /*
         * Test the /PATCH profil entreprise
        */
        describe('/PATCH all updateProfileEntreprise', () => {
            it('it should UPDATE profile entreprise', (done) => {
                chai.request(server)
                    .patch('/profil-entreprise-company')
                    .field("name", 'test name update')
                    .field("size", 3)
                    .field("sector_activity", 'TRAVEL')
                    .field("new_logo", 'image.png')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("profile Entreprise modifié avec succés");
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH profil entreprise facturation
       */
        describe('/PATCH updateFacturationDetails', () => {
            it('it should UPDATE profile entreprise facturation', (done) => {
                let body = {
                    social_reason: "albero",
                    siret: "84492526300015",
                    tva_intracom: "FR28844925263",
                    address: "tunisie",
                    address_plus: "soukraa",
                    id: idCompany
                }
                chai.request(server)
                    .patch('/profil-entreprise-company/facturation')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Détails de facturation de votre entreprise modifiés");
                        done();
                    });
            });
        });
        /*
        * Test the /PATCH validate profil
       */
        describe('/PATCH updateFacturationDetails', () => {
            it('it should validate profil', (done) => {
                let body = {
                    validated: true,
                    id: idCompany
                }
                chai.request(server)
                    .patch('/profil-entreprise-company/validate')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("done");
                        done();
                    });
            });
        });
        /*
       * Test the /PATCH profil entreprise contact
      */
        describe('/PATCH updateContactCompta', () => {
            it('it should UPDATE profile entreprise contact comptable', (done) => {
                let body = {
                    lastName: "test",
                    firstName: "test 2 ",
                    email: "email",
                    send_compta: true
                }
                chai.request(server)
                    .patch('/profil-entreprise-company/contact')
                    .send(body)
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.have.property('message');
                        res.body.message.should.be.eql("Contact comptabilité de votre entreprise modifié");
                        done();
                    });
            });
        });
    });
    /*
    * Test Step 2 : GET ALL
   */
    describe('Step 1 : GET ALL', () => {
        /*
         * Test the /GET all profil entreprise
        */
        describe('/GET all profil entreprise', () => {
            it('it should GET  all profil entreprise', (done) => {
                chai.request(server)
                    .get('/profil-entreprise-company')
                    .end((err, res) => {
                        res.should.have.status(200);
                        res.body.should.be.a('array');
                        res.body.length.should.be.eql(1);
                        done();
                    });
            });
        });
        /*
         * Test the /GET all profil entreprise
        */
        describe('/GET by id profil entreprise', () => {
            it('it should GET  all profil entreprise', (done) => {
                chai.request(server)
                    .get(`/profil-entreprise-company/${idCompany}`)
                    .end((err, res) => {
                        res.should.have.status(200);
                        done();
                    });
            });
        });
    });
});
