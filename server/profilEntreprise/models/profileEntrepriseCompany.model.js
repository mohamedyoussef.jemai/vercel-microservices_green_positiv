const mongoose = require('mongoose');
const Schema = mongoose.Schema;

let profileEntrepriseSchema = new Schema({
    logo: {
        type: String
    },
    name: {
        type: String,
    },
    size: {
        type: String,
    },
    sector_activity: {
        type: String,
    },
    social_reason: {
        type: String,
    },
    siret: {
        type: String,
    },
    tva_intracom: {
        type: String,
    },
    address: {
        type: String,
    },
    address_plus: {
        type: String,
    },
    lastName: {
        type: String,
    },
    firstName: {
        type: String,
    },
    email: {
        type: String,
    },
    send_compta: {
        type: Boolean,
    },
    id_company: {
        type: String
    },
    cloudinary_id: {
        type: String
    }
}, {
    collection: 'profileEntrepriseCompany',
    timestamps: true
})

const profileEntrepriseCompany = mongoose.model("profileEntrepriseCompany", profileEntrepriseSchema);
module.exports = profileEntrepriseCompany;
