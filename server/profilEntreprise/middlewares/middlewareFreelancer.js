require('dotenv/config')
const jwt = require('jsonwebtoken');
const axios = require('axios').default;
const utils = require("../config/utils");

const loggerFile = require("../config/logger");
const apiService = require("../services/apiService");
const legalForms = [
    "Association loi 1901 - avec TVA", "Association loi 1901 - sans TVA", "Auto-entreprise / micro entreprise", "Auto-entreprise / micro entreprise avec option TVA", "Couveuses d'entreprises (société)", "EI (entreprise individuelle)", "EIRL", "MDA/AGESSA sans précompte ni TVA", "MDA/AGESSA sans précompte, avec option TVA", "SARL à capital variable", "SARL/EURL", "SAS/SASU", "SCIC", "SCOP", "SEP (société en participation)", "Société Anonyme (SA)", "Société civile", "Société de portage salarial", "Société en cours d'immatriculation"
];
const ibanType = ['iban', 'iban-us', 'iban-ca', 'others', 'empty'];

module.exports = {
    middlewareTokenFreelancer: async (req, res, next) => {
        loggerFile.info("in middlewareTokenFreelancer")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await axios.get(`${process.env.URL_FREELANCER}/freelancer/get/${_id}`);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err,utils.getClientAddress(req, err))
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user.data && user.data.freelancer.role === "Freelancer") {
                            req.auth = decoded2,
                                loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    validateLegalForms: async (req, res, next) => {
        loggerFile.info("in validateLegalForms")
        try {
            if (legalForms.includes(req.body.legal_form)) {
                loggerFile.debug("validateLegalForms done")
                return next();
            } else {
                loggerFile.debug("la forme légale est inéxistante")
                return res.status(400).json({message: "la forme légale est inéxistante"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateIbanTypeAndData: async (req, res, next) => {
        loggerFile.info("in validateIbanTypeAndData")
        let {
            type_iban,
            cb_iban_postal,
            cb_iban_iban,
            cb_iban_account_number,
            cb_iban_aba_transit_number,
            cb_iban_branch_code,
            cb_iban_number_institution,
            cb_iban_bic_swift
        } = req.body;
        try {
            if (ibanType.includes(req.body.type_iban)) {
                switch (type_iban) {
                    case 'iban': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        let iban_Numbers = cb_iban_iban.slice(2, cb_iban_iban.length);
                        if (!cb_iban_iban || typeof cb_iban_iban !== "string" || !cb_iban_iban[0].match("F") || !cb_iban_iban[1].match("R") || !/^\d+$/.test(iban_Numbers) || cb_iban_iban.length < 27 ) {
                            loggerFile.debug("iban erroné")
                            return res.status(400).json({message: "iban erroné"});
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'iban-us': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_aba_transit_number) || cb_iban_aba_transit_number.length != 9) {
                            return res.status(400).json({message: "ABA Transit number erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'iban-ca': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_branch_code) || cb_iban_branch_code.length != 5) {
                            return res.status(400).json({message: "Code guichet erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_number_institution) || cb_iban_number_institution.length != 3) {
                            return res.status(400).json({message: "Numéro de l'institution erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next();
                        }
                    }
                        break;
                    case 'others': {
                        if (!/^\d+$/.test(cb_iban_postal) || cb_iban_postal.length != 5) {
                            return res.status(400).json({message: "code postal erroné"})
                        }
                        if (!/^\d+$/.test(cb_iban_account_number) || cb_iban_account_number.length != 8) {
                            return res.status(400).json({message: "Numéro de compte erroné"})
                        }
                        if (!cb_iban_bic_swift.match("^[A-Z]+$") || cb_iban_bic_swift.length != 8) {
                            return res.status(400).json({message: "BIC/SWIFT erroné"})
                        } else {
                            loggerFile.debug("validateIbanTypeAndData done")
                            return next()
                        }
                    }
                        break;
                }
            } else {
                loggerFile.debug("le type d'iban n'est pas conforme")
                return res.status(400).json({message: "le type d'iban n'est pas conforme"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateCountryLegalRepresentative: async (req, res, next) => {
        loggerFile.info("in validateCountryLegalRepresentative")
        try {
            const response = await apiService.getCountry();
            if (response.includes(req.body.country_of_birth)) {
                if (response.includes(req.body.nationality)) {
                    loggerFile.debug("validateCountryLegalRepresentative done")
                    return next();
                } else {
                    loggerFile.debug("la nationalité est introuvable")
                    return res.status(400).json({message: "la nationalité est introuvable"});
                }
            } else {
                loggerFile.debug("le pays de naissance est introuvable")
                return res.status(400).json({message: "le pays de naissance est introuvable"});
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateIbanCountry: async (req, res, next) => {
        loggerFile.info("in validateIbanCountry")
        try {
            let {type_iban, cb_iban_country, cb_iban_account_country} = req.body;
            let namesIbanCountry = [];
            let namesIbanUsCaOthersCountry = [];

            if (type_iban == "iban") {
                let response = await apiService.getIbanCountry();
                response.map(async (el) => {
                    await namesIbanCountry.push(el.name);
                });
            } else {
                let response = await apiService.getIbanUsCaOtherCountry();
                response.map(async (el) => {
                    await namesIbanUsCaOthersCountry.push(el.name);
                });
            }

            if (type_iban == 'iban' && namesIbanCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'iban-us' && namesIbanUsCaOthersCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'iban-ca' && namesIbanUsCaOthersCountry.includes(cb_iban_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else if (type_iban == 'others' && namesIbanUsCaOthersCountry.includes(cb_iban_country) && namesIbanUsCaOthersCountry.includes(cb_iban_account_country)) {
                loggerFile.debug("validateIbanCountry done")
                return next();
            } else {
                loggerFile.debug("le pays ne corresponds pas")
                return res.status(400).json({message: "le pays ne corresponds pas"});
            }
        } catch
            (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    verifyMentionLegalCreation: async (req, res, next) => {
        loggerFile.info("in verifyMentionLegalCreation")

        let {sas, siret, rcs, naf, tva_intracom, days} = req.body;
        try {
            if (!sas || Number(sas) < 0) {
                loggerFile.debug("sas non conforme")
                return res.status(400).json({message: "sas non conforme"});
            }
            if (!siret || typeof siret !== "string" || siret.length !== 14 || !/^\d+$/.test(siret)) {
                loggerFile.debug("siret non conforme")
                return res.status(400).json({message: "siret non conforme"});
            }
            if (!rcs || typeof rcs !== "string" || !/^[a-zA-Z]+$/.test(rcs)) {
                loggerFile.debug("rcs non conforme")
                return res.status(400).json({message: "rcs non conforme"});
            }
            let nafNumbers = naf.slice(0, naf.length - 1);
            if (!naf || typeof naf !== "string" || naf.length !== 5 || !naf.match("^[A-Z0-9]+$") || !/^\d+$/.test(nafNumbers) || !naf[naf.length - 1].match("^[A-Z]+$")) {
                loggerFile.debug("naf non conforme")
                return res.status(400).json({message: "naf non conforme"});
            }
            let tva_numbers = tva_intracom.slice(2, tva_intracom.length);
            if (!tva_intracom || typeof tva_intracom !== "string" || !tva_intracom[0].match("F") || !tva_intracom[1].match("R") || !/^\d+$/.test(tva_numbers)) {
                loggerFile.debug("tva_intracom non conforme")
                return res.status(400).json({message: "tva_intracom non conforme"});
            }
            if (days < 0 || days > 60) {
                loggerFile.debug("days non conforme")
                return res.status(400).json({message: "Nombre de jours erroné"});
            } else {
                loggerFile.debug("verifyMentionLegalCreation done")
                return next();
            }

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    ExistParamFreelancer: async (req, res, next) => {
        loggerFile.info("in ExistParamFreelancer")
        try {
            let {id} = req.params;

            let freelancer = await axios.get(`${process.env.URL_FREELANCER}/freelancer/find/${id}`);

            if (freelancer) {
                loggerFile.debug("freelancer trouvé")
                return next();
            } else {
                loggerFile.debug("freelancer introuvable")
                return res.status(400).json({message: "freelancer introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    ExistParamAgence: async (req, res, next) => {
        loggerFile.info("in ExistParamAgence")
        try {
            let {id} = req.params;
            let freelancer = await axios.get(`${process.env.URL_AGENCE}/agence/find/${id}`);

            if (freelancer) {
                loggerFile.debug("freelancer trouvé")
                return next();
            } else {
                loggerFile.debug("freelancer introuvable")
                return res.status(400).json({message: "freelancer introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
}

