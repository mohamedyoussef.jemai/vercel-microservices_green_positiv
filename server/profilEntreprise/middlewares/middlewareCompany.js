require('dotenv/config')
const jwt = require('jsonwebtoken');
const axios = require('axios').default;
const loggerFile = require("../config/logger");
const utils = require("../config/utils");
module.exports = {
    middlewareTokenCompany: async (req, res, next) => {
        loggerFile.info("in middlewareTokenCompany")
        const {token} = req.headers;
        if (!token) {
            loggerFile.error("besoin de token")
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    const user = await axios.get(`${process.env.URL_COMPANY}/company/get/${_id}`);
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            loggerFile.error(err);
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (user && user.data.company.role === "Company") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    middlewareTokenRootOrAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareTokenRootOrAdmin")
        const {token} = req.headers;
        if (!token) {
            return res.status(401).json({user: false, error: "besoin de token"});
        } else {
            try {
                let decoded = jwt.decode(token);
                if (decoded) {
                    const _id = decoded._idConnected;
                    jwt.verify(token, process.env.MY_SECRET, (err, decoded2) => {
                        if (err) {
                            return res.status(401).json({user: false, error: "token invalide"});
                        }
                        if (decoded2.role === "Root" || decoded2.role === "Admin") {
                            req.auth = decoded2
                            loggerFile.debug("token valide")
                            return next();
                        } else {
                            return res.status(401).json({user: false, error: "accés non autorisé"});
                        }
                    });
                } else {
                    return res.status(401).json({user: false, error: "token invalide"});
                }
            } catch (error) {
                loggerFile.error(error,utils.getClientAddress(req, error))
                return res.status(400).json({message: error.message});
            }
        }
    },
    ExistParamCompany: async (req, res, next) => {
        loggerFile.info("in ExistParamCompany")
        try {
            let {id} = req.params;
            const company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("company trouvé")
                return next();
            } else {
                loggerFile.debug("company introuvable")
                return res.status(400).json({message: "entreprise introuvable"})
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    validateFacturationDetails: async (req, res, next) => {
        loggerFile.info("in validateFacturationDetails")
        try {
            let {social_reason, siret, tva_intracom, address, address_plus} = req.body;
            if (!social_reason || typeof social_reason !== "string") {
                loggerFile.debug("social_reason non conforme")
                return res.status(400).json({message: "social_reason non conforme"});
            }
            if (!address || typeof address !== "string") {
                loggerFile.debug("address non conforme")
                return res.status(400).json({message: "address non conforme"});
            }
            if (!address_plus || typeof address_plus !== "string") {
                loggerFile.debug("address_plus non conforme")
                return res.status(400).json({message: "address_plus non conforme"});
            }
            if (!siret || typeof siret !== "string" || siret.length !== 14 || !/^\d+$/.test(siret)) {
                loggerFile.debug("siret non conforme")
                return res.status(400).json({message: "siret non conforme"});
            }
            let tva_numbers = tva_intracom.slice(2, tva_intracom.length);
            if (!tva_intracom || typeof tva_intracom !== "string" || !tva_intracom[0].match("F") || !tva_intracom[1].match("R") || !/^\d+$/.test(tva_numbers)) {
                loggerFile.debug("tva_intracom non conforme")
                return res.status(400).json({message: "tva_intracom non conforme"});
            } else {
                loggerFile.debug("validateFacturationDetails done")
                next();
            }
        } catch
            (err) {
            loggerFile.error(err,utils.getClientAddress(req, err))
            return res.status(400).json({message: err.message})
        }
    },
    middlewareCompanyExist: async (req, res, next) => {
        loggerFile.info("in middlewareCompanyExist")
        try {
            let id = req.auth._idConnected;
            let company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("middlewareCompanyExist done")
                return next();
            } else {
                loggerFile.debug("company not found")
                return res.status(400).json({message: "company introuvable"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    middlewareCompanyExistByAdmin: async (req, res, next) => {
        loggerFile.info("in middlewareCompanyExistByAdmin")
        try {
            let {id} = req.params;
            let company = await axios.get(`${process.env.URL_COMPANY}/company/get/${id}`);
            if (company) {
                loggerFile.debug("middlewareCompanyExistByAdmin done")
                return next();
            } else {
                loggerFile.debug("company not found")
                return res.status(400).json({message: "company introuvable"})
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
