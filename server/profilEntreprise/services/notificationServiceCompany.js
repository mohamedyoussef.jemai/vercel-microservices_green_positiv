const profileEntrepriseCompany = require('../models/profileEntrepriseCompany.model');
const axios = require('axios').default;
const notificationServerURL = process.env.URL_NOTIFICATION;
const companyServerURL = process.env.URL_COMPANY;
const loggerFile = require("../config/logger");

module.exports = {
    createProfilEntrepriseNotif: async (id, body) => {
        loggerFile.info("in createProfilEntrepriseNotif");
        let changes = {};
        try {
            let profile = await profileEntrepriseCompany.findOne({id_company: id});
            let company = await axios.get(`${companyServerURL}/company/get/${id}`);
            let {name, size, logo, sector_activity} = profile;
            if (name != body.name) {
                changes.name = body.name;
            }
            if (size != body.size) {
                changes.size = body.size;
            }
            if (logo != body.logo) {
                changes.logo = body.logo;
            }
            if (sector_activity != body.sector_activity) {
                changes.sector_activity = body.sector_activity;
            }
            let username = company.data.company.username;
            let role = company.data.company.role;
            changes.state = "profil";
            changes.idCompany = company.data.company._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/company/profil`, {
                    username: username,
                    role: role,
                    changes: changes,
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create profil done');
                    } else {
                        loggerFile.debug('problem notification profil create');
                        return
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    return false;
                });

                loggerFile.debug('createProfilEntrepriseNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createFacturationDetailsNotif: async (id, body) => {
        loggerFile.info("in createFacturationDetailsNotif");
        let changes = {};
        try {
            let profile = await profileEntrepriseCompany.findOne({id_company: id});
            let company = await axios.get(`${companyServerURL}/company/get/${id}`);
            console.log("company ",company.data)
            let {social_reason, siret, address, tva_intracom, address_plus} = profile;
            if (social_reason != body.social_reason) {
                changes.social_reason = body.social_reason;
            }
            if (siret != body.siret) {
                changes.siret = body.siret;
            }
            if (address != body.address) {
                changes.address = body.address;
            }
            if (tva_intracom != body.tva_intracom) {
                changes.tva_intracom = body.tva_intracom;
            }
            if (address_plus != body.address_plus) {
                changes.address_plus = body.address_plus;
            }
            let username = company.data.company.username;
            let role = company.data.company.role;
            changes.state = "facturation";
            changes.idCompany = company.data.company._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/company/facture`, {
                    username: username,
                    role: role,
                    changes: changes,
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create facturation done');
                    } else {
                        loggerFile.debug('problem notification facturation create');
                        return;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                });
                loggerFile.debug('createFacturationDetailsNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
    createContactComptaNotif: async (id, body) => {
        loggerFile.info("in createContactComptaNotif");
        let changes = {};
        try {
            let profile = await profileEntrepriseCompany.findOne({id_company: id});
            let company = await axios.get(`${companyServerURL}/company/get/${id}`);
            let {lastName, firstName, email, send_compta} = profile;
            if (lastName != body.lastName) {
                changes.lastName = body.lastName;
            }
            if (firstName != body.firstName) {
                changes.firstName = body.firstName;
            }
            if (email != body.email) {
                changes.email = body.email;
            }
            if (send_compta != body.send_compta) {
                changes.send_compta = body.send_compta;
            }
            let username = company.data.company.username;
            let role = company.data.company.role;
            changes.state = "comptable";
            changes.idCompany = company.data.company._id;
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/company/contact`, {
                    username: username,
                    role: role,
                    changes: changes,
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create contact done');
                    } else {
                        loggerFile.debug('problem notification contact create');
                        return                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                });
                loggerFile.debug('createContactComptaNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return;
        }
    },
}
