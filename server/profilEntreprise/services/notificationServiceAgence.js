const profileEntrepriseAgence = require('../models/profileEntrepriseAgence.model');
const loggerFile = require("../config/logger");
const axios = require('axios').default;
const notificationServerURL = process.env.URL_NOTIFICATION;
const agenceServerURL = process.env.URL_AGENCE;

module.exports = {
    createDetailsContactNotif: async (id, body) => {
        loggerFile.info("in createDetailsContactNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntrepriseAgence.findOne({id_agence: id});
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            let {name, address, address_plus, legal_form} = profile;
            if (name != body.name) {
                changes.name = body.name;
            }
            if (address != body.address) {
                changes.address = body.address;
            }
            if (address_plus != body.address_plus) {
                changes.address_plus = body.address_plus;
            }
            if (legal_form != body.legal_form) {
                changes.legal_form = body.legal_form;
            }
            changes.state = "details";
            changes.idAgence = agence.data.agence._id;
            let username = agence.data.agence.username;
            let role = agence.data.agence.role;
            await axios.patch(`${notificationServerURL}/agence/create-details-contact`, {
                username: username,
                role: role,
                changes: changes
            }).then(function (response2) {
                if (response2.status == 200) {
                    loggerFile.debug('notification create details contact done');
                    test = true;
                } else {
                    loggerFile.debug('problem notification post create');
                    test = false;
                }
            }).catch(function (err) {
                loggerFile.error(err);
                test = false;
            });
            loggerFile.debug('createDetailsContactNotif data done')
            return test;

        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    createLegalRepresentativeNotif: async (id, body) => {
        loggerFile.info("in createLegalRepresentativeNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntrepriseAgence.findOne({id_agence: id});
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            let {lastname, firstname, birthday, postal, city_of_birth, country_of_birth, nationality} = profile;
            if (lastname != body.lastname) {
                changes.lastname = body.lastname;
            }
            if (firstname != body.firstname) {
                changes.firstname = body.firstname;
            }
            if (birthday != body.birthday) {
                changes.birthday = body.birthday;
            }
            if (postal != body.postal) {
                changes.postal = body.postal;
            }
            if (city_of_birth != body.city_of_birth) {
                changes.city_of_birth = body.city_of_birth;
            }
            if (country_of_birth != body.country_of_birth) {
                changes.country_of_birth = body.country_of_birth;
            }
            if (nationality != body.nationality) {
                changes.nationality = body.nationality;
            }
            let username = agence.data.agence.username;
            let role = agence.data.agence.role;
            changes.state = "representation";
            changes.idAgence = agence.data.agence._id;
            console.log("id to copy ", agence.data.agence._id)
            console.log("changes.idAgence ", changes.idAgence)
            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/agence/create-legal-representative`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create legal representative done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification legal reprensetative create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createLegalRepresentativeNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return true;
            }
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    createTaxeNotif: async (id, body) => {
        loggerFile.info("in createTaxeNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntrepriseAgence.findOne({id_agence: id});
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            let {taxe} = profile;
            if (taxe != body.taxe) {
                changes.taxe = body.taxe;
            }
            changes.state = "taxe";
            changes.idAgence = agence.data.agence._id;
            let username = agence.data.agence.username;
            let role = agence.data.agence.role;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/agence/taxe`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create taxe done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification taxe create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createTaxeNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return true;
            }
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    createLegalMentionNotif: async (id, body) => {
        loggerFile.info("in createLegalMentionNotif");
        let changes = {};
        try {
            let test = false;
            let profile = await profileEntrepriseAgence.findOne({id_agence: id});
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            let {sas, siret, rcs, naf, tva_intracom, days} = profile;
            if (sas != body.sas) {
                changes.sas = body.sas;
            }
            if (siret != body.siret) {
                changes.siret = body.siret;
            }
            if (rcs != body.rcs) {
                changes.rcs = body.rcs;
            }
            if (naf != body.naf) {
                changes.naf = body.naf;
            }
            if (tva_intracom != body.tva_intracom) {
                changes.tva_intracom = body.tva_intracom;
            }
            if (days != body.days) {
                changes.days = body.days;
            }
            let username = agence.data.agence.username;
            let role = agence.data.agence.role;
            changes.state = "mention";
            changes.idAgence = agence.data.agence._id;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/agence/legal-mention`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create legal mention done');
                        test = true;
                    } else {
                        loggerFile.debug('problem notification legal mention create');
                        test = false;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                    test = false;
                });
                loggerFile.debug('createLegalMentionNotif data done')
                return test;
            } else {
                loggerFile.debug("no changes for notificaiton")
                return test;
            }
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    createIbanNotif: async (id, body) => {
        loggerFile.info("in createIbanNotif");
        let changes = {};
        try {
            let profile = await profileEntrepriseAgence.findOne({id_agence: id});
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            let test = false;
            //test type of iban
            switch (body.type_iban) {
                case "iban": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_iban
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_iban != body.cb_iban_iban) {
                        changes.cb_iban_iban = body.cb_iban_iban;
                    }
                    changes.state = "iban";
                    changes.idAgence = agence.data.agence._id;
                }
                    break;
                case "iban-us": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_aba_transit_number,
                        cb_iban_account_type
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_aba_transit_number != body.cb_iban_aba_transit_number) {
                        changes.cb_iban_aba_transit_number = body.cb_iban_aba_transit_number;
                    }
                    if (cb_iban_account_type != body.cb_iban_account_type) {
                        changes.cb_iban_account_type = body.cb_iban_account_type;
                    }
                    changes.state = "iban-us";
                    changes.idAgence = agence.data.agence._id;
                }
                    break;
                case "iban-ca": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_bank_name,
                        cb_iban_branch_code,
                        cb_iban_number_institution
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_bank_name != body.cb_iban_bank_name) {
                        changes.cb_iban_bank_name = body.cb_iban_bank_name;
                    }
                    if (cb_iban_branch_code != body.cb_iban_branch_code) {
                        changes.cb_iban_branch_code = body.cb_iban_branch_code;
                    }
                    if (cb_iban_number_institution != body.cb_iban_number_institution) {
                        changes.cb_iban_number_institution = body.cb_iban_number_institution;
                    }
                    changes.state = "iban-ca";
                    changes.idAgence = agence.data.agence._id;
                }
                    break;
                case "others": {
                    let {
                        type_iban,
                        cb_iban_name_lastname,
                        cb_iban_address_holder,
                        cb_iban_postal,
                        cb_iban_city,
                        cb_iban_country,
                        cb_iban_region,
                        cb_iban_account_number,
                        cb_iban_bic_swift,
                        cb_iban_account_country
                    } = profile;
                    if (type_iban != body.type_iban) {
                        changes.type_iban = body.type_iban;
                    }
                    if (cb_iban_name_lastname != body.cb_iban_name_lastname) {
                        changes.cb_iban_name_lastname = body.cb_iban_name_lastname;
                    }
                    if (cb_iban_address_holder != body.cb_iban_address_holder) {
                        changes.cb_iban_address_holder = body.cb_iban_address_holder;
                    }
                    if (cb_iban_postal != body.cb_iban_postal) {
                        changes.cb_iban_postal = body.cb_iban_postal;
                    }
                    if (cb_iban_city != body.cb_iban_city) {
                        changes.cb_iban_city = body.cb_iban_city;
                    }
                    if (cb_iban_country != body.cb_iban_country) {
                        changes.cb_iban_country = body.cb_iban_country;
                    }
                    if (cb_iban_region != body.cb_iban_region) {
                        changes.cb_iban_region = body.cb_iban_region;
                    }
                    if (cb_iban_account_number != body.cb_iban_account_number) {
                        changes.cb_iban_account_number = body.cb_iban_account_number;
                    }
                    if (cb_iban_bic_swift != body.cb_iban_bic_swift) {
                        changes.cb_iban_bic_swift = body.cb_iban_bic_swift;
                    }
                    if (cb_iban_account_country != body.cb_iban_account_country) {
                        changes.cb_iban_account_country = body.cb_iban_account_country;
                    }
                    changes.state = "iban-others";
                    changes.idAgence = agence.data.agence._id;
                }
                    break;
            }

            let username = agence.data.agence.username;
            let role = agence.data.agence.role;

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/agence/iban`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create iban done');
                    } else {
                        loggerFile.debug('problem notification iban create');
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                });
                loggerFile.debug('createIbanNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
    createUploadDocumentNotif: async (id, filename) => {
        loggerFile.info("in createUploadDocumentNotif");
        let changes = {};
        try {
            let agence = await axios.get(`${agenceServerURL}/agence/get/${id}`);
            console.log("in create upload document")
            changes.filename = filename;
            let username = agence.data.agence.username;
            let role = agence.data.agence.role;
            changes.state = "document";
            changes.idAgence = agence.data.agence._id;
            console.log("changes.idAgence")

            if (Object.keys(changes).length > 0) {
                await axios.patch(`${notificationServerURL}/agence/upload-document`, {
                    username: username,
                    role: role,
                    changes: changes
                }).then(function (response2) {
                    if (response2.status == 200) {
                        loggerFile.debug('notification create upload document done');
                    } else {
                        loggerFile.debug('problem notification upload document create');
                        return;
                    }
                }).catch(function (err) {
                    loggerFile.error(err);
                });
                loggerFile.debug('createUploadDocumentNotif data done')
            } else {
                loggerFile.debug("no changes for notificaiton")
            }
        } catch (error) {
            loggerFile.error(error)
            return false;
        }
    },
}
