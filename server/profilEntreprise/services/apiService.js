require('dotenv').config();
const axios = require('axios').default;
const loggerFile = require("../config/logger");

module.exports = {
    getCountry: async () => {
        loggerFile.info('in getCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/countries`);
            if (response) {
                loggerFile.debug("getCountry done")
                return response.data;
            }
        } catch (error) {
            loggerFile.error(error);
            return;
        }
    },
    getIbanCountry: async () => {
        loggerFile.info('in getIbanCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/iban-countries`);
            if (response) {
                loggerFile.debug("getIbanCountry done")
                return response.data;
            }
        } catch (error) {
            loggerFile.error(error);
            return;        }
    },
    getIbanUsCaOtherCountry: async () => {
        loggerFile.info('in getIbanUsCaOtherCountry');
        try {
            const response = await axios.get(`${process.env.URL_API}/api/iban-us-ca-others-countries`);
            if (response) {
                loggerFile.debug("getIbanUsCaOtherCountry done")
                return response.data;
            }
        } catch (error) {
            loggerFile.error(error);
            return;        }
    }
}
