const profileEntrepriseCompany = require('../../models/profileEntrepriseCompany.model');
const loggerFile = require("../../config/logger");
const notificationServiceCompany = require("../../services/notificationServiceCompany");
const fs = require('fs');
module.exports = {
    //step 1 in profile entreprise
    createTest: async (req, res) => {
        loggerFile.info("in create")
        let {name, size, id_company} = req.body;
        try {
            let profil = new profileEntrepriseCompany({name, size, id_company});
            await profileEntrepriseCompany.create(profil).then(data => {
                if (data) {
                    loggerFile.debug("create profil company done")
                    return res.status(200).json({message: "profil ajouté avec succés", data: data});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    createProfileEntrepriseTest: async (req, res) => {
        loggerFile.info("in createProfileEntreprise")
        let {id, new_logo, name, size, sector_activity} = req.body;
        try {

            await notificationServiceCompany.createProfilEntrepriseNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, {
                logo: new_logo,
                name: name,
                size: size,
                sector_activity: sector_activity
            });
            loggerFile.debug('profile Entreprise modifié avec succés')
            return res.status(200).json({message: "profile Entreprise modifié avec succés"});

        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    //step 2 in profile entreprise update
    updateFacturationDetailsTest: async (req, res) => {
        loggerFile.info("in updateFacturationDetailsTest")
        try {
            let {id} = req.body;

            await notificationServiceCompany.createFacturationDetailsNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Détails de facturation de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    //step 3 in profile entreprise update
    updateContactComptaTest: async (req, res) => {
        loggerFile.info("in updateContactComptaTest")
        let {id} = req.body;
        try {
            await notificationServiceCompany.createContactComptaNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Contact comptabilité de votre entreprise modifié"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    // get all profile entreprise
    findAll: async (req, res) => {
        loggerFile.info("in findAll")
        try {
            const profileEntreprises = await profileEntrepriseCompany.find();
            if (profileEntreprises) {
                loggerFile.debug('get all profileEntreprises data done')
                return res.status(200).json(profileEntreprises);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    //find profileEntreprise by ID freelance
    findOne: async (req, res) => {
        loggerFile.info("in findOne");
        let id = req.params.id;
        try {
            const OneprofileEntreprise = await profileEntrepriseCompany.findOne({id_company: id});
            if (OneprofileEntreprise) {
                let profile = {
                    name: OneprofileEntreprise.name,
                    size: OneprofileEntreprise.size,
                    sector_activity: OneprofileEntreprise.sector_activity,
                    logo: OneprofileEntreprise.logo,
                }
                let facturation = {
                    social_reason: OneprofileEntreprise.social_reason,
                    siret: OneprofileEntreprise.siret,
                    tva_intracom: OneprofileEntreprise.tva_intracom,
                    address: OneprofileEntreprise.address,
                    address_plus: OneprofileEntreprise.address_plus,
                }
                let contact = {
                    lastName: OneprofileEntreprise.lastName,
                    firstName: OneprofileEntreprise.firstName,
                    email: OneprofileEntreprise.email,
                    send_compta: OneprofileEntreprise.send_compta,
                }
                return res.status(200).json({
                    profile: profile,
                    facturation: facturation,
                    contact: contact
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    //validate
    validateProfileCompany: async (req, res) => {
        loggerFile.info("in validateProfileCompany")
        let {id_company, validated} = req.body;
        try {
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id_company}, {validated: validated});
            loggerFile.debug("validate data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete profile')
        const {id} = req.params;
        try {
            await profileEntrepriseCompany.findOneAndDelete({id_company: id});
            loggerFile.debug('profile effacé')
            return res.status(200).json({message: "done"})

        } catch (error) {
            loggerFile.error(error)
            return res.status(400).json({message: error.message})
        }
    },


}
