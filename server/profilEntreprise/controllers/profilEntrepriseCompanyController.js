const profileEntrepriseCompany = require('../models/profileEntrepriseCompany.model');
const loggerFile = require("../config/logger");
const notificationServiceCompany = require("../services/notificationServiceCompany");
const cloudinary = require("../config/cloudinary")
const utils = require("../config/utils");
module.exports = {
    create: async (req, res) => {
        loggerFile.info("in create")
        let {name, size, id_company} = req.body;
        try {
            let profil = new profileEntrepriseCompany({name, size, id_company});
            await profileEntrepriseCompany.create(profil).then(data => {
                if (data) {
                    loggerFile.debug("create profil company done")
                    return res.status(200).json({message: "profil ajouté avec succés", data: data});
                }
            }).catch(err => {
                loggerFile.error(err);
                return res.status(400).json({message: err.message});
            });
        } catch (error) {
            loggerFile.error(error);
            return res.status(400).json({message: error.message});
        }
    },
    createProfileEntreprise: async (req, res) => {
        loggerFile.info("in createProfileEntreprise")
        let id = req.auth._idConnected;
        let {name, size, sector_activity} = req.body;
        let new_logo = "";
        let cloudinary_id = "";
        try {
            let profile = await profileEntrepriseCompany.findOne({id_company: id});
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_logo = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_logo)
                        await cloudinary.uploader.destroy(profile.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement de l'image"})
                }
            } else {
                new_logo = req.body.old_logo;

            }
            profile.logo = await new_logo;
            profile.cloudinary_id = cloudinary_id;
            profile.name = name;
            profile.size = size;
            profile.sector_activity = sector_activity;
            await notificationServiceCompany.createProfilEntrepriseNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, profile);
            loggerFile.debug('profile Entreprise modifié avec succés')
            return res.status(200).json({message: "profile Entreprise modifié avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    createProfileEntrepriseByAdmin: async (req, res) => {
        loggerFile.info("in createProfileEntrepriseByAdmin")
        let {id} = req.params;
        let {name, size, sector_activity} = req.body;
        let new_logo = "";
        let cloudinary_id = "";
        try {
            let profile = await profileEntrepriseCompany.findOne({id_company: id});
            if (req.file) {
                const result = await cloudinary.uploader.upload(req.file.path);
                new_logo = result.secure_url;
                cloudinary_id = result.public_id;
                try {
                    if (req.body.old_logo != undefined)
                        await cloudinary.uploader.destroy(profile.cloudinary_id);
                } catch (err) {
                    loggerFile.error(err);
                    return res.status(400).json({message: "une erreur est survenu lors du téléchargement de l'image"})
                }
            } else {
                new_logo = req.body.old_logo;

            }
            profile.logo = await new_logo;
            profile.cloudinary_id = cloudinary_id;
            await notificationServiceCompany.createProfilEntrepriseNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, profile);
            loggerFile.debug('profile Entreprise modifié avec succés')
            return res.status(200).json({message: "profile Entreprise modifié avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFacturationDetailsByAdmin: async (req, res) => {
        loggerFile.info("in updateFacturationDetailsByAdmin")
        try {
            let {id} = req.params;
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Détails de facturation de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFacturationDetails: async (req, res) => {
        loggerFile.info("in updateFacturationDetails")
        try {
            await notificationServiceCompany.createFacturationDetailsNotif(req.auth._idConnected, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: req.auth._idConnected}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Détails de facturation de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateContactComptaByAdmin: async (req, res) => {
        loggerFile.info("in updateContactComptaByAdmin")
        try {
            let {id} = req.params;
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Contact comptabilité de votre entreprise modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateContactCompta: async (req, res) => {
        loggerFile.info("in updateContactCompta")
        try {
            await notificationServiceCompany.createContactComptaNotif(req.auth._idConnected, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: req.auth._idConnected}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Contact comptabilité de votre entreprise modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findAll: async (req, res) => {
        loggerFile.info("in findAll")
        try {
            const profileEntreprises = await profileEntrepriseCompany.find();
            if (profileEntreprises) {
                loggerFile.debug('get all profileEntreprises data done')
                return res.status(200).json(profileEntreprises);
            } else {
                loggerFile.debug('profils Entreprises inéxistants')
                return res.status(400).json({message: "profils Entreprises inexistants"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    findOne: async (req, res) => {
        loggerFile.info("in findOne");
        let {id} = req.params;
        try {
            const OneprofileEntreprise = await profileEntrepriseCompany.findOne({id_company: id});
            if (OneprofileEntreprise) {
                let profile = {
                    name: OneprofileEntreprise.name,
                    size: OneprofileEntreprise.size,
                    sector_activity: OneprofileEntreprise.sector_activity,
                    logo: OneprofileEntreprise.logo,
                }
                let facturation = {
                    social_reason: OneprofileEntreprise.social_reason,
                    siret: OneprofileEntreprise.siret,
                    tva_intracom: OneprofileEntreprise.tva_intracom,
                    address: OneprofileEntreprise.address,
                    address_plus: OneprofileEntreprise.address_plus,
                }
                let contact = {
                    lastName: OneprofileEntreprise.lastName,
                    firstName: OneprofileEntreprise.firstName,
                    email: OneprofileEntreprise.email,
                    send_compta: OneprofileEntreprise.send_compta,
                }
                return res.status(200).json({
                    profile: profile,
                    facturation: facturation,
                    contact: contact
                });
            } else {
                loggerFile.debug("profil Entreprise inéxistant")
                return res.status(400).json({message: "profil Entreprise inéxistant"});
            }
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    validateProfileCompany: async (req, res) => {
        loggerFile.info("in validateProfileCompany")
        let {id_company, validated} = req.body;
        try {
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id_company}, {validated: validated});
            loggerFile.debug("validate data done")
            return res.status(200).json({message: "done"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    delete: async (req, res) => {
        loggerFile.info('in delete profile')
        const {id} = req.params;
        try {
            await profileEntrepriseCompany.findOneAndDelete({id_company: id});
            loggerFile.debug('profile effacé')
            return res.status(200).json({message: "done"})

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message})
        }
    },
    createProfileEntrepriseTest: async (req, res) => {
        loggerFile.info("in createProfileEntreprise")
        let {id, new_logo, name, size, sector_activity} = req.body;
        try {

            await notificationServiceCompany.createProfilEntrepriseNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, {
                logo: new_logo,
                name: name,
                size: size,
                sector_activity: sector_activity
            });
            loggerFile.debug('profile Entreprise modifié avec succés')
            return res.status(200).json({message: "profile Entreprise modifié avec succés"});

        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateFacturationDetailsTest: async (req, res) => {
        loggerFile.info("in updateFacturationDetailsTest")
        try {
            let {id} = req.body;

            await notificationServiceCompany.createFacturationDetailsNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Détails de facturation de votre entreprise modifiés"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
    updateContactComptaTest: async (req, res) => {
        loggerFile.info("in updateContactComptaTest")
        let {id} = req.body;
        try {
            await notificationServiceCompany.createContactComptaNotif(id, req.body);
            await profileEntrepriseCompany.findOneAndUpdate({id_company: id}, req.body);
            loggerFile.debug('updateContactDetails data done')
            return res.status(200).json({message: "Contact comptabilité de votre entreprise modifié"});
        } catch (error) {
            loggerFile.error(error,utils.getClientAddress(req, error))
            return res.status(400).json({message: error.message});
        }
    },
}
