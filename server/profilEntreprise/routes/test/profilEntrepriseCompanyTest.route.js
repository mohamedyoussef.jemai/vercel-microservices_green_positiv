const express = require('express');
const router = express.Router();
const profileEntrepriseCompanyController = require('../../controllers/test/profilEntrepriseCompanyTestController');

router.post('/', profileEntrepriseCompanyController.createTest);
router.patch('/', profileEntrepriseCompanyController.createProfileEntrepriseTest);
router.patch('/facturation', profileEntrepriseCompanyController.updateFacturationDetailsTest);
router.patch('/contact', profileEntrepriseCompanyController.updateContactComptaTest);
router.get('/', profileEntrepriseCompanyController.findAll);
router.get('/:id', profileEntrepriseCompanyController.findOne);
router.patch('/validate', profileEntrepriseCompanyController.validateProfileCompany);
router.delete('/delete/:id', profileEntrepriseCompanyController.delete);

module.exports = router;
