const express = require('express');
const router = express.Router();
const profileEntrepriseController = require('../../controllers/profilEntrepriseController');

router.patch('/validate', profileEntrepriseController.validateProfileFreelane);
router.patch('/update/:id', profileEntrepriseController.update);
router.post('/', profileEntrepriseController.createContactDetails);
router.patch('/', profileEntrepriseController.updateContactDetails);
router.patch('/legal-representative', profileEntrepriseController.UpdateLegalRepresentative);
router.patch('/payment/iban', profileEntrepriseController.updateIbanAccount);
router.patch('/taxes', profileEntrepriseController.UpdateTaxe);
router.patch('/legal-mention', profileEntrepriseController.UpdateLegalMention);
router.delete('/delete/:id', profileEntrepriseController.delete);


router.post('/get', profileEntrepriseController.findOne);
router.get('/', profileEntrepriseController.findAll);
router.get('/:id', profileEntrepriseController.findOne);

module.exports = router;
