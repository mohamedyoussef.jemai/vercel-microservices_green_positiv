const express = require('express');
const router = express.Router();
const profilEntrepriseAgenceController = require('../../controllers/profilEntrepriseAgenceController');


router.patch('/validate', profilEntrepriseAgenceController.validateProfileFreelane);
router.patch('/update/:id', profilEntrepriseAgenceController.update);
router.post('/', profilEntrepriseAgenceController.createContactDetails);
router.patch('/', profilEntrepriseAgenceController.updateContactDetails);
router.patch('/legal-representative', profilEntrepriseAgenceController.UpdateLegalRepresentative);
router.patch('/payment/iban', profilEntrepriseAgenceController.updateIbanAccount);
router.patch('/taxes', profilEntrepriseAgenceController.UpdateTaxe);
router.patch('/legal-mention', profilEntrepriseAgenceController.UpdateLegalMention);
router.delete('/delete/:id', profilEntrepriseAgenceController.delete);


router.post('/get', profilEntrepriseAgenceController.findOne);
router.get('/', profilEntrepriseAgenceController.findAll);
router.get('/:id', profilEntrepriseAgenceController.findOne);

module.exports = router;
